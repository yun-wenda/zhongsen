package com.simba.scaffold;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@ComponentScan(basePackages = {"com.simba", "fykj.microservice"})
@MapperScan({"com.simba.scaffold.security.business.mapper",
            "com.simba.scaffold.weixin.mapper",
            "com.simba.scaffold.zhongsen.mapper",
            "com.simba.scaffold.codeGeneration.mapper"})
@EnableAsync
public class SimbaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimbaApplication.class, args);
    }
}
