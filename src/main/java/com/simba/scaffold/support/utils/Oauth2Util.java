package com.simba.scaffold.support.utils;

import com.simba.scaffold.security.business.domain.BackendUserDetail;
import com.simba.scaffold.security.business.domain.entity.Role;
import com.simba.scaffold.security.business.service.IRoleService;
import com.simba.scaffold.support.conns.Cons;
import exception.BusinessException;
import fykj.microservice.core.support.util.SystemUtil;
import lombok.experimental.UtilityClass;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import result.ResultCode;
import utils.StringUtil;

import java.io.Serializable;
import java.util.Optional;

/**
 * Oauth2工具类
 *
 * @author zhangzhi
 */
@UtilityClass
public class Oauth2Util {

    public static final String ROLE_CODE_ADMIN = "SYS_ADMIN";

    public static BackendUserDetail getUser() {
        Authentication authentication = Optional
                .ofNullable(SecurityContextHolder.getContext())
                .map(SecurityContext::getAuthentication)
                .orElse(null);

        if (authentication instanceof OAuth2Authentication) {
            return (BackendUserDetail) authentication.getPrincipal();
        } else if (authentication instanceof BackendUserDetail) {
            return (BackendUserDetail) authentication;
        }
        return null;
    }

    public static Serializable getUserId() {
        return Optional.ofNullable(getUser())
                .map(BackendUserDetail::getId)
                .orElse(null);
    }

    public static Serializable getRoleId() {
        return Optional.ofNullable(getUser())
                .map(BackendUserDetail::getRoleId)
                .orElse(null);
    }

    public static Role currentRole(){
        BackendUserDetail user = getUser();
        if (user == null) {
            throw new BusinessException(ResultCode.FAIL, "请登录先");
        }

        IRoleService roleService = SystemUtil.getBean(IRoleService.class);
        return roleService.getRoleByCode(user.getRoleCode());
    }

    public static boolean isAdmin() {
        BackendUserDetail user = getUser();
        if (user == null) {
            throw new BusinessException(ResultCode.FAIL, "请登录先");
        }

        String code = user.getRoleCode();
        return StringUtil.equals(code, Cons.ROLE_CODE_ADMIN) || StringUtil.equals(code, Cons.ROLE_CODE_SUPER_ADMIN);
    }

}
