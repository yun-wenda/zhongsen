package com.simba.scaffold.support.utils;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 删除html标签
 * @author wangming
 */
public class DelHtmlTag {
    /**
     * 定义script的正则表达式
     */
    private static final String REGEX_SCRIPT = "<script[^>]*?>[\\s\\S]*?<\\/script>";
    /**
     * 定义style的正则表达式
     */
    private static final String REGEX_STYLE = "<style[^>]*?>[\\s\\S]*?<\\/style>";
    /**
     * 定义HTML标签的正则表达式
     */
    private static final String REGEX_HTML = "<[^>]+>";
    /**
     * 定义空格回车换行符
     */
    private static final String REGEX_SPACE = "\\s*|\t|\r|\n";
    /**
     * 定义所有w标签
     */
    private static final String REGEX_W = "<w[^>]*?>[\\s\\S]*?<\\/w[^>]*?>";

    public static void main( String[] args ) {
        String text1 = "<p><span style=\"color: #333333; font-family: 微软雅黑;\"><span style=\"font-size: 18px; background-color: #fcfcfc;\">小 时候</span></span></p>\n" +
                "<p><span style=\"color: #333333; font-family: 微软雅黑;\"><span style=\"font-size: 18px; background-color: #fcfcfc;\">乡愁是一 枚小小的邮票</span></span></p>";
        System.out.println(delHTMLTag(text1));
    }

    /**
     * @param html
     * @return 删除Html标签
     */
    public static String delHTMLTag(String html) {
        html = replaceTag(html, REGEX_W);
        // 过滤script标签
        html = replaceTag(html, REGEX_SCRIPT);
        // 过滤style标签
        html = replaceTag(html, REGEX_STYLE);
        // 过滤html标签
        html = replaceTag(html, REGEX_HTML);
        // 定义空格回车换行符
        html = replaceTag(html, REGEX_SPACE);
        // 返回文本字符串
        return html.trim();
    }

    /**
     * 去除指定的html标签
     * @param html
     * @param regex
     * @return
     */
    private static String replaceTag(String html, String regex){
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(html);
        // 过滤html标签
        return matcher.replaceAll("");

    }
}
