package com.simba.scaffold.security.business.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.simba.scaffold.security.business.domain.dto.RoleResourceDto;
import com.simba.scaffold.security.business.domain.entity.Role;
import com.simba.scaffold.security.business.domain.params.RoleParams;
import com.simba.scaffold.security.business.mapper.RoleMapper;
import com.simba.scaffold.security.business.service.IRoleResourceService;
import com.simba.scaffold.security.business.service.IRoleService;
import com.simba.scaffold.support.utils.Oauth2Util;
import com.simba.scaffold.support.wrapper.QueryWrapperBuilder;
import constants.Mark;
import fykj.microservice.core.base.BaseServiceImpl;
import fykj.microservice.core.support.util.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import utils.StringUtil;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wangf
 * @since 2019-10-16
 */
@Service
public class RoleServiceImpl extends BaseServiceImpl<RoleMapper, Role> implements IRoleService {
    @Autowired
    private IRoleResourceService roleResourceService;


//    @Override
//    public List<Role> export(RoleParams params) {
//        return baseMapper.export(params);
//    }

    @Override
    public IPage<Role> page(RoleParams params) {
        if (params == null) {
            params = new RoleParams();
        }
        params.setRoleLinked(Oauth2Util.currentRole().getRoleLinked());

        return super.page(params);
    }

    @Override
    public boolean save(RoleResourceDto dto) {
        Role role = new Role();
        BeanUtil.copyProperties(dto, role);
        if (StringUtil.isEmpty(dto.getId())) {
            role.setRoleLinked(Oauth2Util.currentRole().getRoleLinked() + Mark.COMMA + role.getCode());
        } else {
            role.setRoleLinked(null);
        }
        //保存角色
        boolean flag = saveOrUpdate(role);
        //保存角色与菜单关系
        roleResourceService.save(role, dto.getMenuIdList());
        return flag;
    }

    @Override
    public RoleResourceDto findOneById(Long id) {
        Role role = getById(id);
        RoleResourceDto dto = new RoleResourceDto();
        BeanUtil.copyProperties(role, dto);
        List<String> resourceIds = roleResourceService.findResourceIdList(id);
        dto.setMenuIdList(resourceIds);
        return dto;
    }

    @Override
    public boolean checkCode(Long id, String code) {
        return lambdaQuery().eq(Role::getCode, code).ne(StringUtil.isNotEmpty(id), Role::getId, id).count() > 0;
    }

    @Override
    public Role getRoleByCode(String code) {
        return lambdaQuery().eq(Role::getCode, code).one();
    }

    @Override
    public List<Role> list() {
        RoleParams params = new RoleParams();
        params.setRoleLinked(Oauth2Util.currentRole().getRoleLinked());
        return list(QueryWrapperBuilder.build(params));
    }

    /**
     * @Description 当前登录用户是否是系统管理员
     * @Param
     * @Return
     **/
    @Override
    public Boolean isSysAdmin() {
        return Oauth2Util.ROLE_CODE_ADMIN.equals(Oauth2Util.getUser().getRoleCode());
    }
}
