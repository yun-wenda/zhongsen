package com.simba.scaffold.security.business.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.simba.scaffold.security.business.domain.dto.SysOssDto;
import com.simba.scaffold.security.business.domain.entity.SysOss;
import com.simba.scaffold.security.business.domain.params.OssParams;
import com.simba.scaffold.security.business.service.IDictService;
import com.simba.scaffold.security.business.service.ISysOssService;
import com.simba.scaffold.support.conns.Cons;
import fykj.microservice.core.base.BaseController;
import fykj.microservice.core.beans.vo.IdTextVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import result.JsonResult;
import result.Result;
import result.ResultCode;

import java.util.List;

/**
 * 文件上传接口
 *
 * @author zhangzhi
 */
@RestController
@RequestMapping("/admin/oss")
@Api(tags = "文件上传接口")
public class SysOssController extends BaseController<ISysOssService, SysOss, OssParams> {

    @Autowired
    private IDictService dictService;

    @ApiOperation("上传文件")
    @PostMapping(value = "/upload")
    public Result upload(@RequestParam(name = "file") MultipartFile file,
                         @RequestParam(name = "serverCode", required = false) String serverCode,
                         @RequestParam(name = "name", required = false) String name) {
        if (file == null || file.isEmpty()) {
            return new Result(ResultCode.BAD_REQUEST.code(), "请选择要上传的文件");
        }
        SysOss upload = baseService.upload(file, serverCode, name);
        return new JsonResult<>(upload);

    }

    @ApiOperation("文件分类下拉列表")
    @GetMapping(value = "/typeList")
    public JsonResult<List<IdTextVo>> typeList() {
        return new JsonResult<>(dictService.idTextVoList(Cons.FILE_TYPE));
    }

    @ApiOperation("文件存储方式下拉列表")
    @GetMapping(value = "/serverList")
    public JsonResult<List<IdTextVo>> serverList() {
        return new JsonResult<>(dictService.idTextVoList(Cons.SERVER_CODE));
    }

    /**
     * 分页查询
     *
     * @return 分页结果
     */
    @ApiOperation("分页查询列表")
    @PostMapping(value = "/page")
    public JsonResult<IPage<SysOssDto>> page(@RequestBody(required = false) OssParams params) {
        if (params == null) {
            params = new OssParams();
        }
        //查询列表数据
        Page<SysOssDto> page = new Page<>(params.getCurrentPage(), params.getPageSize());
        IPage<SysOssDto> list = baseService.getListWithQuery(page, params);
        return new JsonResult<>(list);
    }

}
