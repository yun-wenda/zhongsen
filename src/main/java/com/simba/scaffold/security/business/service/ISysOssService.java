package com.simba.scaffold.security.business.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.simba.scaffold.security.business.domain.dto.SysOssDto;
import com.simba.scaffold.security.business.domain.entity.SysOss;
import com.simba.scaffold.security.business.domain.params.OssParams;
import fykj.microservice.core.base.IBaseService;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件上传记录服务接口
 *
 * @author zhangzhi
 * @date 2019-03-04
 */
public interface ISysOssService extends IBaseService<SysOss> {

    /**
     * 上传文件
     *
     * @param file       文件
     * @param name 名称
     * @return 文件上传信息  {@link SysOss}
     */
    SysOss upload(MultipartFile file, String serverCode, String name);

    /**
     * 根据文件路径查询文件上传记录
     *
     * @param path 文件相对路径
     * @return 文件上传信息 {@link SysOss}
     */
    SysOss getFileUpload(String path);

    /**
     * 后台分页查询信息
     *
     * @param page
     * @param ossParams
     * @return
     */
    IPage<SysOssDto> getListWithQuery(IPage<SysOssDto> page, OssParams ossParams);

    /**
     * 根据名称获取最新的地址
     * @param name
     * @return
     */
    String getNewestPdfPath(String name);

}
