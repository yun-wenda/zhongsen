package com.simba.scaffold.security.business.domain.params;

import fykj.microservice.core.base.BaseParams;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author yangx
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@ApiModel("第三方接口参数查询参数")
public class ApiParamManageParams extends BaseParams {


    private static final long serialVersionUID = -773443447597957858L;
}
