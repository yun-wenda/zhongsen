package com.simba.scaffold.security.business.controller;


import com.simba.scaffold.security.business.domain.entity.SysLog;
import com.simba.scaffold.security.business.domain.params.SysLogParams;
import com.simba.scaffold.security.business.service.ISysLogService;
import fykj.microservice.core.base.BaseController;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 系统日志 前端控制器
 * </p>
 *
 * @author zhangzhi
 * @since 2019-10-18
 */
@RestController
@Api(tags = "系统日志")
@RequestMapping("/admin/sysLog")
public class SysLogController extends BaseController<ISysLogService, SysLog, SysLogParams> {

}
