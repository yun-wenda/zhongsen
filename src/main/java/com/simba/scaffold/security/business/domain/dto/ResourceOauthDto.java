package com.simba.scaffold.security.business.domain.dto;

import lombok.Data;

/**
 * @author wangfei
 */
@Data
public class ResourceOauthDto {

    private Long roleId;

    private String path;


}
