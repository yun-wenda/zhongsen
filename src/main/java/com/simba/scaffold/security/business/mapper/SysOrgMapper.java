package com.simba.scaffold.security.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.simba.scaffold.security.business.domain.entity.SysOrg;

import java.io.Serializable;
import java.util.List;

/**
 * 组织机构
 *
 * Mapper 接口
 * @author wangming
 * @email ${email}
 * @date 2021-03-12
 */
public interface SysOrgMapper extends BaseMapper<SysOrg> {


    List<String> getCodeLinkedList(Serializable userId);
}
