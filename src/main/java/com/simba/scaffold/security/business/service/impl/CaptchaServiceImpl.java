package com.simba.scaffold.security.business.service.impl;

import com.simba.scaffold.security.business.service.ICaptchaService;

import com.google.code.kaptcha.Producer;
import exception.BusinessException;
import fykj.microservice.cache.config.RedisService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import result.ResultCode;
import utils.StringUtil;

import java.awt.image.BufferedImage;

/**
 * 验证码
 *
 * @author zhangzhi
 */
@Service
public class CaptchaServiceImpl implements ICaptchaService {

    public static final String CAPTCHA_KEY = "captcha_code_key";

    @Autowired
    private Producer producer;

    @Autowired
    private RedisService redisService;

    @Override
    public BufferedImage getCaptcha(String uuid) {
        if (StringUtils.isBlank(uuid)) {
            throw new BusinessException(ResultCode.FAIL, "uuid不能为空");
        }
        //生成文字验证码
        String code = producer.createText();
        redisService.hmSet(CAPTCHA_KEY, uuid, code, 5 * 60);
        return producer.createImage(code);
    }

    @Override
    public boolean validate(String uuid, String code) {
        String standard = redisService.hmGet(CAPTCHA_KEY, uuid);
        if (StringUtil.isEmpty(standard)) {
            throw new BusinessException(ResultCode.FAIL, "图片验证码失效，请刷新后重试");
        }
        //删除验证码
        redisService.hmDel(CAPTCHA_KEY, uuid);
        return standard.equalsIgnoreCase(code);
    }
}
