package com.simba.scaffold.security.business.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class BackendUserDetail extends User {

    private static final long serialVersionUID = 4374424667663347893L;

    /**
     *
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    private String nickName;

    /**
     * 角色关键字
     */
    private Long roleId;

    private String roleCode;

    private String unionId;

    public BackendUserDetail(String username, String password,
                             Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public BackendUserDetail(String username, String password, boolean enabled,
                             boolean accountNonExpired, boolean credentialsNonExpired,
                             boolean accountNonLocked,
                             Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired,
                accountNonLocked, authorities);
    }

}
