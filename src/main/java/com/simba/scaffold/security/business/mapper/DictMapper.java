package com.simba.scaffold.security.business.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.simba.scaffold.security.business.domain.entity.Dict;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhangzhi
 * @since 2019-02-20
 */
public interface DictMapper extends BaseMapper<Dict> {

}
