package com.simba.scaffold.security.business.service.impl;

import com.simba.scaffold.security.business.service.ISendSmsService;
import com.simba.scaffold.support.conns.Cons;
import exception.BusinessException;
import fykj.microservice.cache.config.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import result.Result;
import result.ResultCode;
import utils.StringUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wangm
 * @since 2019-08-21
 */
@Slf4j
@Service
public class SendSmsServiceImpl implements ISendSmsService {

    @Autowired
    private RedisService redisService;

    @Override
    public Result sendSms(String mobile, String templateCode, Map<String, String> mapContext) {
        return new Result();
    }

    @Override
    public Result sendBatchSms(List<String> phoneList, List<String> signList, String templateCode, List<String> paramList) {
        return new Result();
    }

    @Override
    public Result sendVerifyCode(String mobile, String templateCode, String verifyCode) {
        return new Result();
    }

    @Override
    public boolean validCode(String mobile, String verifyCode) {
        String key = mobile + Cons.VERIFY_TEMPLATE_CODE;
        String data = redisService.get(key);
        if(StringUtil.isNotEmpty(data)){
            if (verifyCode.equalsIgnoreCase(data)) {
                redisService.remove(key);
                return true;
            }
            throw new BusinessException(ResultCode.ERROR, "验证码错误");
        }
        throw new BusinessException(ResultCode.ERROR, "验证码错误");
    }
}
