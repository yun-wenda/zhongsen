package com.simba.scaffold.security.business.service;

import java.awt.image.BufferedImage;

/**
 * 验证码
 *
 * @author zhangzhi
 */
public interface ICaptchaService {

    /**
     * * 获取图片验证码
     *
     * @param uuid 识别码
     * @return 返回验证码
     */
    BufferedImage getCaptcha(String uuid);

    /**
     * 验证码效验
     *
     * @param uuid uuid
     * @param code 验证码
     * @return true：成功  false：失败
     */
    boolean validate(String uuid, String code);
}
