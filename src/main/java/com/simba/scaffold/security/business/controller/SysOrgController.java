package com.simba.scaffold.security.business.controller;

import com.simba.scaffold.security.business.domain.entity.SysOrg;
import com.simba.scaffold.security.business.domain.params.SysOrgParams;
import com.simba.scaffold.security.business.service.ISysOrgService;
import com.simba.scaffold.support.utils.Oauth2Util;
import fykj.microservice.core.base.BaseController;
import fykj.microservice.core.support.syslog.SysLogMethod;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import result.JsonResult;
import result.Result;

import java.io.Serializable;
import java.util.List;

/**
 * 组织机构
 *
 * 前端控制器
 * @author wangming
 * @email ${email}
 * @date 2021-03-12
 */
@RestController
@RequestMapping("/admin/org")
@Api(tags = "组织")
public class SysOrgController extends BaseController<ISysOrgService, SysOrg, SysOrgParams> {

    @ApiOperation("获取组织树")
    @GetMapping(value = "tree")
    public JsonResult<List<SysOrg>> tree(
            @ApiParam(name = "parentId", value = "父节点id")
            @RequestParam(required = false) Long parentId,
            @ApiParam(name = "level", value = "树根节点组织级别")
            @RequestParam(required = false) Integer level,
            @ApiParam(name = "lastLevel", value = "树叶子节点组织级别")
            @RequestParam(required = false) Integer lastLevel
    ) {
        List<SysOrg> tree = baseService.tree(level, lastLevel, parentId);
        return new JsonResult<>(tree);
    }

    @ApiOperation("判断编号是否唯一")
    @GetMapping(value = "/code/unique")
    public JsonResult<Boolean> codeUnique(@RequestParam(required = false) Long id, @RequestParam String code) {
        Boolean result = baseService.codeUnique(id, code);
        return new JsonResult<>(result);
    }

    @ApiOperation("获取当前用户组织树")
    @GetMapping(value = "/tree/user")
    public JsonResult<List<SysOrg>> treeOfCurrentUser(
            @ApiParam(name = "parentId", value = "父节点id")
            @RequestParam(required = false) Long parentId,
            @ApiParam(name = "level", value = "树根节点组织级别")
            @RequestParam(required = false) Integer level,
            @ApiParam(name = "lastLevel", value = "树叶子节点组织级别")
            @RequestParam(required = false) Integer lastLevel
    ) {
        if (Oauth2Util.isAdmin()) {
            return tree(parentId, level, lastLevel);
        }
        Serializable userId = Oauth2Util.getUserId();
        List<SysOrg> result = this.baseService.treeOfUser(userId, level, lastLevel, parentId);
        return new JsonResult<>(result);
    }

    @SysLogMethod("查看详情")
    @ApiOperation("根据id获取")
    @GetMapping
    @Override
    public Result get(@RequestParam Long id) {
        return new JsonResult(this.baseService.get(id));
    }
}
