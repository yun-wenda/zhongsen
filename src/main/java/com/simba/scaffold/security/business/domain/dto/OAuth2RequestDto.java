package com.simba.scaffold.security.business.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ApiModel
public class OAuth2RequestDto {
    @NotNull(message = "username必填")
    @ApiModelProperty(value = "用户名")
    private String username;

    @NotNull(message = "password必填")
    @ApiModelProperty(value = "密码")
    private String password;

    @NotNull(message = "登陆方式必填")
    @ApiModelProperty(value = "登陆方式")
    private String loginType;

    @ApiModelProperty(value = "图片验证码识别码")
    private String uuid;

    @ApiModelProperty(value = "图片验证码")
    private String captcha;

}
