package com.simba.scaffold.security.business.domain.dto;

import lombok.*;

/**
 * 手机短信验证码验证Dto
 *
 * @author zhangzhi
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SmsValidCodeDto {

    public static final String SMS_CODE = "verifyCode";

    /**
     * 短信验证码内容
     */
    private String verifyCode;

    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 模板所对应的code
     */
    @Builder.Default
    private String code = SMS_CODE;
}
