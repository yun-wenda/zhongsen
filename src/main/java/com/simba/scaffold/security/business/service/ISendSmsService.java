package com.simba.scaffold.security.business.service;

import result.Result;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangm
 * @since 2019-08-21
 */
public interface ISendSmsService {

    /**
     *
     * @param mobile 手机号码
     * @param templateCode 模板code
     * @param mapContext 内容map
     * @return
     */
    Result sendSms(String mobile, String templateCode, Map<String, String> mapContext);

    /**
     *
     * @param phoneList 手机号码list
     * @param signList 签名list
     * @param templateCode 模板code
     * @param paramList 参数map转string后的list
     * @return
     */
    Result sendBatchSms(List<String> phoneList, List<String> signList,
                        String templateCode, List<String> paramList);

    /**
     * 发送验证码
     * @param mobile 手机号码
     * @param templateCode 模板code
     * @param verifyCode 验证码
     * @return
     */
    Result sendVerifyCode(String mobile, String templateCode, String verifyCode);

    /**
     * 校验code
     * @param mobile
     * @param verifyCode
     * @return
     */
    boolean validCode(String mobile, String verifyCode);
}
