package com.simba.scaffold.security.business.controller;


import com.simba.scaffold.security.business.domain.entity.ScheduleJob;
import com.simba.scaffold.security.business.service.IScheduleJobService;
import fykj.microservice.core.base.BaseController;
import fykj.microservice.core.base.BaseParams;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import result.Result;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author wangm
 * @since 2019-10-19
 */
@RestController
@RequestMapping("/admin/schedule")
@Api(tags = "定时任务")
public class ScheduleJobController extends BaseController<IScheduleJobService, ScheduleJob, BaseParams> {

    /**
     * 立即执行任务
     */
    @GetMapping("/run")
    public Result run(@RequestParam Long jobId) {
        baseService.run(jobId);
        return new Result();
    }

    /**
     * 暂停定时任务
     */
    @GetMapping("/pause")
    public Result pause(@RequestParam Long jobId) {
        baseService.pause(jobId);

        return new Result();
    }

    /**
     * 恢复定时任务
     */
    @GetMapping("/resume")
    public Result resume(@RequestParam Long jobId) {
        baseService.resume(jobId);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(@RequestParam Long id) {
        baseService.removeById(id);
        return new Result();
    }
}
