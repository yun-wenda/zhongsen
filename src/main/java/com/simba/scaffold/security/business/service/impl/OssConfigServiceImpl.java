package com.simba.scaffold.security.business.service.impl;

import com.simba.scaffold.security.business.domain.entity.OssConfig;
import com.simba.scaffold.security.business.mapper.OssConfigMapper;
import fykj.microservice.core.base.BaseServiceImpl;
import com.simba.scaffold.security.business.service.IOssConfigService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wangf
 * @since 2019-10-21
 */
@Service
public class OssConfigServiceImpl extends BaseServiceImpl<OssConfigMapper, OssConfig> implements IOssConfigService {

    @Override
    public OssConfig getConfig() {
        return lambdaQuery().one();
    }
}
