package com.simba.scaffold.security.business.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author wangming
 */
@Getter
@Setter
@ApiModel
public class OAuth2CodeDto {
    @NotNull(message = "授权码错误")
    @ApiModelProperty(value = "授权凭据")
    private String code;


}
