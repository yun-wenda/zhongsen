package com.simba.scaffold.security.business.service.impl;

import com.simba.scaffold.security.business.domain.entity.SysLog;
import com.simba.scaffold.security.business.mapper.SysLogMapper;
import com.simba.scaffold.security.business.service.ISysLogService;
import fykj.microservice.core.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统日志 服务实现类
 * </p>
 *
 * @author zhangzhi
 * @since 2019-10-18
 */
@Service
public class SysLogServiceImpl extends BaseServiceImpl<SysLogMapper, SysLog> implements ISysLogService {

}
