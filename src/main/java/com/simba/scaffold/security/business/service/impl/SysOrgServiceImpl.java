package com.simba.scaffold.security.business.service.impl;

import com.simba.scaffold.security.business.domain.entity.SysOrg;
import com.simba.scaffold.security.business.mapper.SysOrgMapper;
import com.simba.scaffold.security.business.service.ISysOrgService;
import constants.Mark;
import exception.BusinessException;
import fykj.microservice.core.base.BaseServiceImpl;
import fykj.microservice.core.support.util.SystemUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import result.ResultCode;
import utils.StringUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * 组织机构
 *
 * 服务实现类
 * @author wangming
 * @email ${email}
 * @date 2021-03-12
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysOrgServiceImpl extends BaseServiceImpl<SysOrgMapper, SysOrg> implements ISysOrgService {

    @Override
    public List<SysOrg> tree(Integer level, Integer lastLevel, Long pid) {
        List<SysOrg> list = orgList(level, lastLevel, pid);
        return SystemUtil.buildTree(list);
    }

    private List<SysOrg> orgList(Integer level, Integer lastLevel, Long pid) {
        List<SysOrg> areas = lambdaQuery().ge(level != null, SysOrg::getLevel, level)
                .le(lastLevel != null, SysOrg::getLevel, lastLevel)
                .eq(pid != null, SysOrg::getParentId, pid)
                .orderByAsc(SysOrg::getSequence)
                .list();
        if (CollectionUtils.isEmpty(areas)) {
            return Collections.emptyList();
        }
        return areas;
    }

    @Override
    public boolean save(SysOrg entity) {
        if (!codeUnique(entity.getId(),entity.getCode())) {
            throw new BusinessException(ResultCode.BAD_REQUEST, "组织编号重复");
        }
        entity.setLeaf(true);
        updateCodeLinkedAndLevel(entity);
        updateParentIsLeaf(entity.getParentId(), null, null);
        return super.save(entity);
    }

    @Override
    public boolean updateById(SysOrg entity) {
        if (!codeUnique(entity.getId(),entity.getCode())) {
            throw new BusinessException(ResultCode.BAD_REQUEST, "组织编号重复");
        }
        updateCodeLinkedAndLevel(entity);
        SysOrg original = getById(entity.getId());
        if (!super.updateById(entity)) {
            return false;
        }

        updateParentIsLeaf(entity.getParentId(), original.getParentId(), entity.getId());

        String originalLinked = original.getCodeLinked();
        String target = entity.getCodeLinked();
        if (!StringUtil.equals(originalLinked, target)) {
            updateLinked(originalLinked, target);
        }
        return true;
    }

    private boolean updateLinked(String original, String target) {
        List<SysOrg> toUpdateList = lambdaQuery().likeRight(SysOrg::getCodeLinked, original).list();
        if (CollectionUtils.isEmpty(toUpdateList)) {
            return true;
        }
        toUpdateList.parallelStream().forEach(it -> it.setCodeLinked(it.getCodeLinked().replace(original, target)));
        return updateBatchById(toUpdateList);
    }

    private void updateCodeLinkedAndLevel(SysOrg org) {
        Long parentId = org.getParentId();
        String codeLinked = org.getCode();
        int level = 1;
        if (parentId != null) {
            SysOrg parent = getById(parentId);
            level = parent.getLevel() + 1;
            codeLinked = parent.getCodeLinked() + codeLinked;
        } else {
            codeLinked = Mark.COMMA + codeLinked;
        }
        org.setCodeLinked(codeLinked + Mark.COMMA);
        org.setLevel(level);
    }

    private void updateParentIsLeaf(Long parentId, Long originalPid, Long id) {
        lambdaUpdate().eq(SysOrg::getId, parentId).set(SysOrg::getLeaf, Boolean.FALSE).update();

        if (originalPid != null && !parentId.equals(originalPid)) {
            boolean isLeaf = lambdaQuery().eq(SysOrg::getParentId, originalPid)
                    .ne(id != null, SysOrg::getId, id)
                    .count() == 0;
            lambdaUpdate().eq(SysOrg::getId, originalPid).set(SysOrg::getLeaf, isLeaf).update();
        }

        if (id != null) {
            boolean isLeafSelf = lambdaQuery().eq(SysOrg::getParentId, id).count() == 0;
            lambdaUpdate().eq(SysOrg::getId, id).set(SysOrg::getLeaf, isLeafSelf).update();
        }
    }

    @Override
    public boolean codeUnique(Long id, String code) {
        return lambdaQuery().eq(SysOrg::getCode, code)
                .ne(id != null, SysOrg::getId, id)
                .count() == 0;
    }

    @Override
    public List<SysOrg> treeOfUser(Serializable userId, Integer level, Integer lastLevel, Long pid) {
        List<SysOrg> list = orgList(level, lastLevel, pid);
        List<String> gridCodeLinkedList = getCodeLinkedList(userId);
        List<SysOrg> filterList = listDataFilter(list, gridCodeLinkedList);
        return SystemUtil.buildTree(filterList);
    }

    private List<String> getCodeLinkedList(Serializable userId) {
        return baseMapper.getCodeLinkedList(userId);
    }

    public static List<SysOrg> listDataFilter(List<SysOrg> list, List<String> gridCodeLinkedList) {
        List<SysOrg> result = new ArrayList<>();
        list.forEach((it) -> {
            String codeLink = it.getCodeLinked();
            if (gridCodeLinkedList.stream().anyMatch(item -> (codeLink.startsWith(item)))) {
                result.add(it);
            }
        });
        return result;
    }

    @Override
    public SysOrg get(Long id) {
        SysOrg grid = super.getById(id);
        Long pid = grid.getParentId();
        if (pid != null) {
            SysOrg parent = super.getById(pid);
            grid.setParentName(parent.getName());
        }
        return grid;
    }
}
