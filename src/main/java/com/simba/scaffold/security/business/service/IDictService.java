package com.simba.scaffold.security.business.service;

import com.simba.scaffold.security.business.domain.entity.Dict;
import fykj.microservice.cache.support.DictCacheService;
import fykj.microservice.core.base.IBaseService;
import fykj.microservice.core.beans.vo.IdTextVo;

import java.util.List;

/**
 * <p>
 * 数据字典服务类
 * </p>
 *
 * @author zhangzhi
 * @since 2019-02-20
 */
public interface IDictService extends IBaseService<Dict>, DictCacheService {
    /**
     * 查询指定编码的数据字典对象
     *
     * @param code 指定字典编码
     * @return 字典对象
     */
    Dict getByCode(String code);

    /**
     * 查询指定类别code的数据字典列表
     *
     * @param code 数据字典类型编码
     * @return 数据字典列表
     */
    List<Dict> findByParentCode(String code);

    String getValueStrByCode(String code);

    /**
     * 下拉列表获取
     *
     * @param type 下拉列表类型
     * @return 下拉列表
     */
    List<IdTextVo> idTextVoList(String type);

    /**
     * 获取所有顶级数据
     *
     * @return
     */
    List<IdTextVo> findTopDict();

    /**
     * code是否存在
     *
     * @param id
     * @param code
     * @return
     */
    boolean checkCodeExists(Long id, String code);

    /**
     * 根据value模糊查询
     *
     * @param value
     * @return
     */
    String findFileTypeByValue(String value);

    /**
     * 获取Oss存储允许上传后缀列表
     *
     * @return 后缀列表
     */
    List<String> getOssExtList();

    /**
     * 加载数据字典树
     *
     * @param name 名称过滤条件
     * @return 数据字典树
     */
    List<Dict> tree(String name);

    /**
     * 查找指定类型的数据字典树
     *
     * @param code 指定类型code
     * @return 数据字典树
     */
    List<Dict> findTreeByParentCode(String code);

    /**
     * 查询指定code的数据字典和下级数据字典
     *
     * @param code 指定数据字典code
     * @return 数据字典，包含子级
     */
    Dict findParentAndChildren(String code);

    /**
     * 临时用的批量新增数据字典
     * @param parentId
     * @param text
     */
    void batchAdd(long parentId, String text);

}
