package com.simba.scaffold.security.business.service;

import com.simba.scaffold.security.business.domain.entity.ScheduleJob;
import fykj.microservice.core.base.IBaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangm
 * @since 2019-10-19
 */
public interface IScheduleJobService extends IBaseService<ScheduleJob> {

    /**
     * 立即执行
     */
    void run(Long jobId);

    /**
     * 暂停运行
     */
    void pause(Long jobId);

    /**
     * 恢复运行
     */
    void resume(Long jobId);
}
