package com.simba.scaffold.security.business.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import fykj.microservice.core.base.BaseTreeEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 组织机构
 *
 * @author wangming
 * @email ${email}
 * @date 2021-03-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_org")
public class SysOrg extends BaseTreeEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 编码
	 */
	@TableField("code")
	@ApiModelProperty(value = "编码")
	private String code;

	/**
	 * 名称
	 */
	@TableField("name")
	@ApiModelProperty(value = "名称")
	private String name;

	/**
	 * 级别
	 */
	@TableField("level")
	@ApiModelProperty(value = "级别")
	private Integer level;

	/**
	 * 状态
	 */
	@TableField("status")
	@ApiModelProperty(value = "状态")
	private Boolean status;

	/**
	 * 排序
	 */
	@TableField("sequence")
	@ApiModelProperty(value = "排序")
	private Integer sequence;

	/**
	 * 是否是末级节点
	 */
	@TableField("is_leaf")
	@ApiModelProperty(value = "是否是末级节点")
	private Boolean leaf;

	/**
	 * 编码链
	 */
	@TableField("code_linked")
	@ApiModelProperty(value = "编码链")
	private String codeLinked;

	@TableField(exist = false)
	@ApiModelProperty(value = "上级区域名称")
	private String parentName;

}
