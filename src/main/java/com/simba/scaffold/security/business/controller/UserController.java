package com.simba.scaffold.security.business.controller;

import com.simba.scaffold.security.business.domain.BackendUserDetail;
import com.simba.scaffold.security.business.domain.entity.User;
import com.simba.scaffold.security.business.domain.params.UserParams;
import com.simba.scaffold.security.business.service.IUserService;
import com.simba.scaffold.support.utils.Oauth2Util;
import fykj.microservice.core.base.BaseController;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import result.JsonResult;
import result.Result;

import java.util.List;

/**
 * <p>
 * 用户前端前端控制器
 * </p>
 *
 * @author wangf
 * @since 2019-10-16
 */
@RestController
@RequestMapping("/admin/user")
@Slf4j
public class UserController extends BaseController<IUserService, User, UserParams> {

    @GetMapping("loginUser")
    public JsonResult<BackendUserDetail> loginUser() {
        return new JsonResult<>(Oauth2Util.getUser());
    }

    @ApiOperation(value = "修改密码")
    @PostMapping(value = "editPassWord")
    public Result editPassWord(@RequestParam String oldPassWord, @RequestParam String newPassWord) {
        return baseService.editPassWord(oldPassWord, newPassWord);
    }

}
