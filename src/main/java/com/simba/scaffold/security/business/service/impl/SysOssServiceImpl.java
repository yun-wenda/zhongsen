package com.simba.scaffold.security.business.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.simba.scaffold.security.business.domain.dto.SysOssDto;
import com.simba.scaffold.security.business.domain.entity.SysOss;
import com.simba.scaffold.security.business.domain.params.OssParams;
import com.simba.scaffold.security.business.mapper.SysOssMapper;
import com.simba.scaffold.security.business.service.IDictService;
import com.simba.scaffold.security.business.service.ISysOssService;
import com.simba.scaffold.support.conns.Cons;
import com.simba.scaffold.support.oss.OssCons;
import com.simba.scaffold.support.oss.OssSaveUtil;
import com.simba.scaffold.support.utils.ImageUtil;
import exception.BusinessException;
import fykj.microservice.core.base.BaseParams;
import fykj.microservice.core.base.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;
import result.ResultCode;
import utils.StringUtil;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;

/**
 * 文件上传记录服务实现
 *
 * @author zhangzhi
 * @date 2019-03-04
 */
@Service
public class SysOssServiceImpl extends BaseServiceImpl<SysOssMapper, SysOss> implements ISysOssService {

    @Autowired
    private Environment environment;

    @Autowired
    private IDictService dictService;


    @Override
    public SysOss upload(MultipartFile file, String serverCode, String name) {
        SysOss oss = new SysOss();
        String filename = file.getOriginalFilename();
        String fileExt = ImageUtil.getExt(filename);
        List<String> extList = dictService.getOssExtList();
        if (!extList.contains(fileExt.toLowerCase())) {
            throw new BusinessException(ResultCode.NOT_VALID, "不支持的文件格式");
        }
        oss.setType(dictService.findFileTypeByValue(fileExt));
        oss.setFileName(filename);
        oss.setName(name);
        oss.setFileExt(fileExt);
        oss.setFileSize(file.getSize());
        if (StringUtil.isEmpty(serverCode)) {
            serverCode = dictService.getValueStrByCode(Cons.UPLOAD_SERVER_CODE);
        }
        oss.setPath(getPath(file, serverCode, fileExt));
        oss.setServerCode(serverCode);
        save(oss);
        return oss;
    }

    @Override
    public IPage<SysOss> page(BaseParams params) {
        return super.page(params).convert(this::convert);
    }

    @Override
    public SysOss getById(Serializable id) {
        return convert(super.getById(id));
    }

    private SysOss convert(SysOss sysOss) {
        sysOss.setTypeText(dictService.getNameByCode(sysOss.getType()));
        sysOss.setServerText(dictService.getNameByCode(sysOss.getServerCode()));
        return sysOss;
    }

    /**
     * 获取文件访问路径
     *
     * @param file       上传文件兑现
     * @param serverCode 存储服务器编码
     * @param fileExt    文件扩展名
     * @return 文件访问路径
     */
    private String getPath(MultipartFile file, String serverCode, String fileExt) {
        try (InputStream in = file.getInputStream()) {
            return OssSaveUtil.save(in, serverCode, fileExt);
        } catch (IOException e) {
            throw new BusinessException(ResultCode.FAIL, "读取文件失败", e);
        }
    }

    @Override
    public SysOss getFileUpload(String path) {
        SysOss upload = lambdaQuery().eq(SysOss::getPath, path).one();
        if (upload == null) {
            throw new BusinessException(ResultCode.NOT_FOUND, "未找到对应的文件");
        }
        return upload;
    }

    @Override
    public IPage<SysOssDto> getListWithQuery(IPage<SysOssDto> page, OssParams ossParams) {
        return baseMapper.getListWithQuery(page,ossParams);
    }

    /**
     * 根据名称获取最新的地址
     *
     * @param name
     * @return
     */
    @Override
    public String getNewestPdfPath(String name) {
        SysOss sysOss = lambdaQuery().eq(SysOss::getName,name).
                eq(SysOss::getFileExt,"pdf").
                orderByDesc(SysOss::getCreateDate).one();
        if(ObjectUtils.isEmpty(sysOss)){
            return null;
        }
        if(OssCons.OSS_LOCAL_IMPL_BEAN.equals(sysOss.getServerCode())){
            //配置文件里配置域名
            return environment.getProperty("system.domain")+sysOss.getPath();
        }
        return sysOss.getPath();
    }
}
