package com.simba.scaffold.security.business.controller;


import com.simba.scaffold.security.business.domain.entity.Dict;
import com.simba.scaffold.security.business.service.IDictService;
import fykj.microservice.core.base.BaseController;
import fykj.microservice.core.base.BaseEntity;
import fykj.microservice.core.base.BaseParams;
import fykj.microservice.core.beans.vo.IdTextVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import result.JsonResult;
import result.Result;
import result.ResultCode;

import java.util.List;

/**
 * <p>
 * 数据字典前端控制器
 * </p>
 *
 * @author zhangzhi
 * @since 2019-02-20
 */
@RestController
@RequestMapping("/admin/dict")
@Api(tags = "数据字典")
public class DictController extends BaseController<IDictService, Dict, BaseParams> {

    @ApiOperation("获取数据字典树")
    @GetMapping(value = "tree")
    public JsonResult<List<Dict>> tree(
            @ApiParam(name = "name", value = "字典名称查询，模糊匹配")
            @RequestParam(required = false) String name) {
        List<Dict> tree = baseService.tree(name);
        return new JsonResult<>(tree);
    }

    @ApiOperation("保存数据字典")
    @PostMapping(value = "/save")
    @Override
    public Result save(@RequestBody @Validated({BaseEntity.Add.class}) Dict entity) {
        if (baseService.checkCodeExists(entity.getId(), entity.getCode())) {
            return new Result(ResultCode.FAIL.code(), "数据编码已存在");
        }
        boolean result = baseService.save(entity);
        if (result) {
            return OK;
        }
        return new Result(ResultCode.FAIL);
    }

    @ApiOperation("批量添加数据字典")
    @PostMapping(value = "/batchAdd")
    public Result batchAdd(long parentId, String text) {
        baseService.batchAdd(parentId, text);
        return OK;
    }

    @ApiOperation("更新数据字典")
    @PostMapping(value = "/update")
    @Override
    public Result update(@RequestBody @Validated({BaseEntity.Modify.class}) Dict entity) {
        if (baseService.checkCodeExists(entity.getId(), entity.getCode())) {
            return new Result(ResultCode.FAIL.code(), "数据编码已存在");
        }
        if (baseService.updateById(entity)) {
            return OK;
        }
        return new Result(ResultCode.DATA_EXPIRED);
    }


    @GetMapping("/code")
    public JsonResult<Dict> getByCode(@RequestParam String code) {
        return new JsonResult<>(baseService.getByCode(code));
    }

    @GetMapping("/parent")
    public JsonResult<List<Dict>> findByParentCode(@RequestParam String code) {
        List<Dict> dictList = baseService.findByParentCode(code);
        return new JsonResult<>(dictList);
    }

    @GetMapping("/parent/tree")
    public JsonResult<List<Dict>> findTreeByParentCode(@RequestParam String code) {
        List<Dict> dictList = baseService.findTreeByParentCode(code);
        return new JsonResult<>(dictList);
    }

    @GetMapping("/findTopDict")
    public JsonResult<List<IdTextVo>> findTopDict() {
        List<IdTextVo> dictList = baseService.findTopDict();
        return new JsonResult<>(dictList);
    }


}

