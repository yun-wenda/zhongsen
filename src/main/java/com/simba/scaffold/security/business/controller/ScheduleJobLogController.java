package com.simba.scaffold.security.business.controller;


import com.simba.scaffold.security.business.domain.entity.ScheduleJobLog;
import com.simba.scaffold.security.business.domain.params.ScheduleParams;
import com.simba.scaffold.security.business.service.IScheduleJobLogService;
import fykj.microservice.core.base.BaseController;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author wangm
 * @since 2019-10-19
 */
@RestController
@RequestMapping("/admin/scheduleLog")
@Api(tags = "定时任务日志")
public class ScheduleJobLogController extends BaseController<IScheduleJobLogService, ScheduleJobLog, ScheduleParams> {

//    @ApiOperation("分页查询")
//    @PostMapping(value = "/list")
//    @Override
//    public Result list(@RequestBody ScheduleParams params) {
//        return super.list(params);
//    }
}
