package com.simba.scaffold.security.business.service;

import com.simba.scaffold.security.business.domain.entity.SysLog;
import fykj.microservice.core.base.IBaseService;

/**
 * <p>
 * 系统日志 服务类
 * </p>
 *
 * @author zhangzhi
 * @since 2019-10-18
 */
public interface ISysLogService extends IBaseService<SysLog> {
}
