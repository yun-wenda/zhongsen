package com.simba.scaffold.security.business.domain.params;

import fykj.microservice.core.base.BaseParams;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 组织机构
 * 查询参数
 *
 * @author wangming
 * @email ${email}
 * @date 2021-03-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class SysOrgParams extends BaseParams {

}
