package com.simba.scaffold.security.business.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.simba.scaffold.security.business.domain.entity.User;
import fykj.microservice.core.base.BaseParams;
import fykj.microservice.core.base.IBaseService;
import result.Result;

import java.util.List;


/**
 * <p>
 * 用户服务类
 * </p>
 *
 * @author zhangzhi
 * @since 2019-02-20
 */
public interface IUserService extends IBaseService<User> {


    /**
     * 验证用户访问权限
     *
     * @param id  用户id
     * @param uri 请求路径
     * @return code：0-有权限， 其他-没权限
     */
    Result permission(Long id, String uri);

    /**
     * 根据主键获取用户显示名称
     *
     * @param id 用户主键
     * @return 用户名称
     */
    String getNameById(Long id);

    /**
     * 重置密码
     *
     * @param id
     * @return
     */
    Boolean resetPassWord(Long id);

    /**
     * 修改密码
     *
     * @param oldPassWord 旧密码
     * @param newPassWord 新密码
     * @return
     */
    Result editPassWord(String oldPassWord, String newPassWord);

    /**
     * 分页查询
     *
     * @param params 用户签到情况查询参数
     * @return 返回签到用户信息
     */
    IPage<User> page(BaseParams params);


    /**
     * 根据用户名获取用户
     * @param username 用户名
     * @return
     */
    User findByUsername(String username);


    /**
     * 根据手机号获取用户
     * @param mobile 手机号
     * @return
     */
    User findByMobile(String mobile);

    /**
     * 创建普通用户
     * @param mobile
     * @param name
     * @return
     */
    User creatUser(String mobile, String name);
}
