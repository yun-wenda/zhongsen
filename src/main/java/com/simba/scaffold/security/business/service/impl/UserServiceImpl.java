package com.simba.scaffold.security.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.simba.scaffold.security.business.domain.BackendUserDetail;
import com.simba.scaffold.security.business.domain.entity.Role;
import com.simba.scaffold.security.business.domain.entity.User;
import com.simba.scaffold.security.business.domain.params.UserParams;
import com.simba.scaffold.security.business.mapper.UserMapper;
import com.simba.scaffold.security.business.service.IResourceService;
import com.simba.scaffold.security.business.service.IRoleService;
import com.simba.scaffold.security.business.service.IUserService;
import com.simba.scaffold.support.conns.Cons;
import com.simba.scaffold.support.utils.Oauth2Util;
import com.simba.scaffold.support.wrapper.QueryWrapperBuilder;
import exception.BusinessException;
import fykj.microservice.cache.support.DictTransUtil;
import fykj.microservice.core.base.BaseParams;
import fykj.microservice.core.base.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import result.Result;
import result.ResultCode;
import utils.StringUtil;


/**
 * <p>
 * 用户服务实现类
 * </p>
 *
 * @author zhangzhi
 * @since 2019-02-20
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UserServiceImpl extends BaseServiceImpl<UserMapper, User> implements IUserService {


    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private IRoleService roleService;

    @Autowired
    private IResourceService resourceService;

    @Autowired
    private RoleResourceServiceImpl roleResourceService;

    @Override
    public Result permission(Long id, String uri) {
        User user = getById(id);
        if (user == null) {
            return new Result(ResultCode.NOT_FOUND.code(), "用户不存在");
        }

        if (StringUtil.equals(user.getUsername(), "admin")) {
            //管理员拥有全部资源权限
            return new Result();
        }

        boolean exists = resourceService.exists(uri);
        if (!exists) {
            //资源不存在，不需要鉴权
            return new Result();
        }

        //是否需要权限验证（需要鉴权的存储在数据库，不存的不需要鉴权）
        boolean hasAuth = roleResourceService.exists(user.getRoleId(), uri);
        if (hasAuth) {
            return new Result();
        }

        return new Result(ResultCode.TOKEN_FORBIDDEN_CODE);
    }

    @Override
    public String getNameById(Long id) {
        return getById(id).getUsername();
    }


    @Override
    public Boolean resetPassWord(Long id) {
        User user = getById(id);
        user.setPassword(passwordEncoder.encode(Cons.INIT_PSD));
        return super.updateById(user);
    }

    @Override
    public Result editPassWord(String oldPassWord, String newPassWord) {
        BackendUserDetail userInfo = Oauth2Util.getUser();
        if (userInfo == null) {
            return new Result(ResultCode.FAIL.code(), "获取当前用户失败，请重试");
        }
        User user = getById(userInfo.getId());
        if (user == null || !passwordEncoder.matches(oldPassWord, user.getPassword())) {
            return new Result(ResultCode.FAIL.code(), "请输入正确的旧密码");
        }

        user.setPassword(passwordEncoder.encode(newPassWord));
        if (super.updateById(user)) {
            return new Result();
        }
        return new Result(ResultCode.FAIL);
    }


    @Override
    public IPage<User> page(BaseParams params) {
        ((UserParams) params).setRoleLinked(Oauth2Util.currentRole().getRoleLinked());
        QueryWrapper<User> queryWrapper = QueryWrapperBuilder.build(params);
        IPage<User> result = page(params.getPage(), queryWrapper);
        return result.convert(this::convert);
    }

    @Override
    public boolean save(User user) {
        //验证用户名重复
        if (usernameExists(user.getUsername())) {
            throw new BusinessException(ResultCode.FAIL, "用户名已存在");
        }

        if (StringUtil.isNotEmpty(user.getPassword())) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }
        updateRoleLinked(user);
        return super.save(user);
    }

    @Override
    public boolean updateById(User user) {
        User entity = getById(user.getId());
        user.setPassword(entity.getPassword());
        updateRoleLinked(user);
        return super.updateById(user);
    }

    private User convert(User entity) {
        entity.setRoleName(roleService.getById(entity.getRoleId()).getName());
        return entity;
    }

    private boolean usernameExists(String username) {
        return lambdaQuery().eq(User::getUsername, username)
                .count() > 0;
    }

    private void updateRoleLinked(User user) {
        Role role = roleService.getById(user.getRoleId());
        user.setRoleLinked(role.getRoleLinked());
    }

    private User covert(User entity) {
        DictTransUtil.trans(entity);
        return entity;
    }

    @Override
    public User findByUsername(String username) {
        return lambdaQuery().eq(User::getUsername, username).one();
    }


    @Override
    public User findByMobile(String mobile) {
        return lambdaQuery().eq(User::getMobile,mobile).one();
    }

    @Override
    public User creatUser(String mobile, String name) {
        User user = new User();
        user.setUsername(mobile);
        user.setMobile(mobile);
        user.setName(name);
        user.setStatus(true);
        user.setPassword(passwordEncoder.encode(mobile));
        user.setRoleId(roleService.getRoleByCode(Cons.GENERAL_USER).getId());
        save(user);
        return user;
    }
}
