package com.simba.scaffold.security.business.service;

import com.simba.scaffold.security.business.domain.entity.ScheduleJobLog;
import fykj.microservice.core.base.IBaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangm
 * @since 2019-10-19
 */
public interface IScheduleJobLogService extends IBaseService<ScheduleJobLog> {

}
