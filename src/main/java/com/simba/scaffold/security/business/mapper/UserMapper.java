package com.simba.scaffold.security.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.simba.scaffold.security.business.domain.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangf
 * @since 2019-10-16
 */
public interface UserMapper extends BaseMapper<User> {

    /**
    * @Description 获取专题管理员列表
    * @Param
    * @Return
    **/
    List<User> getTopicManagerList(@Param("valueList") List<String> valueList);

}
