package com.simba.scaffold.security.business.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.simba.scaffold.security.business.domain.dto.OAuth2CodeDto;
import com.simba.scaffold.security.business.domain.dto.OAuth2RequestDto;
import com.simba.scaffold.security.business.domain.dto.Oauth2RefreshTokenDto;
import com.simba.scaffold.security.business.service.ICaptchaService;
import exception.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import result.JsonResult;
import result.Result;
import result.ResultCode;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.charset.Charset;

@RestController
@Api(tags = "/login")
@Slf4j
public class LoginController {

    @Autowired
    private ClientCredentialsResourceDetails clientCredentialsResourceDetails;

    @Autowired
    private TokenStore tokenStore;

    @Autowired
    private ICaptchaService captchaService;

    @ApiOperation("用户登录")
    @PostMapping(value = "/simba/login")
    public Result login(@RequestBody @Validated OAuth2RequestDto dto) {
        String loginType = dto.getLoginType();
        StringBuilder uri = new StringBuilder(clientCredentialsResourceDetails.getAccessTokenUri());
        FormBody body = new FormBody.Builder(Charset.forName("UTF-8"))
                .add("username",dto.getUsername())
                .add("password",dto.getPassword())
                .add("grant_type",loginType)
                .add("scope",String.join("", clientCredentialsResourceDetails.getScope()))
                .add("client_id",clientCredentialsResourceDetails.getClientId())
                .add("client_secret",clientCredentialsResourceDetails.getClientSecret())
                .build();
        return oauth(uri,body);
    }

    @ApiOperation("授权码登录")
    @PostMapping(value = "/fykj/code/login")
    public Result codeLogin(@RequestBody @Validated OAuth2CodeDto dto) {
        StringBuilder uri = new StringBuilder(clientCredentialsResourceDetails.getAccessTokenUri());
        FormBody body = new FormBody.Builder(Charset.forName("UTF-8"))
                .add("code",dto.getCode())
                .add("grant_type","one_code")
                .add("scope",String.join("", clientCredentialsResourceDetails.getScope()))
                .add("client_id",clientCredentialsResourceDetails.getClientId())
                .add("client_secret",clientCredentialsResourceDetails.getClientSecret())
                .build();
        return oauth(uri,body);
    }

    @ApiOperation("用户登录")
    @PostMapping(value = "/fykj/refresh_token")
    public Result refreshToken(@RequestBody @Validated Oauth2RefreshTokenDto dto) {
        StringBuilder uri = new StringBuilder(clientCredentialsResourceDetails.getAccessTokenUri());
        FormBody body = new FormBody.Builder(Charset.forName("UTF-8"))
                .add("refresh_token",dto.getRefreshToken())
                .add("grant_type","refresh_token")
                .add("client_id",clientCredentialsResourceDetails.getClientId())
                .add("client_secret",clientCredentialsResourceDetails.getClientSecret())
                .build();
        return oauth(uri,body);
    }
    /**
     * 验证码
     */
    @ApiOperation("获取图片验证码")
    @GetMapping("/login/captcha")
    @ApiImplicitParams(@ApiImplicitParam(name = "uuid", value = "图片验证码的唯一识别码"))
    public void captcha(@RequestParam String uuid, HttpServletResponse response) throws IOException {
        response.setHeader("Cache-Control", "no-store, no-cache");
        response.setContentType("image/jpeg");
        //获取图片验证码
        BufferedImage image = captchaService.getCaptcha(uuid);
        ServletOutputStream out = response.getOutputStream();
        ImageIO.write(image, "jpg", out);
        IOUtils.closeQuietly(out);
    }

    private Result oauth(StringBuilder uri, FormBody body) {
        Request request = new Request.Builder()
                .post(body).url(uri.toString()).build();
        return new JsonResult<>(execute(request));
    }
    private static JSONObject execute(Request request) {
        try {
            Response response = new OkHttpClient().newCall(request).execute();
            JSONObject result = JSON.parseObject(response.body().string());
            if(response.code() != HttpStatus.OK.value()){
                String errorMsg = result.getString("error_description");
                throw new BusinessException(ResultCode.BAD_REQUEST, errorMsg);
            }
            return result;
        } catch (IOException e) {
            throw new BusinessException(ResultCode.FAIL, "登陆失败:", e);
        }
    }

    @ApiOperation("退出登录")
    @GetMapping(value = "/admin/logout")
    public Result logout(@RequestHeader HttpHeaders headers) {
        String value = headers.getFirst("Authorization");
        if ((value.toLowerCase().startsWith(OAuth2AccessToken.BEARER_TYPE.toLowerCase()))) {
            String authHeaderValue = value.substring(OAuth2AccessToken.BEARER_TYPE.length()).trim();
            int commaIndex = authHeaderValue.indexOf(',');
            if (commaIndex > 0) {
                authHeaderValue = authHeaderValue.substring(0, commaIndex);
            }
            OAuth2AccessToken oAuth2AccessToken = tokenStore.readAccessToken(authHeaderValue);
            tokenStore.removeAccessToken(oAuth2AccessToken);
            return new Result();
        }
        return new Result(ResultCode.ERROR);
    }

}
