package com.simba.scaffold.security.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.simba.scaffold.security.business.domain.entity.Dict;
import com.simba.scaffold.security.business.mapper.DictMapper;
import com.simba.scaffold.security.business.service.IDictService;
import com.simba.scaffold.support.conns.Cons;
import constants.Mark;
import exception.BusinessException;
import fykj.microservice.cache.client.DictCacheClient;
import fykj.microservice.cache.config.RedisService;
import fykj.microservice.core.base.BaseServiceImpl;
import fykj.microservice.core.beans.vo.IdTextVo;
import fykj.microservice.core.support.util.SystemUtil;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.MyBatisSystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import result.ResultCode;
import utils.StringUtil;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zhangzhi
 * @since 2019-02-20
 */
@Service
@Slf4j
public class DictServiceImpl extends BaseServiceImpl<DictMapper, Dict> implements IDictService {

    @Autowired
    private DictCacheClient dictCacheClient;

    @Autowired
    private RedisService redisService;

    @Override
    public List<Dict> list() {
        return list(new QueryWrapper<Dict>().orderByAsc("sequence"));
    }

    @Override
    public Dict getByCode(String code) {
        Dict dict = lambdaQuery().eq(Dict::getCode, code).one();
        if (dict == null) {
            throw new BusinessException(ResultCode.NOT_FOUND, "未找到指定code的数据字典:" + code);
        }
        return dict;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Dict> findByParentCode(String code) {
        Dict parent = getByCode(code);
        return lambdaQuery().eq(Dict::getParentId, parent.getId())
                .orderByAsc(Dict::getSequence).list();
    }

    @Override
    public boolean save(Dict entity) {
        if (checkCodeExists(null, entity.getCode())) {
            throw new BusinessException(ResultCode.FAIL, "数据编码已存在");
        }
        updateCodePrefix(entity);
        dictCacheClient.addDict(entity.getCode(), entity.getName());
        return super.save(entity);
    }

    @Override
    public boolean updateById(Dict entity) {
        String code = entity.getCode();
        if (checkCodeExists(entity.getId(), code)) {
            throw new BusinessException(ResultCode.FAIL, "数据编码已存在");
        }
        updateCodePrefix(entity);
        dictCacheClient.deleteDict(code);
        dictCacheClient.addDict(code, entity.getName());
        return super.updateById(entity);
    }

    @Override
    public void refreshDictCache() {
        dictCacheClient.deleteAll();
        Map<String, String> dictMap = list().stream().collect(Collectors.toMap(Dict::getCode, Dict::getName));
        redisService.hmSetBatch(DictCacheClient.DICT_KEY, dictMap, -1);
    }

    @Override
    public String getValueStrByCode(String code) {
        Dict dict = getByCode(code);
        return dict.getValue();
    }

    @Override
    public Dict getById(Serializable id) {
        Dict dict = super.getById(id);
        Long pid = dict.getParentId();
        if (pid != null) {
            Dict parent = super.getById(pid);
            dict.setParentName(parent.getName());
        }
        return dict;
    }


    @Override
    public String getNameByCode(String code) {
        if (StringUtil.isEmpty(code)) {
            return "";
        }

        try {
            Dict dict = getByCode(code);
            return dict.getName();

        } catch (Exception e) {
            log.error("not found of dict code:{}。", code, e);
        }
        return code;
    }

    @Override
    public List<IdTextVo> idTextVoList(String type) {
        return findByParentCode(type).stream()
                .filter(Dict::getStatus)
                .map(it -> new IdTextVo(it.getCode(), it.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public List<IdTextVo> findTopDict() {
        List<Dict> dicts = lambdaQuery().isNull(Dict::getParentId).list();
        return dicts.stream().map(it -> new IdTextVo(it.getId(), it.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public boolean checkCodeExists(Long id, String code) {
        return lambdaQuery().eq(Dict::getCode, code)
                .ne(StringUtil.isNotEmpty(id), Dict::getId, id).count() > 0;
    }

    @Override
    public String findFileTypeByValue(String value) {
        try {
            String parentId = getByCode(Cons.FILE_TYPE).getId().toString();
            Dict dict = lambdaQuery().eq(Dict::getParentId, parentId)
                    .like(Dict::getValue, ',' + value + ',').one();
            return dict != null ? dict.getCode() : Cons.OTHER_TYPES;
        } catch (MyBatisSystemException e) {
            log.error("查询文件类型{}失败", value, e);
            throw new BusinessException(ResultCode.ERROR, "查询文件类型{+" + value + "+}失败");
        }
    }

    @Override
    public List<String> getOssExtList() {
        String parentId = getByCode(Cons.FILE_TYPE).getId().toString();
        return lambdaQuery().eq(Dict::getParentId, parentId)
                .eq(Dict::getStatus, true).list()
                .stream()
                .map(Dict::getValue)
                .map(it -> it.substring(1, it.length() - 1))
                .flatMap(it -> Arrays.stream(it.split(Mark.COMMA)))
                .collect(Collectors.toList());
    }

    @Override
    public List<Dict> tree(String name) {
        List<Dict> list = SystemUtil.buildTree(list());
        if (StringUtil.isNotEmpty(name)) {
            list = SystemUtil.treeDataFilter(list, name, Dict::getName);
        }
        return list;
    }

    @Override
    public List<Dict> findTreeByParentCode(String code) {
        Dict dict = getByCode(code);
        List<Dict> list = lambdaQuery().likeRight(Dict::getCodePrefix, dict.getCodePrefix())
                .ne(Dict::getCode, code).list();
        return SystemUtil.buildTree(list);
    }

    private void updateCodePrefix(Dict dict) {
        Long parentId = dict.getParentId();
        if (parentId != null) {
            Dict parent = getById(parentId);
            dict.setCodePrefix(parent.getCodePrefix() + dict.getCode() + Mark.COMMA);
        } else {
            dict.setCodePrefix(Mark.COMMA + dict.getCode() + Mark.COMMA);
        }
    }

    @Override
    public Dict findParentAndChildren(String code) {
        Dict dict = lambdaQuery().eq(Dict::getCode, code).one();
        dict.setChildren(lambdaQuery().eq(Dict::getParentId, dict.getId()).orderByAsc(Dict::getSequence).list());
        return dict;
    }

    @Override
    public void batchAdd(long parentId, String text) {
        List<String> textLines = Arrays.asList(text.split("\n"));
        for (int idx = 0; idx < textLines.size(); idx++) {
            String line = textLines.get(idx);
            String[] lineContents = line.split(Mark.SPACE);
            String name = lineContents[0];
            String code = lineContents[1];
            Dict dict = new Dict();
            dict.setParentId(parentId);
            dict.setName(name);
            dict.setCode(code);
            dict.setValue(code);
            dict.setSequence(idx + 1);
            dict.setStatus(true);
            save(dict);
        }
    }

}
