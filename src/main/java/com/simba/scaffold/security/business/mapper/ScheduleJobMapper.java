package com.simba.scaffold.security.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.simba.scaffold.security.business.domain.entity.ScheduleJob;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangm
 * @since 2019-10-19
 */
public interface ScheduleJobMapper extends BaseMapper<ScheduleJob> {

}
