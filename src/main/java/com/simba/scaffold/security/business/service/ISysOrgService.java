package com.simba.scaffold.security.business.service;

import com.simba.scaffold.security.business.domain.entity.SysOrg;
import fykj.microservice.core.base.IBaseService;

import java.io.Serializable;
import java.util.List;

/**
 * 组织机构
 *
 * 服务类
 * @author wangming
 * @email ${email}
 * @date 2021-03-12
 */
public interface ISysOrgService extends IBaseService<SysOrg> {

    /**
     * 获取全部区域树
     *
     * @param level     根节点级别
     * @param lastLevel 叶子节点级别
     * @param pid       父级节点id
     * @return 网格树
     */
    List<SysOrg> tree(Integer level, Integer lastLevel, Long pid);

    /**
     * 判断code是否唯一
     *
     * @param id   主键
     * @param code 编码
     * @return 是否唯一
     */
    boolean codeUnique(Long id, String code);


    /**
     * 获取指定当前用户可见的区域树
     *
     * @param level     根节点级别
     * @param lastLevel 叶子节点级别
     * @param userId    指定网格员id
     * @param pid       父级节点id
     * @return 区域树
     */
    List<SysOrg> treeOfUser(Serializable userId, Integer level, Integer lastLevel, Long pid);

    /**
     * 获取组织
     * @param id
     * @return
     */
    SysOrg get(Long id);
}

