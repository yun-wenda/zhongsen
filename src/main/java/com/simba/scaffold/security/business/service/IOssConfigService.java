package com.simba.scaffold.security.business.service;


import com.simba.scaffold.security.business.domain.entity.OssConfig;
import fykj.microservice.core.base.IBaseService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author wangf
 * @since 2019-10-21
 */
public interface IOssConfigService extends IBaseService<OssConfig> {

    /**
     * 获取oss配置
     *
     * @return 配置
     */
    OssConfig getConfig();
}
