package com.simba.scaffold.security.oauth2.onecode;

import com.simba.scaffold.security.oauth2.access.MyUserDetailsServiceImpl;
import fykj.microservice.cache.config.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.stereotype.Component;
import utils.StringUtil;

@Component
public class OneCodeAuthenticationProvider implements AuthenticationProvider {

    private MyUserDetailsServiceImpl myUserDetailsService;

    @Autowired
    private RedisService redisService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        OneCodeAuthenticationToken authenticationToken = (OneCodeAuthenticationToken) authentication;
        String oneCode = (String) authenticationToken.getPrincipal();
        String username = redisService.get(oneCode);
        if(StringUtil.isEmpty(username)){
            throw new InvalidGrantException("授权码错误");
        }
        redisService.remove(oneCode);
        UserDetails user = myUserDetailsService.loadUserByUsername(username);
        OneCodeAuthenticationToken authenticationResult = new OneCodeAuthenticationToken(user, user.getAuthorities());
        authenticationResult.setDetails(authenticationToken.getDetails());
        return authenticationResult;
    }

    public void setUserServiceDetail(MyUserDetailsServiceImpl userServiceDetail) {
        this.myUserDetailsService = userServiceDetail;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return OneCodeAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
