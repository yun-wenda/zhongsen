package com.simba.scaffold.security.oauth2.sms;

import com.simba.scaffold.security.oauth2.access.MyUserDetailsServiceImpl;
import fykj.microservice.cache.config.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.stereotype.Component;
import utils.StringUtil;

@Component
public class SmsCodeAuthenticationProvider implements AuthenticationProvider {

    private MyUserDetailsServiceImpl myUserDetailsService;

    @Autowired
    private RedisService redisService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        SmsCodeAuthenticationToken authenticationToken = (SmsCodeAuthenticationToken) authentication;
        String username = (String) authenticationToken.getPrincipal();
        UserDetails user = myUserDetailsService.loadUserByUsername(username);
        String inputCaptcha = ((SmsCodeAuthenticationToken) authentication).getCode();
        String redisCaptcha = redisService.get(username);
        //redis没有验证码，或者填的验证码和redis对不上
        if (StringUtil.isEmpty(redisCaptcha) || !redisCaptcha.equalsIgnoreCase(inputCaptcha)) {
            throw new InvalidGrantException("验证码错误");
        }
        redisService.remove(username);
        SmsCodeAuthenticationToken authenticationResult = new SmsCodeAuthenticationToken(user, user.getAuthorities());
        authenticationResult.setDetails(authenticationToken.getDetails());
        return authenticationResult;
    }

    public void setUserServiceDetail(MyUserDetailsServiceImpl userServiceDetail) {
        this.myUserDetailsService = userServiceDetail;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return SmsCodeAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
