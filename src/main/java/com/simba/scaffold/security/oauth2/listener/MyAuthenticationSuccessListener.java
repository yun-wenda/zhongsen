package com.simba.scaffold.security.oauth2.listener;

import com.simba.scaffold.security.business.domain.entity.SysLog;
import com.simba.scaffold.security.business.service.ISysLogService;
import fykj.microservice.core.support.util.SystemUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * 登陆成功事件
 *
 * @author wangf
 */
@Component
@Slf4j
public class MyAuthenticationSuccessListener implements ApplicationListener<AuthenticationSuccessEvent> {

    @Autowired
    private ISysLogService sysLogService;

    @Override
    public void onApplicationEvent(AuthenticationSuccessEvent event) {
        Authentication authentication = event.getAuthentication();
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        GrantedAuthority grantedAuthority = new SimpleGrantedAuthority("client");
        if (!authorities.contains(grantedAuthority) && "org.springframework.security.authentication.UsernamePasswordAuthenticationToken".equalsIgnoreCase(event.getSource().getClass().getName())) {
            User user = (User) authentication.getPrincipal();
            SysLog sysLog = new SysLog();
            sysLog.setUsername(user.getUsername());
            sysLog.setOperation("登陆");
            sysLog.setMethod("login");
            sysLog.setTime(0L);
            sysLog.setIp(SystemUtil.getClientIp(SystemUtil.getRequest()));
            sysLogService.save(sysLog);
        }
    }
}
