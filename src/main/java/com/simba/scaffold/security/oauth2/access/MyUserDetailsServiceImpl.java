package com.simba.scaffold.security.oauth2.access;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.simba.scaffold.security.business.domain.BackendUserDetail;
import com.simba.scaffold.security.business.domain.entity.Role;
import com.simba.scaffold.security.business.domain.entity.User;
import com.simba.scaffold.security.business.service.IRoleService;
import com.simba.scaffold.security.business.service.IUserService;
import com.simba.scaffold.support.utils.RegexUtils;
import exception.BusinessException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import result.ResultCode;

/**
 * @author wangf
 */
@Component
public class MyUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private IUserService userService;

    @Autowired
    private IRoleService roleService;

    @Override
    public UserDetails loadUserByUsername(String param) throws UsernameNotFoundException {
        Wrapper<User> queryWrapper = new QueryWrapper<>(User.builder().username(param).build());
        User one = userService.getOne(queryWrapper, true);
        if (one == null) {
            throw new BusinessException(ResultCode.NOT_FOUND, "该用户不存在");
        }
        BackendUserDetail user = new BackendUserDetail(RegexUtils.isMobileExact(param)?one.getUsername():param, one.getPassword(),
                one.getStatus(),
                true, true, true, AuthorityUtils.commaSeparatedStringToAuthorityList(one.getRoleId().toString())
        );
        user.setId(one.getId());
        user.setNickName(one.getName());
        user.setUnionId(one.getUnionId());
        BeanUtils.copyProperties(one, user);
        Role role = roleService.getById(one.getRoleId());
        user.setRoleCode(role.getCode());
        return user;
    }
}
