package com.simba.scaffold.aop;

import com.alibaba.fastjson.JSON;
import com.simba.scaffold.security.business.domain.dto.SmsValidCodeDto;
import exception.BusinessException;
import fykj.microservice.cache.config.RedisService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import result.ResultCode;
import utils.JsonUtils;
import utils.StringUtil;

import java.lang.reflect.Method;

/**
 * 短信接口防刷切面实现类
 *
 * @author: wangming
 * @date: 2019/5/14
 */
@Aspect
@Component
public class PreventAop {
    private static final Logger logger = LoggerFactory.getLogger("PreventAop");

    private static final String SMS_PREVENT = "smsPrevent";

    @Autowired
    private RedisService redisService;


    /**
     * 切入点
     */
    @Pointcut("@annotation(com.simba.scaffold.aop.Prevent)")
    public void pointcut() {
        // used for annotation
    }


    /**
     * 处理前
     *
     * @return
     */
    @Before("pointcut()")
    public void joinPoint(JoinPoint joinPoint) throws Exception {
        String requestStr = JSON.toJSONString(joinPoint.getArgs()[0]);
        if (StringUtil.isEmpty(requestStr) || requestStr.equalsIgnoreCase("{}")) {
            throw new BusinessException(ResultCode.FORBIDDEN,"入参不允许为空");
        }

        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Method method = null;
        try {
            method = joinPoint.getTarget().getClass().getMethod(methodSignature.getName(),
                    methodSignature.getParameterTypes());
        } catch (NoSuchMethodException e) {
            throw new BusinessException(ResultCode.FAIL,"获取方法出错");
        }

        Prevent preventAnnotation = method.getAnnotation(Prevent.class);
        defaultHandle(requestStr, preventAnnotation);
        return;
    }


    /**
     * 默认处理方式
     *
     * @param requestStr
     * @param prevent
     */
    private void defaultHandle(String requestStr, Prevent prevent){
        SmsValidCodeDto smsValidCodeDto = JsonUtils.toEntity(requestStr,SmsValidCodeDto.class);
        String key = SMS_PREVENT + smsValidCodeDto.getMobile();
        long expire = Long.parseLong(prevent.value());

        String resp = redisService.get(key);
        if (StringUtil.isEmpty(resp)) {
            redisService.set(key, requestStr, expire);
        } else {
            String message = !StringUtil.isEmpty(prevent.message()) ? prevent.message() :
                    expire + "秒内不允许重复请求";
            logger.warn("手机号码" + smsValidCodeDto.getMobile() + "秒内重复请求");
            throw new BusinessException(ResultCode.FORBIDDEN,message);
        }
    }
}
