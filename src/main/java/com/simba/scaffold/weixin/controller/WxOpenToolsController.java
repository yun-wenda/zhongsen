package com.simba.scaffold.weixin.controller;

import com.simba.scaffold.support.utils.Encrypt;
import com.simba.scaffold.weixin.cons.WechatCons;
import com.simba.scaffold.weixin.service.IWechatUserService;
import constants.Mark;
import exception.BusinessException;
import fykj.microservice.core.support.util.SystemUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import result.ResultCode;
import springfox.documentation.annotations.ApiIgnore;
import utils.LocalDateTimeUtil;
import utils.StringUtil;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

/**
 * @author wangming
 * @version V1.0
 * @Description:
 * @date 2019/2/12 9:19
 */
@Controller
@RequestMapping("/api/open/tools")
@Api(tags = "微信开放平台")
@Slf4j
public class WxOpenToolsController {
    @Autowired
    private WxMpService wxService;
    @Autowired
    private IWechatUserService wechatUserService;

    public static final String CALL_BACK_URL = "%s/callBack?backUrl=%s";

    @ApiOperation(value = "获取微信登陆二维码地址")
    @RequestMapping(value = "/getLoginQRCodeUrl",method = RequestMethod.GET)
    public String getLoginQRCodeUrl(String code,String backUrl) {
        if (!wxService.switchover(code)) {
            throw new BusinessException(ResultCode.TOKEN_ERROR_CODE, "未配置相关的服务号");
        }
        StringBuffer requestURL = SystemUtil.getRequest().getRequestURL();
        String redirectUrl = String.format(CALL_BACK_URL, requestURL.substring(0, requestURL.lastIndexOf(Mark.SLASH)), Encrypt.base64Encode(backUrl, StandardCharsets.UTF_8.name()));
        return "redirect:" +  wxService.buildQrConnectUrl(redirectUrl, WechatCons.SCOPE,code);
    }

    /**
     * 微信扫码回调处理
     * @param req
     * @return
     * @parameter
     */
    @ApiIgnore
    @RequestMapping(value = "/callBack",method = RequestMethod.GET)
    public String callBack(HttpServletRequest req) throws UnsupportedEncodingException {
        log.info("callBackStart1:{}"+ LocalDateTimeUtil.getNow());
        String code = req.getParameter("code");
        if(StringUtils.isEmpty(code)){
            return "";
        }
        String backUrl = Encrypt.base64Decode(req.getParameter("backUrl"), "UTF-8");
        log.info("backUrl:{}",backUrl);
        String serviceCode = req.getParameter("state");
        if(StringUtils.isEmpty(serviceCode)){
            log.error("serviceCode:{}","map配置出错");
            return "map配置出错";
        }
        //通过code换取网页授权access_token
        log.info("callBackStart2:{}"+ LocalDateTimeUtil.getNow());
        if (!wxService.switchover(code)) {
            throw new BusinessException(ResultCode.TOKEN_ERROR_CODE, "未配置相关的服务号");
        }
        StringBuilder location=new StringBuilder("redirect:").append(backUrl);
        try {
            if (!wxService.switchover(code)) {
                throw new BusinessException(ResultCode.TOKEN_ERROR_CODE, "未配置相关的服务号");
            }
            //获取accessToken
            WxMpOAuth2AccessToken accessToken = wxService.oauth2getAccessToken(code);
            //获取用户信息
            log.info("callBackStart3:{}"+ LocalDateTimeUtil.getNow());
            String oneCode = wechatUserService.codeByOpen(accessToken.getUnionId(),accessToken.getOpenId());
            log.info("callBackStart4:{}"+ LocalDateTimeUtil.getNow());
            if (StringUtil.isNotEmpty(oneCode)) {
                if(backUrl.contains(Mark.QUESTION)){
                    location.append("&code=").append(oneCode);
                }else{
                    location.append("?code=").append(oneCode);
                }
            } else {
                if(backUrl.contains(Mark.QUESTION)){
                    location.append("&unionId=").append(accessToken.getUnionId());
                }else{
                    location.append("?unionId=").append(accessToken.getUnionId());
                }
            }
        } catch (WxErrorException e) {
            log.error("授权失败:{}",e.getMessage());
            return e.getMessage();
        }
        log.info("redirect:{}",location.toString());
        return location.toString();
    }
}
