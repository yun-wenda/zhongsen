package com.simba.scaffold.weixin.controller;

import cn.binarywang.wx.miniapp.api.WxMaService;
import com.simba.scaffold.weixin.config.WxMaConfig;
import exception.BusinessException;
import fykj.microservice.core.support.util.SystemUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import me.chanjar.weixin.common.error.WxErrorException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import result.JsonResult;
import result.Result;
import result.ResultCode;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author wangming
 * @version V1.0
 * @Description:
 * @date 2019/2/12 9:19
 */
@RestController
@RequestMapping("/api/ma/tools")
@Api(tags = "微信小程序")
public class WxMaToolsController {

    @ApiOperation(value = "获取微信小程序access_token")
    @GetMapping(value = "/getAccessToken")
    public Result getAccessToken(@RequestParam String code) {
        WxMaService wxMaService = WxMaConfig.getMaServiceByCode(code);
        try {
            return new JsonResult<>(wxMaService.getAccessToken());
        } catch (WxErrorException e) {
            throw new BusinessException(ResultCode.ERROR, e);
        }
    }

    @ApiOperation(value = "获取微信小程序指定页面二维码")
    @GetMapping(value = "/createWxaCodeUnLimitBytes")
    public void createWxaCodeUnLimitBytes(@ApiParam(name = "小程序code必填")
                                               @RequestParam String code,
                                          @ApiParam(name = "参数，长度不能超过32位")
                                          @RequestParam(required = false) String scene,
                                          @ApiParam(name = "跳转页面")
                                              @RequestParam  String page,
                                          @ApiParam(name = "二维码的宽度，单位 px，最小 280，最大 1280")
                                              @RequestParam(required = false, defaultValue = "280")  int width) {
        WxMaService wxMaService = WxMaConfig.getMaServiceByCode(code);
        try {
            byte[] bytes = wxMaService.getQrcodeService().createWxaCodeUnlimitBytes(scene,page,
                    width,true,null,false);
            HttpServletResponse response = SystemUtil.getResponse();
            response.setContentType("image/jpeg");
            OutputStream stream = response.getOutputStream();
            stream.write(bytes);
            stream.flush();
            stream.close();
        } catch (WxErrorException e) {
            throw new BusinessException(ResultCode.ERROR, e);
        } catch (IOException e) {
            throw new BusinessException(ResultCode.ERROR, e);
        }
    }
}
