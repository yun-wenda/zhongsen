package com.simba.scaffold.weixin.controller;

import com.simba.scaffold.support.utils.Encrypt;
import com.simba.scaffold.weixin.domain.entity.WechatUser;
import com.simba.scaffold.weixin.service.IWechatUserService;
import constants.Mark;
import exception.BusinessException;
import fykj.microservice.core.support.util.SystemUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import result.JsonResult;
import result.Result;
import result.ResultCode;
import springfox.documentation.annotations.ApiIgnore;
import utils.LocalDateTimeUtil;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * 测试url：  http://XXXX/oAuth/authorize?backUrl=https%3A%2F%2Fwww.baidu.com&code=a&scope=2
 *
 * @author wangf
 */
//@ApiIgnore
@Controller
@RequestMapping("/oAuth")
@Api(tags = "微信授权")
@Slf4j
public class OauthController {

    @Autowired
    private WxMpService wxService;

    public static final String AUTH_URI = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=%s&state=%s#wechat_redirect";

    public static final String CALL_BACK_URL = "%s/callBack?backUrl=%s";
    @Autowired
    private IWechatUserService wechatUserService;


    /**
     * @param backUrl
     * @param code    数据库配置的服务号唯一码
     * @param scope   是否是静默授权，2代表静默，默认是静默授权
     */
    @ApiOperation(value = "微信授权")
    @RequestMapping(value = "/authorize", method = RequestMethod.GET)
    public void getAuthUrLToGetCode(@RequestParam String backUrl, @RequestParam String code, @RequestParam String scope) throws UnsupportedEncodingException {
        log.info("thred:{}", Thread.currentThread().getName());
        if (StringUtils.isEmpty(backUrl)) {
            return;
        }
        if (!wxService.switchover(code)) {
            throw new BusinessException(ResultCode.TOKEN_ERROR_CODE, "未配置相关的服务号");
        }
        if ("1".equalsIgnoreCase(scope)) {
            scope = "snsapi_userinfo";
        } else {
            scope = "snsapi_base";
        }
        StringBuffer requestURL = SystemUtil.getRequest().getRequestURL();
        String redirectUrl = String.format(CALL_BACK_URL, requestURL.substring(0, requestURL.lastIndexOf(Mark.SLASH)), Encrypt.base64Encode(backUrl, StandardCharsets.UTF_8.name()));
        String authUrl = String.format(AUTH_URI, wxService.getWxMpConfigStorage().getAppId(), URLEncoder.encode(redirectUrl, StandardCharsets.UTF_8.name()), scope, code);
        try {
            log.info("authUrl:{}" + authUrl);
            log.info("startDate:{}" + LocalDateTimeUtil.getNow());
            SystemUtil.getResponse().sendRedirect(authUrl);
        } catch (IOException e) {
            log.error("微信授权失败" + e.getMessage());
        }
    }

    @ApiIgnore
    @RequestMapping(value = "/callBack", method = RequestMethod.GET)
    public String callBack(HttpServletRequest req) throws WxErrorException, UnsupportedEncodingException {
        log.info("thred:{}",Thread.currentThread().getName());
        String code = req.getParameter("code");
        if (org.springframework.util.StringUtils.isEmpty(code)) {
            return "";
        }
        String state = req.getParameter("state");
        log.info("state:{}",state);
        wxService.switchoverTo(state);
        String backUrl = Encrypt.base64Decode(req.getParameter("backUrl"), "UTF-8");
        log.info("backUrl:{}", backUrl);
        WxMpOAuth2AccessToken accessToken = wxService.oauth2getAccessToken(code);
        WxMpUser wxMpUser = wxService.oauth2getUserInfo(accessToken, null);
        WechatUser user = wechatUserService.saveWxMpUser(wxMpUser);
        StringBuilder location = new StringBuilder("redirect:").append(backUrl);
        if (backUrl.contains(Mark.QUESTION)) {
            location.append("&unionid=").append(user.getUnionId());
        } else {
            location.append("?unionid=").append(user.getUnionId());
        }
        return location.toString();
    }

    @ApiIgnore
    @ApiOperation(value = "获取token")
    @RequestMapping(value = "/getAccessToken", method = RequestMethod.GET)
    @ResponseBody
    public Result getAccessToken(@RequestParam String code){
        String token = "";
        try {
            if (wxService.switchover(code)) {
                token = wxService.getAccessToken();
            }
        } catch (WxErrorException e) {
            log.error("获取token失败，异常信息{}",e.getMessage());
            throw new BusinessException(ResultCode.ERROR,"获取token失败");
        }
        return new JsonResult<>(token);
    }

    @ApiOperation(value = "获取微信公众号授权用户")
    @RequestMapping(path = "/greet", method = RequestMethod.GET)
    @ResponseBody
    public Result greet(@RequestParam String openid) {
        WechatUser user = wechatUserService.getByMpOpenid(openid);
        return new JsonResult<>(user);
    }

}
