package com.simba.scaffold.weixin.controller;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaPhoneNumberInfo;
import cn.binarywang.wx.miniapp.bean.WxMaUserInfo;
import com.simba.scaffold.weixin.config.WxMaConfig;
import com.simba.scaffold.weixin.domain.vo.FullUserInfoVo;
import com.simba.scaffold.weixin.domain.vo.InfoPhoneVo;
import com.simba.scaffold.weixin.domain.vo.InfoVo;
import com.simba.scaffold.weixin.domain.vo.WechatUserVo;
import com.simba.scaffold.weixin.service.IWechatUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.error.WxErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import result.JsonResult;
import result.Result;

/**
 * 微信小程序用户接口
 *
 * @author <a href="https://github.com/binarywang">Binary Wang</a>
 */
@Slf4j
@RestController
@RequestMapping("/api/ma/user")
@Api(tags = "微信小程序用户接口")
public class WxMaUserController {
    @Autowired
    private IWechatUserService wechatUserService;
    /**
     * 登陆接口
     */
    @PostMapping("LoginByMa")
    @ApiOperation("小程序登陆接口")
    public Result login(@RequestBody InfoVo vo) {
        final WxMaService wxMaService = WxMaConfig.getMaServiceByCode(vo.getCode());
        try {
            WxMaJscode2SessionResult session = wxMaService.getUserService().getSessionInfo(vo.getJsCode());
            // 用户信息校验
            log.info("》》》微信返回sessionData：" + session.toString());

            FullUserInfoVo fullUserInfo = vo.getUserInfo();
            if (!wxMaService.getUserService().checkUserInfo(session.getSessionKey(), fullUserInfo.getRawData(), fullUserInfo.getSignature())) {
                log.error("登录失败：数据签名验证失败");
                return new Result(-1, "登录失败");
            }

            // 解密用户信息
            WxMaUserInfo wxMaUserInfo = wxMaService.getUserService().getUserInfo(session.getSessionKey(), fullUserInfo.getEncryptedData(), fullUserInfo.getIv());
            WechatUserVo userVo = wechatUserService.wxMaAuth(wxMaUserInfo);
            return new JsonResult<>(userVo);
        } catch (WxErrorException e) {
            log.error("登录失败：" + e.getMessage());
            return new Result(-1, "登录失败");
        }
    }

    /**
     * <pre>
     * 获取用户绑定手机号信息
     * </pre>
     */
    @ApiOperation("获取小程序用户绑定手机号信息")
    @PostMapping("getPhone")
    public Result getPhone(@RequestBody InfoPhoneVo infoVo) {
        final WxMaService wxMaService = WxMaConfig.getMaServiceByCode(infoVo.getCode());
        try {
            WxMaJscode2SessionResult session = wxMaService.getUserService().getSessionInfo(infoVo.getJsCode());
            // 用户信息校验
            log.info("》》》微信返回sessionData：" + session.toString());
            // 解密
            WxMaPhoneNumberInfo phoneNoInfo = wxMaService.getUserService().getPhoneNoInfo(session.getSessionKey(), infoVo.getEncryptedData(), infoVo.getIv());
            WechatUserVo vo = wechatUserService.saveMobile(infoVo.getOpenid(),phoneNoInfo.getPurePhoneNumber());
            return new JsonResult<>(vo);
        } catch (WxErrorException e) {
            log.error("手机绑定失败：" + e.getMessage());
            return new Result(-1,"手机绑定失败");
        }
    }

}
