package com.simba.scaffold.weixin.controller;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.constant.WxMaConstants;
import com.simba.scaffold.weixin.config.WxMaConfig;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import io.swagger.annotations.Api;
import me.chanjar.weixin.common.bean.result.WxMediaUploadResult;
import me.chanjar.weixin.common.error.WxErrorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import result.JsonResult;
import result.Result;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * <pre>
 *  小程序临时素材接口
 *  Created by BinaryWang on 2017/6/16.
 * </pre>
 *
 * @author <a href="https://github.com/binarywang">Binary Wang</a>
 */
@RestController
@RequestMapping("/api/ma/media")
@Api(tags = "微信小程序临时素材接口")
public class WxMaMediaController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 上传临时素材
     *
     * @return 素材的media_id列表，实际上如果有的话，只会有一个
     */
    @PostMapping("/upload")
    public Result uploadMedia(@RequestParam String code, HttpServletRequest request) throws WxErrorException {
        final WxMaService wxService = WxMaConfig.getMaServiceByCode(code);

        CommonsMultipartResolver resolver = new CommonsMultipartResolver(request.getSession().getServletContext());

        if (!resolver.isMultipart(request)) {
            return new Result(-1,"未获取到request");
        }

        MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
        Iterator<String> it = multiRequest.getFileNames();
        List<String> mediaIds = Lists.newArrayList();
        while (it.hasNext()) {
            try {
                MultipartFile file = multiRequest.getFile(it.next());
                File newFile = new File(Files.createTempDir(), file.getOriginalFilename());
                this.logger.info("filePath is ：" + newFile.toString());
                file.transferTo(newFile);
                WxMediaUploadResult uploadResult = wxService.getMediaService().uploadMedia(WxMaConstants.KefuMsgType.IMAGE, newFile);
                this.logger.info("media_id ： " + uploadResult.getMediaId());
                mediaIds.add(uploadResult.getMediaId());
            } catch (IOException e) {
                this.logger.error(e.getMessage(), e);
                return new Result(-1,"上传失败");
            }
        }

        return new JsonResult<>(mediaIds);
    }

    /**
     * 下载临时素材
     */
    @GetMapping("/download")
    public File getMedia(@RequestParam String code, @RequestParam String mediaId) throws WxErrorException {
        final WxMaService wxService = WxMaConfig.getMaServiceByCode(code);
        return wxService.getMediaService().getMedia(mediaId);
    }
}
