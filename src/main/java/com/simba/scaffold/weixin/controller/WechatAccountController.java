package com.simba.scaffold.weixin.controller;


import com.simba.scaffold.weixin.config.WxMaConfig;
import com.simba.scaffold.weixin.config.WxMpMyConfigImpl;
import com.simba.scaffold.weixin.cons.WechatCons;
import com.simba.scaffold.weixin.domain.entity.WechatAccount;
import com.simba.scaffold.weixin.domain.params.WechatMpAccountParams;
import com.simba.scaffold.weixin.service.IWechatAccountService;
import fykj.microservice.core.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.chanjar.weixin.mp.api.WxMpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import result.JsonResult;
import result.Result;
import result.ResultCode;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author wangf
 * @since 2019-10-30
 */
@ApiIgnore
@RestController
@RequestMapping("/admin/wechat/account")
@Api(tags = "服务号管理")
public class WechatAccountController extends BaseController<IWechatAccountService, WechatAccount, WechatMpAccountParams> {

    @Autowired
    private WxMpService wxService;

    @Autowired
    private IWechatAccountService wechatAccountService;

    @ApiOperation("更新java缓存中的服务号配置，每次修改完服务号配置，点击刷新即可，避免频繁的调用数据库")
    @GetMapping("/refresh")
    public Result updateCache() {
        List<WechatAccount> mpConfigs = wechatAccountService.getByType(WechatCons.WECHAT_MP);
        if (!CollectionUtils.isEmpty(mpConfigs)) {
            wxService.setMultiConfigStorages(mpConfigs
                    .stream().map(a -> {
                        WxMpMyConfigImpl configStorage = new WxMpMyConfigImpl();
                        configStorage.setSecret(a.getSecret());
                        configStorage.setAppId(a.getAppId());
                        configStorage.setToken(a.getToken());
                        configStorage.setAesKey(a.getAesKey());
                        configStorage.setCode(a.getCode());
                        return configStorage;
                    }).collect(Collectors.toMap(WxMpMyConfigImpl::getCode, a -> a, (o, n) -> o)));
        }
        WxMaConfig.initConfig();
        return OK;
    }

    @ApiOperation("分页查询")
    @PostMapping(value = "/list")
    public Result page(@RequestBody WechatMpAccountParams params) {
        return super.list(params);
    }

    public Result get(String id) {
        WechatAccount account = baseService.getById(id);
        account.setAesKey("");
        account.setSecret("");
        account.setToken("");
        return new JsonResult<>(account);
    }

    @Override
    public Result update(@RequestBody WechatAccount entity) {
        if (baseService.updateByIdFromPage(entity)) {
            return OK;
        }
        return new Result(ResultCode.DATA_EXPIRED);
    }
}
