package com.simba.scaffold.weixin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.simba.scaffold.weixin.domain.entity.WechatUser;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangm
 * @since 2019-12-22
 */
public interface WechatUserMapper extends BaseMapper<WechatUser> {

}
