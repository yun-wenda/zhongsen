package com.simba.scaffold.weixin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.simba.scaffold.weixin.domain.entity.WechatAccount;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangf
 * @since 2019-10-30
 */
public interface WechatAccountMapper extends BaseMapper<WechatAccount> {

}
