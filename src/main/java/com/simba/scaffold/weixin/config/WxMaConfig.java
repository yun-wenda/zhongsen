package com.simba.scaffold.weixin.config;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.api.impl.WxMaServiceImpl;
import cn.binarywang.wx.miniapp.config.impl.WxMaDefaultConfigImpl;
import com.simba.scaffold.weixin.cons.WechatCons;
import com.simba.scaffold.weixin.domain.entity.WechatAccount;
import com.simba.scaffold.weixin.service.IWechatAccountService;
import com.simba.scaffold.weixin.service.impl.WechatAccountServiceImpl;
import com.google.common.collect.Maps;
import fykj.microservice.core.support.util.SpringContextUtil;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.util.CollectionUtils;
import utils.StringUtil;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author zhangzhi
 */
@AllArgsConstructor
@Configuration
public class WxMaConfig {

    private static Map<String, WxMaService> maServices = Maps.newHashMap();
    public static final Map<String, String> PROPERTY_MAP = Maps.newHashMap();


    public static WxMaService getMaServiceByCode(String code){
        return getMaService(PROPERTY_MAP.get(code));
    }

    public static WxMaService getMaService(String appId) {
        WxMaService wxService = maServices.get(appId);
        if (wxService == null) {
            throw new IllegalArgumentException(String.format("未找到对应appid=[%s]的配置，请核实！", appId));
        }
        return wxService;
    }

    @Bean
    @DependsOn("springContextUtil")
    public void init() {
        initConfig();
    }

    public static void initConfig() {
        IWechatAccountService wechatAccountService = SpringContextUtil.getBean(WechatAccountServiceImpl.class);
        final List<WechatAccount> configs = wechatAccountService.getByType(WechatCons.WECHAT_MA);
        if (CollectionUtils.isEmpty(configs)) {
            throw new RuntimeException("大哥，请在数据库中添加小程序相关配置，注意别配错了！");
        }
        maServices = configs.stream()
                .map(a -> {
                    WxMaDefaultConfigImpl config = new WxMaDefaultConfigImpl();
                    config.setAppid(a.getAppId());
                    config.setSecret(a.getSecret());
                    config.setToken(a.getToken());
                    config.setAesKey(a.getAesKey());
                    config.setMsgDataFormat(StringUtil.isEmpty(a.getMsgDataFormat())?"JSON":a.getMsgDataFormat());
                    WxMaService service = new WxMaServiceImpl();
                    service.setWxMaConfig(config);
                    PROPERTY_MAP.put(a.getCode(),a.getAppId());
                    return service;
                }).collect(Collectors.toMap(s -> s.getWxMaConfig().getAppid(), a -> a));
    }
}
