package com.simba.scaffold.weixin.cons;

/**
 * @author lenovo
 */
public class WechatCons {

    /**
     * 微信类型-小程序
     */
    public final static String WECHAT_MA = "wechat_ma";

    /**
     * 微信类型-公众号
     */
    public final static String WECHAT_MP = "wechat_mp";

    /**
     * 微信类型-开放平台
     */
    public final static String WECHAT_OPEN = "wechat_open";


    public static final String SCOPE = "snsapi_login";
}
