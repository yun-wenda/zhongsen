package com.simba.scaffold.weixin.service.impl;

import cn.binarywang.wx.miniapp.bean.WxMaUserInfo;
import com.simba.scaffold.security.business.domain.entity.User;
import com.simba.scaffold.security.business.service.IUserService;
import com.simba.scaffold.weixin.domain.entity.WechatUser;
import com.simba.scaffold.weixin.domain.vo.WechatUserVo;
import com.simba.scaffold.weixin.mapper.WechatUserMapper;
import com.simba.scaffold.weixin.service.IWechatUserService;
import fykj.microservice.cache.config.RedisService;
import fykj.microservice.core.base.BaseServiceImpl;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.util.RandomValueStringGenerator;
import org.springframework.stereotype.Service;
import utils.StringUtil;

import java.time.LocalDateTime;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangm
 * @since 2019-07-13
 */
@Service
public class WechatUserServiceImpl extends BaseServiceImpl<WechatUserMapper, WechatUser> implements IWechatUserService {


    @Autowired
    private RedisService redisService;
    @Autowired
    private IUserService userService;

    @Override
    public WechatUserVo wxMaAuth(WxMaUserInfo wxMaUserInfo) {
        WechatUser wechatUser = getByUnionIdOrOpenid(wxMaUserInfo.getUnionId(),wxMaUserInfo.getOpenId());
        if (wechatUser == null) {
            wechatUser = new WechatUser();
        }
        wechatUser.setGender(wxMaUserInfo.getGender());
        wechatUser.setCountry(wxMaUserInfo.getCountry());
        wechatUser.setProvince(wxMaUserInfo.getProvince());
        wechatUser.setCity(wxMaUserInfo.getCity());
        wechatUser.setUnionId(wxMaUserInfo.getUnionId());
        wechatUser.setOpenid(wxMaUserInfo.getOpenId());
        wechatUser.setNickname(wxMaUserInfo.getNickName());
        wechatUser.setHeadImgUrl(wxMaUserInfo.getAvatarUrl());
        saveOrUpdate(wechatUser);
        return getWechatUser(wechatUser);
    }

    public WechatUser getByUnionIdOrOpenid(String unionId, String openid) {
        if (StringUtil.isEmpty(unionId)) {
            return getByOpenid(openid);
        }
        WechatUser wechatUser = getByUnionId(unionId);
        if (wechatUser != null) {
            return wechatUser;
        }
        return getByOpenid(openid);
    }

    @Override
    public WechatUser getByOpenid(String openid) {
        return lambdaQuery().eq(WechatUser::getOpenid,openid).one();
    }

    @Override
    public WechatUser getByMpOpenid(String mpOpenid) {
        return lambdaQuery().eq(WechatUser::getMpOpenid,mpOpenid).one();
    }

    @Override
    public WechatUser getByUnionId(String unionId) {
        return lambdaQuery().eq(WechatUser::getUnionId,unionId).one();
    }

    @Override
    public WechatUserVo saveMobile(String openid, String mobile) {
        WechatUser wechatUser = getByOpenid(openid);
        wechatUser.setMobile(mobile);
        updateById(wechatUser);
        return getWechatUser(wechatUser);
    }

    private WechatUserVo getWechatUser(WechatUser wechatUser){
        WechatUserVo vo = new WechatUserVo();
        vo.setUnionId(wechatUser.getUnionId());
        vo.setOpenid(wechatUser.getOpenid());
        vo.setName(wechatUser.getNickname());
        vo.setHeadImgUrl(wechatUser.getHeadImgUrl());
        vo.setMobile(wechatUser.getMobile());
        vo.setCode(getOneCode(wechatUser.getMobile(),wechatUser.getNickname()));
        return vo;
    }

    @Override
    public String getOneCode(String mobile, String name){
        // 如果手机号为空，直接返回
        if (StringUtil.isEmpty(mobile)) {
            return null;
        }
        //不为空查询用户是否存在
        User user = userService.findByUsername(mobile);
        //不存在则创建用户
        if (user == null) {
            user = userService.creatUser(mobile, name);
        }
        String oneCode = new RandomValueStringGenerator().generate();
        redisService.set(oneCode, mobile, 60 * 5);
        return oneCode;
    }

    @Override
    public void subscribeHandler(WxMpUser userWxInfo) {
        WechatUser wechatUser = getByUnionIdOrMpOpenid(userWxInfo.getUnionId(),userWxInfo.getOpenId());
        if (wechatUser == null) {
            wechatUser = new WechatUser();
            wechatUser.setSubscribeTime(LocalDateTime.now());
        }
        wechatUser.setSubscribeStatus(true);
        wechatUser.setUnionId(userWxInfo.getUnionId());
        wechatUser.setMpOpenid(userWxInfo.getOpenId());
        wechatUser.setNickname(userWxInfo.getNickname());
        wechatUser.setHeadImgUrl(userWxInfo.getHeadImgUrl());
        wechatUser.setGender(String.valueOf(userWxInfo.getSex()));
        wechatUser.setCountry(userWxInfo.getCountry());
        wechatUser.setProvince(userWxInfo.getProvince());
        wechatUser.setCity(userWxInfo.getCity());
        saveOrUpdate(wechatUser);
    }

    @Override
    public WechatUser saveWxMpUser(WxMpUser wxMpUser) {
        WechatUser wechatUser = getByUnionIdOrMpOpenid(wxMpUser.getUnionId(),wxMpUser.getOpenId());
        if (wechatUser != null) {
            return wechatUser;
        }
        wechatUser = new WechatUser();
        wechatUser.setSubscribeStatus(false);
        wechatUser.setMpOpenid(wxMpUser.getOpenId());
        wechatUser.setUnionId(wxMpUser.getUnionId());
        wechatUser.setNickname(wxMpUser.getNickname());
        wechatUser.setHeadImgUrl(wxMpUser.getHeadImgUrl());
        wechatUser.setGender(StringUtil.isEmpty(wxMpUser.getSex()) ? "" : String.valueOf(wxMpUser.getSex()));
        wechatUser.setCountry(wxMpUser.getCountry());
        wechatUser.setProvince(wxMpUser.getProvince());
        wechatUser.setCity(wxMpUser.getCity());
        saveOrUpdate(wechatUser);
        return wechatUser;
    }

    @Override
    public void unsubscribeHandler(String openId) {
        WechatUser wechatUser = getByMpOpenid(openId);
        if (wechatUser == null) {
            return;
        }
        wechatUser.setSubscribeStatus(false);
        saveOrUpdate(wechatUser);
    }


    @Override
    public String codeByOpen(String unionId, String webOpenid) {
        WechatUser wechatUser = getByUnionId(unionId);
        // 如果不存在微信用户
        if (wechatUser == null) {
            wechatUser = new WechatUser();
            wechatUser.setUnionId(unionId);
            wechatUser.setWebOpenid(webOpenid);
            save(wechatUser);
            return null;
        }
        // 如果存在用户,并且开放平台openid不存在，更新
        if (StringUtil.isEmpty(wechatUser.getWebOpenid())) {
            wechatUser.setWebOpenid(webOpenid);
            updateById(wechatUser);
        }
        //返回凭据
        return getOneCode(wechatUser.getMobile(),wechatUser.getNickname());
    }

    @Override
    public String codeByMp(String unionId, String mpOpenid) {
        WechatUser wechatUser = getByUnionIdOrMpOpenid(unionId,mpOpenid);
        if (wechatUser == null) {
            wechatUser = new WechatUser();
            wechatUser.setUnionId(unionId);
            wechatUser.setMpOpenid(mpOpenid);
            save(wechatUser);
        }
        // 如果存在用户,并且开放平台openid不存在，更新
        if (StringUtil.isEmpty(wechatUser.getMpOpenid())) {
            wechatUser.setMpOpenid(mpOpenid);
            updateById(wechatUser);
        }
        //返回凭据
        return getOneCode(wechatUser.getMobile(),wechatUser.getNickname());
    }

    @Override
    public WechatUser getByUnionIdOrMpOpenid(String unionId, String mpOpenid) {
        if (StringUtil.isEmpty(unionId)) {
            return getByMpOpenid(mpOpenid);
        }
        WechatUser wechatUser = getByUnionId(unionId);
        if (wechatUser != null) {
           return wechatUser;
        }
        return getByMpOpenid(mpOpenid);
    }
}
