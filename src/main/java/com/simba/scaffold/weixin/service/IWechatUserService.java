package com.simba.scaffold.weixin.service;

import cn.binarywang.wx.miniapp.bean.WxMaUserInfo;
import com.simba.scaffold.weixin.domain.entity.WechatUser;
import com.simba.scaffold.weixin.domain.vo.WechatUserVo;
import fykj.microservice.core.base.IBaseService;
import me.chanjar.weixin.mp.bean.result.WxMpUser;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangm
 * @since 2019-07-13
 */
public interface IWechatUserService extends IBaseService<WechatUser> {

    /**
     * 小程序用户授权
     * @param wxMaUserInfo
     */
    WechatUserVo wxMaAuth(WxMaUserInfo wxMaUserInfo);

    /**
     *  根据小程序openid，获取小程序用户信息
     * @param openid
     * @return
     */
    WechatUser getByOpenid(String openid);

    /**
     *  根据公众号openid，获取用户信息
     * @param mpOpenid
     * @return
     */
    WechatUser getByMpOpenid(String mpOpenid);

    /**
     *  根据unionId，获取微信用户信息
     * @param unionId
     * @return
     */
    WechatUser getByUnionId(String unionId);

    /**
     * 获取登陆凭据
     * @param mobile
     * @param name
     * @return
     */
    String getOneCode(String mobile, String name);

    /**
     * 保存手机号
     * @param openid
     * @param mobile
     * @return
     */
    WechatUserVo saveMobile(String openid, String mobile);

    /**
     * 添加关注用户到本地数据库
     * @param userWxInfo
     */
    void subscribeHandler(WxMpUser userWxInfo);

    /**
     * 公众号用户取消关注
     * @param mpOpenid
     */
    void unsubscribeHandler(String mpOpenid);

    /**
     * 开放平台获取登陆凭据
     * @param unionId
     * @param mpOpenid
     * @return
     */
    String codeByOpen(String unionId, String mpOpenid);

    /**
     * 微信公众号获取登陆凭据
     * @param unionId
     * @param mpOpenid
     * @return
     */
    String codeByMp(String unionId, String mpOpenid);

    /**
     * 根据unionId和微信公众号openid获取微信用户
     */
    WechatUser getByUnionIdOrMpOpenid(String unionId, String mpOpenid);

    /**
     * 保存微信公众号用户
     * @param wxMpUser
     * @return
     */
    WechatUser saveWxMpUser(WxMpUser wxMpUser);
}
