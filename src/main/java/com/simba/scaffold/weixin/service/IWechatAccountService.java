package com.simba.scaffold.weixin.service;

import com.simba.scaffold.weixin.domain.entity.WechatAccount;
import fykj.microservice.core.base.IBaseService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author wangf
 * @since 2019-10-30
 */
public interface IWechatAccountService extends IBaseService<WechatAccount> {

    /**
     * 从页面修改
     *
     * @param account 账号
     * @return 是否成功
     */
    boolean updateByIdFromPage(WechatAccount account);

    /**
     * 重写save 添加code校验
     *
     * @param account 实体
     * @return 是否成功
     */
    boolean save(WechatAccount account);

    /**
     * 根据类型获取微信账号信息
     * @param type
     * @return
     */
    List<WechatAccount> getByType(String type);
}
