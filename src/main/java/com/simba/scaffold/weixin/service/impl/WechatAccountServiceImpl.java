package com.simba.scaffold.weixin.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.simba.scaffold.weixin.domain.entity.WechatAccount;
import com.simba.scaffold.weixin.mapper.WechatAccountMapper;
import com.simba.scaffold.weixin.service.IWechatAccountService;
import exception.BusinessException;
import fykj.microservice.cache.support.DictTransUtil;
import fykj.microservice.core.base.BaseParams;
import fykj.microservice.core.base.BaseServiceImpl;
import fykj.microservice.core.support.util.BeanUtil;
import org.springframework.stereotype.Service;
import result.ResultCode;
import utils.StringUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wangf
 * @since 2019-10-30
 */
@Service
public class WechatAccountServiceImpl extends BaseServiceImpl<WechatAccountMapper, WechatAccount> implements IWechatAccountService {

    //密文未修改标识
    private final static String NOT_MODIFY_FLAG = "notmodify";

    @Override
    public boolean updateByIdFromPage(WechatAccount account) {
        WechatAccount raw = getById(account.getId());
        if (raw == null) {
            throw new BusinessException(ResultCode.NOT_FOUND, "找不到资源");
        }
        boolean isExistCode = isExistCode(account);
        if (isExistCode) {
            throw new BusinessException(ResultCode.EXIST, "code已存在");
        }
        List<String> notModifyFields = new ArrayList<>();
        if (NOT_MODIFY_FLAG.equals(account.getAesKey())) {
            notModifyFields.add("aesKey");
        }
        if (NOT_MODIFY_FLAG.equals(account.getToken())) {
            notModifyFields.add("token");
        }
        if (NOT_MODIFY_FLAG.equals(account.getSecret())) {
            notModifyFields.add("secret");
        }
        BeanUtil.copyProperties(account, raw, notModifyFields.toArray(new String[0]));
        return updateById(raw);
    }

    @Override
    public boolean save(WechatAccount entity) {
        boolean isExistCode = isExistCode(entity);
        if (isExistCode) {
            throw new BusinessException(ResultCode.EXIST, "code已存在");
        }
        return super.save(entity);
    }

    /**
     * 校验code是否存在
     *
     * @param account
     * @return
     */
    private boolean isExistCode(WechatAccount account) {
        return lambdaQuery().eq(WechatAccount::getCode, account.getCode())
                .ne(StringUtil.isNotEmpty(account.getId()), WechatAccount::getId, account.getId()).count() > 0;
    }

    @Override
    public IPage<WechatAccount> page(BaseParams params) {
        return super.page(params).convert(this::covert);
    }

    private WechatAccount covert(WechatAccount entity) {
        DictTransUtil.trans(entity);
        return entity;
    }

    @Override
    public List<WechatAccount> getByType(String type) {
        return lambdaQuery().eq(WechatAccount::getType,type).list();
    }
}
