package com.simba.scaffold.weixin.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import fykj.microservice.cache.support.DictTrans;
import fykj.microservice.core.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author wangf
 * @since 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class WechatAccount extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 类型(小程序、公众号)
     */
    @TableField("type")
    @DictTrans(transTo = "typeName")
    private String type;

    /**
     * 类型名称
     */
    @TableField(exist = false)
    private String typeName;

    /**
     * 服务号代码
     */
    @TableField("code")
    private String code;

    /**
     * 服务号名称
     */
    @TableField("name")
    private String name;

    /**
     * 公众号或者小程序的appid
     */
    @TableField("app_id")
    private String appId;

    /**
     * 公众号的appsecret
     */
    @TableField("secret")
    private String secret;

    /**
     * 接口配置里的Token值
     */
    @TableField("token")
    private String token;

    /**
     * 接口配置里的EncodingAESKey值
     */
    @TableField("aes_key")
    private String aesKey;

    /**
     * 消息格式，XML或者JSON
     */
    @TableField("msg_data_format")
    private String msgDataFormat;


}
