package com.simba.scaffold.weixin.domain.vo;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 信息视图
 *
 *
 * @author lenovo
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel("小程序用户信息视图")
public class InfoVo implements Serializable {

    /**
     * 小程序appid对应的code
     */
    @ApiModelProperty(value = "小程序appid对应的code")
    private String code;

    /**
     * jsCode
     */
    @ApiModelProperty(value = "jsCode")
    private String jsCode;

    private FullUserInfoVo userInfo;
}
