package com.simba.scaffold.weixin.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import fykj.microservice.core.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author wangm
 * @since 2019-12-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class WechatUser extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 小程序openid
     */
    @TableField("openid")
    private String openid;

    /**
     * unionId
     */
    @TableField("union_id")
    private String unionId;

    /**
     * 公众号openid
     */
    @TableField("mp_openid")
    private String mpOpenid;

    /**
     * 开放平台openid
     */
    @TableField("web_openid")
    private String webOpenid;

    /**
     * 昵称
     */
    @TableField("nickname")
    private String nickname;

    /**
     * 头像地址
     */
    @TableField("head_img_url")
    private String headImgUrl;

    /**
     * 性别
     */
    @TableField("gender")
    private String gender;

    /**
     * 国家
     */
    @TableField("country")
    private String country;

    /**
     * 省份
     */
    @TableField("province")
    private String province;

    /**
     * 市
     */
    @TableField("city")
    private String city;

    /**
     * 手机号
     */
    @TableField("mobile")
    private String mobile;

    /**
     * 公众号关注状态
     */
    @TableField("subscribe_status")
    private Boolean subscribeStatus;

    /**
     * 公众号关注时间
     */
    @TableField("subscribe_time")
    private LocalDateTime subscribeTime;

}
