package com.simba.scaffold.weixin.domain.vo;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 信息视图
 *
 * @author wangming
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel("获取小程序用户手机号视图")
public class InfoPhoneVo {

    /**
     * 小程序appid对应的code
     */
    @ApiModelProperty(value = "小程序appid对应的code")
    private String code;

    /**
     * jsCode
     */
    @ApiModelProperty(value = "jsCode")
    private String jsCode;

    /**
     * openid
     */
    @ApiModelProperty(value = "小程序openid")
    private String openid;

    /**
     * sessionKey
     */
    @ApiModelProperty(value = "sessionKey")
    private String sessionKey;

    /**
     * signature
     */
    @ApiModelProperty(value = "signature")
    private String signature;

    /**
     * rawData
     */
    @ApiModelProperty(value = "rawData")
    private String rawData;

    /**
     * encryptedData
     */
    @ApiModelProperty(value = "encryptedData")
    private String encryptedData;

    /**
     * iv
     */
    @ApiModelProperty(value = "iv")
    private String iv;
}
