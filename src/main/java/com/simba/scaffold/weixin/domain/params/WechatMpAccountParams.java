package com.simba.scaffold.weixin.domain.params;

import fykj.microservice.core.base.BaseParams;
import fykj.microservice.core.support.wrapper.annotation.MatchType;
import fykj.microservice.core.support.wrapper.enums.QueryType;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wangf
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class WechatMpAccountParams extends BaseParams {
    @MatchType(fieldName = "name", value = QueryType.LIKE)
    private String name;
}
