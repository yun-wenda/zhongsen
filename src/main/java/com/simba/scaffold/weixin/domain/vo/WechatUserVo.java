package com.simba.scaffold.weixin.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户信息视图
 *
 * @author wangming
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel("用户信息视图")
public class WechatUserVo {

    /**
     * 微信unionId
     */
    @ApiModelProperty(value = "unionId")
    private String unionId;
    /**
     * 微信openid
     */
    @ApiModelProperty(value = "openid")
    private String openid;

    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名")
    private String name;

    /**
     * 头像地址
     */
    @ApiModelProperty(value = "头像地址")
    private String headImgUrl;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    private String mobile;
    /**
     * 登陆凭据
     */
    @ApiModelProperty(value = "登陆凭据")
    private String code;


}
