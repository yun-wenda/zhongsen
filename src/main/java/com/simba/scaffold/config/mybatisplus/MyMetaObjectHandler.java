package com.simba.scaffold.config.mybatisplus;

import com.simba.scaffold.support.utils.Oauth2Util;
import fykj.microservice.core.configs.mybatisplus.core.handlers.AbstractMetaObjectHandler;

import java.io.Serializable;

/**
 * @author wangf
 */
public class MyMetaObjectHandler extends AbstractMetaObjectHandler {
    public MyMetaObjectHandler() {
        this(MyMetaObjectHandler.class);
    }

    public MyMetaObjectHandler(Class handler) {
        super(handler);
    }

    @Override
    public Serializable userId() {
        return  Oauth2Util.getUser().getId();
    }}
