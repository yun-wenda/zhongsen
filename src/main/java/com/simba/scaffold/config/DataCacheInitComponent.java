package com.simba.scaffold.config;

import fykj.microservice.cache.support.DictCacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * 项目启动完成后初始化数据字典到缓存
 * @author zhangzhi
 */
@Component
@Slf4j
public class DataCacheInitComponent implements CommandLineRunner {

    @Autowired
    private DictCacheService dictService;

    @Override
    public void run(String... args) {
        log.info("初始化字典数据到缓存");
        dictService.refreshDictCache();
    }
}
