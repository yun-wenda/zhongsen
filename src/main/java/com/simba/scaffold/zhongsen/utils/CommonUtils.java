package com.simba.scaffold.zhongsen.utils;

import org.springframework.stereotype.Component;

/**
 * 一些简单的通用方法
 */
@Component
public class CommonUtils {

    /**
     * 工具方法：将文章类型（数字）转化为汉字
     * @param type
     * @return
     */
    public String transType(int type){
        String name = "";
        if(type == 0){
            name = "其他";
        }else if(type == 1){
            name = "正常";
        }else if(type == 2){
            name = "主推";
        }else if(type == 3){
            name = "探索";
        }else if(type == 4){
            name = "关于我们";
        }

        return name;
    }


}
