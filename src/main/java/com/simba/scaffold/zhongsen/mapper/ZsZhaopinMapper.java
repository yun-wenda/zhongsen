package com.simba.scaffold.zhongsen.mapper;

import com.simba.scaffold.zhongsen.domain.entity.ZsZhaopin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 *
 *
 * Mapper 接口
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
public interface ZsZhaopinMapper extends BaseMapper<ZsZhaopin> {

    List<ZsZhaopin> talentConcept();

    List<ZsZhaopin> teamElegantDemeanour();

    List<ZsZhaopin> selTalentConceptLikeName(String name);

    List<ZsZhaopin> selFengCaiLikeName(String name);

}
