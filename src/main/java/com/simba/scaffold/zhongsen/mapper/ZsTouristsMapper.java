package com.simba.scaffold.zhongsen.mapper;

import com.simba.scaffold.zhongsen.domain.entity.ZsTourists;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 *
 *
 * Mapper 接口
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
public interface ZsTouristsMapper extends BaseMapper<ZsTourists> {

    List<ZsTourists> selHotel();

    List<ZsTourists> meetingExpand();

    List<ZsTourists> studyBase();

    List<ZsTourists> ruralSpecially();

    List<ZsTourists> selApple();

    List<ZsTourists> selHotelLikeName(String name);

    List<ZsTourists> selMeetingLikeName(String name);

    List<ZsTourists> selBaselLikeName(String name);

    List<ZsTourists> selSpeciallyLikeName(String name);

    List<ZsTourists> selAppleLikeName(String name);
}
