package com.simba.scaffold.zhongsen.mapper;

import com.simba.scaffold.zhongsen.domain.entity.ZsDictionary;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.MapKey;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * Mapper 接口
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
public interface ZsDictionaryMapper extends BaseMapper<ZsDictionary> {

    List<ZsDictionary> getProMessage();

    ZsDictionary selTitle();

    ZsDictionary selTelUs();

    ZsDictionary selCoordinates();

    List<ZsDictionary> selAllOpinion();

    List<ZsDictionary> selLikeOpinion(String opinion);

    List<ZsDictionary>  selTelType(String type);
}
