package com.simba.scaffold.zhongsen.mapper;

import com.simba.scaffold.zhongsen.domain.entity.ZsPhoto;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 *
 *
 * Mapper 接口
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
public interface ZsPhotoMapper extends BaseMapper<ZsPhoto> {

    List<ZsPhoto> selByName(String name);

    int selCountByType(String type);

    List<ZsPhoto> selShowLbt();

    List<ZsPhoto> selAllLbt();
}
