package com.simba.scaffold.zhongsen.mapper;

import com.simba.scaffold.zhongsen.domain.entity.ZsAnswer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 
 *
 * Mapper 接口
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
public interface ZsAnswerMapper extends BaseMapper<ZsAnswer> {
	
}
