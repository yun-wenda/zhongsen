package com.simba.scaffold.zhongsen.mapper;

import com.simba.scaffold.zhongsen.domain.entity.ZsQuestion;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.simba.scaffold.zhongsen.domain.params.ZsQuestionParams;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 *
 * Mapper 接口
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@Mapper
@Repository
public interface ZsQuestionMapper extends BaseMapper<ZsQuestion> {

    ZsQuestion selQuestion();

    List<ZsQuestion> selLikeQuestion(String question);

}
