package com.simba.scaffold.zhongsen.mapper;

import com.simba.scaffold.zhongsen.domain.entity.ZsDictionary;
import com.simba.scaffold.zhongsen.domain.entity.ZsProject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.MapKey;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * Mapper 接口
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
public interface ZsProjectMapper extends BaseMapper<ZsProject> {

    int selCountByType(int type);

    List<ZsProject> selZhuTui();

    List<ZsProject> selTanSuo();

    List<ZsProject> selhuoDongZiXun();

    List<ZsProject> selLikeTitle(String title);

    List<ZsProject> selActionLikeName(String name);

    List<ZsProject> selProjectIntr();

    List<ZsProject> sellLikeProIntr(String name);

    List<ZsProject> selById(String id);

}
