package com.simba.scaffold.zhongsen.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.simba.scaffold.zhongsen.domain.entity.ZsCompany;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.simba.scaffold.zhongsen.domain.entity.ZsProject;

import java.util.List;

/**
 *
 *
 * Mapper 接口
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
public interface ZsCompanyMapper extends BaseMapper<ZsCompany> {

    List<ZsProject> aboutZs();

    List<ZsCompany> selVision();

    List<ZsCompany> selHistory();

    List<ZsCompany> selHonor();

    List<ZsCompany> selNews();

    @SqlParser(filter = true)
    List<ZsProject> selIntroduceLikeTitle(String title);

    List<ZsCompany> selVisionLikeName(String name);

    List<ZsCompany> selHistoryLikeName(String name);

    List<ZsCompany> selHonorLikeName(String name);

    List<ZsCompany> selNewsLikeName(String name);
}
