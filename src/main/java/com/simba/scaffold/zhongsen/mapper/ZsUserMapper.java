package com.simba.scaffold.zhongsen.mapper;

import com.simba.scaffold.zhongsen.domain.entity.ZsUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 *
 *
 * Mapper 接口
 * @author wangming
 * @email ${email}
 * @date 2021-07-27
 */
public interface ZsUserMapper extends BaseMapper<ZsUser> {

    List<ZsUser> selByName(String name);
}
