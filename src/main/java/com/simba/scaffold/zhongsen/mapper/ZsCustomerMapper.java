package com.simba.scaffold.zhongsen.mapper;

import com.simba.scaffold.zhongsen.domain.entity.ZsCustomer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * 
 *
 * Mapper 接口
 * @author wangming
 * @email ${email}
 * @date 2021-07-24
 */
public interface ZsCustomerMapper extends BaseMapper<ZsCustomer> {


}
