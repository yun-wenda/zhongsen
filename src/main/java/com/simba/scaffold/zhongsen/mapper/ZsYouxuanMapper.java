package com.simba.scaffold.zhongsen.mapper;

import com.simba.scaffold.zhongsen.domain.entity.ZsYouxuan;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 *
 *
 * Mapper 接口
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
public interface ZsYouxuanMapper extends BaseMapper<ZsYouxuan> {

    List<ZsYouxuan> farmTourism();

    List<ZsYouxuan> buyFruitTree();

    List<ZsYouxuan> cultureSurrounding();

    List<ZsYouxuan> buyApple();

    List<ZsYouxuan> selTourismLikeName(String name);

    List<ZsYouxuan> selBuyTreeLikeName(String name);

    List<ZsYouxuan> selSurroundingLikeName(String name);

    List<ZsYouxuan> selBuyAppleLikeName(String name);

}
