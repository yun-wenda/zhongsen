package com.simba.scaffold.zhongsen.mapper;

import com.simba.scaffold.zhongsen.domain.entity.ZsManor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * Mapper 接口
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
public interface ZsManorMapper extends BaseMapper<ZsManor> {

	List<ZsManor> selManorPlan();

	List<ZsManor> advantageShow();

	List<ZsManor> selScenery();

	List<ZsManor> selAmusement();

	List<ZsManor> selPicking();

	List<ZsManor> selPlanLikeName(String name);

	List<ZsManor> selAdvantageLikeName(String name);

	List<ZsManor> selSceneryLikeName(String name);

	List<ZsManor> selAmusementLikeName(String name);

	List<ZsManor> selPickingLikeName(String name);


}
