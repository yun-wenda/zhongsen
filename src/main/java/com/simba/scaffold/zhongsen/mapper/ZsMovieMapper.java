package com.simba.scaffold.zhongsen.mapper;

import com.simba.scaffold.zhongsen.domain.entity.ZsMovie;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 *
 *
 * Mapper 接口
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
public interface ZsMovieMapper extends BaseMapper<ZsMovie> {

    ZsMovie selShowMovie();

    List<ZsMovie> selLikeName(String name);

}
