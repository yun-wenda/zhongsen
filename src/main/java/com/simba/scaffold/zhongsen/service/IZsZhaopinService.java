package com.simba.scaffold.zhongsen.service;

import fykj.microservice.core.base.IBaseService;
import com.simba.scaffold.zhongsen.domain.entity.ZsZhaopin;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * 服务类
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
public interface IZsZhaopinService extends IBaseService<ZsZhaopin> {

    List<ZsZhaopin> talentConcept();

    List<ZsZhaopin> teamElegantDemeanour();

    List<ZsZhaopin> selTalentConceptLikeName(String name);

    List<ZsZhaopin> selFengCaiLikeName(String name);

    Map<String,Object> upPhoto(MultipartFile file,String type);

    int addTalentConcept(ZsZhaopin zsZhaopin);

    int addFengCai(ZsZhaopin zsZhaopin);

}

