package com.simba.scaffold.zhongsen.service;

import fykj.microservice.core.base.IBaseService;
import com.simba.scaffold.zhongsen.domain.entity.ZsFeedback;

import java.util.List;

/**
 *
 *
 * 服务类
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
public interface IZsFeedbackService extends IBaseService<ZsFeedback> {

    List<ZsFeedback> selLikeName(String name);

}

