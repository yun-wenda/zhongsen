package com.simba.scaffold.zhongsen.service;

import fykj.microservice.core.base.IBaseService;
import com.simba.scaffold.zhongsen.domain.entity.ZsAnswer;

/**
 * 
 *
 * 服务类
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
public interface IZsAnswerService extends IBaseService<ZsAnswer> {

}

