package com.simba.scaffold.zhongsen.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.simba.scaffold.zhongsen.domain.dto.DictionaryDto;
import com.simba.scaffold.zhongsen.domain.entity.ZsDictionary;
import com.simba.scaffold.zhongsen.mapper.ZsDictionaryMapper;
import com.simba.scaffold.zhongsen.service.IZsDictionaryService;
import fykj.microservice.core.base.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 *
 *
 * 服务实现类
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ZsDictionaryServiceImpl extends BaseServiceImpl<ZsDictionaryMapper, ZsDictionary> implements IZsDictionaryService {

    @Resource
    private ZsDictionaryMapper zsDictionaryMapper;

    /**
     * 查询首页上的5个统计数据
     * @return
     */
    @Override
    public List<ZsDictionary> getProMessage() {
        List<ZsDictionary> proMessage = zsDictionaryMapper.getProMessage();
        //System.out.println("查询结果："+proMessage);
        return proMessage;
    }

    /**
     * 更新首页轮播图上的标题内容
     * @param title 新标题内容
     * @return
     */
    @Override
    public int upTitle(String title) {
        int i = -1;
        ZsDictionary zsDictionary = zsDictionaryMapper.selTitle();
        if(StringUtils.isEmpty(zsDictionary)){//没有title的时候直接按照当前填入的title进行新增（有备无患）
            ZsDictionary zsDictionary1 = new ZsDictionary();
            zsDictionary1.setSid("title0");
            zsDictionary1.setTitle("title0");
            zsDictionary1.setContent(title);
            i = this.baseMapper.insert(zsDictionary1);
        }else{
            ZsDictionary zsDictionary1 = new ZsDictionary();
            zsDictionary1.setContent(title);
            UpdateWrapper updateWrapper = new UpdateWrapper();
            updateWrapper.eq("s_id","title0");
            i = this.baseMapper.update(zsDictionary1,updateWrapper);
        }
        return i;
    }

    /**
     * 查询联系我们的相关信息
     * @return
     */
    @Override
    public ZsDictionary selTelUs() {
        ZsDictionary zsDictionary = zsDictionaryMapper.selTelUs();
        return zsDictionary;
    }

    /**
     * 意见箱意见提交
     * @param opinion
     * @return
     */
    @Override
    public int subOpinion(String opinion) {
        ZsDictionary zsDictionary = new ZsDictionary();
        zsDictionary.setSid("opinion");
        zsDictionary.setContent(opinion);
        zsDictionary.setTitle("opinion");
        int i = this.baseMapper.insert(zsDictionary);
        return i;
    }

    /**
     * 获取地址定位坐标
     * @return
     */
    @Override
    public ZsDictionary selCoordinates() {
        ZsDictionary zsDictionary = zsDictionaryMapper.selCoordinates();
        return zsDictionary;
    }

    /**
     * 根据所传类型来更新标题和内容
     * @param type
     * @param title
     * @param content
     * @return
     */
    @Override
    public int upByType(String type, String title, String content) {
        UpdateWrapper updateWrapper = new UpdateWrapper();
        updateWrapper.eq("s_id",type);
        ZsDictionary zsDictionary = new ZsDictionary();
        zsDictionary.setTitle(title);
        zsDictionary.setContent(content);
        int update = this.baseMapper.update(zsDictionary, updateWrapper);
        return update;
    }

    /**
     * 查询首页轮播图上的标题文字
     * @return
     */
    @Override
    public ZsDictionary selTitle() {
        ZsDictionary zsDictionary = zsDictionaryMapper.selTitle();
        return zsDictionary;
    }

    /**
     * 更新统计数据
     * @param dictionaryDto
     * @return
     */
    @Override
    public int upMessage(DictionaryDto dictionaryDto) {
        int flag = 0;
        //获取当前数据库里的统计信息
        List<ZsDictionary> proMessage = getProMessage();
        Map<String,String> msg = new HashMap<>();
        for (ZsDictionary dic: proMessage) {
            msg.put(dic.getTitle(),dic.getContent());
        }
        //更新
        //如果原本数据中不包含某条统计数据的时候选择新增入库
        if(!msg.keySet().contains(dictionaryDto.getTitle())){
            ZsDictionary dictionary = new ZsDictionary();
            dictionary.setContent(dictionaryDto.getContent());
            dictionary.setTitle(dictionaryDto.getTitle());
            dictionary.setSid("promsg");
            int i = baseMapper.insert(dictionary);
            flag += i;
        }else {
            //新值和旧值不等时更新
            if (!msg.get(dictionaryDto.getTitle()).equals(dictionaryDto.getContent())) {
                UpdateWrapper updateWrapper = new UpdateWrapper();
                updateWrapper.eq("s_id", "promsg");
                updateWrapper.eq("title", dictionaryDto.getTitle());
                ZsDictionary dictionary = new ZsDictionary();
                dictionary.setContent(dictionaryDto.getContent());
                int i = baseMapper.update(dictionary, updateWrapper);
                flag += i;
            }
        }
        return flag;
    }

    /**
     *查询所有的意见箱意见
     * @return
     */
    @Override
    public List<ZsDictionary> selAllOpinion() {
        List<ZsDictionary> zsDictionaries = baseMapper.selAllOpinion();
        return zsDictionaries;
    }

    /**
     *模糊查询意见箱意见
     * @return
     */
    @Override
    public List<ZsDictionary> selLikeOpinion(String opinion) {
        List<ZsDictionary> zsDictionaries = baseMapper.selLikeOpinion(opinion);
        return zsDictionaries;
    }


    /**
     * 更新联系我们的三条信息
     * @param address 地址
     * @param tel 电话
     * @param email 邮箱
     * @return
     */
    @Override
    public int upTelUs(String address,String tel,String email){
        ZsDictionary zsDictionary1 = baseMapper.selTelUs();
        ZsDictionary zsDictionary = new ZsDictionary();
        int i = -1;
        if(StringUtils.isEmpty(zsDictionary1)){
            zsDictionary.setSid("telus");
            zsDictionary.setTitle("联系我们");
            zsDictionary.setContent(address+";"+tel+";"+email);
            i = baseMapper.insert(zsDictionary);
        }else{
            UpdateWrapper updateWrapper = new UpdateWrapper();
            updateWrapper.eq("s_id","telus");
            zsDictionary1.setContent(address+";"+tel+";"+email);
            i = baseMapper.update(zsDictionary1, updateWrapper);
        }
        return i;
    }

    /**
     * 增加联系我们的选择类型
     * @param zsDictionary
     * @return
     */
    @Override
    public int addTelType(ZsDictionary zsDictionary) {
        zsDictionary.setSid("teltype");
        zsDictionary.setTitle("teltype");
        int i = baseMapper.insert(zsDictionary);
        return i;
    }

    /**
     * 查询联系我们的选项
     * @param type 模糊查询所用选项片段（可为空）
     * @return
     */
    @Override
    public List<ZsDictionary> selTelType(String type) {
        List<ZsDictionary> zsDictionaries = baseMapper.selTelType(type);
        return zsDictionaries;
    }

    @Override
    public int upCoordinate(ZsDictionary zsDictionary) {
        ZsDictionary coordinates = selCoordinates();
        int i = -1;
        if(StringUtils.isEmpty(coordinates)){
            zsDictionary.setSid("coordinate");
            zsDictionary.setTitle("coordinate");
            i = baseMapper.insert(zsDictionary);
        }else{
            UpdateWrapper updateWrapper = new UpdateWrapper();
            updateWrapper.eq("id",coordinates.getId());
            coordinates.setExp1(zsDictionary.getExp1());
            coordinates.setExp2(zsDictionary.getExp2());
            i = baseMapper.update(coordinates, updateWrapper);
        }
        return i;
    }
}
