package com.simba.scaffold.zhongsen.service;

import com.simba.scaffold.zhongsen.domain.params.ZsQuestionParams;
import fykj.microservice.core.base.IBaseService;
import com.simba.scaffold.zhongsen.domain.entity.ZsQuestion;

import java.util.List;

/**
 *
 *
 * 服务类
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
public interface IZsQuestionService extends IBaseService<ZsQuestion> {

    ZsQuestion selQuestion();

    int addQuestion(String question);

    int upQuestion(String questionId,String answer);

    List<ZsQuestion> selLikeQuestion(String question);



}

