package com.simba.scaffold.zhongsen.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.simba.scaffold.zhongsen.domain.entity.ZsQuestion;
import com.simba.scaffold.zhongsen.domain.params.ZsQuestionParams;
import com.simba.scaffold.zhongsen.mapper.ZsQuestionMapper;
import com.simba.scaffold.zhongsen.service.IZsQuestionService;
import fykj.microservice.core.base.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 *
 *
 * 服务实现类
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ZsQuestionServiceImpl extends BaseServiceImpl<ZsQuestionMapper, ZsQuestion> implements IZsQuestionService {

    @Resource
    private ZsQuestionMapper zsQuestionMapper;

    /**
     * 查询客户问题和答复
     * @return
     */
    @Override
    public ZsQuestion selQuestion() {
        ZsQuestion zsQuestions = zsQuestionMapper.selQuestion();
        return zsQuestions;
    }

    /**
     * 新增问题
     * @param question
     * @return
     */
    @Override
    public int addQuestion(String question) {
        ZsQuestion zsQuestion = new ZsQuestion();
        zsQuestion.setQuestion(question);
        zsQuestion.setIsanswer(0);
        long l = System.currentTimeMillis();
        zsQuestion.setQuestionid(String.valueOf(l));
        int i = baseMapper.insert(zsQuestion);
        return i;
    }

    /**
     * 根据问题id解答问题
     * @param questionId 问题id
     * @param answer 答案
     * @return
     */
    @Override
    public int upQuestion(String questionId, String answer) {
        UpdateWrapper updateWrapper = new UpdateWrapper();
        updateWrapper.eq("questionid",questionId);
        ZsQuestion zsQuestion = new ZsQuestion();
        zsQuestion.setAnswer(answer);
        zsQuestion.setIsanswer(1);
        int i = baseMapper.update(zsQuestion, updateWrapper);
        return i;
    }

    /**
     * 根据问题部分内容进行模糊查询
     * @param question
     * @return
     */
    @Override
    public List<ZsQuestion> selLikeQuestion(String question) {
        List<ZsQuestion> zsQuestions = baseMapper.selLikeQuestion(question);
        return zsQuestions;
    }
}
