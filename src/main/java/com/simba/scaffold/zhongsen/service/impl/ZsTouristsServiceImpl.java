package com.simba.scaffold.zhongsen.service.impl;

import com.simba.scaffold.zhongsen.domain.entity.ZsTourists;
import com.simba.scaffold.zhongsen.mapper.ZsTouristsMapper;
import com.simba.scaffold.zhongsen.service.IZsTouristsService;
import fykj.microservice.core.base.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


/**
 *
 *
 * 服务实现类
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ZsTouristsServiceImpl extends BaseServiceImpl<ZsTouristsMapper, ZsTourists> implements IZsTouristsService {

    @Resource
    private ZsPhotoServiceImpl zsPhotoService;


    /**
     * 查询餐饮住宿类信息
     * @return
     */
    @Override
    public List<ZsTourists> selHotel() {
        List<ZsTourists> zsTourists = baseMapper.selHotel();
        return zsTourists;
    }

    /**
     * 查询会议拓展类信息
     * @return
     */
    @Override
    public List<ZsTourists> meetingExpand() {
        List<ZsTourists> zsTourists = baseMapper.meetingExpand();
        return zsTourists;
    }

    /**
     * 查询学研基地类信息
     * @return
     */
    @Override
    public List<ZsTourists> studyBase() {
        List<ZsTourists> zsTourists = baseMapper.studyBase();
        return zsTourists;
    }

    /**
     * 查询乡村特产类信息
     * @return
     */
    @Override
    public List<ZsTourists> ruralSpecially() {
        List<ZsTourists> zsTourists = baseMapper.ruralSpecially();
        return zsTourists;
    }

    /**
     * 查询相腮苹果类信息
     * @return
     */
    @Override
    public List<ZsTourists> selApple() {
        List<ZsTourists> zsTourists = baseMapper.selApple();
        //System.out.println("tour:"+zsTourists);
        return zsTourists;
    }

    /**
     * 模糊查询餐饮住宿相关文章
     * @param name
     * @return
     */
    @Override
    public List<ZsTourists> selHotelLikeName(String name) {
        List<ZsTourists> zsTourists = baseMapper.selHotelLikeName(name);
        return zsTourists;
    }

    /**
     * 模糊查询会议拓展相关文章
     * @param name
     * @return
     */
    @Override
    public List<ZsTourists> selMeetingLikeName(String name) {
        List<ZsTourists> zsTourists = baseMapper.selMeetingLikeName(name);
        return zsTourists;
    }

    /**
     * 模糊查询学研基地相关文章
     * @param name
     * @return
     */
    @Override
    public List<ZsTourists> selBaselLikeName(String name) {
        List<ZsTourists> zsTourists = baseMapper.selBaselLikeName(name);
        return zsTourists;
    }

    /**
     * 模糊查询乡土农特相关文章
     * @param name
     * @return
     */
    @Override
    public List<ZsTourists> selSpeciallyLikeName(String name) {
        List<ZsTourists> zsTourists = baseMapper.selSpeciallyLikeName(name);
        return zsTourists;
    }

    /**
     * 模糊查询香腮苹果相关文章
     * @param name
     * @return
     */
    @Override
    public List<ZsTourists> selAppleLikeName(String name) {
        List<ZsTourists> zsTourists = baseMapper.selAppleLikeName(name);
        return zsTourists;
    }

    /**
     * 图片信息上传并保存
     * @param file
     * @param type
     * @return
     */
    @Override
    public Map<String, Object> upPhoto(MultipartFile file, String type) {
        Map<String, Object> map = zsPhotoService.upPhoto(file, type);
        return map;
    }

    /**
     * 添加餐饮住宿相关文章
     * @param zsTourists
     * @return
     */
    @Override
    public int addHotelInfo(ZsTourists zsTourists) {
        zsTourists.setType(1);
        int i = baseMapper.insert(zsTourists);
        return i;
    }

    /**
     * 添加会议拓展相关文章
     * @param zsTourists
     * @return
     */
    @Override
    public int addMeetingInfo(ZsTourists zsTourists) {
        zsTourists.setType(2);
        int i = baseMapper.insert(zsTourists);
        return i;
    }

    /**
     * 添加学研基地相关文章
     * @param zsTourists
     * @return
     */
    @Override
    public int addBaseInfo(ZsTourists zsTourists) {
        zsTourists.setType(3);
        int i = baseMapper.insert(zsTourists);
        return i;
    }

    /**
     * 添加乡土农特相关文章
     * @param zsTourists
     * @return
     */
    @Override
    public int addSpeciallyInfo(ZsTourists zsTourists) {
        zsTourists.setType(4);
        int i = baseMapper.insert(zsTourists);
        return i;
    }

    /**
     * 添加香腮苹果相关文章
     * @param zsTourists
     * @return
     */
    @Override
    public int addAppleInfo(ZsTourists zsTourists) {
        zsTourists.setType(5);
        int i = baseMapper.insert(zsTourists);
        return i;
    }
}
