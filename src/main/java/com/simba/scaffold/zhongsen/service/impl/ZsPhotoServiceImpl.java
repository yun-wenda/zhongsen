package com.simba.scaffold.zhongsen.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.simba.scaffold.zhongsen.domain.entity.ZsAnswer;
import com.simba.scaffold.zhongsen.domain.entity.ZsPhoto;
import com.simba.scaffold.zhongsen.mapper.ZsPhotoMapper;
import com.simba.scaffold.zhongsen.service.IZsPhotoService;
import com.simba.scaffold.zhongsen.utils.FileUtils;
import fykj.microservice.core.base.BaseServiceImpl;
import org.apache.ibatis.transaction.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ClassUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


/**
 * 服务实现类
 *
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ZsPhotoServiceImpl extends BaseServiceImpl<ZsPhotoMapper, ZsPhoto> implements IZsPhotoService {

    @Resource
    private ZsPhotoMapper zsPhotoMapper;

    @Value("${localUrl}")
    private String url;

    @Value("${file.path.images}")
    private String imageFilePath;

    /**
     * @param file 图片路径
     * @return result
     */
    @Override
    public Map<String, Object> upPhoto(MultipartFile file, String type) {
        //一个文件上传的结果
        Map<String, Object> result = new HashMap<>();
        ZsPhoto photo = new ZsPhoto();
        //判断上传文件格式
        String fileType = file.getContentType();
        if (fileType.equals("image/jpeg") || fileType.equals("image/png") || fileType.equals("image/jpg")) {
            // 要上传的目标文件存放的相对路径
            // final String localPath = path.toString().replace(System.getProperty("user.dir"), url);
            //上传后保存的文件名(需要防止图片重名导致的文件覆盖)
            //获取文件名
            String fileName = file.getOriginalFilename();
            //获取文件后缀名
//            String suffixName = fileName.substring(fileName.lastIndexOf("."));
            //重新生成文件名
//            fileName = UUID.randomUUID()+suffixName;
            String address = "";
            try {
                address = Inet4Address.getLocalHost().getHostAddress();
            } catch (UnknownHostException e) {
                System.out.println("获取本地ip失败");
                e.printStackTrace();
            }

            if ("lunbotu".equals(type)) {
                photo.setShowFlag(1);
            } else {
                photo.setShowFlag(0);
            }
            photo.setSid(type);
            photo.setType(fileType.substring(6, fileType.length()));
            photo.setAddress(imageFilePath);
            photo.setName(fileName);
            photo.setShowurl(url.concat("/image"));
            int res = savePhoto(photo);
            if (res == 1) {
                if (FileUtils.upload(file, imageFilePath, fileName)) {
//                    if (FileUtils.upload(file, localPath, fileName)) {
                    //文件存放的相对路径(一般存放在数据库用于image标签的src)
                    String relativePath = "image/" + fileName;
                    result.put("relativePath", url + "/image/" + fileName);
                    result.put("savePath", imageFilePath);
                    result.put("fileName", fileName);
                    //result=fileName;
                } else {
                    result.put("result", "数据保存成功");
                }
            } else {
                result.put("result", "数据保存失败");
            }
        } else {
            result.put("result", "图片格式不正确,目前支持上传图片格式为jpg，jpeg，png。");
        }

        return result;
    }

    /**
     * 根据图片名称变更图片的作用
     *
     * @param photoName
     * @param type
     * @return
     */
    @Override
    public int upPhotoType(String photoName, String type) {
        UpdateWrapper<ZsPhoto> updateWrapper = new UpdateWrapper<ZsPhoto>();
        updateWrapper.eq("name", photoName);
        ZsPhoto photo = new ZsPhoto();
        photo.setType(type);
        int i = baseMapper.update(photo, updateWrapper);
        return i;
    }

    /**
     * 轮播图展示查询
     *
     * @return
     */
    @Override
    public List<ZsPhoto> selShowLbt() {
        List<ZsPhoto> zsPhotos = zsPhotoMapper.selShowLbt();
        return zsPhotos;
    }

    /**
     * 所有轮播图图片查询
     *
     * @return
     */
    @Override
    public List<ZsPhoto> selAllLbt() {
        List<ZsPhoto> zsPhotos = zsPhotoMapper.selAllLbt();
        return zsPhotos;
    }

    /**
     * 查看当前类型前台展示的图片数量
     *
     * @param type
     * @return
     */
    @Override
    public int showPhotoCount(String type) {
        int i = zsPhotoMapper.selCountByType(type);
        return i;
    }

    /**
     * 存储图片信息到数据库
     *
     * @param photo
     * @return
     */
    public int savePhoto(ZsPhoto photo) {
        int insert = this.baseMapper.insert(photo);
        return insert;
    }


    /**
     * 通过名称模糊查找图片信息
     *
     * @param name
     * @return
     */
    @Override
    public List<ZsPhoto> selByName(String name) {
        List<ZsPhoto> zsPhoto = zsPhotoMapper.selByName(name);
        return zsPhoto;
    }

    public static void main(String[] args) {

    }

}
