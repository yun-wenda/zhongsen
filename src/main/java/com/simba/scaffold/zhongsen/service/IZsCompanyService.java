package com.simba.scaffold.zhongsen.service;

import com.simba.scaffold.zhongsen.domain.entity.ZsProject;
import fykj.microservice.core.base.IBaseService;
import com.simba.scaffold.zhongsen.domain.entity.ZsCompany;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * 服务类
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
public interface IZsCompanyService extends IBaseService<ZsCompany> {

    List<ZsProject> aboutZs();

    List<ZsCompany> selVision();

    List<ZsCompany> selHistory();

    List<ZsCompany> selHonor();

    List<ZsCompany> selNews();

    List<ZsProject> selIntroduceLikeTitle(String title);

    List<ZsCompany> selVisionLikeName(String name);

    List<ZsCompany> selHistoryLikeName(String name);

    List<ZsCompany> selHonorLikeName(String name);

    List<ZsCompany> selNewsLikeName(String name);

    Map<String,Object> upPhoto(MultipartFile file, String type);

    int addCompanyVision(ZsCompany zsCompany);

    int addHistory(ZsCompany zsCompany);

    int addHonorInfo(ZsCompany zsCompany);


}

