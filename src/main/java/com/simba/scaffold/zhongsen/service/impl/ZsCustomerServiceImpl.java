package com.simba.scaffold.zhongsen.service.impl;

import com.simba.scaffold.zhongsen.domain.entity.ZsCustomer;
import com.simba.scaffold.zhongsen.mapper.ZsCustomerMapper;
import com.simba.scaffold.zhongsen.service.IZsCustomerService;
import fykj.microservice.core.base.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * 
 *
 * 服务实现类
 * @author wangming
 * @email ${email}
 * @date 2021-07-24
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ZsCustomerServiceImpl extends BaseServiceImpl<ZsCustomerMapper, ZsCustomer> implements IZsCustomerService {

    @Resource
    private ZsCustomerMapper zsCustomerMapper;

    @Override
    public int addCustomer(ZsCustomer customer) {
            int insert = this.baseMapper.insert(customer);
        return insert;
    }
}

