package com.simba.scaffold.zhongsen.service;

import fykj.microservice.core.base.IBaseService;
import com.simba.scaffold.zhongsen.domain.entity.ZsYouxuan;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * 服务类
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
public interface IZsYouxuanService extends IBaseService<ZsYouxuan> {

    List<ZsYouxuan> farmTourism();

    List<ZsYouxuan> buyFruitTree();

    List<ZsYouxuan> cultureSurrounding();

    List<ZsYouxuan> buyApple();

    Map<String,Object> upPhoto(MultipartFile file,String type);

    List<ZsYouxuan> selTourismLikeName(String name);

    List<ZsYouxuan> selBuyTreeLikeName(String name);

    List<ZsYouxuan> selSurroundingLikeName(String name);

    List<ZsYouxuan> selBuyAppleLikeName(String name);

    int addTourism(ZsYouxuan zsYouxuan);

    int addBuyTree(ZsYouxuan zsYouxuan);

    int addSurrounding(ZsYouxuan zsYouxuan);

    int addBuyApple(ZsYouxuan zsYouxuan);

}

