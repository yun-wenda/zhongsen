package com.simba.scaffold.zhongsen.service.impl;

import com.simba.scaffold.zhongsen.domain.entity.ZsAnswer;
import com.simba.scaffold.zhongsen.mapper.ZsAnswerMapper;
import com.simba.scaffold.zhongsen.service.IZsAnswerService;
import fykj.microservice.core.base.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * 
 *
 * 服务实现类
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ZsAnswerServiceImpl extends BaseServiceImpl<ZsAnswerMapper, ZsAnswer> implements IZsAnswerService {


}