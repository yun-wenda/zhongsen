package com.simba.scaffold.zhongsen.service;

import com.simba.scaffold.zhongsen.domain.dto.DictionaryDto;
import com.simba.scaffold.zhongsen.domain.entity.ZsQuestion;
import fykj.microservice.core.base.IBaseService;
import com.simba.scaffold.zhongsen.domain.entity.ZsDictionary;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * 服务类
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
public interface IZsDictionaryService extends IBaseService<ZsDictionary> {

    List<ZsDictionary> getProMessage();

    int upTitle(String title);

    ZsDictionary selTelUs();

    int subOpinion(String opinion);

    ZsDictionary selCoordinates();

    int upByType(String type,String title,String content);

    ZsDictionary selTitle();

    int upMessage(DictionaryDto dictionaryDto);

    List<ZsDictionary> selAllOpinion();

    List<ZsDictionary> selLikeOpinion(String opinion);

    int upTelUs(String address,String tel,String email);

    int addTelType(ZsDictionary zsDictionary);

    List<ZsDictionary> selTelType(String type);

    int upCoordinate(ZsDictionary zsDictionary);
}

