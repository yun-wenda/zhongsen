package com.simba.scaffold.zhongsen.service;

import fykj.microservice.core.base.IBaseService;
import com.simba.scaffold.zhongsen.domain.entity.ZsPhoto;
import org.springframework.web.multipart.MultipartFile;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * 服务类
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
public interface IZsPhotoService extends IBaseService<ZsPhoto> {

//    String upPhotoToLbt(MultipartFile file,String type);

    int showPhotoCount(String type);

    List<ZsPhoto> selByName(String name);

    Map<String,Object> upPhoto(MultipartFile file,String type) throws UnknownHostException;

    int upPhotoType(String photoName,String type);

    List<ZsPhoto> selShowLbt();

    List<ZsPhoto> selAllLbt();

}

