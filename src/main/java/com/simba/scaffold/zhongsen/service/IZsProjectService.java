package com.simba.scaffold.zhongsen.service;

import com.simba.scaffold.zhongsen.domain.entity.ZsDictionary;
import fykj.microservice.core.base.IBaseService;
import com.simba.scaffold.zhongsen.domain.entity.ZsProject;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * 服务类
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
public interface IZsProjectService extends IBaseService<ZsProject> {

    int upProToZhuTui(String title);

    int upProToTanSuo(String title);

    int selCountByType(int type);

    int upToShow(String name,int type);

    int upUnShow(String name,int type);

    int upToZC(String name,int type);

    int saveProject(ZsProject project);

    List<ZsProject> selZhuTui();

    List<ZsProject> selTanSuo();

    List<ZsProject> selhuoDongZiXun();

    List<ZsProject> selLikeTitle(String title);

    int addActionInfo(ZsProject zsProject);

    Map<String,Object> upPhoto(MultipartFile file, String type);

    int addIntroduceInfo(ZsProject zsProject);

    List<ZsProject> selActionLikeName(String name);

    int addProjectIntr(ZsProject zsProject);

    List<ZsProject> selProjectIntr();

    List<ZsProject> sellLikeProIntr(String name);

    List<ZsProject> selById(String id);

}

