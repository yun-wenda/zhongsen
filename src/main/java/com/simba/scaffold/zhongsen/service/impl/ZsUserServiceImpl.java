package com.simba.scaffold.zhongsen.service.impl;

import com.simba.scaffold.zhongsen.domain.entity.ZsUser;
import com.simba.scaffold.zhongsen.mapper.ZsUserMapper;
import com.simba.scaffold.zhongsen.service.IZsUserService;
import fykj.microservice.core.base.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 *
 *
 * 服务实现类
 * @author wangming
 * @email ${email}
 * @date 2021-07-27
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ZsUserServiceImpl extends BaseServiceImpl<ZsUserMapper, ZsUser> implements IZsUserService {


    @Override
    public List<ZsUser> selByName(String name) {
        List<ZsUser> zsUsers = baseMapper.selByName(name);
        return zsUsers;
    }
}
