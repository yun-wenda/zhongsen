package com.simba.scaffold.zhongsen.service.impl;

import com.simba.scaffold.zhongsen.domain.entity.ZsFeedback;
import com.simba.scaffold.zhongsen.mapper.ZsFeedbackMapper;
import com.simba.scaffold.zhongsen.service.IZsFeedbackService;
import fykj.microservice.core.base.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 *
 *
 * 服务实现类
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ZsFeedbackServiceImpl extends BaseServiceImpl<ZsFeedbackMapper, ZsFeedback> implements IZsFeedbackService {


    /**
     * 根据客户名称模糊查询反馈信息
     * @param name
     * @return
     */
    @Override
    public List<ZsFeedback> selLikeName(String name) {
        List<ZsFeedback> zsFeedbacks = baseMapper.selLikeName(name);
        return zsFeedbacks;
    }
}
