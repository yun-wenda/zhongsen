package com.simba.scaffold.zhongsen.service;

import fykj.microservice.core.base.IBaseService;
import com.simba.scaffold.zhongsen.domain.entity.ZsMovie;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * 服务类
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
public interface IZsMovieService extends IBaseService<ZsMovie> {

    Map<String,Object> upMovie(MultipartFile file);

    ZsMovie selShowMovie();

    int unShowMovie(String name);

    int showMovie(String name);

    List<ZsMovie> selLikeName(String name);

}

