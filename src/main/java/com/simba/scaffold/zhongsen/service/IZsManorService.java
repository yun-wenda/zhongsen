package com.simba.scaffold.zhongsen.service;

import fykj.microservice.core.base.IBaseService;
import com.simba.scaffold.zhongsen.domain.entity.ZsManor;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * 服务类
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
public interface IZsManorService extends IBaseService<ZsManor> {

    List<ZsManor> manorPlan();

    List<ZsManor> advantageShow();

    List<ZsManor> selScenery();

    List<ZsManor> selAmusement();

    List<ZsManor> selPicking();

    List<ZsManor> selPlanLikeName(String name);

    List<ZsManor> selAdvantageLikeName(String name);

    List<ZsManor> selSceneryLikeName(String name);

    List<ZsManor> selAmusementLikeName(String name);

    List<ZsManor> selPickingLikeName(String name);

    Map<String,Object> upPhoto(MultipartFile file,String type);

    int addPlanInfo(ZsManor zsManor);

    int addAdvantageInfo(ZsManor zsManor);

    int addSceneryInfo(ZsManor zsManor);

    int addAmusementInfo(ZsManor zsManor);

    int addPickingInfo(ZsManor zsManor);

}

