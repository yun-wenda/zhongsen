package com.simba.scaffold.zhongsen.service.impl;

import com.simba.scaffold.zhongsen.domain.entity.ZsYouxuan;
import com.simba.scaffold.zhongsen.mapper.ZsYouxuanMapper;
import com.simba.scaffold.zhongsen.service.IZsYouxuanService;
import fykj.microservice.core.base.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


/**
 *
 *
 * 服务实现类
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ZsYouxuanServiceImpl extends BaseServiceImpl<ZsYouxuanMapper, ZsYouxuan> implements IZsYouxuanService {

    @Resource
    private ZsYouxuanMapper zsYouxuanMapper;
    @Resource
    private ZsPhotoServiceImpl zsPhotoService;

    /**
     * 查询庄园旅游类文章
     * @return
     */
    @Override
    public List<ZsYouxuan> farmTourism() {
        List<ZsYouxuan> zsYouxuans = zsYouxuanMapper.farmTourism();
        return zsYouxuans;
    }

    /**
     * 查询认购果树类文章
     * @return
     */
    @Override
    public List<ZsYouxuan> buyFruitTree() {
        List<ZsYouxuan> zsYouxuans = zsYouxuanMapper.buyFruitTree();
        return zsYouxuans;
    }

    /**
     * 查询文创周边类文章
     * @return
     */
    @Override
    public List<ZsYouxuan> cultureSurrounding() {
        List<ZsYouxuan> zsYouxuans = zsYouxuanMapper.cultureSurrounding();
        return zsYouxuans;
    }

    /**
     * 查询苹果订购类文章
     * @return
     */
    @Override
    public List<ZsYouxuan> buyApple() {
        List<ZsYouxuan> zsYouxuans = zsYouxuanMapper.buyApple();
        return zsYouxuans;
    }

    /**
     * 保存图片信息
     * @param file
     * @param type
     * @return
     */
    @Override
    public Map<String, Object> upPhoto(MultipartFile file, String type) {
        Map<String, Object> map = zsPhotoService.upPhoto(file, type);
        return map;
    }

    /**
     * 模糊查询庄园旅游
     * @param name
     * @return
     */
    @Override
    public List<ZsYouxuan> selTourismLikeName(String name) {
        List<ZsYouxuan> zsYouxuans = baseMapper.selTourismLikeName(name);
        return zsYouxuans;
    }

    /**
     * 模糊查询订购果树
     * @param name
     * @return
     */
    @Override
    public List<ZsYouxuan> selBuyTreeLikeName(String name) {
        List<ZsYouxuan> zsYouxuans = baseMapper.selBuyTreeLikeName(name);
        return zsYouxuans;
    }

    /**
     * 模糊查询文创周边
     * @param name
     * @return
     */
    @Override
    public List<ZsYouxuan> selSurroundingLikeName(String name) {
        List<ZsYouxuan> zsYouxuans = baseMapper.selSurroundingLikeName(name);
        return zsYouxuans;
    }

    /**
     * 模糊查询苹果订购
     * @param name
     * @return
     */
    @Override
    public List<ZsYouxuan> selBuyAppleLikeName(String name) {
        List<ZsYouxuan> zsYouxuans = baseMapper.selBuyAppleLikeName(name);
        return zsYouxuans;
    }

    /**
     * 新增庄园旅游相关文章
     * @param zsYouxuan
     * @return
     */
    @Override
    public int addTourism(ZsYouxuan zsYouxuan) {
        zsYouxuan.setType(1);
        int i = baseMapper.insert(zsYouxuan);
        return i;
    }

    /**
     * 新增认购果树相关文章
     * @param zsYouxuan
     * @return
     */
    @Override
    public int addBuyTree(ZsYouxuan zsYouxuan) {
        zsYouxuan.setType(2);
        int i = baseMapper.insert(zsYouxuan);
        return i;
    }

    /**
     * 新增文创周边相关文章
     * @param zsYouxuan
     * @return
     */
    @Override
    public int addSurrounding(ZsYouxuan zsYouxuan) {
        zsYouxuan.setType(3);
        int i = baseMapper.insert(zsYouxuan);
        return i;
    }

    /**
     * 新增苹果订购相关文章
     * @param zsYouxuan
     * @return
     */
    @Override
    public int addBuyApple(ZsYouxuan zsYouxuan) {
        zsYouxuan.setType(4);
        int i = baseMapper.insert(zsYouxuan);
        return i;
    }


}
