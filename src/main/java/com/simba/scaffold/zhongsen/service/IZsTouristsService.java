package com.simba.scaffold.zhongsen.service;

import com.simba.scaffold.zhongsen.domain.entity.ZsProject;
import fykj.microservice.core.base.IBaseService;
import com.simba.scaffold.zhongsen.domain.entity.ZsTourists;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * 服务类
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
public interface IZsTouristsService extends IBaseService<ZsTourists> {

    List<ZsTourists>  selHotel();

    List<ZsTourists> meetingExpand();

    List<ZsTourists> studyBase();

    List<ZsTourists> ruralSpecially();

    List<ZsTourists> selApple();

    List<ZsTourists> selHotelLikeName(String name);

    List<ZsTourists> selMeetingLikeName(String name);

    List<ZsTourists> selBaselLikeName(String name);

    List<ZsTourists> selSpeciallyLikeName(String name);

    List<ZsTourists> selAppleLikeName(String name);

    Map<String,Object> upPhoto(MultipartFile file,String type);

    int addHotelInfo(ZsTourists zsTourists);

    int addMeetingInfo(ZsTourists zsTourists);

    int addBaseInfo(ZsTourists zsTourists);

    int addSpeciallyInfo(ZsTourists zsTourists);

    int addAppleInfo(ZsTourists zsTourists);


}

