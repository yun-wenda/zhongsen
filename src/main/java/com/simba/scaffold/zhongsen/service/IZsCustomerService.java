package com.simba.scaffold.zhongsen.service;

import fykj.microservice.core.base.IBaseService;
import com.simba.scaffold.zhongsen.domain.entity.ZsCustomer;

/**
 * 
 *
 * 服务类
 * @author wangming
 * @email ${email}
 * @date 2021-07-24
 */
public interface IZsCustomerService extends IBaseService<ZsCustomer> {

    int addCustomer(ZsCustomer customer);


}

