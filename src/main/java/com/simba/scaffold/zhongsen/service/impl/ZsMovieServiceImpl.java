package com.simba.scaffold.zhongsen.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.simba.scaffold.zhongsen.domain.entity.ZsMovie;
import com.simba.scaffold.zhongsen.mapper.ZsMovieMapper;
import com.simba.scaffold.zhongsen.service.IZsMovieService;
import com.simba.scaffold.zhongsen.utils.FileUtils;
import fykj.microservice.core.base.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


/**
 * 服务实现类
 *
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ZsMovieServiceImpl extends BaseServiceImpl<ZsMovieMapper, ZsMovie> implements IZsMovieService {

    @Resource
    private ZsMovieMapper zsMovieMapper;

    @Value("${file.path.videos}")
    private String videoFilePath;

    @Value("${localUrl}")
    private String url;

    /**
     * 上传视频文件到系统中的movie文件夹下
     *
     * @param file
     * @return
     */
    @Override
    public Map<String, Object> upMovie(MultipartFile file) {
        Map<String, Object> result = new HashMap<>();
        String result_msg = "";
        //判断上传文件格式
        String fileType = file.getContentType();
        if (fileType.equals("video/mp4") || fileType.equals("video/wmv") || fileType.equals("video/wmv")) {
            // 要上传的目标文件存放的绝对路径
            String localPath = videoFilePath;
            //上传后保存的文件名(需要防止图片重名导致的文件覆盖)
            //获取文件名
            String fileName = file.getOriginalFilename();
            //获取文件后缀名
//            String suffixName = fileName.substring(fileName.lastIndexOf("."));
            //重新生成文件名
//            fileName = UUID.randomUUID()+suffixName;
            String address = "";
            try {
                address = Inet4Address.getLocalHost().getHostAddress();
            } catch (UnknownHostException e) {
                System.out.println("获取本地ip失败");
                e.printStackTrace();
            }
            if (FileUtils.upload(file, localPath, fileName)) {
                //文件存放的相对路径(一般存放在数据库用于img标签的src)
//                String relativePath="movie/"+fileName;
//                result.put("relativePath",relativePath);//前端根据是否存在该字段来判断上传是否成功
                ZsMovie zsMovie = zsMovieMapper.selShowMovie();

                if (!StringUtils.isEmpty(zsMovie)) {
                    UpdateWrapper updateWrapper = new UpdateWrapper();
                    updateWrapper.eq("name", zsMovie.getName());
                    zsMovie.setShowFlag(0);
                    this.baseMapper.update(zsMovie, updateWrapper);
                }
                ZsMovie movie = new ZsMovie();
                movie.setSid("public");
                movie.setType(fileType.substring(6, fileType.length()));
                movie.setAddress(localPath);
                movie.setName(fileName);
                movie.setShowFlag(1);
                movie.setShowurl(url.concat("/movie"));
                int i = baseMapper.insert(movie);
                if (i == 1) {
                    result_msg = "视频上传成功";
                    result.put("relativePath", url + "/movie/" + fileName);
                    result.put("savePath", localPath);
                    result.put("fileName", fileName);
                } else {
                    result_msg = "数据保存失败";
                }
            } else {
                result_msg = "视频上传失败";
            }
        } else {
            result_msg = "抱歉，目前仅支持mp4，avi，wmv格式的视频文件上传";
        }
        result.put("result_msg", result_msg);

        return result;
    }

    /**
     * 查询当前正在展示的视频信息
     *
     * @return
     */
    @Override
    public ZsMovie selShowMovie() {
        ZsMovie zsMovie = zsMovieMapper.selShowMovie();
        return zsMovie;
    }

    /**
     * 设置视频在主页面不可见
     *
     * @param name 视频名称
     * @return
     */
    @Override
    public int unShowMovie(String name) {
        UpdateWrapper updateWrapper = new UpdateWrapper();
        updateWrapper.eq("name", name);
        ZsMovie zsMovie = new ZsMovie();
        zsMovie.setShowFlag(0);
        int i = baseMapper.update(zsMovie, updateWrapper);
        return i;
    }

    /**
     * 设置视频在主页可见
     *
     * @param name
     * @return
     */
    @Override
    public int showMovie(String name) {
        UpdateWrapper updateWrapper = new UpdateWrapper();
        updateWrapper.eq("name", name);
        ZsMovie zsMovie = new ZsMovie();
        zsMovie.setShowFlag(1);
        int i = baseMapper.update(zsMovie, updateWrapper);
        return i;
    }

    /**
     * 根据名称模糊查询视频信息
     *
     * @param name
     * @return
     */
    @Override
    public List<ZsMovie> selLikeName(String name) {
        List<ZsMovie> zsMovies = baseMapper.selLikeName(name);
        return zsMovies;
    }

}
