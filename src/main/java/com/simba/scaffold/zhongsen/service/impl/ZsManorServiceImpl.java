package com.simba.scaffold.zhongsen.service.impl;

import com.simba.scaffold.zhongsen.domain.entity.ZsManor;
import com.simba.scaffold.zhongsen.mapper.ZsManorMapper;
import com.simba.scaffold.zhongsen.service.IZsManorService;
import fykj.microservice.core.base.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


/**
 *
 *
 * 服务实现类
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ZsManorServiceImpl extends BaseServiceImpl<ZsManorMapper, ZsManor> implements IZsManorService {

    @Resource
    private ZsManorMapper zsManorMapper;
    @Resource
    private ZsPhotoServiceImpl zsPhotoService;

    /**
     * 查询庄园规划相关信息
     * @return
     */
    @Override
    public List<ZsManor> manorPlan() {
        List<ZsManor> zsManor = zsManorMapper.selManorPlan();
        return zsManor;
    }

    /**
     * 查询优势展示相关信息
     * @return
     */
    @Override
    public List<ZsManor> advantageShow() {
        List<ZsManor> zsManor = zsManorMapper.advantageShow();
        return zsManor;
    }

    /**
     * 查询风光实景相关信息
     * @return
     */
    @Override
    public List<ZsManor> selScenery() {
        List<ZsManor> zsManor = zsManorMapper.selScenery();
        return zsManor;
    }

    /**
     * 查询游乐体验相关信息
     * @return
     */
    @Override
    public List<ZsManor> selAmusement() {
        List<ZsManor> zsManor = zsManorMapper.selAmusement();
        return zsManor;
    }

    /**
     * 查询果蔬采摘相关信息
     * @return
     */
    @Override
    public List<ZsManor> selPicking() {
        List<ZsManor> zsManor = zsManorMapper.selPicking();
        return zsManor;
    }

    /**
     * 根据部分标题内容模糊查询庄园规划类文章
     * @param name 部分标题内容
     * @return
     */
    @Override
    public List<ZsManor> selPlanLikeName(String name) {
        List<ZsManor> zsManors = baseMapper.selPlanLikeName(name);
        return zsManors;
    }

    /**
     * 根据部分标题内容模糊查询庄园规划类文章
     * @param name 部分标题内容
     * @return
     */
    @Override
    public List<ZsManor> selAdvantageLikeName(String name) {
        List<ZsManor> zsManors = baseMapper.selAdvantageLikeName(name);
        return zsManors;
    }

    /**
     * 根据部分标题内容模糊查询庄园规划类文章
     * @param name 部分标题内容
     * @return
     */
    @Override
    public List<ZsManor> selSceneryLikeName(String name) {
        List<ZsManor> zsManors = baseMapper.selSceneryLikeName(name);
        return zsManors;
    }

    /**
     * 根据部分标题内容模糊查询庄园规划类文章
     * @param name 部分标题内容
     * @return
     */
    @Override
    public List<ZsManor> selAmusementLikeName(String name) {
        List<ZsManor> zsManors = baseMapper.selAmusementLikeName(name);
        return zsManors;
    }

    /**
     * 根据部分标题内容模糊查询庄园规划类文章
     * @param name 部分标题内容
     * @return
     */
    @Override
    public List<ZsManor> selPickingLikeName(String name) {
        List<ZsManor> zsManors = baseMapper.selPickingLikeName(name);
        return zsManors;
    }

    /**
     * 上传图片
     * @param file 图片路径
     * @param type  图片类型
     * @return
     */
    @Override
    public Map<String, Object> upPhoto(MultipartFile file, String type) {
        Map<String, Object> map = zsPhotoService.upPhoto(file, type);
        return map;
    }

    /**
     * 新增庄园规划类文章
     * @param zsManor
     * @return
     */
    @Override
    public int addPlanInfo(ZsManor zsManor) {
        zsManor.setType(1);
        int i = baseMapper.insert(zsManor);
        return i;
    }

    /**
     * 新增优势展示类文章
     * @param zsManor
     * @return
     */
    @Override
    public int addAdvantageInfo(ZsManor zsManor) {
        zsManor.setType(2);
        int i = baseMapper.insert(zsManor);
        return i;
    }

    /**
     * 新增风光实景类文章
     * @param zsManor
     * @return
     */
    @Override
    public int addSceneryInfo(ZsManor zsManor) {
        zsManor.setType(3);
        int i = baseMapper.insert(zsManor);
        return i;
    }

    /**
     * 新增游乐体验类文章
     * @param zsManor
     * @return
     */
    @Override
    public int addAmusementInfo(ZsManor zsManor) {
        zsManor.setType(4);
        int i = baseMapper.insert(zsManor);
        return i;
    }

    /**
     * 新增果蔬采摘类文章
     * @param zsManor
     * @return
     */
    @Override
    public int addPickingInfo(ZsManor zsManor) {
        zsManor.setType(5);
        int i = baseMapper.insert(zsManor);
        return i;
    }
}
