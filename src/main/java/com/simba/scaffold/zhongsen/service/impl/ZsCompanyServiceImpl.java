package com.simba.scaffold.zhongsen.service.impl;

import com.simba.scaffold.zhongsen.domain.entity.ZsCompany;
import com.simba.scaffold.zhongsen.domain.entity.ZsProject;
import com.simba.scaffold.zhongsen.mapper.ZsCompanyMapper;
import com.simba.scaffold.zhongsen.service.IZsCompanyService;
import fykj.microservice.core.base.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


/**
 *
 *
 * 服务实现类
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ZsCompanyServiceImpl extends BaseServiceImpl<ZsCompanyMapper, ZsCompany> implements IZsCompanyService {

    @Resource
    private ZsCompanyMapper zsCompanyMapper;
    @Resource
    private ZsPhotoServiceImpl zsPhotoService;

    /**
     * 查询关于众森的信息
     * @return
     */
    @Override
    public List<ZsProject> aboutZs() {
        List<ZsProject> zsProject = zsCompanyMapper.aboutZs();
        return zsProject;
    }

    /**
     * 查询企业愿景信息
     * @return
     */
    @Override
    public List<ZsCompany> selVision() {
        List<ZsCompany> zsCompany = zsCompanyMapper.selVision();
        return zsCompany;
    }

    /**
     * 查询发展历程相关信息
     * @return
     */
    @Override
    public List<ZsCompany> selHistory() {
        List<ZsCompany> zsCompany = zsCompanyMapper.selHistory();
        return zsCompany;
    }

    /**
     * 查询企业荣誉相关信息
     * @return
     */
    @Override
    public List<ZsCompany> selHonor() {
        List<ZsCompany> zsCompany = zsCompanyMapper.selHonor();
        return zsCompany;
    }

    /**
     * 查询新闻资讯相关信息
     * @return
     */
    @Override
    public List<ZsCompany> selNews() {
        List<ZsCompany> zsCompany = zsCompanyMapper.selNews();
        return zsCompany;
    }

    /**
     * 根据标题模糊查询众森介绍
     * @return
     */
    @Override
    public List<ZsProject> selIntroduceLikeTitle(String title) {
        List<ZsProject> zsProject = baseMapper.selIntroduceLikeTitle(title);
        return zsProject;
    }

    /**
     *根据标题模糊查询企业愿景
     * @param name
     * @return
     */
    @Override
    public List<ZsCompany> selVisionLikeName(String name) {
        List<ZsCompany> zsCompanies = baseMapper.selVisionLikeName(name);
        return zsCompanies;
    }

    /**
     *根据标题模糊查询发展历程
     * @param name
     * @return
     */
    @Override
    public List<ZsCompany> selHistoryLikeName(String name) {
        List<ZsCompany> zsCompanies = baseMapper.selHistoryLikeName(name);
        return zsCompanies;
    }

    /**
     *根据标题模糊查询企业荣誉
     * @param name
     * @return
     */
    @Override
    public List<ZsCompany> selHonorLikeName(String name) {
        List<ZsCompany> zsCompanies = baseMapper.selHonorLikeName(name);
        return zsCompanies;
    }

    /**
     *根据标题模糊查询新闻资讯
     * @param name
     * @return
     */
    @Override
    public List<ZsCompany> selNewsLikeName(String name) {
        List<ZsCompany> zsCompanies = baseMapper.selNewsLikeName(name);
        return zsCompanies;
    }

    /**
     * 上传图片
     * @param file
     * @param type
     * @return
     */
    @Override
    public Map<String, Object> upPhoto(MultipartFile file, String type) {
        Map<String, Object> map = zsPhotoService.upPhoto(file, type);
        return map;
    }

    /**
     * 增加企业愿景
     * @param zsCompany
     * @return
     */
    @Override
    public int addCompanyVision(ZsCompany zsCompany) {
        zsCompany.setType(1);
        int i = baseMapper.insert(zsCompany);
        return i;
    }

    /**
     * 增加发展历程
     * @param zsCompany
     * @return
     */
    @Override
    public int addHistory(ZsCompany zsCompany) {
        zsCompany.setType(2);
        int i = baseMapper.insert(zsCompany);
        return 0;
    }

    /**
     * 增加企业荣誉
     * @param zsCompany
     * @return
     */
    @Override
    public int addHonorInfo(ZsCompany zsCompany) {
        zsCompany.setType(3);
        int i = baseMapper.insert(zsCompany);
        return 0;
    }
}
