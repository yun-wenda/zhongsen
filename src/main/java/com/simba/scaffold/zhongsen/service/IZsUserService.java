package com.simba.scaffold.zhongsen.service;

import fykj.microservice.core.base.IBaseService;
import com.simba.scaffold.zhongsen.domain.entity.ZsUser;

import java.util.List;

/**
 *
 *
 * 服务类
 * @author wangming
 * @email ${email}
 * @date 2021-07-27
 */
public interface IZsUserService extends IBaseService<ZsUser> {

    List<ZsUser> selByName(String name);

}

