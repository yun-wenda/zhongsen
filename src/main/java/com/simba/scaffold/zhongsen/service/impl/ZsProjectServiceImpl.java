package com.simba.scaffold.zhongsen.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.simba.scaffold.zhongsen.domain.entity.ZsDictionary;
import com.simba.scaffold.zhongsen.domain.entity.ZsProject;
import com.simba.scaffold.zhongsen.mapper.ZsProjectMapper;
import com.simba.scaffold.zhongsen.service.IZsProjectService;
import fykj.microservice.core.base.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


/**
 *
 *
 * 服务实现类
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ZsProjectServiceImpl extends BaseServiceImpl<ZsProjectMapper, ZsProject> implements IZsProjectService {

    @Resource
    private ZsProjectMapper zsProjectMapper;

    @Resource
    private ZsPhotoServiceImpl zsPhotoServiceImpl;


    /**
     * 将文章类型修改为主推
     * @param title 文章标题
     * @return
     */
    @Override
    public int upProToZhuTui(String title) {
        UpdateWrapper updateWrapper = new UpdateWrapper();
        updateWrapper.eq("name",title);
        ZsProject project = new ZsProject();
        project.setType(2);
        int i = this.baseMapper.update(project, updateWrapper);
        return i;
    }

    /**
     * 将文章类型修改为探索
     * @param title 文章标题
     * @return
     */
    @Override
    public int upProToTanSuo(String title) {
        UpdateWrapper updateWrapper = new UpdateWrapper();
        updateWrapper.eq("name",title);
        ZsProject project = new ZsProject();
        project.setType(3);
        int i = this.baseMapper.update(project, updateWrapper);
        return i;
    }


    /**
     * 通过文章所属类型来查询当前类型的总数量
     * @param type 文章类型
     * @return
     */
    @Override
    public int selCountByType(int type) {
        int i = zsProjectMapper.selCountByType(type);
        return i;
    }

    /**
     * 根据文章所属类型和文章标题将文章修改为可见
     * @param name  文章标题
     * @param type  文章类型
     * @return
     */
    @Override
    public int upToShow(String name, int type) {
        //设定where条件
        UpdateWrapper updateWrapper = new UpdateWrapper();
        updateWrapper.eq("name",name);
        updateWrapper.eq("type",type);
        //设定要更新的内容
        ZsProject project = new ZsProject();
        project.setIsflag(1);
        //执行更新，返回变更的条数
        int i = this.baseMapper.update(project, updateWrapper);
        return i;
    }

    /**
     * 将文章修改为页面不可见
     * @param name  文章标题
     * @param type  文章类型
     * @return
     */
    @Override
    public int upUnShow(String name, int type) {
        //设定where条件
        UpdateWrapper updateWrapper = new UpdateWrapper();
        updateWrapper.eq("name",name);
        updateWrapper.eq("type",type);
        //设定要更新的内容
        ZsProject project = new ZsProject();
        project.setIsflag(0);
        //执行更新，返回变更的条数
        int i = this.baseMapper.update(project, updateWrapper);
        return i;
    }

    /**
     * 将当前文章类型修改为正常并主页面不可见
     * @param name  文章标题
     * @param type  当前文章类型
     * @return
     */
    @Override
    public int upToZC(String name, int type) {
        //设定where条件
        UpdateWrapper updateWrapper = new UpdateWrapper();
        updateWrapper.eq("name",name);
        updateWrapper.eq("type",type);
        //设定要更新的内容
        ZsProject project = new ZsProject();
        project.setType(1);
        project.setIsflag(0);
        //执行更新，返回变更的条数
        int i = this.baseMapper.update(project, updateWrapper);
        return i;
    }

    /**
     * 保存文章信息
     * @param project
     * @return
     */
    @Override
    public int saveProject(ZsProject project) {
        int i = this.baseMapper.insert(project);
        return i;
    }

    /**
     * 查询主推类文章信息
     * @return
     */
    @Override
    public List<ZsProject> selZhuTui() {
        List<ZsProject> zsProjects = zsProjectMapper.selZhuTui();
        return zsProjects;
    }

    /**
     * 查询探索类文章信息
     * @return
     */
    @Override
    public List<ZsProject> selTanSuo() {
        List<ZsProject> zsProjects = baseMapper.selTanSuo();
        return zsProjects;
    }

    /**
     * 查询活动资讯
     * @return
     */
    @Override
    public List<ZsProject> selhuoDongZiXun() {
        List<ZsProject> zsProjects = zsProjectMapper.selhuoDongZiXun();
        return zsProjects;
    }

    /**
     * 根据文章标题模糊查询
     * @return
     */
    @Override
    public List<ZsProject> selLikeTitle(String title) {
        List<ZsProject> zsProjects = baseMapper.selLikeTitle(title);
        return zsProjects;
    }

    /**
     * 增加活动资讯类文章
     * @param zsProject
     * @return
     */
    @Override
    public int addActionInfo(ZsProject zsProject) {
        zsProject.setType(5);
        zsProject.setIsflag(0);
        int i = baseMapper.insert(zsProject);
        return i;
    }


    /**
     * 活动资讯的图片上传
     * @param file
     * @param type
     * @return
     */
    @Override
    public Map<String, Object> upPhoto(MultipartFile file, String type) {
        Map<String, Object> map = zsPhotoServiceImpl.upPhoto(file, type);
        return map;
    }

    /**
     * 新增活动资讯类文章
     * @param zsProject
     * @return
     */
    @Override
    public int addIntroduceInfo(ZsProject zsProject) {
        zsProject.setType(4);
        zsProject.setIsflag(0);
        int i = baseMapper.insert(zsProject);
        return i;
    }

    /**
     * 根据部分标题内容模糊查询活动资讯文章
     * @param name
     * @return
     */
    @Override
    public List<ZsProject> selActionLikeName(String name) {
        List<ZsProject> zsProjects = baseMapper.selActionLikeName(name);
        return zsProjects;
    }

    /**
     * 增加项目介绍相关文章
     * @param zsProject
     * @return
     */
    @Override
    public int addProjectIntr(ZsProject zsProject) {
        int i = baseMapper.insert(zsProject);
        return i;
    }

    /**
     * 查询全部项目介绍文章
     * @return
     */
    @Override
    public List<ZsProject> selProjectIntr() {
        List<ZsProject> zsProjects = baseMapper.selProjectIntr();
        return zsProjects;
    }

    /**
     * 模糊查询项目介绍类文章
     * @param name
     * @return
     */
    @Override
    public List<ZsProject> sellLikeProIntr(String name) {
        List<ZsProject> zsProjects = baseMapper.sellLikeProIntr(name);
        return zsProjects;
    }

    /**
     * 根据id获取数据
     * @param id
     * @return
     */
    @Override
    public List<ZsProject> selById(String id) {
        List<ZsProject> zsProjects = baseMapper.selById(id);
        return zsProjects;
    }
}
