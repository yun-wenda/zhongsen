package com.simba.scaffold.zhongsen.service.impl;

import com.simba.scaffold.zhongsen.domain.entity.ZsZhaopin;
import com.simba.scaffold.zhongsen.mapper.ZsZhaopinMapper;
import com.simba.scaffold.zhongsen.service.IZsZhaopinService;
import fykj.microservice.core.base.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


/**
 *
 *
 * 服务实现类
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ZsZhaopinServiceImpl extends BaseServiceImpl<ZsZhaopinMapper, ZsZhaopin> implements IZsZhaopinService {

    @Resource
    private ZsZhaopinMapper zsZhaopinMapper;
    @Resource
    private ZsPhotoServiceImpl zsPhotoService;

    /**
     * 查询关于众森招聘相关文章信息
     * @return
     */
    @Override
    public List<ZsZhaopin> talentConcept() {
        List<ZsZhaopin> zsZhaopins = zsZhaopinMapper.talentConcept();
        return zsZhaopins;
    }

    /**
     * 查询关于众森招聘相关文章信息
     * @return
     */
    @Override
    public List<ZsZhaopin> teamElegantDemeanour() {
        List<ZsZhaopin> zsZhaopins = zsZhaopinMapper.teamElegantDemeanour();
        return zsZhaopins;
    }

    /**
     * 模糊查询人才理念相关文章
     * @param name
     * @return
     */
    @Override
    public List<ZsZhaopin> selTalentConceptLikeName(String name) {
        List<ZsZhaopin> zsZhaopins = baseMapper.selTalentConceptLikeName(name);
        return zsZhaopins;
    }

    /**
     * 模糊查询团队风采相关文章
     * @param name
     * @return
     */
    @Override
    public List<ZsZhaopin> selFengCaiLikeName(String name) {
        List<ZsZhaopin> zsZhaopins = baseMapper.selFengCaiLikeName(name);
        return zsZhaopins;
    }

    /**
     * 图片上传
     * @param file
     * @param type
     * @return
     */
    @Override
    public Map<String, Object> upPhoto(MultipartFile file, String type) {
        Map<String, Object> map = zsPhotoService.upPhoto(file, type);
        return map;
    }

    /**
     * 添加人才理念
     * @param zsZhaopin
     * @return
     */
    @Override
    public int addTalentConcept(ZsZhaopin zsZhaopin) {
        zsZhaopin.setType(1);
        int i = baseMapper.insert(zsZhaopin);
        return i;
    }

    /**
     * 添加团队风采
     * @param zsZhaopin
     * @return
     */
    @Override
    public int addFengCai(ZsZhaopin zsZhaopin) {
        zsZhaopin.setType(2);
        int i = baseMapper.insert(zsZhaopin);
        return i;
    }
}
