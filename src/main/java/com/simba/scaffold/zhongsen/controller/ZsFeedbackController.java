package com.simba.scaffold.zhongsen.controller;

import com.simba.scaffold.zhongsen.domain.entity.ZsFeedback;
import com.simba.scaffold.zhongsen.domain.params.ZsFeedbackParams;
import com.simba.scaffold.zhongsen.service.IZsFeedbackService;
import fykj.microservice.core.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import result.JsonResult;
import result.Result;

import java.util.List;

/**
 *
 *
 * 前端控制器
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@RestController
@RequestMapping("/zsfeedback")
@Api(tags = "客户反馈信息相关接口")
public class ZsFeedbackController extends BaseController<IZsFeedbackService, ZsFeedback,ZsFeedbackParams> {


    @ApiOperation(value = "根据客户名称模糊查询反馈内容",response = ZsFeedback.class)
    @GetMapping("/likeCustomer")
    public Result selLikeName(@RequestParam("name")String name){
        List<ZsFeedback> zsFeedback = baseService.selLikeName(name);
        return new JsonResult<>(zsFeedback);
    }



}
