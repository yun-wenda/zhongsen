package com.simba.scaffold.zhongsen.controller;

import com.simba.scaffold.zhongsen.domain.dto.TouristsDto;
import com.simba.scaffold.zhongsen.domain.entity.ZsCompany;
import com.simba.scaffold.zhongsen.domain.entity.ZsManor;
import com.simba.scaffold.zhongsen.domain.entity.ZsProject;
import com.simba.scaffold.zhongsen.domain.entity.ZsTourists;
import com.simba.scaffold.zhongsen.domain.params.ZsTouristsParams;
import com.simba.scaffold.zhongsen.service.IZsTouristsService;
import fykj.microservice.core.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import result.JsonResult;
import result.Result;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * 前端控制器
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@RestController
@RequestMapping("/zstourists")
@Api(tags = "游客服务接口")
public class ZsTouristsController extends BaseController<IZsTouristsService, ZsTourists,ZsTouristsParams> {

    @Resource
    private IZsTouristsService zsTouristsService;

    @ApiOperation("查询餐饮住宿类相关信息")
    @GetMapping("/hotel")
    public Result selHotel(){
        List<ZsTourists> zsTourists = zsTouristsService.selHotel();
        return new JsonResult<>(zsTourists);
    }

    @ApiOperation("查询会议拓展类相关信息")
    @GetMapping("/meeting")
    public Result meetingExpand(){
        List<ZsTourists> zsTourists = zsTouristsService.meetingExpand();
        return new JsonResult<>(zsTourists);
    }

    @ApiOperation("查询学研基地类相关信息")
    @GetMapping("/base")
    public Result studyBase(){
        List<ZsTourists> zsTourists = zsTouristsService.studyBase();
        return new JsonResult<>(zsTourists);
    }

    @ApiOperation("查询乡土农特类相关信息")
    @GetMapping("/specially")
    public Result ruralSpecially(){
        List<ZsTourists> zsTourists = zsTouristsService.ruralSpecially();
        return new JsonResult<>(zsTourists);
    }

    @ApiOperation("查询香腮苹果类相关信息")
    @GetMapping("/apple")
    public Result selApple(){
        List<ZsTourists> zsTourists = zsTouristsService.selApple();
        return new JsonResult<>(zsTourists);
    }

    @ApiImplicitParam(name="name",value = "标题内容部分字段",dataType = "String")
    @ApiOperation(value = "模糊查询餐饮住宿的相关信息",response = ZsTourists.class)
    @GetMapping("/fuzzySelHotel")
    public Result selHotelLikeName(@RequestParam("name") String name){
        List<ZsTourists> zsTourists = baseService.selHotelLikeName(name);
        return new JsonResult<>(zsTourists);
    }

    @ApiImplicitParam(name="name",value = "标题内容部分字段",dataType = "String")
    @ApiOperation(value = "模糊查询会议拓展的相关信息",response = ZsTourists.class)
    @GetMapping("/fuzzySelMeeting")
    public Result selMeetingLikeName(@RequestParam("name") String name){
        List<ZsTourists> zsTourists = baseService.selMeetingLikeName(name);
        return new JsonResult<>(zsTourists);
    }

    @ApiImplicitParam(name="name",value = "标题内容部分字段",dataType = "String")
    @ApiOperation(value = "模糊查询学研基地的相关信息",response = ZsTourists.class)
    @GetMapping("/fuzzySelBase")
    public Result selBaselLikeName(@RequestParam("name") String name){
        List<ZsTourists> zsTourists = baseService.selBaselLikeName(name);
        return new JsonResult<>(zsTourists);
    }

    @ApiImplicitParam(name="name",value = "标题内容部分字段",dataType = "String")
    @ApiOperation(value = "模糊查询乡土农特的相关信息",response = ZsTourists.class)
    @GetMapping("/fuzzySelSpecially")
    public Result selSpeciallyLikeName(@RequestParam("name") String name){
        List<ZsTourists> zsTourists = baseService.selSpeciallyLikeName(name);
        return new JsonResult<>(zsTourists);
    }

    @ApiImplicitParam(name="name",value = "标题内容部分字段",dataType = "String")
    @ApiOperation(value = "模糊查询香腮苹果的相关信息",response = ZsTourists.class)
    @GetMapping("/fuzzySelApple")
    public Result selAppleLikeName(@RequestParam("name") String name){
        List<ZsTourists> zsTourists = baseService.selAppleLikeName(name);
        return new JsonResult<>(zsTourists);
    }

    @ApiOperation("增加餐饮住宿相关文章")
    @PostMapping(value = "/addHotel")
    public Result addHotelInfo(@RequestBody TouristsDto touristsDto){
//        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();
        Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果
//        result = baseService.upPhoto(file, "hotel");
        ZsTourists zsTourists = new ZsTourists();
        zsTourists.setName(touristsDto.getName());
        zsTourists.setContent(touristsDto.getContent());
        zsTourists.setAssophoto(touristsDto.getFileName());
        zsTourists.setPhotoUrl(touristsDto.getRelativePath());
        zsTourists.setShortDescription(touristsDto.getDescription());
        int i = baseService.addHotelInfo(zsTourists);
        result.put("result",i);
        return new JsonResult<>(result);
    }

    @ApiOperation("增加会议拓展相关文章")
    @PostMapping(value = "/addMeeting")
    public Result addMeetingInfo(@RequestBody TouristsDto touristsDto){
//        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();
        Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果
//        result = baseService.upPhoto(file, "meeting");
        ZsTourists zsTourists = new ZsTourists();
        zsTourists.setName(touristsDto.getName());
        zsTourists.setContent(touristsDto.getContent());
        zsTourists.setAssophoto(touristsDto.getFileName());
        zsTourists.setPhotoUrl(touristsDto.getRelativePath());
        zsTourists.setShortDescription(touristsDto.getDescription());
        int i = baseService.addMeetingInfo(zsTourists);
        result.put("result",i);
        return new JsonResult<>(result);
    }

    @ApiOperation("增加学研基地相关文章")
    @PostMapping(value = "/addBase")
    public Result addBaseInfo(@RequestBody TouristsDto touristsDto){

//        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();
        Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果
//        result = baseService.upPhoto(file, "base");
        ZsTourists zsTourists = new ZsTourists();
        zsTourists.setName(touristsDto.getName());
        zsTourists.setContent(touristsDto.getContent());
        zsTourists.setAssophoto(touristsDto.getFileName());
        zsTourists.setPhotoUrl(touristsDto.getRelativePath());
        zsTourists.setShortDescription(touristsDto.getDescription());
        int i = baseService.addBaseInfo(zsTourists);
        result.put("result",i);
        return new JsonResult<>(result);
    }

    @ApiOperation("增加乡土农特相关文章")
    @PostMapping(value = "/addSpecial")
    public Result addSpeciallyInfo(@RequestBody TouristsDto touristsDto){
//        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();
        Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果
//        result = baseService.upPhoto(file, "special");
        ZsTourists zsTourists = new ZsTourists();
        zsTourists.setUrl(touristsDto.getUrl());
        zsTourists.setName(touristsDto.getName());
        zsTourists.setContent(touristsDto.getContent());
        zsTourists.setAssophoto(touristsDto.getFileName());
        zsTourists.setPhotoUrl(touristsDto.getRelativePath());
        zsTourists.setShortDescription(touristsDto.getDescription());
        int i = baseService.addSpeciallyInfo(zsTourists);
        result.put("result",i);
        return new JsonResult<>(result);
    }

    @ApiOperation("增加香腮苹果相关文章")
    @PostMapping(value = "/addApple")
    public Result addAppleInfo(@RequestBody TouristsDto touristsDto){
//        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();
        Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果
//        result = baseService.upPhoto(file, "hotel");
        ZsTourists zsTourists = new ZsTourists();
        zsTourists.setUrl(touristsDto.getUrl());
        zsTourists.setName(touristsDto.getName());
        zsTourists.setContent(touristsDto.getContent());
        zsTourists.setAssophoto(touristsDto.getFileName());
        zsTourists.setPhotoUrl(touristsDto.getRelativePath());
        zsTourists.setShortDescription(touristsDto.getDescription());
        int i = baseService.addAppleInfo(zsTourists);
        result.put("result",i);
        return new JsonResult<>(result);
    }

}
