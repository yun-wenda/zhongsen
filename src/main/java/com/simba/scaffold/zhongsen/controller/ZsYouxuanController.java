package com.simba.scaffold.zhongsen.controller;

import com.simba.scaffold.zhongsen.domain.dto.YouXuanDto;
import com.simba.scaffold.zhongsen.domain.entity.ZsYouxuan;
import com.simba.scaffold.zhongsen.domain.entity.ZsZhaopin;
import com.simba.scaffold.zhongsen.domain.params.ZsYouxuanParams;
import com.simba.scaffold.zhongsen.service.IZsYouxuanService;
import fykj.microservice.core.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import result.JsonResult;
import result.Result;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * 前端控制器
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@RestController
@RequestMapping("/zsyouxuan")
@Api(tags = "众森优选相关接口")
public class ZsYouxuanController extends BaseController<IZsYouxuanService, ZsYouxuan,ZsYouxuanParams> {

    @Resource
    private IZsYouxuanService zsYouxuanService;

    @ApiOperation(value = "查询庄园旅游类文章",response = ZsYouxuan.class)
    @GetMapping("/tourism")
    public Result farmTourism(){
        List<ZsYouxuan> zsYouxuans = zsYouxuanService.farmTourism();
        return new JsonResult<>(zsYouxuans);
    }

    @ApiOperation(value = "查询认购果树类文章",response = ZsYouxuan.class)
    @GetMapping("/buyTree")
    public Result buyFruitTree(){
        List<ZsYouxuan> zsYouxuans = zsYouxuanService.buyFruitTree();
        return new JsonResult<>(zsYouxuans);
    }

    @ApiOperation(value = "查询文创周边类文章",response = ZsYouxuan.class)
    @GetMapping("/surrounding")
    public Result cultureSurrounding(){
        List<ZsYouxuan> zsYouxuans = zsYouxuanService.cultureSurrounding();
        return new JsonResult<>(zsYouxuans);
    }

    @ApiOperation(value = "查询苹果订购类文章",response = ZsYouxuan.class)
    @GetMapping("/buyApple")
    public Result buyApple(){
        List<ZsYouxuan> zsYouxuans = zsYouxuanService.buyApple();
        return new JsonResult<>(zsYouxuans);
    }


    @ApiImplicitParam(name="name",value = "标题内容部分字段",dataType = "String")
    @ApiOperation(value = "模糊查询庄园旅游的相关信息",response = ZsYouxuan.class)
    @GetMapping("/fuzzyTourism")
    public Result selTourismLikeName(@RequestParam("name") String name){
        List<ZsYouxuan> zsYouxuans = baseService.selTourismLikeName(name);
        return new JsonResult<>(zsYouxuans);
    }

    @ApiImplicitParam(name="name",value = "标题内容部分字段",dataType = "String")
    @ApiOperation(value = "模糊查询认购果树的相关信息",response = ZsYouxuan.class)
    @GetMapping("/fuzzyBuyTree")
    public Result selBuyTreeLikeName(@RequestParam("name") String name){
        List<ZsYouxuan> zsYouxuans = baseService.selBuyTreeLikeName(name);
        return new JsonResult<>(zsYouxuans);
    }

    @ApiImplicitParam(name="name",value = "标题内容部分字段",dataType = "String")
    @ApiOperation(value = "模糊查询文创周边的相关信息",response = ZsYouxuan.class)
    @GetMapping("/fuzzySurrounding")
    public Result selSurroundingLikeName(@RequestParam("name") String name){
        List<ZsYouxuan> zsYouxuans = baseService.selSurroundingLikeName(name);
        return new JsonResult<>(zsYouxuans);
    }

    @ApiImplicitParam(name="name",value = "标题内容部分字段",dataType = "String")
    @ApiOperation(value = "模糊查询苹果订购的相关信息",response = ZsYouxuan.class)
    @GetMapping("/fuzzyBuyApple")
    public Result selBuyAppleLikeName(@RequestParam("name") String name){
        List<ZsYouxuan> zsYouxuans = baseService.selBuyAppleLikeName(name);
        return new JsonResult<>(zsYouxuans);
    }

    @ApiOperation("增加庄园旅游相关文章")
    @PostMapping(value = "/addTourism")
    public Result addTourism(@RequestBody YouXuanDto youXuanDto){
//        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();
        Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果
//        result = baseService.upPhoto(file, "tourism");
        ZsYouxuan zsYouxuan = new ZsYouxuan();
        zsYouxuan.setName(youXuanDto.getName());
        zsYouxuan.setContent(youXuanDto.getContent());
        zsYouxuan.setUrl(youXuanDto.getUrl());
        zsYouxuan.setAssophoto(youXuanDto.getFileName());
        zsYouxuan.setPhotoUrl(youXuanDto.getRelativePath());
        zsYouxuan.setShortDescription(youXuanDto.getDescription());
        int i = baseService.addTourism(zsYouxuan);
        result.put("result",i);
        return new JsonResult<>(result);
    }

    @ApiOperation("增加认购果树相关文章")
    @PostMapping(value = "/addBuyTree")
    public Result addBuyTree(@RequestBody YouXuanDto youXuanDto){
//        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();
        Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果
//        result = baseService.upPhoto(file, "btree");
        ZsYouxuan zsYouxuan = new ZsYouxuan();
        zsYouxuan.setName(youXuanDto.getName());
        zsYouxuan.setContent(youXuanDto.getContent());
        zsYouxuan.setUrl(youXuanDto.getUrl());
        zsYouxuan.setAssophoto(youXuanDto.getFileName());
        zsYouxuan.setPhotoUrl(youXuanDto.getRelativePath());
        zsYouxuan.setShortDescription(youXuanDto.getDescription());
        int i = baseService.addBuyTree(zsYouxuan);
        result.put("result",i);
        return new JsonResult<>(result);
    }

    @ApiOperation("增加文创周边相关文章")
    @PostMapping(value = "/addSurrounding")
    public Result addSurrounding(@RequestBody YouXuanDto youXuanDto){
//        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();
        Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果
//        result = baseService.upPhoto(file, "surrounding");
        ZsYouxuan zsYouxuan = new ZsYouxuan();
        zsYouxuan.setName(youXuanDto.getName());
        zsYouxuan.setContent(youXuanDto.getContent());
        zsYouxuan.setUrl(youXuanDto.getUrl());
        zsYouxuan.setAssophoto(youXuanDto.getFileName());
        zsYouxuan.setPhotoUrl(youXuanDto.getRelativePath());
        zsYouxuan.setShortDescription(youXuanDto.getDescription());
        int i = baseService.addSurrounding(zsYouxuan);
        result.put("result",i);
        return new JsonResult<>(result);
    }

    @ApiOperation("增加苹果订购相关文章")
    @PostMapping(value = "/addBuyApple")
    public Result addBuyApple(@RequestBody YouXuanDto youXuanDto){
//        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();
        Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果
//        result = baseService.upPhoto(file, "bapple");
        ZsYouxuan zsYouxuan = new ZsYouxuan();
        zsYouxuan.setName(youXuanDto.getName());
        zsYouxuan.setContent(youXuanDto.getContent());
        zsYouxuan.setUrl(youXuanDto.getUrl());
        zsYouxuan.setAssophoto(youXuanDto.getFileName());
        zsYouxuan.setPhotoUrl(youXuanDto.getRelativePath());
        zsYouxuan.setShortDescription(youXuanDto.getDescription());
        int i = baseService.addBuyApple(zsYouxuan);
        result.put("result",i);
        return new JsonResult<>(result);
    }

}
