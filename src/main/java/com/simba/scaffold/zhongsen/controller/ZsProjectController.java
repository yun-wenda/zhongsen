package com.simba.scaffold.zhongsen.controller;

import com.simba.scaffold.zhongsen.domain.dto.ZsProjectDto;
import com.simba.scaffold.zhongsen.domain.entity.ZsCompany;
import com.simba.scaffold.zhongsen.domain.entity.ZsProject;
import com.simba.scaffold.zhongsen.domain.params.ZsProjectParams;
import com.simba.scaffold.zhongsen.service.IZsPhotoService;
import com.simba.scaffold.zhongsen.service.IZsProjectService;
import com.simba.scaffold.zhongsen.utils.CommonUtils;
import fykj.microservice.core.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import result.JsonResult;
import result.Result;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * 前端控制器
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@RestController
@RequestMapping("/zsproject")
@Api(tags = "文章类接口")
public class ZsProjectController extends BaseController<IZsProjectService, ZsProject,ZsProjectParams> {

    @Resource
    private IZsProjectService zsProjectService;
    @Resource
    private CommonUtils commonUtils;
    @Resource
    private IZsPhotoService zsPhotoService;

    @ApiOperation("根据文章标题修改当前文章为主推类文章")
    @GetMapping("/upProToZhuTui")
    public String upProToZhuTui(@RequestParam("title") String title){
        String result = "";
        int count = zsProjectService.selCountByType(2);
        if(count >= 4){
            result = "主推类文章展示数量已超出4个，请先取消主推类文章数量再进行修改。";
        }else{
            int i = zsProjectService.upProToZhuTui(title);
            if(i != 0){
                result = "变更成功";
            }else{
                result = "变更失败";
            }
        }
        return result;
    }

    @ApiOperation("根据文章标题修改当前文章为探索类文章")
    @GetMapping("/upProToTanSuo")
    public String upProToTanSuo(@RequestParam("title") String title){
        String result = "";
        int count = zsProjectService.selCountByType(3);
        if(count >= 4){
            result = "探索类文章展示数量已超出4个，请先取消探索类文章数量再进行修改。";
        }else{
            int i = zsProjectService.upProToTanSuo(title);
            if(i != 0){
                result = "变更成功";
            }else{
                result = "变更失败";
            }
        }
        return result;
    }

    @ApiOperation("将文章修改为主页面可见")
    //@RequestMapping("/upToShow")
    public String upShow(@RequestParam("name") String name ,@RequestParam("type") int type){
        String result = "";
        String s = commonUtils.transType(type);
        int i = zsProjectService.selCountByType(type);
        if(i>=4){
            result = "当前页面展示的"+s+"类型的文章已经超出展示上限（4），请先取消一些文章的展示再行添加。";
        }else{
            int i1 = zsProjectService.upToShow(name, type);
            if(i1 > 0){
                result = "已修改";
            }
        }
        return result;
    }

    @ApiOperation("将文章修改为页面不可见")
    //@RequestMapping("/upUnShow")
    public String upUnShow(@RequestParam("name")String name,@RequestParam("type")int type){
        String result = "";
        int i = zsProjectService.upUnShow(name, type);
        if(i > 0){
            result = "当前文章已不展示在页面上";
        }else{
            result = "修改失败";
        }
        return result;
    }

    @ApiOperation("将当前选定的文章类型修改为正常文章并主页面不可见")
    @PostMapping("/upToZC")
    public Result upToZC(@RequestParam("name")String name,@RequestParam("type")int type){
        String result = "";
        Map<String,Object> root=new HashMap<String, Object>();
        int i = zsProjectService.upToZC(name, type);
        String s = commonUtils.transType(type);
        if(i > 0){
            result = "当前文章类型已从"+s+"中移除";
        }else{
            result = "变更失败，请检查当前文章类型是否已非"+s+"类型";
        }
        root.put("result",result);
        return new JsonResult<>(result);
    }

    @ApiOperation("添加图片成功后的文章保存")
    @PostMapping("/addProject")
    public Result addProject(@RequestParam("photoName")String photoName,
                             @RequestParam("photoType")String photoType,
                             @RequestBody ZsProject project){
        String result = "";
        Map<String,Object> root=new HashMap<String, Object>();
        if("".equals(photoName)){
            result = "请先上传图片，待图片上传成功后在保存文章信息";
        }else{
            int i = zsPhotoService.upPhotoType(photoName, photoType);
            int i1 = zsProjectService.saveProject(project);
            if(i > 0 && i1 > 0){
                result = "文章保存成功";
            }else{
                result = "文章保存失败";
            }
        }
        root.put("result",result);
        return new JsonResult<>(root);
    }

    @ApiOperation(value = "页面展示项目介绍文章",response = ZsProject.class)
    @GetMapping("/projectIntr")
    public Result selProjectIntr(){
        List<ZsProject> zsProjects = zsProjectService.selProjectIntr();
        return new JsonResult<>(zsProjects);
    }

    @ApiOperation(value = "页面展示主推类文章",response = ZsProject.class)
    @GetMapping("/zhuTui")
    public Result selZhuTui(){
        List<ZsProject> zsProjects = zsProjectService.selZhuTui();
        return new JsonResult<>(zsProjects);
    }

    @ApiOperation(value = "页面展示探索类文章",response = ZsProject.class)
    @GetMapping("/tanSuo")
    public Result selTanSuo(){
        List<ZsProject> zsProjects = zsProjectService.selTanSuo();
        return new JsonResult<>(zsProjects);
    }


    @ApiOperation(value = "页面展示活动资讯",response = ZsProject.class)
    @GetMapping("/huoDongZiXun")
    public Result selhuoDongZiXun(){
        List<ZsProject> zsProjects = zsProjectService.selhuoDongZiXun();
        return new JsonResult<>(zsProjects);
    }


    @ApiOperation(value = "根据文章标题模糊查询",response = ZsProject.class)
    @GetMapping("/likeTitle")
    public Result selLikeTitle(@RequestParam("title")String title){
        List<ZsProject> zsProjects = baseService.selLikeTitle(title);
        return new JsonResult<>(zsProjects);
    }

    @ApiOperation(value = "根据项目介绍标题模糊查询",response = ZsProject.class)
    @GetMapping("/likeProIntr")
    public Result sellLikeProIntr(@RequestParam("name")String name){
        List<ZsProject> zsProjects = baseService.sellLikeProIntr(name);
        return new JsonResult<>(zsProjects);
    }

    @ApiOperation(value = "根据文章标题模糊查询活动资讯文章",response = ZsProject.class)
    @GetMapping("/actionLike")
    public Result selActionLikeName(@RequestParam("name")String name){
        List<ZsProject> zsProjects = baseService.selActionLikeName(name);
        return new JsonResult<>(zsProjects);
    }


    @ApiOperation("增加活动资讯相关文章")
    @PostMapping(value = "/actionInfo")
    public Result addActionInfo(@RequestBody ZsProjectDto zsProjectDto){

        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();
        Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果
//        result = baseService.upPhoto(file, "actionInfo");
        ZsProject zsProject = new ZsProject();
        zsProject.setName(zsProjectDto.getName());
        zsProject.setContent(zsProjectDto.getContent());
        zsProject.setAssoPhoto(zsProjectDto.getFileName());
        zsProject.setPhotoUrl(zsProjectDto.getRelativePath());
        zsProject.setShortDescription(zsProjectDto.getDescription());
        int i = baseService.addActionInfo(zsProject);
        result.put("result",i);
        return new JsonResult<>(result);
    }


    @ApiOperation("增加关于众森相关文章")
    @PostMapping(value = "/intrInfo")
    public Result addIntroduceInfo(@RequestBody ZsProjectDto zsProjectDto){

        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();
        Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果
//        result = baseService.upPhoto(file, "introduceZS");
        ZsProject zsProject = new ZsProject();
        zsProject.setName(zsProjectDto.getName());
        zsProject.setContent(zsProjectDto.getContent());
        zsProject.setAssoPhoto(zsProjectDto.getFileName());
        zsProject.setPhotoUrl(zsProjectDto.getRelativePath());
        zsProject.setShortDescription(zsProjectDto.getDescription());
        int i = baseService.addIntroduceInfo(zsProject);
        result.put("result",i);
        return new JsonResult<>(result);
    }

    @ApiOperation("增加项目介绍相关文章")
    @PostMapping(value = "/addProjectIntr")
    public Result addProjectIntr(@RequestBody ZsProjectDto zsProjectDto){

        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();
        Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果
//        result = baseService.upPhoto(file, "introducePro");
        ZsProject zsProject = new ZsProject();
        zsProject.setName(zsProjectDto.getName());
        zsProject.setContent(zsProjectDto.getContent());
        zsProject.setType(zsProjectDto.getType());
        zsProject.setShortDescription(zsProjectDto.getDescription());
        zsProject.setAssoPhoto(zsProjectDto.getFileName());
        zsProject.setPhotoUrl(zsProjectDto.getRelativePath());
        int i = baseService.addProjectIntr(zsProject);
        result.put("result", i);
        return new JsonResult<>(result);
    }

    @ApiOperation("根据id获取数据")
    @GetMapping("/selById")
    public Result selById(@RequestParam("id")String id){
        List<ZsProject> zsProjects = baseService.selById(id);
        return new JsonResult<>(zsProjects);
    }

}
