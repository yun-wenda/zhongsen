package com.simba.scaffold.zhongsen.controller;

import com.alibaba.fastjson.JSON;
import com.simba.scaffold.zhongsen.domain.dto.PhotoDto;
import com.simba.scaffold.zhongsen.domain.entity.ZsPhoto;
import com.simba.scaffold.zhongsen.domain.params.ZsPhotoParams;
import com.simba.scaffold.zhongsen.service.IZsPhotoService;
import com.simba.scaffold.zhongsen.utils.FileUtils;
import fykj.microservice.core.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import result.JsonResult;
import result.Result;
import result.ResultCode;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

/**
 *
 *
 * 前端控制器
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@RestController
@RequestMapping("/zsphoto")
@Api(tags = "图片相关接口")
public class ZsPhotoController extends BaseController<IZsPhotoService, ZsPhoto,ZsPhotoParams> {

    @Resource
    private IZsPhotoService zsPhotoService;

    /**
     * 批量上传（暂未测试）
     */
    //@PostMapping(value = "/upPhotos",consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public List upPhotos(@RequestParam("uploadFiles") MultipartFile[] files) throws Exception{

        System.out.println("上传的图片数："+files.length);

        String separator = File.separator;
        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();

        for (MultipartFile file : files) {    //循环保存文件

            Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果
            String result_msg="";//上传结果信息

            if (file.getSize() / 100000 > 200){
                result_msg="图片大小不能超过20MB";
            }
            else{
                //判断上传文件格式
                String fileType = file.getContentType();
                if (fileType.equals("image/jpeg") || fileType.equals("image/png") || fileType.equals("image/jpeg")) {
                    // 要上传的目标文件存放的绝对路径
                    final String localPath=System.getProperty("user.dir")+"\\src\\main\\resources\\static\\image";
                    //获取文件名
                    String fileName = file.getOriginalFilename();
                    //获取文件后缀名
                    String suffixName = fileName.substring(fileName.lastIndexOf("."));
                    //重新生成文件名
                    fileName = UUID.randomUUID()+suffixName;
                    if (FileUtils.upload(file, localPath, fileName)) {
                        //文件存放的相对路径(一般存放在数据库用于img标签的src)
                        String relativePath="img/"+fileName;
                        result.put("relativePath",relativePath);//前端根据是否存在该字段来判断上传是否成功
                        result_msg="图片上传成功";
                    }
                    else{
                        result_msg="图片上传失败";
                    }
                }
                else{
                    result_msg="图片格式不正确";
                }
            }
            result.put("result_msg",result_msg);
            root.add(result);
        }
        String root_json= JSON.toJSONString(root);
        System.out.println(root_json);
        return root;
    }

    /**
     * 单一图片上传
     * @param file
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "单一图片上传", response = List.class)
    @PostMapping(value = "/upPhoto",consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public Result upPhoto(@RequestParam("file") MultipartFile file) throws Exception{

        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();
        Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果
        //Map<String, Object> result_msg="";//上传结果信息

        if(file.getSize() == 0){
            result.put("result_msg","上传图片为0KB，请重新上传");
        }
//        else if (file.getSize() / 100000 > 200){
//            result_msg="图片大小不能超过20MB";
//        }
        else{
            result = zsPhotoService.upPhoto(file,"");
        }

        root.add(result);

        String root_json= JSON.toJSONString(root);
        System.out.println(root_json);
        // return root;
        return new JsonResult<>(root);
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "图片名称", required = true, paramType = "query", dataType = "string")
    })
    @ApiOperation(value = "通过图片名称模糊查询图片详细信息",response = ZsPhoto.class)
    @GetMapping("/byName")
    public Result selByName(@RequestParam("name") String name){
        List<ZsPhoto> zsPhoto = zsPhotoService.selByName(name);
        return new JsonResult<>(zsPhoto);

    }

    /**
     * 单一图片上传
     * @param file
     * @return
     * @throws Exception
     */

    @ApiOperation(value = "轮播图图片上传（一次一张）", response = List.class)
    @PostMapping(value = "/photoForLbt",consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public Result PhotoForLbt(@RequestParam("file") MultipartFile file) throws Exception{

        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();
        Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果
        result = zsPhotoService.upPhoto(file,"lunbotu");
        root.add(result);

        String root_json= JSON.toJSONString(root);
        System.out.println(root_json);
        // return root;
        return new JsonResult<>(root);
    }

    @ApiOperation(value = "轮播图展示查询",response = ZsPhoto.class)
    @GetMapping("/showLbt")
    public Result showLbt(){
        List<ZsPhoto> zsPhotos = zsPhotoService.selShowLbt();
        return new JsonResult<>(zsPhotos);
    }

    @ApiOperation(value = "查询所有轮播图图片",response = ZsPhoto.class)
    @GetMapping("/allLbt")
    public Result selAllLbt(){
        List<ZsPhoto> zsPhotos = zsPhotoService.selAllLbt();
        return new JsonResult<>(zsPhotos);
    }



}
