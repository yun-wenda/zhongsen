package com.simba.scaffold.zhongsen.controller;

import com.simba.scaffold.zhongsen.domain.entity.ZsDictionary;
import com.simba.scaffold.zhongsen.domain.entity.ZsProject;
import com.simba.scaffold.zhongsen.domain.entity.ZsQuestion;
import com.simba.scaffold.zhongsen.domain.params.ZsQuestionParams;
import com.simba.scaffold.zhongsen.service.IZsQuestionService;
import fykj.microservice.core.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import result.JsonResult;
import result.Result;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * 前端控制器
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@RestController
@RequestMapping("/zsquestion")
@Api(tags = "客户提问相关接口")
public class ZsQuestionController extends BaseController<IZsQuestionService, ZsQuestion,ZsQuestionParams> {

    @Resource
    private IZsQuestionService zsQuestionService;

    @ApiOperation(value = "查询客户提问和对应的解答",response = ZsQuestionParams.class)
    @GetMapping("/question")
    public Result selQuestion(){
        ZsQuestion zsQuestions = zsQuestionService.selQuestion();
        return new JsonResult<>(zsQuestions);
    }


    @ApiOperation("新增问题")
    @PostMapping("/addQu")
    public Result addQuestion(@RequestParam("question")String question){
        int i = baseService.addQuestion(question);
        Map<String,Object> result = new HashMap<>();
        result.put("result",i);
        return new JsonResult<>(result);
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "answer",value = "答案",dataType = "String"),
            @ApiImplicitParam(name = "questionid",value = "问题id",dataType = "String")
    })
    @ApiOperation("解答问题")
    @PostMapping("/answer")
    public int getAnswer(@RequestParam("answer")String answer,@RequestParam("questionId")String questionId){
        int i = baseService.upQuestion(questionId, answer);
        return i;
    }

    @ApiOperation(value = "根据问题片段模糊查询",response = ZsQuestion.class)
    @GetMapping("/likeQuestion")
    public Result selLikeQuestion(@RequestParam(name = "question",required = false)String question){
        List<ZsQuestion> zsQuestion = baseService.selLikeQuestion(question);
        return new JsonResult<>(zsQuestion);
    }



}
