package com.simba.scaffold.zhongsen.controller;

import com.simba.scaffold.zhongsen.domain.dto.ZhaoPinDto;
import com.simba.scaffold.zhongsen.domain.entity.ZsZhaopin;
import com.simba.scaffold.zhongsen.domain.params.ZsZhaopinParams;
import com.simba.scaffold.zhongsen.service.IZsZhaopinService;
import fykj.microservice.core.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import result.JsonResult;
import result.Result;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * 前端控制器
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@RestController
@RequestMapping("/zszhaopin")
@Api(tags = "人才招聘相关接口")
public class ZsZhaopinController extends BaseController<IZsZhaopinService, ZsZhaopin,ZsZhaopinParams> {

    @Resource
    private IZsZhaopinService zsZhaopinService;


    @ApiOperation("查询人才理念相关文章信息")
    @GetMapping("/concept")
    public Result talentConcept(){
        List<ZsZhaopin> zsZhaopin = zsZhaopinService.talentConcept();
        return new JsonResult<>(zsZhaopin);
    }

    @ApiOperation("查询团队风采相关文章信息")
    @GetMapping("/fengcai")
    public Result teamElegantDemeanour(){
        List<ZsZhaopin> zsZhaopin = zsZhaopinService.teamElegantDemeanour();
        return new JsonResult<>(zsZhaopin);
    }

    @ApiImplicitParam(name="name",value = "标题内容部分字段",dataType = "String")
    @ApiOperation(value = "模糊查询人才理念的相关信息",response = ZsZhaopin.class)
    @GetMapping("/fuzzyTalentConcept")
    public Result selTalentConceptLikeName(@RequestParam("name") String name){
        List<ZsZhaopin> zsZhaopins = baseService.selTalentConceptLikeName(name);
        return new JsonResult<>(zsZhaopins);
    }

    @ApiImplicitParam(name="name",value = "标题内容部分字段",dataType = "String")
    @ApiOperation(value = "模糊查询团队风采的相关信息",response = ZsZhaopin.class)
    @GetMapping("/fuzzyFengCai")
    public Result selFengCaiLikeName(@RequestParam("name") String name){
        List<ZsZhaopin> zsZhaopins = baseService.selFengCaiLikeName(name);
        return new JsonResult<>(zsZhaopins);
    }

    @ApiOperation("增加人才理念相关文章")
    @PostMapping(value = "/addTalentConcept")
    public Result addTalentConcept(@RequestBody ZhaoPinDto zhaoPinDto){
//        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();
        Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果
//        result = baseService.upPhoto(file, "concept");
        ZsZhaopin zsZhaopin = new ZsZhaopin();
        zsZhaopin.setName(zhaoPinDto.getName());
        zsZhaopin.setContent(zhaoPinDto.getContent());
        zsZhaopin.setAssophoto(zhaoPinDto.getFileName());
        zsZhaopin.setPhotoUrl(zhaoPinDto.getRelativePath());
        zsZhaopin.setShortDescription(zhaoPinDto.getDescription());
        int i = baseService.addTalentConcept(zsZhaopin);
        result.put("result",i);
        return new JsonResult<>(result);
    }

    @ApiOperation("增加团队风采相关文章")
    @PostMapping(value = "/addFengCai")
    public Result addFengCai(@RequestBody ZhaoPinDto zhaoPinDto){
//        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();
        Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果
//        result = baseService.upPhoto(file, "fengcai");
        ZsZhaopin zsZhaopin = new ZsZhaopin();
        zsZhaopin.setName(zhaoPinDto.getName());
        zsZhaopin.setContent(zhaoPinDto.getContent());
        zsZhaopin.setAssophoto(zhaoPinDto.getFileName());
        zsZhaopin.setPhotoUrl(zhaoPinDto.getRelativePath());
        zsZhaopin.setShortDescription(zhaoPinDto.getDescription());
        int i = baseService.addFengCai(zsZhaopin);
        result.put("result",i);
        return new JsonResult<>(result);
    }


}
