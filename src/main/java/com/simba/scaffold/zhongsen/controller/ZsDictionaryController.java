package com.simba.scaffold.zhongsen.controller;

import com.simba.scaffold.zhongsen.domain.dto.DictionaryDto;
import com.simba.scaffold.zhongsen.domain.entity.ZsDictionary;
import com.simba.scaffold.zhongsen.domain.entity.ZsQuestion;
import com.simba.scaffold.zhongsen.domain.params.ZsDictionaryParams;
import com.simba.scaffold.zhongsen.service.IZsDictionaryService;
import fykj.microservice.core.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import result.JsonResult;
import result.Result;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * 前端控制器
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@RestController
@RequestMapping("/zsdictionary")
@Api(tags = "杂项接口")
public class ZsDictionaryController extends BaseController<IZsDictionaryService, ZsDictionary,ZsDictionaryParams> {

    @Resource
    private IZsDictionaryService zsDictionaryService;

    /**
     * 查询 PROJECT COMPLETED等5条数据统计信息
     * @return
     */
    @ApiOperation("查询 PROJECT COMPLETED等5条数据统计信息")
    @PostMapping("/selectMessage")
    public List<ZsDictionary> selectMessage(){
        List<ZsDictionary> message = zsDictionaryService.getProMessage();
        return message;
    }


    @ApiImplicitParam(name = "newTitle",value = "新标题名称",required = true,dataType = "String")
    @ApiOperation("更新首页中的标题")
    @GetMapping("/upTitle")
    public Result upTitle(@RequestParam("newTitle") String title){
        int i = zsDictionaryService.upTitle(title);
        Map<String,Object> result = new HashMap<>();
        return new JsonResult<>(result.put("result",i));
    }

    @ApiOperation(value = "查询联系我们的信息",response = ZsDictionary.class)
    @GetMapping("/telUs")
    public Result selTelUs(){
        ZsDictionary zsDictionary = zsDictionaryService.selTelUs();
        return new JsonResult<>(zsDictionary);
    }


    @ApiOperation(value = "意见箱内容提交")
    @GetMapping("/option")
    public Result subOpinion(@RequestParam("opinion") String opinion){
        int i = zsDictionaryService.subOpinion(opinion);
        Map<String,Object> result = new HashMap<>();
        return new JsonResult<>(result.put("result",i));
    }


    @ApiOperation(value = "获取公司定位坐标",response = ZsDictionary.class)
    @GetMapping("/coordinates")
    public Result selCoordinates(){
        ZsDictionary zsDictionary = zsDictionaryService.selCoordinates();
        return new JsonResult<>(zsDictionary);
    }


//    @ApiOperation("修改联系我们的相关信息")
//    @PostMapping("/uptelus")
    public Result upTelUs(@RequestParam("telus")String telUs,@RequestParam("type")String type){
        int i = zsDictionaryService.upByType(type,"",telUs);
        Map<String,Object> result = new HashMap<>();
        return new JsonResult<>(result.put("result",i));
    }

    @ApiOperation(value = "查询首页轮播图上的主题文字",response = ZsDictionary.class)
    @GetMapping("/title")
    public Result selTitle(){
        ZsDictionary zsDictionary = zsDictionaryService.selTitle();
        return new JsonResult<>(zsDictionary);
    }

    @ApiOperation("更新统计数据")
    @GetMapping("/upMessage")
    public Result upMessage(DictionaryDto dictionaryDto){
        int i = baseService.upMessage(dictionaryDto);
        Map<String,Object> result = new HashMap<>();
        return new JsonResult<>(result.put("result",i));
    }

    @ApiOperation(value = "查询全部意见",response = ZsDictionary.class)
    @GetMapping("/allOpinion")
    public Result selAllOpinion(){
        List<ZsDictionary> zsDictionarys = baseService.selAllOpinion();
        return new JsonResult<>(zsDictionarys);
    }

    @ApiOperation(value = "模糊查询意见",response = ZsDictionary.class)
    @GetMapping("/likeOpinion")
    public Result selLikeOpinion(@RequestParam("opinion")String opinion){
        List<ZsDictionary> zsDictionarys = baseService.selLikeOpinion(opinion);
        return new JsonResult<>(zsDictionarys);
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "address",value = "地址信息",dataType = "String"),
            @ApiImplicitParam(name = "tel",value = "电话",dataType = "String"),
            @ApiImplicitParam(name = "email",value = "邮箱",dataType = "String")
    })
    @ApiOperation("更新底部栏联系我们中的信息")
    @GetMapping("/upTelus")
    public Result upTelUs(@RequestParam(value = "address",required = false)String address,
                       @RequestParam(value = "tel",required = false)String tel,
                       @RequestParam(value = "email",required = false)String email){
        int i = baseService.upTelUs(address, tel, email);
        Map<String,Object> result = new HashMap<>();
        return new JsonResult<>(result.put("result",i));
    }

    @ApiOperation("增加联系我们的类型选择")
    @PostMapping("/addType")
    public Result addTelType(@RequestBody ZsDictionary zsDictionary){
        int i = baseService.addTelType(zsDictionary);
        Map<String,Object> result = new HashMap<>();
        return new JsonResult<>(result.put("result",i));
    }

    @ApiOperation("查询联系我们的类型（首页）")
    @GetMapping("/selType")
    public Result selTelType(@RequestParam(value = "type",required = false) String type){
        List<ZsDictionary> zsDictionarys = baseService.selTelType(type);
        return new JsonResult<>(zsDictionarys);
    }

    @ApiOperation("更新地址坐标，之前为空时自动新增")
    @PostMapping("/upCoordinate")
    public Result upCoordinate(@RequestBody ZsDictionary zsDictionary){
        int i = baseService.upCoordinate(zsDictionary);
        return new JsonResult<>(i);
    }

}
