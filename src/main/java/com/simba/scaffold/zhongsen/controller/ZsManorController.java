package com.simba.scaffold.zhongsen.controller;

import com.simba.scaffold.zhongsen.domain.dto.ManorDto;
import com.simba.scaffold.zhongsen.domain.entity.ZsCompany;
import com.simba.scaffold.zhongsen.domain.entity.ZsManor;
import com.simba.scaffold.zhongsen.domain.params.ZsManorParams;
import com.simba.scaffold.zhongsen.service.IZsManorService;
import fykj.microservice.core.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import result.JsonResult;
import result.Result;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * 前端控制器
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@RestController
@RequestMapping("/zsmanor")
@Api(tags = "庄园风光相关接口")
public class ZsManorController extends BaseController<IZsManorService, ZsManor,ZsManorParams> {

    @Resource
    private IZsManorService zsManorService;


    @ApiOperation("查询庄园规划相关信息")
    @GetMapping("/plan")
    public Result manorPlan(){
        List<ZsManor> zsManor = zsManorService.manorPlan();
        return new JsonResult<>(zsManor);
    }

    @ApiOperation("查询优势展示相关信息")
    @GetMapping("/advantage")
    public Result advantageShow(){
        List<ZsManor> zsManor = zsManorService.advantageShow();
        return new JsonResult<>(zsManor);
    }

    @ApiOperation("查询风光实景相关信息")
    @GetMapping("/scenery")
    public Result selScenery(){
        List<ZsManor> zsManor = zsManorService.selScenery();
        return new JsonResult<>(zsManor);
    }

    @ApiOperation("查询游乐体验相关信息")
    @GetMapping("/amusement")
    public Result selAmusement(){
        List<ZsManor> zsManor = zsManorService.selAmusement();
        return new JsonResult<>(zsManor);
    }

    @ApiOperation("查询果蔬采摘相关信息")
    @GetMapping("/picking")
    public Result selPicking(){
        List<ZsManor> zsManor = zsManorService.selPicking();
        return new JsonResult<>(zsManor);
    }


    @ApiImplicitParam(name="name",value = "标题内容部分字段",dataType = "String")
    @ApiOperation(value = "模糊查询庄园规划的相关信息",response = ZsManor.class)
    @GetMapping("/fuzzySelPlan")
    public Result selPlanLikeName(@RequestParam("name") String name){
        List<ZsManor> zsManors = baseService.selPlanLikeName(name);
        return new JsonResult<>(zsManors);
    }

    @ApiImplicitParam(name="name",value = "标题内容部分字段",dataType = "String")
    @ApiOperation(value = "模糊查询优势展示的相关信息",response = ZsManor.class)
    @GetMapping("/fuzzySelAdvantage")
    public Result selAdvantageLikeName(@RequestParam("name") String name){
        List<ZsManor> zsManors = baseService.selAdvantageLikeName(name);
        return new JsonResult<>(zsManors);
    }

    @ApiImplicitParam(name="name",value = "标题内容部分字段",dataType = "String")
    @ApiOperation(value = "模糊查询风光实景的相关信息",response = ZsManor.class)
    @GetMapping("/fuzzySelScenery")
    public Result selSceneryLikeName(@RequestParam("name") String name){
        List<ZsManor> zsManors = baseService.selSceneryLikeName(name);
        return new JsonResult<>(zsManors);
    }

    @ApiImplicitParam(name="name",value = "标题内容部分字段",dataType = "String")
    @ApiOperation(value = "模糊查询游乐体验的相关信息",response = ZsManor.class)
    @GetMapping("/fuzzySelAmusement")
    public Result selAmusementLikeName(@RequestParam("name") String name){
        List<ZsManor> zsManors = baseService.selAmusementLikeName(name);
        return new JsonResult<>(zsManors);
    }

    @ApiImplicitParam(name="name",value = "标题内容部分字段",dataType = "String")
    @ApiOperation(value = "模糊查询果蔬采摘的相关信息",response = ZsManor.class)
    @GetMapping("/fuzzySelPicking")
    public Result selPickingLikeName(@RequestParam("name") String name){
        List<ZsManor> zsManors = baseService.selPickingLikeName(name);
        return new JsonResult<>(zsManors);
    }


    @ApiOperation("增加庄园规划相关文章")
    @PostMapping(value = "/addPlan")
    public int addPlanInfo(@RequestBody ManorDto manorDto){

        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();
        Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果
//        result = baseService.upPhoto(file, "plan");
        ZsManor zsManor = new ZsManor();
        zsManor.setName(manorDto.getName());
        zsManor.setContent(manorDto.getContent());
        zsManor.setAssophoto(manorDto.getFileName());
        zsManor.setPhotoUrl(manorDto.getRelativePath());
        zsManor.setShortDescription(manorDto.getDescription());
        int i = baseService.addPlanInfo(zsManor);
        return i;
    }

    @ApiOperation("增加优势展示相关文章")
    @PostMapping(value = "/addAdvantage")
    public Result addAdvantageInfo(@RequestBody ManorDto manorDto){
        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();
        Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果
//        result = baseService.upPhoto(file, "advantage");
        ZsManor zsManor = new ZsManor();
        zsManor.setName(manorDto.getName());
        zsManor.setContent(manorDto.getContent());
        zsManor.setAssophoto(manorDto.getFileName());
        zsManor.setPhotoUrl(manorDto.getRelativePath());
        zsManor.setShortDescription(manorDto.getDescription());
        int i = baseService.addAdvantageInfo(zsManor);
        result.put("result",i);
        return new JsonResult<>(result);
    }

    @ApiOperation("增加风光实景相关文章")
    @PostMapping(value = "/addScenery")
    public Result addSceneryInfo(@RequestBody ManorDto manorDto){

//        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();
        Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果
//        result = baseService.upPhoto(file, "scenery");
        ZsManor zsManor = new ZsManor();
        zsManor.setName(manorDto.getName());
        zsManor.setContent(manorDto.getContent());
        zsManor.setAssophoto(manorDto.getFileName());
        zsManor.setPhotoUrl(manorDto.getRelativePath());
        zsManor.setShortDescription(manorDto.getDescription());
        int i = baseService.addSceneryInfo(zsManor);
        result.put("result",i);
        return new JsonResult<>(result);
    }

    @ApiOperation("增加游乐体验相关文章")
    @PostMapping(value = "/addAmusement")
    public Result addAmusementInfo(@RequestBody ManorDto manorDto){

//        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();
        Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果
//        result = baseService.upPhoto(file, "amusement");
        ZsManor zsManor = new ZsManor();
        zsManor.setName(manorDto.getName());
        zsManor.setContent(manorDto.getContent());
        zsManor.setAssophoto(manorDto.getFileName());
        zsManor.setPhotoUrl(manorDto.getRelativePath());
        zsManor.setShortDescription(manorDto.getDescription());
        int i = baseService.addAmusementInfo(zsManor);
        result.put("result",i);
        return new JsonResult<>(result);
    }

    @ApiOperation("增加果蔬采摘相关文章")
    @PostMapping(value = "/addPicking")
    public Result addPickingInfo(@RequestBody ManorDto manorDto){

//        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();
        Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果
//        result = baseService.upPhoto(file, "picking");
        ZsManor zsManor = new ZsManor();
        zsManor.setName(manorDto.getName());
        zsManor.setContent(manorDto.getContent());
        zsManor.setAssophoto(manorDto.getFileName());
        zsManor.setPhotoUrl(manorDto.getRelativePath());
        zsManor.setShortDescription(manorDto.getDescription());
        int i = baseService.addPickingInfo(zsManor);
        result.put("result",i);
        return new JsonResult<>(result);
    }

}
