package com.simba.scaffold.zhongsen.controller;

import com.simba.scaffold.zhongsen.domain.entity.ZsCustomer;
import com.simba.scaffold.zhongsen.domain.entity.ZsUser;
import com.simba.scaffold.zhongsen.domain.params.ZsUserParams;
import com.simba.scaffold.zhongsen.service.IZsUserService;
import fykj.microservice.core.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import result.JsonResult;
import result.Result;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * 前端控制器
 * @author wangming
 * @email ${email}
 * @date 2021-07-27
 */
@RestController
@RequestMapping("/zsuser")
@Api(tags = "新增人员相关接口")
public class ZsUserController extends BaseController<IZsUserService, ZsUser,ZsUserParams> {

    @ApiOperation("根据姓名模糊查询人员信息")
    @GetMapping("/selByName")
    public Result selByName(@RequestParam(value = "name",required = false)String name){
        List<ZsUser> zsUsers = baseService.selByName(name);
        return new JsonResult<>(zsUsers);
    }


}
