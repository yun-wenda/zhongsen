package com.simba.scaffold.zhongsen.controller;

import com.simba.scaffold.zhongsen.domain.entity.ZsCustomer;
import com.simba.scaffold.zhongsen.domain.params.ZsCustomerParams;
import com.simba.scaffold.zhongsen.service.IZsCustomerService;
import fykj.microservice.core.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import result.JsonResult;
import result.Result;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 *
 *
 * 前端控制器
 * @author wangming
 * @email ${email}
 * @date 2021-07-24
 */
@RestController
@RequestMapping("/zscustomer")
@Api(tags = "客户相关信息接口")
public class ZsCustomerController extends BaseController<IZsCustomerService, ZsCustomer,ZsCustomerParams> {

    @Resource
    private IZsCustomerService zsCustomerService;

    /**
    增加客户
     */
    @ApiOperation("增加客户")
    @PostMapping("/addCustomer")
    public Result addCustomer (@RequestBody ZsCustomer customer){
        String flag = "";
//        System.out.println("customer:"+customer);
        Map<String,Object>root = new HashMap<>();
        int result = zsCustomerService.addCustomer(customer);
        if(1 == result){
            flag = "保存成功";
        }else{
            flag = "保存失败";
        }
        root.put("result",flag);
        return new JsonResult<>(root);
    }



}
