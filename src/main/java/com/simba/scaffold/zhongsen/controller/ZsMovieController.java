package com.simba.scaffold.zhongsen.controller;

import com.alibaba.fastjson.JSON;
import com.simba.scaffold.zhongsen.domain.entity.ZsMovie;
import com.simba.scaffold.zhongsen.domain.entity.ZsProject;
import com.simba.scaffold.zhongsen.domain.params.ZsMovieParams;
import com.simba.scaffold.zhongsen.service.IZsMovieService;
import fykj.microservice.core.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import result.JsonResult;
import result.Result;

import javax.annotation.Resource;
import java.util.*;

/**
 *
 *
 * 前端控制器
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@RestController
@RequestMapping("/zsmovie")
@Api(tags = "视频文件接口")
public class ZsMovieController extends BaseController<IZsMovieService, ZsMovie,ZsMovieParams> {

    @Resource
    private IZsMovieService zsMovieService;


    @ApiOperation("上传视频,自动设置为当前展示视频")
    @PostMapping(value = "/upOneMovie",consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public List upOneMovie(@RequestParam("uploadFile") MultipartFile file) throws Exception{

        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();
        Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果

        result = zsMovieService.upMovie(file);
        root.add(result);

        String root_json= JSON.toJSONString(root);
//        System.out.println(root_json);
        return root;
    }

    @ApiOperation("设置展示用视频，设置展示的时候自动将前一个展示视频设为不可见")
    @PostMapping("/showMovie")
    public String showMovie(@RequestParam("name") String name){
        String result = "";

        ZsMovie zsMovie = zsMovieService.selShowMovie();
        if(!"".equals(zsMovie.getName())||!"".equals(zsMovie.getAddress())){
            zsMovie.setShowFlag(0);
            zsMovieService.unShowMovie(zsMovie.getName());
        }
        zsMovieService.showMovie(name);

        return result;
    }


    @ApiOperation(value = "根据视频名称模糊查询",response = ZsMovie.class)
    @GetMapping("/likeName")
    public Result selLikeName(@RequestParam("name")String name){
        List<ZsMovie> zsMovie = baseService.selLikeName(name);
        return new JsonResult<>(zsMovie);
    }

    @ApiOperation(value = "前台页面视频展示",response = ZsMovie.class)
    @GetMapping("/frontShow")
    public Result showMovieFront(){
        ZsMovie zsMovie = baseService.selShowMovie();
        return new JsonResult<>(zsMovie);
    }

}
