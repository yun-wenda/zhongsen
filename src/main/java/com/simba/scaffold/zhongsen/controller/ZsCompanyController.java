package com.simba.scaffold.zhongsen.controller;

import com.simba.scaffold.zhongsen.domain.entity.ZsCompany;
import com.simba.scaffold.zhongsen.domain.entity.ZsProject;
import com.simba.scaffold.zhongsen.domain.params.ZsCompanyParams;
import com.simba.scaffold.zhongsen.service.IZsCompanyService;
import fykj.microservice.core.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import result.JsonResult;
import result.Result;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * 前端控制器
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@RestController
@RequestMapping("/zscompany")
@Api(tags = "众森相关信息接口")
public class ZsCompanyController extends BaseController<IZsCompanyService, ZsCompany,ZsCompanyParams> {

    @Resource
    private IZsCompanyService zsCompanyService;

    @ApiOperation(value = "查询众森介绍信息",response = ZsCompany.class)
    @GetMapping("/about")
    public Result aboutZs(){
        List<ZsProject> zsProject = zsCompanyService.aboutZs();
        return new JsonResult<>(zsProject);
    }

    @ApiOperation(value = "查询企业愿景信息",response = ZsCompany.class)
    @GetMapping("/vision")
    public Result selVision(){
        List<ZsCompany> zsCompany = zsCompanyService.selVision();
        return new JsonResult<>(zsCompany);
    }

    @ApiOperation(value = "查询发展历程信息",response = ZsCompany.class)
    @GetMapping("/history")
    public Result selHistory(){
        List<ZsCompany> zsCompany = zsCompanyService.selHistory();
        return new JsonResult<>(zsCompany);
    }

    @ApiOperation(value = "查询企业荣誉信息",response = ZsCompany.class)
    @GetMapping("/honor")
    public Result selHonor(){
        List<ZsCompany> zsCompany = zsCompanyService.selHonor();
        return new JsonResult<>(zsCompany);
    }

    @ApiOperation(value = "查询新闻资讯信息",response = ZsCompany.class)
    @GetMapping("/news")
    public Result selNews(){
        List<ZsCompany> zsCompany = zsCompanyService.selNews();
        return new JsonResult<>(zsCompany);
    }


    @ApiImplicitParam(name="name",value = "标题内容部分字段",dataType = "String")
    @ApiOperation(value = "模糊查询（众森介绍）的相关信息",response = ZsCompany.class)
    @GetMapping("/fuzzySelIntroduce")
    public Result selIntroduceLikeTitle(@RequestParam(name = "name",required = false) String name){
        List<ZsProject> zsProject = baseService.selIntroduceLikeTitle(name);
        return new JsonResult<>(zsProject);
    }

    @ApiImplicitParam(name="name",value = "标题内容部分字段",dataType = "String")
    @ApiOperation(value = "模糊查询企业愿景的相关信息",response = ZsCompany.class)
    @GetMapping("/fuzzySelVision")
    public Result selVisionLikeName(@RequestParam("name") String name){
        List<ZsCompany> zsCompanies = baseService.selVisionLikeName(name);
        return new JsonResult<>(zsCompanies);
    }

    @ApiImplicitParam(name="name",value = "标题内容部分字段",dataType = "String")
    @ApiOperation(value = "模糊查询发展历程的相关信息",response = ZsCompany.class)
    @GetMapping("/fuzzySelHistory")
    public Result selHistoryLikeName(@RequestParam("name") String name){
        List<ZsCompany> zsCompanies = baseService.selHistoryLikeName(name);
        return new JsonResult<>(zsCompanies);
    }

    @ApiImplicitParam(name="name",value = "标题内容部分字段",dataType = "String")
    @ApiOperation(value = "模糊查询企业荣誉的相关信息",response = ZsCompany.class)
    @GetMapping("/fuzzySelHonor")
    public Result selHonorLikeName(@RequestParam("name") String name){
        List<ZsCompany> zsCompanies = baseService.selHonorLikeName(name);
        return new JsonResult<>(zsCompanies);
    }

    @ApiImplicitParam(name="name",value = "标题内容部分字段",dataType = "String")
    @ApiOperation(value = "模糊查询新闻资讯的相关信息",response = ZsCompany.class)
    @GetMapping("/fuzzySelNews")
    public Result selNewsLikeName(@RequestParam("name") String name){
        List<ZsCompany> zsCompanies = baseService.selNewsLikeName(name);
        return new JsonResult<>(zsCompanies);
    }

    @ApiOperation("增加企业愿景相关文章")
    @PostMapping(value = "/addVision")
    public Result addCompanyVision(@RequestBody ZsCompany company){
//        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();
        Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果
//        result = baseService.upPhoto(file, "vision");
        ZsCompany zsCompany = new ZsCompany();
        zsCompany.setName(company.getName());
        zsCompany.setContent(company.getContent());
        zsCompany.setAssophoto(company.getAssophoto());
        zsCompany.setPhotoUrl(company.getPhotoUrl());
        zsCompany.setShortDescription(company.getShortDescription());
        int i = baseService.addCompanyVision(zsCompany);
        result.put("result",i);
        return new JsonResult<>(result);
    }


    @ApiOperation("增加发展历程相关文章")
    @PostMapping(value = "/addHistory")
    public Result addHistory(@RequestBody ZsCompany company){
//        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();
        Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果
//        result = baseService.upPhoto(file, "history");
        ZsCompany zsCompany = new ZsCompany();
        zsCompany.setName(company.getName());
        zsCompany.setContent(company.getContent());
        zsCompany.setAssophoto(company.getAssophoto());
        zsCompany.setPhotoUrl(company.getPhotoUrl());
        zsCompany.setShortDescription(company.getShortDescription());
        zsCompany.setDevYear(company.getDevYear());
        int i = baseService.addHistory(zsCompany);
        result.put("result",i);
        return new JsonResult<>(result);
    }

    @ApiOperation("增加企业荣誉相关文章")
    @PostMapping(value = "/addhonor")
    public Result addHonorInfo(@RequestBody ZsCompany company){

//        List<Map<String,Object>> root=new ArrayList<Map<String,Object>>();
        Map<String,Object> result=new HashMap<String, Object>();//一个文件上传的结果
//        result = baseService.upPhoto(file, "honor");
        ZsCompany zsCompany = new ZsCompany();
        zsCompany.setName(company.getName());
        zsCompany.setContent(company.getContent());
        zsCompany.setAssophoto(company.getAssophoto());
        zsCompany.setPhotoUrl(company.getPhotoUrl());
        zsCompany.setShortDescription(company.getShortDescription());
        int i = baseService.addHonorInfo(zsCompany);
        result.put("result",i);
        return new JsonResult<>(result);
    }

}
