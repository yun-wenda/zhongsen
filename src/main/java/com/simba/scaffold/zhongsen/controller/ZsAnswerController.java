package com.simba.scaffold.zhongsen.controller;

import com.simba.scaffold.zhongsen.domain.entity.ZsAnswer;
import com.simba.scaffold.zhongsen.domain.params.ZsAnswerParams;
import com.simba.scaffold.zhongsen.service.IZsAnswerService;
import fykj.microservice.core.base.BaseController;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 *
 * 前端控制器
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
//@RestController
//@RequestMapping("/zsanswer")
//@Api(tags = "问题答案接口")
//答案合并到问题表中，暂时不用该接口
public class ZsAnswerController extends BaseController<IZsAnswerService, ZsAnswer,ZsAnswerParams> {

}
