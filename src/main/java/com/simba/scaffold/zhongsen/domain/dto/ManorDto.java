package com.simba.scaffold.zhongsen.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode()
@Accessors(chain = true)
public class ManorDto {

    @ApiModelProperty(value = "图片展示地址")
    private String relativePath;

    @ApiModelProperty(value = "图片名称")
    private String fileName;

    @ApiModelProperty(value = "标题")
    private String name;

    @ApiModelProperty(value = "内容")
    private String content;

    @ApiModelProperty(value = "简短描述")
    private String description;

    @ApiModelProperty(value = "文章类型")
    private int type;
}
