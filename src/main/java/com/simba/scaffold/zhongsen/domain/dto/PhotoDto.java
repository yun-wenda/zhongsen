package com.simba.scaffold.zhongsen.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode()
@Accessors(chain = true)
public class PhotoDto {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "图片名称")
    private String name;



}
