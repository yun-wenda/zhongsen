package com.simba.scaffold.zhongsen.domain.params;

import fykj.microservice.core.base.BaseParams;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 
 * 查询参数
 *
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class ZsCustomerParams extends BaseParams {
	
}
