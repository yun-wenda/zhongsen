package com.simba.scaffold.zhongsen.domain.params;

import fykj.microservice.core.base.BaseParams;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 
 * 查询参数
 *
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class ZsQuestionParams extends BaseParams {

    @ApiModelProperty(value = "客户名称")
    private String customer;


    @ApiModelProperty(value = "问题类型")
    private String questionType;


    @ApiModelProperty(value = "问题编号")
    private String questionId;


    @ApiModelProperty(value = "问题内容")
    private String question;


    @ApiModelProperty(value = "答复")
    private String answer;
	
}
