package com.simba.scaffold.zhongsen.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import fykj.microservice.core.base.BaseEntity;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 *
 *
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("zs_question")
public class ZsQuestion extends BaseEntity  {

	private static final long serialVersionUID = 1L;

	/**
	 * 客户名称
	 */
	@TableField("customer")
    @ApiModelProperty(value = "客户名称")
		private String customer;

	/**
	 * 问题类型
	 */
	@TableField("questionType")
    @ApiModelProperty(value = "问题类型")
		private String questiontype;

	/**
	 * 问题编号
	 */
	@TableField("questionid")
    @ApiModelProperty(value = "问题编号")
		private String questionid;

	/**
	 * 问题描述
	 */
	@TableField("question")
    @ApiModelProperty(value = "问题描述")
		private String question;

	/**
	 * 是否已解答  0：否      1：是
	 */
	@TableField("isanswer")
    @ApiModelProperty(value = "是否已解答  0：否      1：是")
		private Integer isanswer;

	/**
	 * 问题答案
	 */
	@TableField("answer")
	@ApiModelProperty(value = "问题答案")
	private String answer;

}
