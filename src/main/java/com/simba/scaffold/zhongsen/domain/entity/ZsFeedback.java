package com.simba.scaffold.zhongsen.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import fykj.microservice.core.base.BaseEntity;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * 
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("zs_feedback")
public class ZsFeedback extends BaseEntity  {

	private static final long serialVersionUID = 1L;

	/**
	 * 客户名称
	 */
	@TableField("customer")
    @ApiModelProperty(value = "客户名称")
		private String customer;

	/**
	 * 客户反馈内容
	 */
	@TableField("feedback")
    @ApiModelProperty(value = "客户反馈内容")
		private String feedback;


}
