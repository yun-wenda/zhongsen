package com.simba.scaffold.zhongsen.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import fykj.microservice.core.base.BaseEntity;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * 
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("zs_answer")
public class ZsAnswer extends BaseEntity  {

	private static final long serialVersionUID = 1L;

	/**
	 * 客户名称
	 */
	@TableField("customer")
    @ApiModelProperty(value = "客户名称")
		private String customer;

	/**
	 * 问题编号
	 */
	@TableField("questionid")
    @ApiModelProperty(value = "问题编号")
		private String questionid;

	/**
	 * 答复
	 */
	@TableField("answer")
    @ApiModelProperty(value = "答复")
		private String answer;


}
