package com.simba.scaffold.zhongsen.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import fykj.microservice.core.base.BaseEntity;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 *
 *
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("zs_zhaopin")
public class ZsZhaopin extends BaseEntity  {

	private static final long serialVersionUID = 1L;

	/**
	 * 标题
	 */
	@TableField("name")
    @ApiModelProperty(value = "标题")
		private String name;

	/**
	 * 内容类型：1.人才理念 2.团队风采 3.加入众森
	 */
	@TableField("type")
    @ApiModelProperty(value = "内容类型：1.人才理念 2.团队风采 3.加入众森")
		private Integer type;

	/**
	 * 具体内容
	 */
	@TableField("content")
    @ApiModelProperty(value = "具体内容")
		private String content;

	/**
	 * 关联图片的名称
	 */
	@TableField("assoPhoto")
    @ApiModelProperty(value = "关联图片的名称")
		private String assophoto;

	/**
	 * 图片路径
	 */
	@TableField("photourl")
	@ApiModelProperty(value = "图片路径")
	private String photoUrl;

	/**
	 * 简短描述
	 */
	@TableField("shortdescription")
	@ApiModelProperty(value = "简短描述")
	private String shortDescription;

}
