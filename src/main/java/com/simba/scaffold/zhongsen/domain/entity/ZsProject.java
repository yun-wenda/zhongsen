package com.simba.scaffold.zhongsen.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import fykj.microservice.core.base.BaseEntity;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 *
 *
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("zs_project")
public class ZsProject extends BaseEntity  {

	private static final long serialVersionUID = 1L;

	/**
	 * 标题
	 */
	@TableField("name")
    @ApiModelProperty(value = "标题")
		private String name;

	/**
	 * 具体内容
	 */
	@TableField("content")
    @ApiModelProperty(value = "具体内容")
		private String content;

	/**
	 * 文章所属：0.其他/1.正常/2.主推/3.探索/4.关于我们
	 */
	@TableField("type")
    @ApiModelProperty(value = "文章所属：0.其他/1.正常/2.主推/3.探索/4.关于我们")
		private Integer type;

	/**
	 * 是否展示在首页上：1.是   0.否
	 */
	@TableField("isFlag")
    @ApiModelProperty(value = "是否展示在首页上：1.是   0.否")
		private Integer isflag;


	/**
	 * 关联图片名称
	 */
	@TableField("assophoto")
	@ApiModelProperty(value = "关联图片名称")
	private String assoPhoto;

	/**
	 * 图片路径
	 */
	@TableField("photourl")
	@ApiModelProperty(value = "图片路径")
	private String photoUrl;

	/**
	 * 简短描述
	 */
	@TableField("shortdescription")
	@ApiModelProperty(value = "简短描述")
	private String shortDescription;

}
