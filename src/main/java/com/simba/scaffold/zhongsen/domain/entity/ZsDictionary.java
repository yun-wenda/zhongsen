package com.simba.scaffold.zhongsen.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import fykj.microservice.core.base.BaseEntity;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * 
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("zs_dictionary")
public class ZsDictionary extends BaseEntity  {

	private static final long serialVersionUID = 1L;

	/**
	 * 关联标记
	 */
	@TableField("s_id")
    @ApiModelProperty(value = "关联标记")
		private String sid;

	/**
	 * 标题
	 */
	@TableField("title")
    @ApiModelProperty(value = "标题")
		private String title;

	/**
	 * 内容
	 */
	@TableField("content")
    @ApiModelProperty(value = "内容")
		private String content;

	/**
	 * 备用字段1
	 */
	@TableField("exp1")
	@ApiModelProperty(value = "备用字段1")
	private String exp1;

	/**
	 * 备用字段2
	 */
	@TableField("exp2")
	@ApiModelProperty(value = "备用字段2")
	private String exp2;

}
