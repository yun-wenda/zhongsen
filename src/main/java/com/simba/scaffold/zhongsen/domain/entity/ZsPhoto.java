package com.simba.scaffold.zhongsen.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import fykj.microservice.core.base.BaseEntity;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 *
 *
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("zs_photo")
public class ZsPhoto extends BaseEntity  {

	private static final long serialVersionUID = 1L;

	/**
	 * 关联标题
	 */
	@TableField("s_id")
    @ApiModelProperty(value = "关联标题")
	private String sid;

	/**
	 * 文件类型
	 */
	@TableField("type")
    @ApiModelProperty(value = "文件类型")
		private String type;

	/**
	 * 文件存放地址
	 */
	@TableField("address")
    @ApiModelProperty(value = "文件存放地址")
		private String address;

	/**
	 * 文件名称
	 */
	@TableField("name")
    @ApiModelProperty(value = "文件名称")
		private String name;

	/**
	 * 文件名称
	 */
	@TableField("showflag")
	@ApiModelProperty(value = "展示标志：0.不展示 1.展示")
		private Integer showFlag;

	/**
	 * 对外展示路径
	 */
	@TableField("showurl")
	@ApiModelProperty(value = "对外展示路径")
		private String showurl;

}
