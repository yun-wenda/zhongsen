package com.simba.scaffold.zhongsen.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import fykj.microservice.core.base.BaseEntity;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * 
 * @author wangming
 * @email ${email}
 * @date 2021-07-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("zs_customer")
public class ZsCustomer extends BaseEntity  {

	private static final long serialVersionUID = 1L;

	/**
	 * 姓名
	 */
	@TableField("cName")
    @ApiModelProperty(value = "姓名")
		private String cname;

	/**
	 * 手机号
	 */
	@TableField("phone")
    @ApiModelProperty(value = "手机号")
		private String phone;

	/**
	 * 类型
	 */
	@TableField("type")
    @ApiModelProperty(value = "类型")
		private String type;

	/**
	 * 备注
	 */
	@TableField("message")
    @ApiModelProperty(value = "备注")
		private String message;


}
