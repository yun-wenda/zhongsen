package com.simba.scaffold.zhongsen.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import fykj.microservice.core.base.BaseEntity;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * 
 * @author wangming
 * @email ${email}
 * @date 2021-07-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("zs_user")
public class ZsUser extends BaseEntity  {

	private static final long serialVersionUID = 1L;

	/**
	 * 姓名
	 */
	@TableField("name")
    @ApiModelProperty(value = "姓名")
		private String name;

	/**
	 * 电话
	 */
	@TableField("phone")
    @ApiModelProperty(value = "电话")
		private Integer phone;

	/**
	 * 描述
	 */
	@TableField("message")
    @ApiModelProperty(value = "描述")
		private String message;


}
