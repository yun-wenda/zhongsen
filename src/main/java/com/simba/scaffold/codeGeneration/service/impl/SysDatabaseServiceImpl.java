package com.simba.scaffold.codeGeneration.service.impl;

import com.simba.scaffold.codeGeneration.domain.entity.SysDatabaseEntity;
import com.simba.scaffold.codeGeneration.mapper.SysDatabaseMapper;
import com.simba.scaffold.codeGeneration.service.ISysDatabaseService;
import fykj.microservice.core.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class SysDatabaseServiceImpl extends BaseServiceImpl<SysDatabaseMapper, SysDatabaseEntity> implements ISysDatabaseService {
}
