package com.simba.scaffold.codeGeneration.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.simba.scaffold.codeGeneration.DynamicDataSource.DynamicDataSource;
import com.simba.scaffold.codeGeneration.common.utils.GenUtils;
import com.simba.scaffold.codeGeneration.domain.dto.GeneratorDto;
import com.simba.scaffold.codeGeneration.domain.params.GeneratorParams;
import com.simba.scaffold.codeGeneration.domain.vo.GeneratorVo;
import com.simba.scaffold.codeGeneration.mapper.*;
import com.simba.scaffold.codeGeneration.service.IGeneratorService;
import com.simba.scaffold.codeGeneration.service.ISysDatabaseService;
import exception.BusinessException;
import fykj.microservice.core.base.BaseServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import result.ResultCode;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

//import com.fykj.scaffold.codeGeneration.DynamicDataSource.DataSourceHelper;

@Slf4j
@Service
public class GeneratorServiceImpl extends BaseServiceImpl<GeneratorMapper, GeneratorDto> implements IGeneratorService {

    @Autowired
    private GeneratorMapper generatorMapper;

    @Autowired
    private ISysDatabaseService databaseService;

    @Autowired
    private DynamicDataSource dynamicDataSource;

    @Autowired
    private MySQLGeneratorMapper mySQLGeneratorMapper;
    @Autowired
    private OracleGeneratorMapper oracleGeneratorMapper;
    @Autowired
    private SQLServerGeneratorMapper sqlServerGeneratorMapper;
    @Autowired
    private PostgreSQLGeneratorMapper postgreSQLGeneratorMapper;

    @Override
    public IPage<GeneratorVo> queryList(IPage<GeneratorVo> page, GeneratorParams params) throws Exception {
        String databaseType = params.getDatabaseType();

        IPage<GeneratorVo> iPage = new Page<>();
        if("MySQL".equalsIgnoreCase(databaseType)){
            iPage = mySQLGeneratorMapper.queryList(page, params);
        }else if("Oracle".equalsIgnoreCase(databaseType)){
            iPage = oracleGeneratorMapper.queryList(page, params);
        }else if("SQLServer".equalsIgnoreCase(databaseType)){
            iPage = sqlServerGeneratorMapper.queryList(page, params);
        }else if("PostgreSQL".equalsIgnoreCase(databaseType)){
            iPage = postgreSQLGeneratorMapper.queryList(page, params);
        }else {
            throw new BusinessException(ResultCode.FAIL, "不支持当前数据库：" + databaseType);
        }

        return iPage;
    }

    @Override
    public Map<String, String> queryTable(String databaseType, String tableName) {
        if("MySQL".equalsIgnoreCase(databaseType)){
            return mySQLGeneratorMapper.queryTable(tableName);
        }else if("Oracle".equalsIgnoreCase(databaseType)){
            return oracleGeneratorMapper.queryTable(tableName);
        }else if("SQLServer".equalsIgnoreCase(databaseType)){
            return sqlServerGeneratorMapper.queryTable(tableName);
        }else if("PostgreSQL".equalsIgnoreCase(databaseType)){
            return postgreSQLGeneratorMapper.queryTable(tableName);
        }
        return null;
    }

    @Override
    public List<Map<String, String>> queryColumns(String databaseType, String tableName) {
        if("MySQL".equalsIgnoreCase(databaseType)){
            return mySQLGeneratorMapper.queryColumns(tableName);
        }else if("Oracle".equalsIgnoreCase(databaseType)){
            return oracleGeneratorMapper.queryColumns(tableName);
        }else if("SQLServer".equalsIgnoreCase(databaseType)){
            return sqlServerGeneratorMapper.queryColumns(tableName);
        }else if("PostgreSQL".equalsIgnoreCase(databaseType)){
            return postgreSQLGeneratorMapper.queryColumns(tableName);
        }
        return null;
    }

    @Override
    public byte[] generatorCode(String databaseType, List<String> tableNameList) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outputStream);

        for(String tableName : tableNameList){
            //查询表信息
            Map<String, String> table = queryTable(databaseType, tableName);
            //查询列信息
            List<Map<String, String>> columns = queryColumns(databaseType, tableName);
            //生成代码
            GenUtils.generatorCode(table, columns, zip);
        }
        IOUtils.closeQuietly(zip);
        return outputStream.toByteArray();
    }
}
