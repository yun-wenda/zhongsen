package com.simba.scaffold.codeGeneration.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.simba.scaffold.codeGeneration.domain.dto.GeneratorDto;
import com.simba.scaffold.codeGeneration.domain.params.GeneratorParams;
import com.simba.scaffold.codeGeneration.domain.vo.GeneratorVo;
import fykj.microservice.core.base.IBaseService;

import java.util.List;
import java.util.Map;

public interface IGeneratorService extends IBaseService<GeneratorDto> {

    IPage<GeneratorVo> queryList(IPage<GeneratorVo> page, GeneratorParams params) throws Exception;

    Map<String, String> queryTable(String databaseName, String tableName);

    List<Map<String, String>> queryColumns(String databaseName, String tableName);

    byte[] generatorCode(String databaseName, List<String> tableNameList);
}

