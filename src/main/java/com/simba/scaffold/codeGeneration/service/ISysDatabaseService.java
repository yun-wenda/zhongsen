package com.simba.scaffold.codeGeneration.service;

import com.simba.scaffold.codeGeneration.domain.entity.SysDatabaseEntity;
import fykj.microservice.core.base.IBaseService;

public interface ISysDatabaseService extends IBaseService<SysDatabaseEntity> {

}
