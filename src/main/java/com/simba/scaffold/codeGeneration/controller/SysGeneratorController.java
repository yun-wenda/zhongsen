package com.simba.scaffold.codeGeneration.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.simba.scaffold.codeGeneration.DynamicDataSource.DBContextHolder;
import com.simba.scaffold.codeGeneration.DynamicDataSource.DynamicDataSource;
import com.simba.scaffold.codeGeneration.domain.entity.SysDatabaseEntity;
import com.simba.scaffold.codeGeneration.domain.params.GeneratorParams;
import com.simba.scaffold.codeGeneration.domain.vo.GeneratorVo;
import com.simba.scaffold.codeGeneration.service.IGeneratorService;
import com.simba.scaffold.codeGeneration.service.ISysDatabaseService;
import constants.Mark;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import result.JsonResult;
import utils.StringUtil;

import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

/**
 * 代码生成器
 */
@RestController
@RequestMapping("/admin/generator")
public class SysGeneratorController {

    @Autowired
    private IGeneratorService generatorService;

    @Autowired
    private DynamicDataSource dynamicDataSource;

    @Autowired
    private ISysDatabaseService sysDatabaseService;

    /**
     * 列表
     */
    @PostMapping(value = "/page")
    public JsonResult<IPage<GeneratorVo>> page(@RequestBody(required = false) GeneratorParams params) throws Exception {
        if (params == null) {
            params = new GeneratorParams();
        }
        String databaseName = params.getDatabaseName();
        String databaseType = params.getDatabaseType();
        if (StringUtil.isEmpty(databaseName) || StringUtil.isEmpty(databaseType)) {
            JsonResult result = new JsonResult();
            result.setCode(1);
            result.setMsg("查询参数不能为空");
            return result;
        }
        SysDatabaseEntity databaseEntity = sysDatabaseService.getOne(
                new QueryWrapper<SysDatabaseEntity>().eq("database_type", databaseType).eq("database_name", databaseName)
        );
        //创建数据源连接&检查 若存在则不需重新创建
        dynamicDataSource.createDataSourceWithCheck(databaseEntity);
        //切换到该数据源
        DBContextHolder.setDataSource(databaseEntity.getId().toString());
        //查询列表数据
        Page<GeneratorVo> page = new Page<>(params.getCurrentPage(), params.getPageSize());
        IPage<GeneratorVo> list = generatorService.queryList(page, params);
        DBContextHolder.clearDataSource();
        return new JsonResult<>(list);
    }

    /**
     * 生成代码并下载
     */
    @PostMapping(value = "/code")
    public void code(@RequestParam("databaseType") String databaseType,
                     @RequestParam("databaseName") String databaseName,
                     @RequestParam("tableNames") String tableNames,
                     HttpServletResponse response) throws Exception {
        List<String> tableNameList = Arrays.asList(tableNames.split(Mark.COMMA));
        SysDatabaseEntity databaseEntity = sysDatabaseService.getOne(
                new QueryWrapper<SysDatabaseEntity>().eq("database_type", databaseType).eq("database_name", databaseName)
        );
        //创建数据源连接&检查 若存在则不需重新创建
        dynamicDataSource.createDataSourceWithCheck(databaseEntity);
        //切换到该数据源
        DBContextHolder.setDataSource(databaseEntity.getId().toString());
        byte[] data = generatorService.generatorCode(databaseType, tableNameList);
        response.reset();
        response.setHeader("Content-Disposition", "attachment; filename=\"fykj.zip\"");
        response.addHeader("Content-Length", "" + data.length);
        response.setContentType("application/zip; charset=UTF-8");
        response.addHeader("Access-Control-Allow-Origin", "http://localhost:8080");
        response.addHeader("Access-Control-Allow-Credentials", "true");
        IOUtils.write(data, response.getOutputStream());
        DBContextHolder.clearDataSource();
    }

}
