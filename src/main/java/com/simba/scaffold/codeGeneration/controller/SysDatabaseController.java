package com.simba.scaffold.codeGeneration.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.simba.scaffold.codeGeneration.domain.entity.SysDatabaseEntity;
import com.simba.scaffold.codeGeneration.domain.params.SysDatabaseParams;
import com.simba.scaffold.codeGeneration.service.ISysDatabaseService;
import fykj.microservice.core.base.BaseController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import result.JsonResult;

import java.util.List;

@RestController
@RequestMapping("/admin/sysDatabase")
public class SysDatabaseController extends BaseController<ISysDatabaseService, SysDatabaseEntity, SysDatabaseParams> {

    @PostMapping(value = "/getDatabaseList")
    public JsonResult<List<SysDatabaseEntity>> getDataInfo(SysDatabaseParams params){
        if (params == null) {
            params = new SysDatabaseParams();
        }
        String databaseType = params.getDatabaseType();

        List<SysDatabaseEntity> list = baseService.list(
                new QueryWrapper<SysDatabaseEntity>().eq("database_type", databaseType)
        );
        return new JsonResult<>(list);
    }
}
