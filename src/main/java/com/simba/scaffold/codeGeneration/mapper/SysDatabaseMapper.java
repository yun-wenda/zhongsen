package com.simba.scaffold.codeGeneration.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.simba.scaffold.codeGeneration.domain.entity.SysDatabaseEntity;

public interface SysDatabaseMapper extends BaseMapper<SysDatabaseEntity> {
}
