package com.simba.scaffold.codeGeneration.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.simba.scaffold.codeGeneration.domain.dto.GeneratorDto;
import com.simba.scaffold.codeGeneration.domain.params.GeneratorParams;
import com.simba.scaffold.codeGeneration.domain.vo.GeneratorVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 数据库接口
 */
public interface GeneratorMapper extends BaseMapper<GeneratorDto> {

    IPage<GeneratorVo> queryList(@Param("page") IPage<GeneratorVo> page, @Param("params") GeneratorParams params);

    Map<String, String> queryTable(String tableName);

    List<Map<String, String>> queryColumns(String tableName);
}
