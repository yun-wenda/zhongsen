package com.simba.scaffold.codeGeneration.mapper;

import org.springframework.context.annotation.Primary;

/**
 * MySQL代码生成器
 */
@Primary
public interface MySQLGeneratorMapper extends GeneratorMapper {

}
