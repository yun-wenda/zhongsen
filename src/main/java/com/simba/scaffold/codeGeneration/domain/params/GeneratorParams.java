package com.simba.scaffold.codeGeneration.domain.params;

import fykj.microservice.core.base.BaseParams;
import fykj.microservice.core.support.wrapper.annotation.MatchType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class GeneratorParams extends BaseParams {

    private static final long serialVersionUID = -9060155054253723859L;

    @MatchType
    private String databaseName;

    @MatchType
    private String tableName;

    @MatchType
    private String databaseType;

}
