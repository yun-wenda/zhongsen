package com.simba.scaffold.codeGeneration.domain.dto;

import fykj.microservice.core.base.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@Getter
@Setter
@NoArgsConstructor
@EntityScan
public class GeneratorDto extends BaseEntity {

    private static final long serialVersionUID = -3907295019242850519L;

    private String tableName;

    private String engine;

    private String tableComment;

}
