package com.simba.scaffold.codeGeneration.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import fykj.microservice.core.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_database")
public class SysDatabaseEntity extends BaseEntity {

    private static final long serialVersionUID = 646410046264399658L;

    @TableField("ip_address")
    private String ipAddress;

    @TableField("database_name")
    private String databaseName;

    @TableField("database_type")
    private String databaseType;

    @TableField("driver_class_name")
    private String driverClassName;

    @TableField("database_url")
    private String databaseUrl;

    @TableField("username")
    private String username;

    @TableField("password")
    private String password;
}
