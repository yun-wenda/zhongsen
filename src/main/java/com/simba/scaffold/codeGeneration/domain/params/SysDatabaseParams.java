package com.simba.scaffold.codeGeneration.domain.params;

import fykj.microservice.core.base.BaseParams;
import fykj.microservice.core.support.wrapper.annotation.MatchType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class SysDatabaseParams extends BaseParams {

    private static final long serialVersionUID = -8022764879214750512L;

    @MatchType
    private String databaseType;

}
