package com.simba.scaffold.codeGeneration.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class GeneratorVo implements Serializable {
    private static final long serialVersionUID = -1757367588511867999L;

    private String tableName;

    private String engine;

    private String tableComment;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
