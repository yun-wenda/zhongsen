/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.56.110
 Source Server Type    : MySQL
 Source Server Version : 50646
 Source Host           : 192.168.56.110:3306
 Source Schema         : generation

 Target Server Type    : MySQL
 Target Server Version : 50646
 File Encoding         : 65001

 Date: 19/03/2020 17:20:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for api_manage
-- ----------------------------
DROP TABLE IF EXISTS `api_manage`;
CREATE TABLE `api_manage`  (
  `id` bigint(19) NOT NULL COMMENT '主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '版本号（乐观锁）',
  `is_deleted` bit(1) NOT NULL COMMENT '是否已删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '最后更新时间',
  `creator` bigint(19) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '最后更新人',
  `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '接口code',
  `url` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '接口地址',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '接口请求方式',
  `param_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数类型',
  `mark` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '接口描述',
  `status` bit(1) NULL DEFAULT NULL COMMENT '接口状态',
  `response_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '第三方接口返回类型',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for api_param_manage
-- ----------------------------
DROP TABLE IF EXISTS `api_param_manage`;
CREATE TABLE `api_param_manage`  (
  `id` bigint(19) NOT NULL COMMENT '?Զ???????',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '?ֹ???',
  `is_deleted` bit(1) NOT NULL COMMENT '?߼?ɾ??',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '????ʱ?',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '????ʱ?',
  `creator` bigint(19) NULL DEFAULT NULL COMMENT '?????',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '?????',
  `type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数类型',
  `name` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数名',
  `url_id` bigint(19) NULL DEFAULT NULL COMMENT '所属接口id',
  `url_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属接口code',
  `sort_num` int(100) NULL DEFAULT NULL COMMENT '参数序号',
  `need` bit(1) NULL DEFAULT NULL COMMENT '是否必填',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_category
-- ----------------------------
DROP TABLE IF EXISTS `cms_category`;
CREATE TABLE `cms_category`  (
  `id` bigint(19) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '更新人',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `sequence` int(10) NOT NULL DEFAULT 0 COMMENT '排序',
  `seo_title` varchar(260) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'SEO标题',
  `seo_keyword` varchar(260) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'SEO关健字',
  `seo_remark` varchar(260) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'SEO描述',
  `img_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片链接',
  `url_link` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'URL链接',
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '状态（启用，未启用）',
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '描述',
  `parent_id` bigint(19) NULL DEFAULT NULL COMMENT '父节点',
  `level` int(2) NULL DEFAULT NULL COMMENT '级别',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_category_content
-- ----------------------------
DROP TABLE IF EXISTS `cms_category_content`;
CREATE TABLE `cms_category_content`  (
  `id` bigint(19) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '更新人',
  `category_id` bigint(19) NULL DEFAULT NULL COMMENT '栏目id',
  `content_id` bigint(19) NULL DEFAULT NULL COMMENT '内容id',
  `sequence` int(10) NOT NULL DEFAULT 0 COMMENT '排序',
  `important_level` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '重要度',
  `is_stick` bit(1) NOT NULL DEFAULT b'0' COMMENT '置顶',
  `is_hot` bit(1) NOT NULL DEFAULT b'0' COMMENT '热门',
  `is_comment` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否可以评论',
  `parent_id` bigint(19) NULL DEFAULT NULL COMMENT '栏目父节点id',
  `category_list` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '栏目list id',
  `is_recycling` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否加入回收站',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_category_label
-- ----------------------------
DROP TABLE IF EXISTS `cms_category_label`;
CREATE TABLE `cms_category_label`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `updater` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `category_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '栏目id',
  `label_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标签id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_category_prop
-- ----------------------------
DROP TABLE IF EXISTS `cms_category_prop`;
CREATE TABLE `cms_category_prop`  (
  `id` bigint(19) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '更新人',
  `category_id` bigint(19) NULL DEFAULT NULL COMMENT '栏目id',
  `prop_id` bigint(19) NULL DEFAULT NULL COMMENT '扩展属性id',
  `prop_value` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '扩展属性值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_comment
-- ----------------------------
DROP TABLE IF EXISTS `cms_comment`;
CREATE TABLE `cms_comment`  (
  `id` bigint(19) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '更新人',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '评论人名',
  `comment_time` datetime(0) NULL DEFAULT NULL COMMENT '评论时间',
  `phone` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `ip` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '评论人ip',
  `is_reply` bit(1) NULL DEFAULT NULL COMMENT '是否回复',
  `reply_person` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '回复人',
  `reply_person_id` bigint(19) NULL DEFAULT NULL COMMENT '回复人id',
  `reply_time` datetime(0) NULL DEFAULT NULL COMMENT '回复时间',
  `reply_content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '回复内容',
  `category_id` bigint(19) NULL DEFAULT NULL COMMENT '栏目id',
  `content_id` bigint(19) NULL DEFAULT NULL COMMENT '内容id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_content
-- ----------------------------
DROP TABLE IF EXISTS `cms_content`;
CREATE TABLE `cms_content`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `updater` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `title` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `seo_title` varchar(260) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'SEO标题',
  `seo_keyword` varchar(260) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'SEO关健字',
  `seo_remark` varchar(260) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'SEO描述',
  `title_alias` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题别名',
  `title_img_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题图片url',
  `brief_introduction` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '简介',
  `custom_links` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '自定义链接',
  `description` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '详细描述',
  `effective_date` datetime(0) NULL DEFAULT NULL COMMENT '生效日期',
  `expiration_date` datetime(0) NULL DEFAULT NULL COMMENT '过期日期',
  `audit_status` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '审核状态',
  `is_audit` bit(1) NULL DEFAULT NULL COMMENT '是否需要审核',
  `audit_memo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '审核备注',
  `virtual_reading` int(20) NULL DEFAULT NULL COMMENT '虚拟阅读量',
  `actual_reading` int(20) NULL DEFAULT NULL COMMENT '实际阅读量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_content_attachment
-- ----------------------------
DROP TABLE IF EXISTS `cms_content_attachment`;
CREATE TABLE `cms_content_attachment`  (
  `id` bigint(19) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '更新人',
  `content_id` bigint(19) NULL DEFAULT NULL COMMENT '内容id',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `file_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件名称',
  `file_path` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件路径',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_content_prop
-- ----------------------------
DROP TABLE IF EXISTS `cms_content_prop`;
CREATE TABLE `cms_content_prop`  (
  `id` bigint(19) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '更新人',
  `category_id` bigint(19) NULL DEFAULT NULL COMMENT '栏目id',
  `prop_id` bigint(19) NULL DEFAULT NULL COMMENT '扩展属性id',
  `content_id` bigint(19) NULL DEFAULT NULL COMMENT '内容id',
  `prop_value` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '属性值',
  `is_category_relate` bit(1) NULL DEFAULT b'0' COMMENT '是否和栏目相关',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_extended_prop
-- ----------------------------
DROP TABLE IF EXISTS `cms_extended_prop`;
CREATE TABLE `cms_extended_prop`  (
  `id` bigint(19) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '更新人',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '状态（启用，未启用）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_label
-- ----------------------------
DROP TABLE IF EXISTS `cms_label`;
CREATE TABLE `cms_label`  (
  `id` bigint(19) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '更新人',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '状态（启用，未启用）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_reading_record
-- ----------------------------
DROP TABLE IF EXISTS `cms_reading_record`;
CREATE TABLE `cms_reading_record`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `updater` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `reader_user_name` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '读者用户名',
  `content_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '内容id',
  `reading_time` datetime(0) NULL DEFAULT NULL COMMENT '阅读时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for copyright
-- ----------------------------
DROP TABLE IF EXISTS `copyright`;
CREATE TABLE `copyright`  (
  `id` bigint(19) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '更新人',
  `mobile` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '服务热线',
  `fax` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '传真',
  `address` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
  `copyright_company` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '版权公司',
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属网站',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for oauth_client_details
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_details`;
CREATE TABLE `oauth_client_details`  (
  `client_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `resource_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `client_secret` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `scope` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `authorized_grant_types` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `web_server_redirect_uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `authorities` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `access_token_validity` int(11) NULL DEFAULT NULL,
  `refresh_token_validity` int(11) NULL DEFAULT NULL,
  `additional_information` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `autoapprove` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'false',
  PRIMARY KEY (`client_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of oauth_client_details
-- ----------------------------
INSERT INTO `oauth_client_details` VALUES ('client_2', 'order', '{bcrypt}$2a$04$NqiScrd3C57bCFTOynqfEu.3MIpiI2wtL1OzxHCneMyWdWeIBNA9C', 'select', 'password,refresh_token', '', 'client', 3600, 3660, NULL, 'false');
INSERT INTO `oauth_client_details` VALUES ('client_3', 'order', '{bcrypt}$2a$04$NqiScrd3C57bCFTOynqfEu.3MIpiI2wtL1OzxHCneMyWdWeIBNA9C', 'all', 'authorization_code', 'http://www.baidu.com', NULL, 3600, 3600, NULL, 'false');

-- ----------------------------
-- Table structure for schedule_job
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job`;
CREATE TABLE `schedule_job`  (
  `id` bigint(19) NOT NULL,
  `id_bak` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL,
  `updater` bigint(19) NULL DEFAULT NULL,
  `bean_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数',
  `cron_expression` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'cron表达式',
  `status` bit(1) NULL DEFAULT b'1' COMMENT '状态（启用，未启用）',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of schedule_job
-- ----------------------------
INSERT INTO `schedule_job` VALUES (1207590690766450689, '07008622e5bc1f30633390de080abdc0', 16, b'0', '2019-10-21 15:48:08', '2019-10-22 09:16:57', 1207590691320098817, 1207590691320098817, 'testTask3', 'testTask3', '0/20 * * * * ?', b'1', 'testTask3');
INSERT INTO `schedule_job` VALUES (1207590691185881089, '7419ece186eb518d01eb474cc1942ad5', 14, b'0', '2019-10-29 14:11:46', '2019-10-29 18:15:09', 1207590691320098817, 1207590691320098817, '123', '1', '0 0 12 * * ?', b'1', '<a onclick=\"alert(\'hello\')\"><img width=\"20px\" height=\"20px\"></img></a>');

-- ----------------------------
-- Table structure for schedule_job_log
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job_log`;
CREATE TABLE `schedule_job_log`  (
  `id` bigint(19) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '更新人',
  `job_id` bigint(19) NULL DEFAULT NULL COMMENT '任务id',
  `bean_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数',
  `status` bit(1) NULL DEFAULT b'1' COMMENT '任务状态   1：成功   0：失败',
  `error` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '失败信息',
  `times` int(11) NULL DEFAULT NULL COMMENT '耗时(单位：毫秒)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_advice
-- ----------------------------
DROP TABLE IF EXISTS `sys_advice`;
CREATE TABLE `sys_advice`  (
  `id` bigint(19) NOT NULL,
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '更新人',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标题',
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_advice
-- ----------------------------
INSERT INTO `sys_advice` VALUES (1207590691190075393, 0, b'0', '2019-11-22 10:32:52', '2019-11-22 10:32:52', 1207590691320098817, 1207590691320098817, '我要咨询', 'advice');
INSERT INTO `sys_advice` VALUES (1207590691198464002, 0, b'0', '2019-11-22 15:06:54', '2019-11-22 15:06:54', 1207590691320098817, 1207590691320098817, '投诉建议', 'suggestion');

-- ----------------------------
-- Table structure for sys_advice_field
-- ----------------------------
DROP TABLE IF EXISTS `sys_advice_field`;
CREATE TABLE `sys_advice_field`  (
  `id` bigint(19) NOT NULL,
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL,
  `updater` bigint(19) NULL DEFAULT NULL,
  `code` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编号',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字段类型编号',
  `is_required` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否必填',
  `size_limit` int(10) NULL DEFAULT NULL COMMENT '限制字数长度',
  `sequence` int(10) NOT NULL DEFAULT 0 COMMENT '排序',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `is_options` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否是选择项（冗余）',
  `advice_id` bigint(19) NULL DEFAULT NULL,
  `advice_id_bak` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '咨询建议主键',
  `advice_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '咨询建议标题',
  `check_rule` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '校验规则',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_advice_field
-- ----------------------------
INSERT INTO `sys_advice_field` VALUES (1207590691206852609, 3, b'0', '2019-11-22 14:48:34', '2019-11-27 16:57:59', 1207590691320098817, 1207590691320098817, 'mobile', '联系电话', 'text', b'1', 15, 1, '联系电话', b'0', 1207590691190075393, '75248b6ed423fc6661f95d502107840f', '我要咨询', '^1[3456789]\\d{9}$');
INSERT INTO `sys_advice_field` VALUES (1207590691215241218, 0, b'0', '2019-11-22 14:47:59', '2019-11-22 14:47:59', 1207590691320098817, 1207590691320098817, 'name', '姓名', 'text', b'1', 20, 0, '姓名', b'0', 1207590691190075393, '75248b6ed423fc6661f95d502107840f', '我要咨询', NULL);
INSERT INTO `sys_advice_field` VALUES (1207590691227824129, 0, b'0', '2019-11-22 15:31:00', '2019-11-22 15:31:00', 1207590691320098817, 1207590691320098817, 'name', '姓名', 'text', b'1', 50, 0, NULL, b'0', 1207590691198464002, 'c0ef36db52ef574a7a2b9b4969fab99d', '投诉建议', NULL);
INSERT INTO `sys_advice_field` VALUES (1207590691227824130, 0, b'0', '2019-11-22 14:59:27', '2019-11-22 14:59:27', 1207590691320098817, 1207590691320098817, 'title', '标题', 'text', b'1', 100, 6, NULL, b'0', 1207590691190075393, '75248b6ed423fc6661f95d502107840f', '我要咨询', NULL);
INSERT INTO `sys_advice_field` VALUES (1207590691244601345, 0, b'0', '2019-11-22 14:53:00', '2019-11-22 14:53:00', 1207590691320098817, 1207590691320098817, 'address', '地址', 'text', b'0', 200, 3, '地址', b'0', 1207590691190075393, '75248b6ed423fc6661f95d502107840f', '我要咨询', NULL);
INSERT INTO `sys_advice_field` VALUES (1207590691244601346, 0, b'0', '2019-11-22 14:58:51', '2019-11-22 14:58:51', 1207590691320098817, 1207590691320098817, 'advice-type', '咨询类别', 'select', b'1', NULL, 5, NULL, b'1', 1207590691190075393, '75248b6ed423fc6661f95d502107840f', '我要咨询', NULL);
INSERT INTO `sys_advice_field` VALUES (1207590691252989954, 2, b'0', '2019-11-22 15:33:05', '2019-11-27 16:59:35', 1207590691320098817, 1207590691320098817, 'mobile', '联系电话', 'text', b'1', 15, 1, NULL, b'0', 1207590691198464002, 'c0ef36db52ef574a7a2b9b4969fab99d', '投诉建议', '^1[3456789]\\d{9}$');
INSERT INTO `sys_advice_field` VALUES (1207590691252989955, 0, b'0', '2019-11-22 15:34:58', '2019-11-22 15:34:58', 1207590691320098817, 1207590691320098817, 'other-contact', '其他联系方式', 'text', b'0', 50, 5, NULL, b'0', 1207590691198464002, 'c0ef36db52ef574a7a2b9b4969fab99d', '投诉建议', NULL);
INSERT INTO `sys_advice_field` VALUES (1207590691261378562, 0, b'0', '2019-11-22 14:59:56', '2019-11-22 14:59:56', 1207590691320098817, 1207590691320098817, 'context', '内容', 'textarea', b'1', 450, 7, NULL, b'0', 1207590691190075393, '75248b6ed423fc6661f95d502107840f', '我要咨询', NULL);
INSERT INTO `sys_advice_field` VALUES (1207590691265572865, 0, b'0', '2019-11-22 14:53:43', '2019-11-22 14:53:43', 1207590691320098817, 1207590691320098817, 'other-contact', '其他联系方式', 'text', b'0', 30, 4, NULL, b'0', 1207590691190075393, '75248b6ed423fc6661f95d502107840f', '我要咨询', NULL);
INSERT INTO `sys_advice_field` VALUES (1207590691273961473, 0, b'0', '2019-11-22 15:35:21', '2019-11-22 15:35:21', 1207590691320098817, 1207590691320098817, 'title', '标题', 'text', b'1', 100, 6, NULL, b'0', 1207590691198464002, 'c0ef36db52ef574a7a2b9b4969fab99d', '投诉建议', NULL);
INSERT INTO `sys_advice_field` VALUES (1207590691282350082, 1, b'0', '2019-11-22 14:52:33', '2019-11-27 16:57:16', 1207590691320098817, 1207590691320098817, 'email', '电子邮箱', 'text', b'1', 100, 2, '电子邮箱', b'0', 1207590691190075393, '75248b6ed423fc6661f95d502107840f', '我要咨询', '^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$');
INSERT INTO `sys_advice_field` VALUES (1207590691303321601, 1, b'0', '2019-11-22 15:33:48', '2019-11-27 16:59:46', 1207590691320098817, 1207590691320098817, 'email', '电子邮箱', 'text', b'1', 100, 3, NULL, b'0', 1207590691198464002, 'c0ef36db52ef574a7a2b9b4969fab99d', '投诉建议', '^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$');
INSERT INTO `sys_advice_field` VALUES (1207590691303321602, 0, b'0', '2019-11-22 15:35:39', '2019-11-22 15:35:39', 1207590691320098817, 1207590691320098817, 'context', '内容', 'textarea', b'1', 450, 7, NULL, b'0', 1207590691198464002, 'c0ef36db52ef574a7a2b9b4969fab99d', '投诉建议', NULL);
INSERT INTO `sys_advice_field` VALUES (1207590691315904513, 0, b'0', '2019-11-22 15:34:16', '2019-11-22 15:34:16', 1207590691320098817, 1207590691320098817, 'address', '地址', 'text', b'0', 200, 0, NULL, b'0', 1207590691198464002, 'c0ef36db52ef574a7a2b9b4969fab99d', '投诉建议', NULL);

-- ----------------------------
-- Table structure for sys_database
-- ----------------------------
DROP TABLE IF EXISTS `sys_database`;
CREATE TABLE `sys_database`  (
  `id` bigint(30) NOT NULL COMMENT '?Զ???????',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '?ֹ???',
  `is_deleted` bit(1) NOT NULL COMMENT '?߼?ɾ??',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '????ʱ?',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '????ʱ?',
  `creator` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '?????',
  `updater` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '?????',
  `database_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `driver_class_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `database_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ip_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_database
-- ----------------------------
INSERT INTO `sys_database` VALUES (1239843481359245314, 0, b'0', NULL, NULL, NULL, NULL, 'MySQL', 'com.mysql.jdbc.Driver', 'jdbc:mysql://fyxmt2018.mysql.rds.aliyuncs.com:3310/public_cultural_center?serverTimezone=GMT%2B8&useUnicode=true', 'cultural_center', 'L8rKeysx02c', 'fyxmt2018.mysql.rds.aliyuncs.com:3310');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `id` bigint(19) NOT NULL,
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '更新人',
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色代码',
  `code_prefix` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '编号拼接',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `sequence` int(10) NULL DEFAULT NULL COMMENT '排序',
  `status` int(1) NULL DEFAULT NULL COMMENT '激活状态（启用，未启用）',
  `value` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '值域（扩展时使用第三字段）',
  `parent_id` bigint(19) NULL DEFAULT NULL,
  `parent_id_bak` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '上级字典主键',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES (1207590691320098817, 0, b'0', '2019-10-21 15:01:51', '2019-10-21 15:01:51', 1207590691320098817, 1207590691320098817, 'LocalServer', NULL, '服务器', 4, 1, 'LocalServer', 1207590691454316547, 'bd5998e2251c36d6c1beb0861c5b5aad');
INSERT INTO `sys_dict` VALUES (1207590691320098818, 0, b'0', '2019-10-31 09:12:27', '2019-10-31 09:12:27', 1207590691320098817, 1207590691320098817, 'apiType', NULL, '接口请求类型', 0, 1, '0', NULL, '');
INSERT INTO `sys_dict` VALUES (1207590691332681729, 0, b'0', '2019-11-02 15:03:13', '2019-11-02 15:03:13', 1207590691320098817, 1207590691320098817, 'scancode_waitmsg', NULL, '扫码且弹出提示框', 94, 1, 'scancode_waitmsg', 1207590691437539329, '92dda67db68a44c79e12ce5f02bf95a5');
INSERT INTO `sys_dict` VALUES (1207590691332681730, 0, b'0', '2019-10-21 15:00:05', '2019-10-21 15:00:05', 1207590691320098817, 1207590691320098817, 'QiniuCloud', NULL, '七牛云', 1, 1, 'QiniuCloud', 1207590691454316547, 'bd5998e2251c36d6c1beb0861c5b5aad');
INSERT INTO `sys_dict` VALUES (1207590691341070338, 0, b'0', '2019-10-31 10:18:28', '2019-10-31 10:18:28', 1207590691320098817, 1207590691320098817, 'responseTypeTwo', NULL, 'application/json', 0, 1, '0', 1207590691353653251, '2b29e1664c132cf411a25dff07c2880d');
INSERT INTO `sys_dict` VALUES (1207590691345264642, 0, b'0', '2019-11-02 15:00:35', '2019-11-02 15:00:35', 1207590691320098817, 1207590691320098817, 'click', NULL, '点击事件', 91, 1, 'click', 1207590691437539329, '92dda67db68a44c79e12ce5f02bf95a5');
INSERT INTO `sys_dict` VALUES (1207590691353653250, 0, b'0', '2019-11-02 15:01:56', '2019-11-02 15:01:56', 1207590691320098817, 1207590691320098817, 'view', NULL, '跳转URL', 92, 1, 'view', 1207590691437539329, '92dda67db68a44c79e12ce5f02bf95a5');
INSERT INTO `sys_dict` VALUES (1207590691353653251, 0, b'0', '2019-10-31 10:16:27', '2019-10-31 10:16:27', 1207590691320098817, 1207590691320098817, 'responseType', NULL, '接口返回类型', 0, 1, '0', NULL, '');
INSERT INTO `sys_dict` VALUES (1207590691362041858, 0, b'0', '2019-11-02 15:20:03', '2019-11-02 15:20:03', 1207590691320098817, 1207590691320098817, 'father_button', NULL, '父菜单', 914, 1, 'father_button', 1207590691437539329, '92dda67db68a44c79e12ce5f02bf95a5');
INSERT INTO `sys_dict` VALUES (1207590691362041859, 0, b'0', '2019-07-17 15:14:21', '2019-07-17 15:14:21', 1207590691320098817, 1207590691320098817, 'new', NULL, '新增', 1, 1, '1', 1207590691391401986, '7846f7340059365d65d67e804dd378e3');
INSERT INTO `sys_dict` VALUES (1207590691370430465, 0, b'0', '2019-11-02 15:04:06', '2019-11-02 15:04:06', 1207590691320098817, 1207590691320098817, 'pic_photo_or_album', NULL, '弹出拍照或相册', 96, 1, 'pic_photo_or_album', 1207590691437539329, '92dda67db68a44c79e12ce5f02bf95a5');
INSERT INTO `sys_dict` VALUES (1207590691374624769, 2, b'0', '2019-10-21 09:43:20', '2019-10-21 14:23:15', 1207590691320098817, 1207590691320098817, 'video', NULL, '视频', 0, 1, ',mp4,avi,flv,mov,', 1207590691483676674, 'f807c36875254ad3305cf9253c196ce1');
INSERT INTO `sys_dict` VALUES (1207590691374624770, 0, b'0', '2019-10-21 15:00:42', '2019-10-21 15:00:42', 1207590691320098817, 1207590691320098817, 'AliCloud', NULL, '阿里云', 2, 0, 'AliCloud', 1207590691454316547, 'bd5998e2251c36d6c1beb0861c5b5aad');
INSERT INTO `sys_dict` VALUES (1207590691374624771, 0, b'0', '2019-10-31 09:13:34', '2019-10-31 09:13:34', 1207590691320098817, 1207590691320098817, 'Get', NULL, 'Get类型接口请求', 0, 1, '0', 1207590691320098818, '0ea28d05030a1a7a0a0645078a58c3c0');
INSERT INTO `sys_dict` VALUES (1207590691383013378, 0, b'0', '2019-03-07 14:10:49', '2019-03-07 14:10:49', 1207590691320098817, 1207590691320098817, 'CheckPending', NULL, '待审核', 0, 1, '待审核', 1207590691445927939, 'a4bddd20b8a8511bf9d167f1934fb5ca');
INSERT INTO `sys_dict` VALUES (1207590691383013379, 0, b'0', '2019-10-31 10:03:14', '2019-10-31 10:03:14', 1207590691320098817, 1207590691320098817, 'paramType', NULL, '第三方接口参数类型', 0, 1, '0', NULL, '');
INSERT INTO `sys_dict` VALUES (1207590691391401986, 0, b'0', '2019-07-17 15:13:06', '2019-07-17 15:13:06', 1207590691320098817, 1207590691320098817, 'operate', NULL, '操作类型', 3, 1, '0', NULL, NULL);
INSERT INTO `sys_dict` VALUES (1207590691395596289, 1, b'0', '2019-10-21 15:01:13', '2019-10-21 15:02:06', 1207590691320098817, 1207590691320098817, 'QCloud', NULL, '腾讯云', 3, 0, 'QCloud', 1207590691454316547, 'bd5998e2251c36d6c1beb0861c5b5aad');
INSERT INTO `sys_dict` VALUES (1207590691395596290, 0, b'0', '2019-11-02 15:03:37', '2019-11-02 15:03:37', 1207590691320098817, 1207590691320098817, 'pic_sysphoto', NULL, '系统拍照', 95, 1, 'pic_sysphoto', 1207590691437539329, '92dda67db68a44c79e12ce5f02bf95a5');
INSERT INTO `sys_dict` VALUES (1207590691403984898, 0, b'0', '2019-10-31 10:17:46', '2019-10-31 10:17:46', 1207590691320098817, 1207590691320098817, 'responseTypeOne', NULL, 'text/plain', 0, 1, '0', 1207590691353653251, '2b29e1664c132cf411a25dff07c2880d');
INSERT INTO `sys_dict` VALUES (1207590691412373506, 2, b'0', '2019-11-02 15:05:47', '2019-11-02 15:07:03', 1207590691320098817, 1207590691320098817, 'media_id', NULL, '下发消息（除文本）', 99, 1, 'media_id', 1207590691437539329, '92dda67db68a44c79e12ce5f02bf95a5');
INSERT INTO `sys_dict` VALUES (1207590691412373507, 1, b'0', '2019-11-02 15:08:22', '2019-11-02 15:08:22', 1207590691320098817, 1207590691320098817, 'miniprogram', NULL, '跳转小程序', 911, 1, 'miniprogram', 1207590691437539329, '92dda67db68a44c79e12ce5f02bf95a5');
INSERT INTO `sys_dict` VALUES (1207590691412373508, 0, b'0', '2019-04-29 15:20:56', '2019-04-29 15:20:56', 1207590691320098817, 1207590691320098817, 'm', NULL, '男', 10, 1, '0', 1207590691420762114, '8499643104c6701b4b8335c1e9998b74');
INSERT INTO `sys_dict` VALUES (1207590691420762114, 0, b'0', '2019-04-29 15:20:07', '2019-04-29 15:20:07', 1207590691320098817, 1207590691320098817, 'gender', NULL, '性别', 0, 1, '0', NULL, NULL);
INSERT INTO `sys_dict` VALUES (1207590691429150722, 0, b'0', '2019-10-31 10:15:00', '2019-10-31 10:15:00', 1207590691320098817, 1207590691320098817, 'paramTypeThree', NULL, 'commonParam', 0, 1, '0', 1207590691383013379, '7787bf2db59bfd4d2f1795cf440a0b22');
INSERT INTO `sys_dict` VALUES (1207590691437539329, 3, b'0', '2019-11-02 15:00:04', '2019-11-02 15:16:08', 1207590691320098817, 1207590691320098817, 'wechat_menu_event', NULL, '微信菜单事件', 9, 1, 'wechat_menu_event', NULL, NULL);
INSERT INTO `sys_dict` VALUES (1207590691437539330, 1, b'0', '2019-10-21 10:33:41', '2019-10-21 14:23:28', 1207590691320098817, 1207590691320098817, 'audio', NULL, '音频', 1, 1, ',mp3,wav,wma,flac,', 1207590691483676674, 'f807c36875254ad3305cf9253c196ce1');
INSERT INTO `sys_dict` VALUES (1207590691445927938, 0, b'0', '2019-11-02 15:05:31', '2019-11-02 15:05:31', 1207590691320098817, 1207590691320098817, 'location_select', NULL, '弹出地理位置选择器', 98, 1, 'location_select', 1207590691437539329, '92dda67db68a44c79e12ce5f02bf95a5');
INSERT INTO `sys_dict` VALUES (1207590691445927939, 0, b'0', '2019-03-07 14:09:45', '2019-03-07 14:09:45', 1207590691320098817, 1207590691320098817, 'validStatus', NULL, '审核状态', 0, 1, '审核状态', NULL, NULL);
INSERT INTO `sys_dict` VALUES (1207590691445927940, 0, b'0', '2019-11-02 15:05:11', '2019-11-02 15:05:11', 1207590691320098817, 1207590691320098817, 'pic_weixin', NULL, '弹出微信发图器', 97, 1, 'pic_weixin', 1207590691437539329, '92dda67db68a44c79e12ce5f02bf95a5');
INSERT INTO `sys_dict` VALUES (1207590691454316546, 0, b'0', '2019-03-07 14:10:57', '2019-03-07 14:10:57', 1207590691320098817, 1207590691320098817, 'pass', NULL, '审核通过', 1, 1, '审核通过', 1207590691445927939, 'a4bddd20b8a8511bf9d167f1934fb5ca');
INSERT INTO `sys_dict` VALUES (1207590691454316547, 0, b'0', '2019-10-21 14:58:00', '2019-10-21 14:58:00', 1207590691320098817, 1207590691320098817, 'server-code', NULL, '文件存储服务器', 0, 1, '文件存储服务器', NULL, NULL);
INSERT INTO `sys_dict` VALUES (1207590691454316548, 1, b'0', '2019-10-21 10:42:10', '2019-10-21 14:23:37', 1207590691320098817, 1207590691320098817, 'document', NULL, '文本', 2, 1, ',txt,doc,docx,xls,xlsx,pdf，', 1207590691483676674, 'f807c36875254ad3305cf9253c196ce1');
INSERT INTO `sys_dict` VALUES (1207590691462705153, 0, b'0', '2019-03-07 14:11:52', '2019-03-07 14:11:52', 1207590691320098817, 1207590691320098817, 'notPass', NULL, '审核不通过', 2, 1, '审核不通过', 1207590691445927939, 'a4bddd20b8a8511bf9d167f1934fb5ca');
INSERT INTO `sys_dict` VALUES (1207590691466899457, 2, b'0', '2019-07-17 15:14:56', '2019-07-17 15:17:53', 1207590691320098817, 1207590691320098817, 'delete', NULL, '删除', 3, 1, '3', 1207590691391401986, '7846f7340059365d65d67e804dd378e3');
INSERT INTO `sys_dict` VALUES (1207590691466899458, 0, b'0', '2019-10-31 10:07:39', '2019-10-31 10:07:39', 1207590691320098817, 1207590691320098817, 'paramTypeOne', NULL, 'application/x-www-form-urlencoded', 0, 1, '0', 1207590691383013379, '7787bf2db59bfd4d2f1795cf440a0b22');
INSERT INTO `sys_dict` VALUES (1207590691466899459, 0, b'0', '2019-10-31 10:12:13', '2019-10-31 10:12:13', 1207590691320098817, 1207590691320098817, 'paramTypeTwo', NULL, 'application/json', 0, 1, '0', 1207590691383013379, '7787bf2db59bfd4d2f1795cf440a0b22');
INSERT INTO `sys_dict` VALUES (1207590691475288066, 0, b'0', '2019-04-29 15:21:38', '2019-04-29 15:21:38', 1207590691320098817, 1207590691320098817, 'f', NULL, '女', 5, 1, '1', 1207590691420762114, '8499643104c6701b4b8335c1e9998b74');
INSERT INTO `sys_dict` VALUES (1207590691475288067, 0, b'0', '2019-07-17 15:14:41', '2019-07-17 15:14:41', 1207590691320098817, 1207590691320098817, 'update', NULL, '更新', 2, 1, '2', 1207590691391401986, '7846f7340059365d65d67e804dd378e3');
INSERT INTO `sys_dict` VALUES (1207590691483676673, 0, b'0', '2019-11-02 15:02:22', '2019-11-02 15:02:22', 1207590691320098817, 1207590691320098817, 'scancode_push', NULL, '扫码推送', 93, 1, 'scancode_push', 1207590691437539329, '92dda67db68a44c79e12ce5f02bf95a5');
INSERT INTO `sys_dict` VALUES (1207590691483676674, 0, b'0', '2019-10-21 09:40:23', '2019-10-21 09:40:23', 1207590691320098817, 1207590691320098817, 'fileType', NULL, '文件类型', 0, 1, '文件类型', NULL, NULL);
INSERT INTO `sys_dict` VALUES (1207590691492065281, 1, b'0', '2019-10-21 10:44:58', '2019-10-21 14:23:52', 1207590691320098817, 1207590691320098817, 'picture', NULL, '图片', 3, 1, ',bmp,jpg,png,jpeg,', 1207590691483676674, 'f807c36875254ad3305cf9253c196ce1');
INSERT INTO `sys_dict` VALUES (1207590691500453890, 0, b'0', '2019-10-31 09:13:53', '2019-10-31 09:13:53', 1207590691320098817, 1207590691320098817, 'Post', NULL, 'Post类型接口请求', 0, 1, '0', 1207590691320098818, '0ea28d05030a1a7a0a0645078a58c3c0');
INSERT INTO `sys_dict` VALUES (1207590691500453891, 0, b'0', '2019-11-02 15:06:31', '2019-11-02 15:06:31', 1207590691320098817, 1207590691320098817, 'view_limited', NULL, '跳转图文消息', 910, 1, 'view_limited', 1207590691437539329, '92dda67db68a44c79e12ce5f02bf95a5');

-- ----------------------------
-- Table structure for sys_field_option
-- ----------------------------
DROP TABLE IF EXISTS `sys_field_option`;
CREATE TABLE `sys_field_option`  (
  `id` bigint(19) NOT NULL,
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `updater` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `field_id` bigint(19) NULL DEFAULT NULL,
  `field_id_bak` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '咨询建议字段主键',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称（对应选项显示内容）',
  `value` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '值（对应选项值）',
  `sequence` int(10) NOT NULL DEFAULT 0 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_field_option
-- ----------------------------
INSERT INTO `sys_field_option` VALUES (1207590691508842497, 0, b'0', '2019-11-22 14:58:51', '2019-11-22 14:58:51', '1207590691320098817', '1207590691320098817', 1207590691244601346, '8f2742f22b23f81c7ec3709e287c379f', '金融服务', 'jinrong', 3);
INSERT INTO `sys_field_option` VALUES (1207590691508842498, 0, b'0', '2019-11-22 14:58:51', '2019-11-22 14:58:51', '1207590691320098817', '1207590691320098817', 1207590691244601346, '8f2742f22b23f81c7ec3709e287c379f', '国际贸易', 'maoyi', 1);
INSERT INTO `sys_field_option` VALUES (1207590691508842499, 0, b'0', '2019-11-22 14:58:51', '2019-11-22 14:58:51', '1207590691320098817', '1207590691320098817', 1207590691244601346, '8f2742f22b23f81c7ec3709e287c379f', '投资咨询', 'touzi', 0);
INSERT INTO `sys_field_option` VALUES (1207590691517231105, 0, b'0', '2019-11-22 14:58:51', '2019-11-22 14:58:51', '1207590691320098817', '1207590691320098817', 1207590691244601346, '8f2742f22b23f81c7ec3709e287c379f', '企业办理', 'qiye', 4);
INSERT INTO `sys_field_option` VALUES (1207590691517231106, 0, b'0', '2019-11-22 14:58:51', '2019-11-22 14:58:51', '1207590691320098817', '1207590691320098817', 1207590691244601346, '8f2742f22b23f81c7ec3709e287c379f', '规划土地', 'guihua', 10);
INSERT INTO `sys_field_option` VALUES (1207590691517231107, 0, b'0', '2019-11-22 14:58:51', '2019-11-22 14:58:51', '1207590691320098817', '1207590691320098817', 1207590691244601346, '8f2742f22b23f81c7ec3709e287c379f', '税收管理', 'shuishou', 13);
INSERT INTO `sys_field_option` VALUES (1207590691525619714, 0, b'0', '2019-11-22 14:58:51', '2019-11-22 14:58:51', '1207590691320098817', '1207590691320098817', 1207590691244601346, '8f2742f22b23f81c7ec3709e287c379f', '人才户籍出入境', 'huji', 14);
INSERT INTO `sys_field_option` VALUES (1207590691529814017, 0, b'0', '2019-11-22 14:58:51', '2019-11-22 14:58:51', '1207590691320098817', '1207590691320098817', 1207590691244601346, '8f2742f22b23f81c7ec3709e287c379f', '其他', 'qita', 15);
INSERT INTO `sys_field_option` VALUES (1207590691534008322, 0, b'0', '2019-11-22 14:58:51', '2019-11-22 14:58:51', '1207590691320098817', '1207590691320098817', 1207590691244601346, '8f2742f22b23f81c7ec3709e287c379f', '注册许可', 'zhuce', 6);
INSERT INTO `sys_field_option` VALUES (1207590691534008323, 0, b'0', '2019-11-22 14:58:51', '2019-11-22 14:58:51', '1207590691320098817', '1207590691320098817', 1207590691244601346, '8f2742f22b23f81c7ec3709e287c379f', '劳动人事', 'laodong', 12);
INSERT INTO `sys_field_option` VALUES (1207590691542396930, 0, b'0', '2019-11-22 14:58:51', '2019-11-22 14:58:51', '1207590691320098817', '1207590691320098817', 1207590691244601346, '8f2742f22b23f81c7ec3709e287c379f', '建设审批', 'jianshe', 9);
INSERT INTO `sys_field_option` VALUES (1207590691546591234, 0, b'0', '2019-11-22 14:58:51', '2019-11-22 14:58:51', '1207590691320098817', '1207590691320098817', 1207590691244601346, '8f2742f22b23f81c7ec3709e287c379f', '科创政策', 'kechuang', 8);
INSERT INTO `sys_field_option` VALUES (1207590691546591235, 0, b'0', '2019-11-22 14:58:51', '2019-11-22 14:58:51', '1207590691320098817', '1207590691320098817', 1207590691244601346, '8f2742f22b23f81c7ec3709e287c379f', '政策扶持', 'zhengce', 7);
INSERT INTO `sys_field_option` VALUES (1207590691554979842, 0, b'0', '2019-11-22 14:58:51', '2019-11-22 14:58:51', '1207590691320098817', '1207590691320098817', 1207590691244601346, '8f2742f22b23f81c7ec3709e287c379f', '市容环保', 'shirong', 11);
INSERT INTO `sys_field_option` VALUES (1207590691563368449, 0, b'0', '2019-11-22 14:58:51', '2019-11-22 14:58:51', '1207590691320098817', '1207590691320098817', 1207590691244601346, '8f2742f22b23f81c7ec3709e287c379f', '质量监督', 'zhiliang', 5);

-- ----------------------------
-- Table structure for sys_field_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_field_type`;
CREATE TABLE `sys_field_type`  (
  `id` bigint(19) NOT NULL,
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '更新人',
  `code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编号',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `is_options` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否是选择项',
  `sequence` int(10) NOT NULL DEFAULT 0 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_field_type
-- ----------------------------
INSERT INTO `sys_field_type` VALUES (1207590691571757057, 0, b'0', '2019-11-21 16:24:02', '2019-11-21 16:24:02', 1207590691320098817, 1207590691320098817, 'textarea', '文本域', b'0', 5);
INSERT INTO `sys_field_type` VALUES (1207590691571757058, 0, b'0', '2019-11-21 16:19:50', '2019-11-21 16:19:50', 1207590691320098817, 1207590691320098817, 'text', '文本类型', b'0', 0);
INSERT INTO `sys_field_type` VALUES (1207590691580145665, 0, b'0', '2019-11-21 16:21:42', '2019-11-21 16:21:42', 1207590691320098817, 1207590691320098817, 'radio', '单选按钮', b'1', 2);
INSERT INTO `sys_field_type` VALUES (1207590691588534274, 5, b'1', '2019-11-22 09:47:22', '2019-11-22 09:47:58', 1207590691320098817, 1207590691320098817, 'video', '视频', b'0', 7);
INSERT INTO `sys_field_type` VALUES (1207590691588534275, 0, b'0', '2019-11-21 16:20:15', '2019-11-21 16:20:15', 1207590691320098817, 1207590691320098817, 'select', '选择类型', b'1', 1);
INSERT INTO `sys_field_type` VALUES (1207590691596922881, 0, b'0', '2019-11-21 16:25:51', '2019-11-21 16:25:51', 1207590691320098817, 1207590691320098817, 'image', '图片类型', b'0', 6);
INSERT INTO `sys_field_type` VALUES (1207590691596922882, 0, b'0', '2019-11-21 16:23:19', '2019-11-21 16:23:19', 1207590691320098817, 1207590691320098817, 'file', '文件', b'0', 4);
INSERT INTO `sys_field_type` VALUES (1207590691605311490, 0, b'0', '2019-11-21 16:22:27', '2019-11-21 16:22:27', 1207590691320098817, 1207590691320098817, 'checkBox', '复选框', b'1', 3);

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` bigint(19) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '更新人',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `operation` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户操作',
  `method` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求方法',
  `params` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '请求参数',
  `time` bigint(20) NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'IP地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统日志' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES (1238031006744862722, 0, b'0', NULL, NULL, NULL, NULL, 'admin', '登陆', 'login', NULL, 0, '127.0.0.1');
INSERT INTO `sys_log` VALUES (1239833533938204674, 0, b'0', NULL, NULL, NULL, NULL, 'admin', '登陆', 'login', NULL, 0, '127.0.0.1');
INSERT INTO `sys_log` VALUES (1239834359901519874, 0, b'0', NULL, NULL, NULL, NULL, 'admin', '登陆', 'login', NULL, 0, '127.0.0.1');
INSERT INTO `sys_log` VALUES (1239835275643912193, 0, b'0', NULL, NULL, NULL, NULL, 'admin', '登陆', 'login', NULL, 0, '127.0.0.1');
INSERT INTO `sys_log` VALUES (1239836823404961794, 0, b'0', NULL, NULL, NULL, NULL, 'admin', '登陆', 'login', NULL, 0, '127.0.0.1');
INSERT INTO `sys_log` VALUES (1239841191604805634, 0, b'0', NULL, NULL, NULL, NULL, 'admin', '登陆', 'login', NULL, 0, '127.0.0.1');
INSERT INTO `sys_log` VALUES (1239841455137120258, 0, b'0', NULL, NULL, NULL, NULL, 'admin', '登陆', 'login', NULL, 0, '127.0.0.1');

-- ----------------------------
-- Table structure for sys_oss
-- ----------------------------
DROP TABLE IF EXISTS `sys_oss`;
CREATE TABLE `sys_oss`  (
  `id` bigint(19) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '更新人',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '自定义文件名',
  `file_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件名',
  `file_ext` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件扩展名',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件访问路径',
  `file_size` bigint(20) NULL DEFAULT NULL COMMENT '文件大小',
  `type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件类型',
  `server_code` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '存储服务器编码',
  `is_media` bit(1) NULL DEFAULT NULL COMMENT '是否是媒体库',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_oss_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_oss_config`;
CREATE TABLE `sys_oss_config`  (
  `id` bigint(19) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NOT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '更新人',
  `url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '本地存储访问url',
  `storage_location` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '本地存储路径',
  `qn_domain` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '七牛云域名',
  `qn_access_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '七牛云访问key',
  `qn_secret_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '七牛云key',
  `qn_bucket_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '七牛云空间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_oss_config
-- ----------------------------
INSERT INTO `sys_oss_config` VALUES (1207590691605311491, 7, b'0', '2019-10-21 14:03:06', '2019-10-22 16:22:10', 1207590691320098817, 1207590691320098817, '/vpath/data', 'D:/', 'http://szycmediatest.fyxmt.com', 'reXUCSBHMTga5cu4jNeNnghGsfTCa-ixWr1I9AIQ', 'CCxVQ1_8FdQX1W4OdvV6kwabN8K43RmfjNo6-xVe', 'yancao');

-- ----------------------------
-- Table structure for sys_oss_label
-- ----------------------------
DROP TABLE IF EXISTS `sys_oss_label`;
CREATE TABLE `sys_oss_label`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `updater` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `oss_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '对象存储id',
  `label_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标签id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_resource
-- ----------------------------
DROP TABLE IF EXISTS `sys_resource`;
CREATE TABLE `sys_resource`  (
  `id` bigint(19) NOT NULL,
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(36) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(36) NULL DEFAULT NULL COMMENT '更新人',
  `code` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '授权代码',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '资源名称',
  `sequence` int(10) NULL DEFAULT NULL COMMENT '排序',
  `parent_id` bigint(19) NULL DEFAULT NULL,
  `parent_id_bak` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '父节点主键',
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '前端路由',
  `type` int(1) NULL DEFAULT NULL COMMENT '类型 0：目录 1：菜单 2：按钮',
  `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '后台访问接口路径',
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单图标'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_resource
-- ----------------------------
INSERT INTO `sys_resource` VALUES (1207590691605311492, 0, b'0', '2019-03-01 10:20:14', '2019-03-01 10:20:17', 1207590691320098817, 1207590691320098817, 'sys:user:save', '新增', 103002, 1207590691924078594, 'eeb5967fd6e3597ab80e7fc36cbd81d8', NULL, 2, '/admin/user/save', NULL);
INSERT INTO `sys_resource` VALUES (1207590691613700098, 6, b'0', '2019-10-25 09:47:48', '2019-10-30 14:48:14', 1207590691320098817, 1207590691320098817, 'cms:content:save', '新增', 201001, 1207590691806638082, '67f5a12256f22ae1e9f4d4885afce078', NULL, 2, '/admin/cms/categoryContent/saveContent', NULL);
INSERT INTO `sys_resource` VALUES (1207590691613700099, 0, b'0', '2019-03-01 10:21:58', '2019-03-01 10:22:01', 1207590691320098817, 1207590691320098817, 'sys:user:update', '修改', 103003, 1207590691924078594, 'eeb5967fd6e3597ab80e7fc36cbd81d8', NULL, 2, '/admin/user/update', NULL);
INSERT INTO `sys_resource` VALUES (1207590691622088705, 0, b'0', '2019-03-01 10:23:03', '2019-03-01 10:23:06', 1207590691320098817, 1207590691320098817, 'sys:user:delete', '删除', 103004, 1207590691924078594, 'eeb5967fd6e3597ab80e7fc36cbd81d8', NULL, 2, '/admin/user/delete', NULL);
INSERT INTO `sys_resource` VALUES (1207590691622088706, 0, b'0', '2019-11-06 13:54:18', '2019-11-06 13:54:18', 1207590691320098817, 1207590691320098817, 'cms:label:list', '查询', 111004, 1207590691689197570, '246534cb6ed6ed53d6fa41ad8aa64d5a', NULL, 2, '/admin/cms/label/pages', NULL);
INSERT INTO `sys_resource` VALUES (1207590691634671617, 1, b'0', '2019-10-25 09:39:41', '2019-10-25 09:40:08', 1207590691320098817, 1207590691320098817, NULL, 'CMS管理', 20, NULL, NULL, NULL, 1, '#', 'config');
INSERT INTO `sys_resource` VALUES (1207590691643060225, 2, b'0', '2019-10-25 11:14:07', '2019-10-25 11:14:07', 1207590691320098817, 1207590691320098817, 'cms:category:delete', '删除', 202003, 1207590691819220995, '7a42bfc3ee7046535c200ce15b4a7bf1', NULL, 2, '/admin/cms/category/delete', NULL);
INSERT INTO `sys_resource` VALUES (1207590691643060226, 1, b'0', '2019-10-31 09:27:28', '2019-10-31 09:31:32', 1207590691320098817, 1207590691320098817, 'sys:api:update', '修改', 5, 1207590691907301377, 'df4f4cd50a6496a7dbbee833cef32a79', NULL, 2, '#', NULL);
INSERT INTO `sys_resource` VALUES (1207590691651448833, 0, b'0', '2019-11-08 10:24:16', '2019-11-08 10:24:16', 1207590691320098817, 1207590691320098817, 'cms:categoryProp:delete', '属性删除', 202007, 1207590691819220995, '7a42bfc3ee7046535c200ce15b4a7bf1', NULL, 2, '/admin/cms/categoryProp/removeByIds', NULL);
INSERT INTO `sys_resource` VALUES (1207590691655643138, 2, b'0', '2019-10-26 17:02:12', '2019-10-26 17:09:21', 1207590691320098817, 1207590691320098817, 'oss-upload', '上传文件', 109003, 1207590691877941251, 'c969c7d79cf54b29d2eca4e17c685b03', NULL, 2, '/admin/oss/upload', NULL);
INSERT INTO `sys_resource` VALUES (1207590691655643139, 0, b'0', '2019-10-30 14:47:37', '2019-10-30 14:47:37', 1207590691320098817, 1207590691320098817, 'cms:content:stick', '置顶', 201006, 1207590691806638082, '67f5a12256f22ae1e9f4d4885afce078', NULL, 2, '/admin/cms/categoryContent/setStick', NULL);
INSERT INTO `sys_resource` VALUES (1207590691664031745, 2, b'0', '2019-10-26 17:00:53', '2019-10-26 17:02:21', 1207590691320098817, 1207590691320098817, 'sys:dict:delete', '删除', 104003, 1207590691668226050, '168e38c8-6b92-4445-bb4b-2979113d409b', NULL, 2, '/admin/dict/delete', NULL);
INSERT INTO `sys_resource` VALUES (1207590691664031746, 0, b'0', '2019-11-06 17:03:51', '2019-11-06 17:03:51', 1207590691320098817, 1207590691320098817, 'cms:recovery:list', '查询', 206001, 1207590691982798851, 'fef72fae29826cb279447542b1bbb2b8', NULL, 2, '/admin/cms/categoryContent/page', NULL);
INSERT INTO `sys_resource` VALUES (1207590691668226049, 5, b'0', '2019-10-30 13:52:34', '2019-10-30 14:16:58', 1207590691320098817, 1207590691320098817, 'xss:tag:list', '标签管理', 1, 1207590691898912770, 'daa507bf9f97b4c68e447bcef7c6ca7f', 'xss/tag', 1, '/admin/whiteList/tags/pages', 'config');
INSERT INTO `sys_resource` VALUES (1207590691668226050, 2, b'0', '2019-02-27 15:30:18', '2019-10-18 13:54:54', 1207590691320098817, 1207590691320098817, 'sys:dict:list', '数据字典', 1040, 1207590691705974787, '2c6caefe34d69a2ac987df1e9afb567f', '/dict/dict', 1, '/admin/dict/all', 'dict1');
INSERT INTO `sys_resource` VALUES (1207590691668226051, 4, b'0', '2019-10-16 16:33:58', '2019-10-18 13:56:24', 1207590691320098817, 1207590691320098817, 'role:delete', '删除', 101003, 1207590691731140611, '4ab80b834574f3c55acbb30f69f972a6', NULL, 2, '/admin/role/delete', NULL);
INSERT INTO `sys_resource` VALUES (1207590691676614657, 4, b'0', '2019-11-22 09:07:54', '2019-11-22 09:44:20', 1207590691320098817, 1207590691320098817, 'fieldType-page', '字段类型', 2501, 1207590691676614658, '212ced5404ff2a870c3d6d40619dc9f8', 'advice/field-type', 1, '/admin/advice/fieldType/pages', 'config');
INSERT INTO `sys_resource` VALUES (1207590691676614658, 5, b'0', '2019-11-22 09:04:40', '2019-11-22 09:08:06', 1207590691320098817, 1207590691320098817, '', '咨询建议管理', 25, NULL, NULL, NULL, 1, '#', 'duanxin');
INSERT INTO `sys_resource` VALUES (1207590691685003266, 2, b'0', '2019-10-25 11:13:27', '2019-10-25 11:13:27', 1207590691320098817, 1207590691320098817, 'cms:category:update', '修改', 202002, 1207590691819220995, '7a42bfc3ee7046535c200ce15b4a7bf1', NULL, 2, '/admin/cms/category/update', NULL);
INSERT INTO `sys_resource` VALUES (1207590691689197570, 3, b'0', '2019-11-04 10:46:09', '2019-11-04 10:48:09', 1207590691320098817, 1207590691320098817, 'cms:label:list', '标签管理', 1110, 1207590691705974787, '2c6caefe34d69a2ac987df1e9afb567f', '/label/label', 1, '/admin/cms/label/pages', 'tubiao');
INSERT INTO `sys_resource` VALUES (1207590691689197571, 0, b'0', '2019-10-31 09:30:44', '2019-10-31 09:30:44', 1207590691320098817, 1207590691320098817, 'sys:api:reset', '重置', 2, 1207590691907301377, 'df4f4cd50a6496a7dbbee833cef32a79', NULL, 2, '#', NULL);
INSERT INTO `sys_resource` VALUES (1207590691701780481, 2, b'0', '2019-10-26 17:18:44', '2019-10-26 17:21:14', 1207590691320098817, 1207590691320098817, 'cms:category:enable', '启用/禁用', 202004, 1207590691819220995, '7a42bfc3ee7046535c200ce15b4a7bf1', NULL, 2, '/admin/cms/category/setEnable', NULL);
INSERT INTO `sys_resource` VALUES (1207590691705974786, 3, b'0', '2019-10-26 17:25:07', '2019-10-30 14:49:04', 1207590691320098817, 1207590691320098817, 'cms:content:list', '查询', 201004, 1207590691806638082, '67f5a12256f22ae1e9f4d4885afce078', NULL, 2, '/admin/cms/categoryContent/page', NULL);
INSERT INTO `sys_resource` VALUES (1207590691705974787, 2, b'0', '2019-02-27 15:25:45', '2019-10-25 09:40:02', 1207590691320098817, 1207590691320098817, NULL, '系统管理', 10, NULL, NULL, NULL, 1, '#', 'system');
INSERT INTO `sys_resource` VALUES (1207590691714363394, 2, b'0', '2019-10-31 09:25:43', '2019-10-31 09:30:57', 1207590691320098817, 1207590691320098817, 'sys:api:deleteAll', '批量删除', 3, 1207590691907301377, 'df4f4cd50a6496a7dbbee833cef32a79', NULL, 2, '#', NULL);
INSERT INTO `sys_resource` VALUES (1207590691714363395, 0, b'0', '2019-11-22 10:28:34', '2019-11-22 10:28:34', 1207590691320098817, 1207590691320098817, 'advice-delete', '删除', 250203, 1207590691945050113, 'ef69bbfd77d1ac98cdcafd79be52d096', NULL, 2, '/admin/advice/removeByIds', NULL);
INSERT INTO `sys_resource` VALUES (1207590691714363396, 0, b'0', '2019-12-18 16:53:03', '2019-12-18 16:53:03', 1207590691320098817, 1207590691320098817, 'advice-export', '导出', 250205, 1207590691945050113, 'ef69bbfd77d1ac98cdcafd79be52d096', NULL, 2, '/admin/advice/export', NULL);
INSERT INTO `sys_resource` VALUES (1207590691714363397, 0, b'0', '2019-11-22 09:33:09', '2019-11-22 09:33:09', 1207590691320098817, 1207590691320098817, 'fieldType-save', '新增', 250101, 1207590691676614657, '20a24675d0fef15cdb43db30e2338a2f', NULL, 2, '/admin/advice/fieldType/save', NULL);
INSERT INTO `sys_resource` VALUES (1207590691722752001, 0, b'0', '2019-10-31 09:25:20', '2019-10-31 09:25:20', 1207590691320098817, 1207590691320098817, 'sys:api:save', '新增', 1, 1207590691907301377, 'df4f4cd50a6496a7dbbee833cef32a79', NULL, 2, '#', NULL);
INSERT INTO `sys_resource` VALUES (1207590691722752002, 2, b'0', '2019-10-26 17:09:03', '2019-10-26 17:10:51', 1207590691320098817, 1207590691320098817, 'oss-delete', '删除', 109007, 1207590691877941251, 'c969c7d79cf54b29d2eca4e17c685b03', NULL, 2, '/admin/oss/removeByIds', NULL);
INSERT INTO `sys_resource` VALUES (1207590691722752003, 2, b'0', '2019-10-31 14:37:51', '2019-10-31 14:39:11', 1207590691320098817, 1207590691320098817, 'wechat:menu', '微信管理', 40, NULL, NULL, '/wechat', 1, '/wechat', 'tixing');
INSERT INTO `sys_resource` VALUES (1207590691731140610, 2, b'0', '2019-10-26 17:03:21', '2019-10-26 17:09:40', 1207590691320098817, 1207590691320098817, 'oss-batch-remove', '批量删除', 109004, 1207590691877941251, 'c969c7d79cf54b29d2eca4e17c685b03', NULL, 2, '/admin/oss/removeByIds', NULL);
INSERT INTO `sys_resource` VALUES (1207590691731140611, 0, b'0', '2019-02-27 15:27:40', '2019-02-27 15:33:05', 1207590691320098817, 1207590691320098817, 'sys:role:list', '角色管理', 1010, 1207590691705974787, '2c6caefe34d69a2ac987df1e9afb567f', 'sys/role', 1, '/admin/role/list', 'role');
INSERT INTO `sys_resource` VALUES (1207590691731140612, 1, b'0', '2019-11-08 10:14:47', '2019-11-08 10:22:12', 1207590691320098817, 1207590691320098817, 'cms:category:prop', '属性', 202005, 1207590691819220995, '7a42bfc3ee7046535c200ce15b4a7bf1', NULL, 2, '/admin/cms/categoryProp/pages', NULL);
INSERT INTO `sys_resource` VALUES (1207590691752112130, 0, b'0', '2019-11-06 17:13:42', '2019-11-06 17:13:42', 1207590691320098817, 1207590691320098817, 'copyright:list:removeByIds', '删除', 3, 1207590691781472258, '5a459622041155566ca7cb889c6b043d', NULL, 2, 'admin/copyright/removeByIds', NULL);
INSERT INTO `sys_resource` VALUES (1207590691768889345, 2, b'0', '2019-10-19 13:56:13', '2019-10-19 13:58:28', 1207590691320098817, 1207590691320098817, 'sys:role:batchDelete', '批量删除', 101004, 1207590691731140611, '4ab80b834574f3c55acbb30f69f972a6', NULL, 2, '/admin/role/removeByIds', NULL);
INSERT INTO `sys_resource` VALUES (1207590691777277954, 1, b'0', '2019-11-06 11:31:07', '2019-11-06 11:39:07', 1207590691320098817, 1207590691320098817, 'cms:extended:save', '新增', 205001, 1207590691848581125, 'ad5149a678b83e0c78ef212bab91b1d5', NULL, 2, '/admin/cms/extended/save', NULL);
INSERT INTO `sys_resource` VALUES (1207590691781472257, 2, b'0', '2019-11-06 16:37:10', '2019-11-06 17:06:29', 1207590691320098817, 1207590691320098817, 'copyright:menu', '版权信息管理', 50, NULL, NULL, NULL, 1, '#', 'zhedie');
INSERT INTO `sys_resource` VALUES (1207590691781472258, 1, b'0', '2019-11-06 16:38:11', '2019-11-06 17:08:43', 1207590691320098817, 1207590691320098817, 'copyright:list', '列表', 1, 1207590691781472257, '5823f1d9964679a343a29b7965bebe65', 'copyright/list', 1, 'admin/copyright', 'zhedie');
INSERT INTO `sys_resource` VALUES (1207590691789860865, 4, b'0', '2019-10-25 09:50:23', '2019-10-30 14:48:50', 1207590691320098817, 1207590691320098817, 'cms:content:update', '修改', 201002, 1207590691806638082, '67f5a12256f22ae1e9f4d4885afce078', NULL, 2, '/admin/cms/categoryContent/updateContent', NULL);
INSERT INTO `sys_resource` VALUES (1207590691789860866, 0, b'0', '2019-11-06 11:32:29', '2019-11-06 11:32:29', 1207590691320098817, 1207590691320098817, 'cms:extended:delete', '删除', 205003, 1207590691848581125, 'ad5149a678b83e0c78ef212bab91b1d5', NULL, 2, '/admin/cms/extended/delete', NULL);
INSERT INTO `sys_resource` VALUES (1207590691798249474, 0, b'0', '2019-11-08 10:23:24', '2019-11-08 10:23:24', 1207590691320098817, 1207590691320098817, 'cms:categoryProp:save', '属性新增', 202006, 1207590691819220995, '7a42bfc3ee7046535c200ce15b4a7bf1', NULL, 2, '/admin/cms/categoryProp/save', NULL);
INSERT INTO `sys_resource` VALUES (1207590691798249475, 0, b'0', '2019-12-23 09:23:09', '2019-12-23 09:23:09', 1207590691320098817, 1207590691320098817, 'cms:content:prop', '属性', 201011, 1207590691806638082, '67f5a12256f22ae1e9f4d4885afce078', NULL, 2, '/admin/cms/contentProp', NULL);
INSERT INTO `sys_resource` VALUES (1207590691798249476, 1, b'0', '2019-10-22 10:08:31', '2019-10-26 16:46:18', 1207590691320098817, 1207590691320098817, 'menu-save', '新增', 1, 1207590691924078593, 'edac8e5783c95c9c4034dbf097d8d49d', NULL, 2, '/admin/resource/save', NULL);
INSERT INTO `sys_resource` VALUES (1207590691798249477, 0, b'0', '2019-11-06 17:12:00', '2019-11-06 17:12:00', 1207590691320098817, 1207590691320098817, 'copyright:list:save', '保存', 1, 1207590691781472258, '5a459622041155566ca7cb889c6b043d', NULL, 2, 'admin/copyright/save', NULL);
INSERT INTO `sys_resource` VALUES (1207590691806638082, 4, b'0', '2019-10-25 09:41:34', '2019-10-25 11:10:21', 1207590691320098817, 1207590691320098817, 'cms:content:list', '内容管理', 2010, 1207590691634671617, '055a0693fb4274ef8461caf85841286c', '/content/content-list', 1, '/admin/cms/content/page', 'log');
INSERT INTO `sys_resource` VALUES (1207590691806638083, 6, b'0', '2019-10-25 09:51:07', '2019-11-06 17:05:13', 1207590691320098817, 1207590691320098817, 'cms:content:delete', '删除', 201003, 1207590691806638082, '67f5a12256f22ae1e9f4d4885afce078', NULL, 2, '/admin/cms/categoryContent/recyclingByIds', NULL);
INSERT INTO `sys_resource` VALUES (1207590691806638084, 4, b'0', '2019-11-04 10:50:58', '2019-11-04 10:51:17', 1207590691320098817, 1207590691320098817, 'cms:label:update', '修改', 111002, 1207590691689197570, '246534cb6ed6ed53d6fa41ad8aa64d5a', NULL, 2, '/admin/cms/label/update', NULL);
INSERT INTO `sys_resource` VALUES (1207590691806638085, 0, b'0', '2019-11-22 10:29:52', '2019-11-22 10:29:52', 1207590691320098817, 1207590691320098817, 'advice-field', '字段维护', 250204, 1207590691945050113, 'ef69bbfd77d1ac98cdcafd79be52d096', NULL, 2, '/admin/advice/field', NULL);
INSERT INTO `sys_resource` VALUES (1207590691815026689, 0, b'0', '2019-11-22 10:27:27', '2019-11-22 10:27:27', 1207590691320098817, 1207590691320098817, 'advice-save', '新增', 250201, 1207590691945050113, 'ef69bbfd77d1ac98cdcafd79be52d096', NULL, 2, '/admin/advice/save', NULL);
INSERT INTO `sys_resource` VALUES (1207590691815026690, 1, b'0', '2019-10-22 09:02:18', '2019-10-22 09:10:55', 1207590691320098817, 1207590691320098817, 'sys:oss:config', 'OSS配置', 109001, 1207590691877941251, 'c969c7d79cf54b29d2eca4e17c685b03', NULL, 2, '/admin/ossConfig/getConfig', NULL);
INSERT INTO `sys_resource` VALUES (1207590691819220994, 1, b'0', '2019-10-19 15:38:23', '2019-10-19 15:38:44', 1207590691320098817, 1207590691320098817, 'sys:schedule:list', '定时任务', 1050, 1207590691705974787, '2c6caefe34d69a2ac987df1e9afb567f', 'job/schedule', 1, '/admin/schedule/pages', 'job');
INSERT INTO `sys_resource` VALUES (1207590691819220995, 2, b'0', '2019-10-25 11:10:06', '2019-10-25 11:10:06', 1207590691320098817, 1207590691320098817, 'cms:category:list', '栏目管理', 2020, 1207590691634671617, '055a0693fb4274ef8461caf85841286c', 'category/category-list', 1, '/admin/cms/category/page', 'log');
INSERT INTO `sys_resource` VALUES (1207590691835998209, 1, b'0', '2019-10-28 11:39:54', '2019-10-28 11:40:06', 1207590691320098817, 1207590691320098817, 'cms:content:audit', '审核', 201005, 1207590691806638082, '67f5a12256f22ae1e9f4d4885afce078', NULL, 2, '/admin/cms/content/audit', NULL);
INSERT INTO `sys_resource` VALUES (1207590691835998210, 1, b'0', '2019-10-22 10:28:22', '2019-10-26 16:46:37', 1207590691320098817, 1207590691320098817, 'menu-delete', '删除 ', 3, 1207590691924078593, 'edac8e5783c95c9c4034dbf097d8d49d', NULL, 2, '/admin/resource/delete', NULL);
INSERT INTO `sys_resource` VALUES (1207590691844386817, 4, b'0', '2019-10-30 14:51:28', '2019-10-30 14:51:28', 1207590691320098817, 1207590691320098817, 'cms:content:hot', '热门', 201007, 1207590691806638082, '67f5a12256f22ae1e9f4d4885afce078', NULL, 2, '/admin/cms/categoryContent/setHot', NULL);
INSERT INTO `sys_resource` VALUES (1207590691848581122, 0, b'0', '2019-11-22 09:36:22', '2019-11-22 09:36:22', 1207590691320098817, 1207590691320098817, 'fieldType-update', '修改', 250103, 1207590691676614657, '20a24675d0fef15cdb43db30e2338a2f', NULL, 2, '/admin/advice/fieldType/update', NULL);
INSERT INTO `sys_resource` VALUES (1207590691848581123, 1, b'0', '2019-05-14 13:25:50', '2019-10-18 17:43:16', 1207590691320098817, 1207590691320098817, 'sys:role:update', '修改', 101002, 1207590691731140611, '4ab80b834574f3c55acbb30f69f972a6', NULL, 2, '/admin/reserve', NULL);
INSERT INTO `sys_resource` VALUES (1207590691848581124, 2, b'0', '2019-11-04 10:52:12', '2019-11-04 10:52:12', 1207590691320098817, 1207590691320098817, 'cms:label:delete', '删除', 111003, 1207590691689197570, '246534cb6ed6ed53d6fa41ad8aa64d5a', NULL, 2, '/admin/cms/label/delete', NULL);
INSERT INTO `sys_resource` VALUES (1207590691848581125, 0, b'0', '2019-11-06 10:25:13', '2019-11-06 10:25:13', 1207590691320098817, 1207590691320098817, 'cms:extended:list', '扩展属性', 2050, 1207590691634671617, '055a0693fb4274ef8461caf85841286c', 'extended/extended', 1, '/admin/cms/extendedProp/pages', 'zonghe');
INSERT INTO `sys_resource` VALUES (1207590691848581126, 0, b'0', '2019-11-06 11:44:16', '2019-11-06 11:44:16', 1207590691320098817, 1207590691320098817, 'cms:extended:list', '查询', 205004, 1207590691848581125, 'ad5149a678b83e0c78ef212bab91b1d5', NULL, 2, '/admin/cms/extended/page', NULL);
INSERT INTO `sys_resource` VALUES (1207590691856969729, 0, b'0', '2019-10-30 16:46:55', '2019-10-30 16:46:55', 1207590691320098817, 1207590691320098817, 'cms:content:attach', '附件', 201008, 1207590691806638082, '67f5a12256f22ae1e9f4d4885afce078', NULL, 2, '/admin/cms/contentAttach/pages', NULL);
INSERT INTO `sys_resource` VALUES (1207590691856969730, 0, b'0', '2019-11-22 10:28:00', '2019-11-22 10:28:00', 1207590691320098817, 1207590691320098817, 'advice-update', '编辑', 250202, 1207590691945050113, 'ef69bbfd77d1ac98cdcafd79be52d096', NULL, 2, '/admin/advice/update', NULL);
INSERT INTO `sys_resource` VALUES (1207590691856969731, 1, b'0', '2019-10-26 16:48:33', '2019-10-26 16:49:40', 1207590691320098817, 1207590691320098817, 'sysLog-list', '查询', 0, 1207590691877941250, 'c857bab117f5e5aa784e3d06f2398d87', NULL, 2, '/admin/sysLog/pages', NULL);
INSERT INTO `sys_resource` VALUES (1207590691856969732, 1, b'0', '2019-10-31 09:26:12', '2019-10-31 09:31:15', 1207590691320098817, 1207590691320098817, 'sys:api:status', '批量更改状态', 4, 1207590691907301377, 'df4f4cd50a6496a7dbbee833cef32a79', NULL, 2, '#', NULL);
INSERT INTO `sys_resource` VALUES (1207590691865358338, 2, b'0', '2019-10-22 09:39:48', '2019-10-22 09:40:01', 1207590691320098817, 1207590691320098817, 'sys:oss:save', '保存OSS配置', 109002, 1207590691877941251, 'c969c7d79cf54b29d2eca4e17c685b03', NULL, 2, '/admin/ossConfig/save', NULL);
INSERT INTO `sys_resource` VALUES (1207590691865358339, 2, b'0', '2019-10-26 17:06:42', '2019-10-26 17:10:15', 1207590691320098817, 1207590691320098817, 'oss-view', '查看', 109005, 1207590691877941251, 'c969c7d79cf54b29d2eca4e17c685b03', NULL, 2, '/admin/oss', NULL);
INSERT INTO `sys_resource` VALUES (1207590691865358340, 0, b'0', '2019-10-30 16:50:31', '2019-10-30 16:50:31', 1207590691320098817, 1207590691320098817, 'cms:content:attach:delete', '附件删除', 201010, 1207590691806638082, '67f5a12256f22ae1e9f4d4885afce078', NULL, 2, '/admin/cms/contentAttach/removeByIds', NULL);
INSERT INTO `sys_resource` VALUES (1207590691873746945, 0, b'0', '2019-11-22 09:34:28', '2019-11-22 09:34:28', 1207590691320098817, 1207590691320098817, 'fieldType-delete', '删除', 250102, 1207590691676614657, '20a24675d0fef15cdb43db30e2338a2f', NULL, 2, '/admin/advice/fieldType/removeByIds', NULL);
INSERT INTO `sys_resource` VALUES (1207590691877941250, 7, b'0', '2019-05-14 13:28:34', '2019-10-26 16:49:27', 1207590691320098817, 1207590691320098817, '', '系统日志', 1080, 1207590691705974787, '2c6caefe34d69a2ac987df1e9afb567f', 'sys/log', 1, '/admin/sysLog/pages', 'log');
INSERT INTO `sys_resource` VALUES (1207590691877941251, 3, b'0', '2019-10-21 10:19:27', '2019-10-26 16:50:52', 1207590691320098817, 1207590691320098817, '', '对象存储', 1090, 1207590691705974787, '2c6caefe34d69a2ac987df1e9afb567f', '/sys/oss', 1, '/admin/oss/pages', 'zonghe');
INSERT INTO `sys_resource` VALUES (1207590691882135554, 0, b'0', '2019-10-26 17:17:03', '2019-10-26 17:17:03', 1207590691320098817, 1207590691320098817, 'oss-list', '查询', 109008, 1207590691877941251, 'c969c7d79cf54b29d2eca4e17c685b03', NULL, 2, '/admin/oss/pages', NULL);
INSERT INTO `sys_resource` VALUES (1207590691882135555, 0, b'0', '2019-10-31 09:24:53', '2019-10-31 09:24:53', 1207590691320098817, 1207590691320098817, 'sys:api:list', '查询', 0, 1207590691907301377, 'df4f4cd50a6496a7dbbee833cef32a79', NULL, 2, '#', NULL);
INSERT INTO `sys_resource` VALUES (1207590691890524161, 1, b'0', '2019-10-22 10:09:39', '2019-10-26 16:47:14', 1207590691320098817, 1207590691320098817, 'menu-update', '编辑', 2, 1207590691924078593, 'edac8e5783c95c9c4034dbf097d8d49d', NULL, 2, '/admin/resource/update', NULL);
INSERT INTO `sys_resource` VALUES (1207590691890524162, 3, b'0', '2019-11-04 10:49:03', '2019-11-04 10:51:31', 1207590691320098817, 1207590691320098817, 'cms:label:save', '新增', 111001, 1207590691689197570, '246534cb6ed6ed53d6fa41ad8aa64d5a', NULL, 2, '/admin/cms/label/save', NULL);
INSERT INTO `sys_resource` VALUES (1207590691890524163, 1, b'0', '2019-10-31 09:28:09', '2019-10-31 09:31:43', 1207590691320098817, 1207590691320098817, 'sys:api:delete', '删除', 6, 1207590691907301377, 'df4f4cd50a6496a7dbbee833cef32a79', NULL, 2, '#', NULL);
INSERT INTO `sys_resource` VALUES (1207590691898912770, 1, b'0', '2019-10-30 13:24:37', '2019-10-30 13:24:48', 1207590691320098817, 1207590691320098817, NULL, 'Xss白名单管理', 30, NULL, NULL, NULL, 1, '#', 'system');
INSERT INTO `sys_resource` VALUES (1207590691907301377, 1, b'0', '2019-10-30 16:49:28', '2019-10-30 16:50:06', 1207590691320098817, 1207590691320098817, NULL, '接口管理', 1100, 1207590691705974787, '2c6caefe34d69a2ac987df1e9afb567f', 'sys/api', 1, '/admin/api/pages', 'suoding');
INSERT INTO `sys_resource` VALUES (1207590691907301378, 0, b'0', '2019-11-06 17:11:11', '2019-11-06 17:11:11', 1207590691320098817, 1207590691320098817, 'cms:recovery:detail', '查看', 206003, 1207590691982798851, 'fef72fae29826cb279447542b1bbb2b8', NULL, 2, '/admin/cms/categoryContent', NULL);
INSERT INTO `sys_resource` VALUES (1207590691915689986, 0, b'0', '2019-11-06 17:12:49', '2019-11-06 17:12:49', 1207590691320098817, 1207590691320098817, 'copyright:list:update', '修改', 2, 1207590691781472258, '5a459622041155566ca7cb889c6b043d', NULL, 2, 'admin/copyright/update', NULL);
INSERT INTO `sys_resource` VALUES (1207590691915689987, 0, b'0', '2019-11-06 11:31:53', '2019-11-06 11:31:53', 1207590691320098817, 1207590691320098817, 'cms:extended:update', '修改', 205002, 1207590691848581125, 'ad5149a678b83e0c78ef212bab91b1d5', NULL, 2, '/admin/cms/extended/update', NULL);
INSERT INTO `sys_resource` VALUES (1207590691915689988, 1, b'0', '2019-10-31 09:28:47', '2019-10-31 09:31:55', 1207590691320098817, 1207590691320098817, 'sys:api:debug', '调试', 7, 1207590691907301377, 'df4f4cd50a6496a7dbbee833cef32a79', NULL, 2, '#', NULL);
INSERT INTO `sys_resource` VALUES (1207590691924078593, 2, b'0', '2019-02-27 15:29:02', '2019-10-26 16:46:55', 1207590691320098817, 1207590691320098817, '', '菜单管理', 1020, 1207590691705974787, '2c6caefe34d69a2ac987df1e9afb567f', 'sys/menu', 1, '/admin/resource/all', 'menu');
INSERT INTO `sys_resource` VALUES (1207590691924078594, 0, b'0', '2019-02-27 15:29:40', '2019-02-27 15:29:40', 1207590691320098817, 1207590691320098817, 'sys:user:list', '用户管理', 1030, 1207590691705974787, '2c6caefe34d69a2ac987df1e9afb567f', 'sys/user', 1, '/admin/user/pages', 'admin');
INSERT INTO `sys_resource` VALUES (1207590691945050113, 4, b'0', '2019-11-22 09:09:37', '2019-11-22 09:09:37', 1207590691320098817, 1207590691320098817, 'advice-page', '咨询建议', 2502, 1207590691676614658, '212ced5404ff2a870c3d6d40619dc9f8', 'advice/advice', 1, '/admin/advice', 'bianji');
INSERT INTO `sys_resource` VALUES (1207590691953438721, 2, b'0', '2019-10-26 17:08:24', '2019-10-26 17:10:34', 1207590691320098817, 1207590691320098817, 'oss-download', '下载', 109006, 1207590691877941251, 'c969c7d79cf54b29d2eca4e17c685b03', NULL, 2, '#', NULL);
INSERT INTO `sys_resource` VALUES (1207590691957633026, 0, b'0', '2019-11-06 17:05:02', '2019-11-06 17:05:02', 1207590691320098817, 1207590691320098817, 'cms:recovery:recovery', '恢复', 206002, 1207590691982798851, 'fef72fae29826cb279447542b1bbb2b8', NULL, 2, '/admin/cms/categoryContent/recoveryByIds', NULL);
INSERT INTO `sys_resource` VALUES (1207590691957633027, 1, b'0', '2019-10-21 16:53:55', '2019-10-26 17:01:56', 1207590691320098817, 1207590691320098817, 'sys:dict:save', '新增', 104001, 1207590691668226050, '168e38c8-6b92-4445-bb4b-2979113d409b', NULL, 2, '/admin/dict/save', NULL);
INSERT INTO `sys_resource` VALUES (1207590691961827330, 0, b'0', '2019-07-16 09:35:29', '2019-07-16 09:35:26', 1207590691320098817, 1207590691320098817, 'sys:user:list', '查看', 103001, 1207590691924078594, 'eeb5967fd6e3597ab80e7fc36cbd81d8', NULL, 2, '/admin/reserve/his', NULL);
INSERT INTO `sys_resource` VALUES (1207590691961827331, 3, b'0', '2019-11-06 09:50:50', '2019-11-06 10:14:25', 1207590691320098817, 1207590691320098817, NULL, '评论管理', 2040, 1207590691634671617, '055a0693fb4274ef8461caf85841286c', '/comment/comment', 1, '/admin/cms/comment', 'bianji');
INSERT INTO `sys_resource` VALUES (1207590691961827332, 7, b'0', '2019-10-31 14:41:18', '2019-10-31 16:27:29', 1207590691320098817, 1207590691320098817, 'wechat:account', '微信号管理', 41, 1207590691722752003, '4196cd4d58f26716a879284d18e0d665', 'wechat/account', 1, '/wechat/account', 'zonghe');
INSERT INTO `sys_resource` VALUES (1207590691974410241, 0, b'0', '2019-10-30 16:49:30', '2019-10-30 16:49:30', 1207590691320098817, 1207590691320098817, 'cms:content:attach:save', '附件新增', 201009, 1207590691806638082, '67f5a12256f22ae1e9f4d4885afce078', NULL, 2, '/admin/cms/contentAttach/save', NULL);
INSERT INTO `sys_resource` VALUES (1207590691974410242, 1, b'0', '2019-05-14 13:23:32', '2019-10-18 13:51:27', 1207590691320098817, 1207590691320098817, 'sys:role:save', '新增', 101001, 1207590691731140611, '4ab80b834574f3c55acbb30f69f972a6', NULL, 2, '/admin/approve', NULL);
INSERT INTO `sys_resource` VALUES (1207590691974410243, 4, b'0', '2019-10-25 11:12:05', '2019-11-04 10:49:44', 1207590691320098817, 1207590691320098817, 'cms:category:save', '新增', 202001, 1207590691819220995, '7a42bfc3ee7046535c200ce15b4a7bf1', NULL, 2, '/admin/cms/category/save', NULL);
INSERT INTO `sys_resource` VALUES (1207590691982798850, 1, b'0', '2019-10-21 16:55:57', '2019-10-26 17:02:10', 1207590691320098817, 1207590691320098817, 'sys:dict:update', '修改', 104002, 1207590691668226050, '168e38c8-6b92-4445-bb4b-2979113d409b', NULL, 2, '/admin/dict/update', NULL);
INSERT INTO `sys_resource` VALUES (1207590691982798851, 1, b'0', '2019-11-06 16:55:54', '2019-11-06 16:56:21', 1207590691320098817, 1207590691320098817, 'cms:recycling:list', '回收站', 2060, 1207590691634671617, '055a0693fb4274ef8461caf85841286c', '/recycling/recycling-list', 1, '/admin/cms/categoryContent/page', 'shanchu');
INSERT INTO `sys_resource` VALUES (1239834247431258113, 0, b'0', NULL, NULL, NULL, NULL, NULL, '代码生成', 0, NULL, NULL, '#', 1, '#', 'tubiao');
INSERT INTO `sys_resource` VALUES (1239834812055879681, 2, b'0', NULL, NULL, NULL, NULL, NULL, '数据库管理', 0, 1239834247431258113, NULL, '/code/database-list', 1, '/admin/sysDatabase/pages', 'shezhi');
INSERT INTO `sys_resource` VALUES (1239835116650430466, 0, b'0', NULL, NULL, NULL, NULL, NULL, '代码生成', 1, 1239834247431258113, NULL, '/code/code-generation', 1, '/admin/generator/page', 'config');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint(19) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '更新人',
  `code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色代码',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `sequence` int(10) NULL DEFAULT NULL COMMENT '排序',
  `status` int(1) NULL DEFAULT NULL COMMENT '激活状态（启用，未启用）',
  `role_linked` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建角色编码链表',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1207590691953438721, 0, b'0', '2019-02-26 11:11:50', NULL, 1207590691320098817, NULL, 'SYS_ADMIN', '系统管理员1', 1, 1, 'SYS_ADMIN');

-- ----------------------------
-- Table structure for sys_role_resource
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_resource`;
CREATE TABLE `sys_role_resource`  (
  `id` bigint(20) NOT NULL,
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `updater` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `role_id` bigint(19) NULL DEFAULT NULL COMMENT '角色主键id',
  `resource_id` bigint(20) NULL DEFAULT NULL COMMENT '菜单主键id',
  `resource_id_bak` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单主键id',
  `role_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色名称（冗余字段）',
  `resource_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单名称（冗余字段）',
  `resource_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单请求（冗余字段）',
  `is_half_checked` bit(1) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role_resource
-- ----------------------------
INSERT INTO `sys_role_resource` VALUES (1239841403014504449, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1239834247431258113, NULL, '系统管理员1', '代码生成', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504450, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1239834812055879681, NULL, '系统管理员1', '数据库管理', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504451, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1239835116650430466, NULL, '系统管理员1', '代码生成', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504452, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691705974787, NULL, '系统管理员1', '系统管理', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504453, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691731140611, NULL, '系统管理员1', '角色管理', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504454, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691974410242, NULL, '系统管理员1', '新增', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504455, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691848581123, NULL, '系统管理员1', '修改', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504456, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691668226051, NULL, '系统管理员1', '删除', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504457, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691768889345, NULL, '系统管理员1', '批量删除', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504458, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691924078593, NULL, '系统管理员1', '菜单管理', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504459, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691798249476, NULL, '系统管理员1', '新增', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504460, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691890524161, NULL, '系统管理员1', '编辑', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504461, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691835998210, NULL, '系统管理员1', '删除 ', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504462, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691924078594, NULL, '系统管理员1', '用户管理', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504463, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691961827330, NULL, '系统管理员1', '查看', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504464, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691605311492, NULL, '系统管理员1', '新增', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504465, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691613700099, NULL, '系统管理员1', '修改', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504466, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691622088705, NULL, '系统管理员1', '删除', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504467, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691668226050, NULL, '系统管理员1', '数据字典', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504468, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691957633027, NULL, '系统管理员1', '新增', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504469, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691982798850, NULL, '系统管理员1', '修改', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504470, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691664031745, NULL, '系统管理员1', '删除', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504471, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691819220994, NULL, '系统管理员1', '定时任务', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504472, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691877941250, NULL, '系统管理员1', '系统日志', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504473, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691856969731, NULL, '系统管理员1', '查询', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504474, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691877941251, NULL, '系统管理员1', '对象存储', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504475, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691815026690, NULL, '系统管理员1', 'OSS配置', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504476, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691865358338, NULL, '系统管理员1', '保存OSS配置', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504477, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691655643138, NULL, '系统管理员1', '上传文件', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504478, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691731140610, NULL, '系统管理员1', '批量删除', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504479, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691865358339, NULL, '系统管理员1', '查看', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504480, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691953438721, NULL, '系统管理员1', '下载', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504481, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691722752002, NULL, '系统管理员1', '删除', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504482, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691882135554, NULL, '系统管理员1', '查询', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504483, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691907301377, NULL, '系统管理员1', '接口管理', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504484, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691882135555, NULL, '系统管理员1', '查询', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504485, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691722752001, NULL, '系统管理员1', '新增', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504486, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691689197571, NULL, '系统管理员1', '重置', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504487, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691714363394, NULL, '系统管理员1', '批量删除', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504488, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691856969732, NULL, '系统管理员1', '批量更改状态', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504489, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691643060226, NULL, '系统管理员1', '修改', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504490, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691890524163, NULL, '系统管理员1', '删除', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504491, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691915689988, NULL, '系统管理员1', '调试', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504492, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691689197570, NULL, '系统管理员1', '标签管理', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504493, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691890524162, NULL, '系统管理员1', '新增', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504494, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691806638084, NULL, '系统管理员1', '修改', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504495, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691848581124, NULL, '系统管理员1', '删除', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504496, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691622088706, NULL, '系统管理员1', '查询', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504497, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691634671617, NULL, '系统管理员1', 'CMS管理', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504498, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691806638082, NULL, '系统管理员1', '内容管理', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504499, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691613700098, NULL, '系统管理员1', '新增', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504500, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691789860865, NULL, '系统管理员1', '修改', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504501, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691806638083, NULL, '系统管理员1', '删除', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504502, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691705974786, NULL, '系统管理员1', '查询', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504503, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691835998209, NULL, '系统管理员1', '审核', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504504, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691655643139, NULL, '系统管理员1', '置顶', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504505, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691844386817, NULL, '系统管理员1', '热门', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504506, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691856969729, NULL, '系统管理员1', '附件', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504507, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691974410241, NULL, '系统管理员1', '附件新增', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504508, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691865358340, NULL, '系统管理员1', '附件删除', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403014504509, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691798249475, NULL, '系统管理员1', '属性', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403341660161, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691819220995, NULL, '系统管理员1', '栏目管理', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403341660162, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691974410243, NULL, '系统管理员1', '新增', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403341660163, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691685003266, NULL, '系统管理员1', '修改', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403341660164, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691643060225, NULL, '系统管理员1', '删除', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403341660165, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691701780481, NULL, '系统管理员1', '启用/禁用', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403404574721, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691731140612, NULL, '系统管理员1', '属性', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403404574722, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691798249474, NULL, '系统管理员1', '属性新增', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403404574723, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691651448833, NULL, '系统管理员1', '属性删除', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403408769025, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691961827331, NULL, '系统管理员1', '评论管理', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403408769026, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691848581125, NULL, '系统管理员1', '扩展属性', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403408769027, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691777277954, NULL, '系统管理员1', '新增', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403408769028, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691915689987, NULL, '系统管理员1', '修改', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403408769029, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691789860866, NULL, '系统管理员1', '删除', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403408769030, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691848581126, NULL, '系统管理员1', '查询', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403408769031, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691982798851, NULL, '系统管理员1', '回收站', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403408769032, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691664031746, NULL, '系统管理员1', '查询', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403408769033, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691957633026, NULL, '系统管理员1', '恢复', NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1239841403408769034, 0, b'0', NULL, NULL, NULL, NULL, 1207590691953438721, 1207590691907301378, NULL, '系统管理员1', '查看', NULL, b'0');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint(19) NOT NULL,
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `updater` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登陆账号',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `tel` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '座机',
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色',
  `status` int(1) NULL DEFAULT NULL COMMENT '状态',
  `employee_id` bigint(20) NULL DEFAULT NULL COMMENT '员工id',
  `mobile` char(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '显示名称（冗余，保存时取员工姓名）',
  `email` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `role_linked` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色编码链表',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1207590691320098817, 4, b'0', '2019-06-03 11:31:33', '2019-10-22 19:17:43', '1207590691320098817', '1207590691320098817', 'admin', '{bcrypt}$2a$10$JiWrEt.4W.IzHyqaqSt25O6cFJzxk9E2oDvkNSTIHGbPjRfAZ.T7S', '座机号', 1207590691953438721, 1, NULL, '手机号', '管理员—徐泓卿', '邮箱号', 'SYS_ADMIN');

-- ----------------------------
-- Table structure for sys_user_field_value
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_field_value`;
CREATE TABLE `sys_user_field_value`  (
  `id` bigint(20) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(36) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(36) NULL DEFAULT NULL COMMENT '更新人',
  `user_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户主键',
  `advice_id` bigint(36) NULL DEFAULT NULL COMMENT '咨询建议主键',
  `field_id` bigint(36) NOT NULL COMMENT '咨询建议字段主键',
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '值（对应选项值）',
  `data_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wechat_cp_kefu_msg
-- ----------------------------
DROP TABLE IF EXISTS `wechat_cp_kefu_msg`;
CREATE TABLE `wechat_cp_kefu_msg`  (
  `id` bigint(20) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `user_id` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'openid',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '发送内容',
  `msg_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '消息类型',
  `agent_id` int(7) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wechat_mp_account
-- ----------------------------
DROP TABLE IF EXISTS `wechat_mp_account`;
CREATE TABLE `wechat_mp_account`  (
  `id` bigint(20) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '服务号代码',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '服务号名称',
  `appId` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '一个公众号的appid',
  `secret` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公众号的appsecret',
  `token` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '接口配置里的Token值',
  `aesKey` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '接口配置里的EncodingAESKey值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wechat_mp_account
-- ----------------------------
INSERT INTO `wechat_mp_account` VALUES (1207590692221874182, 0, b'0', '2020-03-12 17:06:23', '2020-03-12 17:06:26', 1207590691320098817, 1207590691320098817, 'a1', '风云网络', 'wx79f37875acffa66d', '0e7f7c0914b0798ff55124955f78af0f', 'test', '123');
INSERT INTO `wechat_mp_account` VALUES (1207590692230262786, 0, b'0', '2020-03-12 17:08:43', '2020-03-12 17:08:45', 1207590691320098817, 1207590691320098817, 'szyc_test', '苏州烟草测试号1', 'wx60ac16005e550fc7', '38af32c7e28b6af8c7a75ef28598de3e', 'szyc_weixin', NULL);

-- ----------------------------
-- Table structure for wechat_mp_menu
-- ----------------------------
DROP TABLE IF EXISTS `wechat_mp_menu`;
CREATE TABLE `wechat_mp_menu`  (
  `id` bigint(20) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `mp_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '服务号code',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类型',
  `url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'url',
  `wx_key` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'key',
  `app_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'appId',
  `page_path` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '小程序页面路径',
  `media_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '素材id',
  `parent_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '父菜单id',
  `sequence` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for xss_tag_attribute_protocol
-- ----------------------------
DROP TABLE IF EXISTS `xss_tag_attribute_protocol`;
CREATE TABLE `xss_tag_attribute_protocol`  (
  `id` bigint(20) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `tag` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标签名',
  `attribute_key` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '属性名',
  `attribute_value` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '属性值',
  `protocol` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '协议值',
  `status` bit(1) NULL DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
