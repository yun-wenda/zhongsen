/*
 Navicat Premium Data Transfer

 Source Server         : 腾讯数据库
 Source Server Type    : MySQL
 Source Server Version : 50718
 Source Host           : cdb-hb8s6hqa.bj.tencentcdb.com:10183
 Source Schema         : common

 Target Server Type    : MySQL
 Target Server Version : 50718
 File Encoding         : 65001

 Date: 20/03/2021 15:43:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for oauth_client_details
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_details`;
CREATE TABLE `oauth_client_details`  (
  `client_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `resource_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `client_secret` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `scope` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `authorized_grant_types` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `web_server_redirect_uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `authorities` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `access_token_validity` int(11) NULL DEFAULT NULL,
  `refresh_token_validity` int(11) NULL DEFAULT NULL,
  `additional_information` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `autoapprove` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'false',
  PRIMARY KEY (`client_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '授权' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of oauth_client_details
-- ----------------------------
INSERT INTO `oauth_client_details` VALUES ('client_2', 'order', '{bcrypt}$2a$04$NqiScrd3C57bCFTOynqfEu.3MIpiI2wtL1OzxHCneMyWdWeIBNA9C', 'select', 'password,refresh_token,sms_code,one_code', '', 'client', 36000, 36060, NULL, 'false');
INSERT INTO `oauth_client_details` VALUES ('client_3', 'order', '{bcrypt}$2a$04$NqiScrd3C57bCFTOynqfEu.3MIpiI2wtL1OzxHCneMyWdWeIBNA9C', 'all', 'authorization_code', 'http://www.baidu.com', NULL, 7200, 7260, NULL, 'false');

-- ----------------------------
-- Table structure for schedule_job
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job`;
CREATE TABLE `schedule_job`  (
  `id` bigint(19) NOT NULL,
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL,
  `updater` bigint(19) NULL DEFAULT NULL,
  `bean_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数',
  `cron_expression` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'cron表达式',
  `status` bit(1) NULL DEFAULT b'1' COMMENT '状态（启用，未启用）',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of schedule_job
-- ----------------------------
INSERT INTO `schedule_job` VALUES (1372128358889779202, 3, b'0', '2021-03-17 18:11:08', '2021-03-18 15:40:28', 1207590691320098817, 1207590691320098817, 'noticeTask', '', '0 0 9 18,19,20 * ? ', b'0', '18,19,20号的9点发送短信提醒');

-- ----------------------------
-- Table structure for schedule_job_log
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job_log`;
CREATE TABLE `schedule_job_log`  (
  `id` bigint(19) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '更新人',
  `job_id` bigint(19) NULL DEFAULT NULL COMMENT '任务id',
  `bean_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数',
  `status` bit(1) NULL DEFAULT b'1' COMMENT '任务状态   1：成功   0：失败',
  `error` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '失败信息',
  `times` int(11) NULL DEFAULT NULL COMMENT '耗时(单位：毫秒)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of schedule_job_log
-- ----------------------------
INSERT INTO `schedule_job_log` VALUES (1372352049538166785, 0, b'0', '2021-03-18 09:00:00', '2021-03-18 09:00:00', NULL, NULL, 1372128358889779202, 'noticeTask', '', b'0', 'java.lang.reflect.InvocationTargetException', 147);
INSERT INTO `schedule_job_log` VALUES (1372352049718554625, 0, b'0', '2021-03-18 09:00:00', '2021-03-18 09:00:00', NULL, NULL, 1372128358889779202, 'noticeTask', '', b'1', NULL, 215);

-- ----------------------------
-- Table structure for subject
-- ----------------------------
DROP TABLE IF EXISTS `subject`;
CREATE TABLE `subject`  (
  `id` bigint(19) NOT NULL,
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '更新人',
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `sub_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '副标题',
  `head_pic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头图',
  `subject_year` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '专题年代',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '专题' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of subject
-- ----------------------------

-- ----------------------------
-- Table structure for sys_database
-- ----------------------------
DROP TABLE IF EXISTS `sys_database`;
CREATE TABLE `sys_database`  (
  `id` bigint(30) NOT NULL COMMENT '?Զ???????',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '?ֹ???',
  `is_deleted` bit(1) NOT NULL COMMENT '?߼?ɾ??',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '????ʱ?',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '????ʱ?',
  `creator` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '?????',
  `updater` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '?????',
  `database_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `driver_class_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `database_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ip_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `database_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_database
-- ----------------------------
INSERT INTO `sys_database` VALUES (1239843481359245315, 0, b'0', NULL, NULL, NULL, NULL, 'MySQL', 'com.mysql.jdbc.Driver', 'jdbc:mysql://cdb-hb8s6hqa.bj.tencentcdb.com:10183/guoshi?useUnicode=true&characterEncoding=UTF-8&serverTimezone=GMT%2B8', 'root', 'Kuateyi123!@', 'cdb-hb8s6hqa.bj.tencentcdb.com:10183', '国事数据库');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `id` bigint(19) NOT NULL,
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '更新人',
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典代码',
  `code_prefix` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '编号拼接',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典名称',
  `sequence` int(10) NULL DEFAULT NULL COMMENT '排序',
  `status` int(1) NULL DEFAULT NULL COMMENT '激活状态（启用，未启用）',
  `value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '值域（扩展时使用第三字段）',
  `parent_id` bigint(19) NULL DEFAULT NULL COMMENT '父id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '数据字典' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES (1207590691320098817, 1, b'0', '2019-10-21 15:01:51', '2021-03-20 15:23:59', 1207590691320098817, 1207590691320098817, 'LocalServer', ',server-code,LocalServer,', '服务器', 4, 1, 'LocalServer', 1207590691454316547);
INSERT INTO `sys_dict` VALUES (1207590691320098818, 1, b'0', '2019-10-31 09:12:27', '2021-02-19 13:27:46', 1207590691320098817, 1207590691320098817, 'apiType', ',apiType,', '接口请求类型', 402, 1, '0', NULL);
INSERT INTO `sys_dict` VALUES (1207590691332681730, 1, b'0', '2019-10-21 15:00:05', '2021-03-20 15:23:53', 1207590691320098817, 1207590691320098817, 'QiniuCloud', ',server-code,QiniuCloud,', '七牛云', 1, 1, 'QiniuCloud', 1207590691454316547);
INSERT INTO `sys_dict` VALUES (1207590691341070338, 1, b'0', '2019-10-31 10:18:28', '2021-03-20 15:24:48', 1207590691320098817, 1207590691320098817, 'responseTypeTwo', ',responseType,responseTypeTwo,', 'application/json', 0, 1, '0', 1207590691353653251);
INSERT INTO `sys_dict` VALUES (1207590691353653251, 1, b'0', '2019-10-31 10:16:27', '2021-02-19 13:27:34', 1207590691320098817, 1207590691320098817, 'responseType', ',responseType,', '接口返回类型', 401, 1, '0', NULL);
INSERT INTO `sys_dict` VALUES (1207590691362041859, 1, b'0', '2019-07-17 15:14:21', '2021-03-20 15:24:05', 1207590691320098817, 1207590691320098817, 'new', ',operate,new,', '新增', 1, 1, '1', 1207590691391401986);
INSERT INTO `sys_dict` VALUES (1207590691374624769, 3, b'0', '2019-10-21 09:43:20', '2021-03-20 15:23:20', 1207590691320098817, 1207590691320098817, 'video', ',fileType,video,', '视频', 0, 1, ',mp4,avi,flv,mov,', 1207590691483676674);
INSERT INTO `sys_dict` VALUES (1207590691374624771, 2, b'0', '2019-10-31 09:13:34', '2021-03-20 15:25:00', 1207590691320098817, 1207590691320098817, 'Get', ',apiType,Get,', 'Get类型接口请求', 0, 1, '0', 1207590691320098818);
INSERT INTO `sys_dict` VALUES (1207590691383013378, 2, b'0', '2019-03-07 14:10:49', '2021-03-20 15:22:56', 1207590691320098817, 1207590691320098817, 'CheckPending', ',validStatus,CheckPending,', '待审核', 0, 1, '待审核', 1207590691445927939);
INSERT INTO `sys_dict` VALUES (1207590691383013379, 1, b'0', '2019-10-31 10:03:14', '2021-02-19 13:27:24', 1207590691320098817, 1207590691320098817, 'paramType', ',paramType,', '第三方接口参数类型', 400, 1, '0', NULL);
INSERT INTO `sys_dict` VALUES (1207590691391401986, 1, b'0', '2019-07-17 15:13:06', '2021-02-19 13:28:55', 1207590691320098817, 1207590691320098817, 'operate', ',operate,', '操作类型', 301, 1, '0', NULL);
INSERT INTO `sys_dict` VALUES (1207590691403984898, 1, b'0', '2019-10-31 10:17:46', '2021-03-20 15:24:42', 1207590691320098817, 1207590691320098817, 'responseTypeOne', ',responseType,responseTypeOne,', 'text/plain', 0, 1, '0', 1207590691353653251);
INSERT INTO `sys_dict` VALUES (1207590691412373508, 1, b'0', '2019-04-29 15:20:56', '2021-03-20 15:22:22', 1207590691320098817, 1207590691320098817, 'm', ',gender,m,', '男', 10, 1, '0', 1207590691420762114);
INSERT INTO `sys_dict` VALUES (1207590691420762114, 2, b'0', '2019-04-29 15:20:07', '2021-02-19 13:56:31', 1207590691320098817, 1207590691320098817, 'gender', ',gender,', '性别', 20, 1, '0', NULL);
INSERT INTO `sys_dict` VALUES (1207590691429150722, 1, b'0', '2019-10-31 10:15:00', '2021-03-20 15:24:23', 1207590691320098817, 1207590691320098817, 'paramTypeThree', ',paramType,paramTypeThree,', 'commonParam', 0, 1, '0', 1207590691383013379);
INSERT INTO `sys_dict` VALUES (1207590691437539330, 2, b'0', '2019-10-21 10:33:41', '2021-03-20 15:23:27', 1207590691320098817, 1207590691320098817, 'audio', ',fileType,audio,', '音频', 1, 1, ',mp3,wav,wma,flac,', 1207590691483676674);
INSERT INTO `sys_dict` VALUES (1207590691445927939, 1, b'0', '2019-03-07 14:09:45', '2021-02-19 13:27:09', 1207590691320098817, 1207590691320098817, 'validStatus', ',validStatus,', '审核状态', 30, 1, '审核状态', NULL);
INSERT INTO `sys_dict` VALUES (1207590691454316546, 1, b'0', '2019-03-07 14:10:57', '2021-03-20 15:23:02', 1207590691320098817, 1207590691320098817, 'pass', ',validStatus,pass,', '审核通过', 1, 1, '审核通过', 1207590691445927939);
INSERT INTO `sys_dict` VALUES (1207590691454316547, 1, b'0', '2019-10-21 14:58:00', '2021-02-19 13:27:01', 1207590691320098817, 1207590691320098817, 'server-code', ',server-code,', '文件存储服务器', 200, 1, '文件存储服务器', NULL);
INSERT INTO `sys_dict` VALUES (1207590691454316548, 2, b'0', '2019-10-21 10:42:10', '2021-03-20 15:23:33', 1207590691320098817, 1207590691320098817, 'document', ',fileType,document,', '文本', 2, 1, ',txt,doc,docx,xls,xlsx,pdf，', 1207590691483676674);
INSERT INTO `sys_dict` VALUES (1207590691462705153, 1, b'0', '2019-03-07 14:11:52', '2021-03-20 15:23:08', 1207590691320098817, 1207590691320098817, 'notPass', ',validStatus,notPass,', '审核不通过', 2, 1, '审核不通过', 1207590691445927939);
INSERT INTO `sys_dict` VALUES (1207590691466899457, 3, b'0', '2019-07-17 15:14:56', '2021-03-20 15:24:17', 1207590691320098817, 1207590691320098817, 'delete', ',operate,delete,', '删除', 3, 1, '3', 1207590691391401986);
INSERT INTO `sys_dict` VALUES (1207590691466899458, 1, b'0', '2019-10-31 10:07:39', '2021-03-20 15:24:29', 1207590691320098817, 1207590691320098817, 'paramTypeOne', ',paramType,paramTypeOne,', 'application/x-www-form-urlencoded', 0, 1, '0', 1207590691383013379);
INSERT INTO `sys_dict` VALUES (1207590691466899459, 1, b'0', '2019-10-31 10:12:13', '2021-03-20 15:24:36', 1207590691320098817, 1207590691320098817, 'paramTypeTwo', ',paramType,paramTypeTwo,', 'application/json', 0, 1, '0', 1207590691383013379);
INSERT INTO `sys_dict` VALUES (1207590691475288066, 1, b'0', '2019-04-29 15:21:38', '2021-03-20 15:22:08', 1207590691320098817, 1207590691320098817, 'f', ',gender,f,', '女', 5, 1, '1', 1207590691420762114);
INSERT INTO `sys_dict` VALUES (1207590691475288067, 1, b'0', '2019-07-17 15:14:41', '2021-03-20 15:24:11', 1207590691320098817, 1207590691320098817, 'update', ',operate,update,', '更新', 2, 1, '2', 1207590691391401986);
INSERT INTO `sys_dict` VALUES (1207590691483676674, 1, b'0', '2019-10-21 09:40:23', '2021-02-19 13:26:52', 1207590691320098817, 1207590691320098817, 'fileType', ',fileType,', '文件类型', 100, 1, '文件类型', NULL);
INSERT INTO `sys_dict` VALUES (1207590691492065281, 2, b'0', '2019-10-21 10:44:58', '2021-03-20 15:23:46', 1207590691320098817, 1207590691320098817, 'picture', ',fileType,picture,', '图片', 3, 1, ',bmp,jpg,png,jpeg,', 1207590691483676674);
INSERT INTO `sys_dict` VALUES (1207590691500453890, 1, b'0', '2019-10-31 09:13:53', '2021-03-20 15:24:54', 1207590691320098817, 1207590691320098817, 'Post', ',apiType,Post,', 'Post类型接口请求', 0, 1, '0', 1207590691320098818);
INSERT INTO `sys_dict` VALUES (1239842498828599297, 0, b'0', '2020-10-30 16:07:45', '2020-10-30 16:07:45', NULL, NULL, 'sys_config', ',sys_config,', '系统参数', 70, 1, 'sys_config', NULL);
INSERT INTO `sys_dict` VALUES (1288300148605235202, 1, b'0', NULL, '2021-02-19 13:27:17', NULL, 1207590691320098817, 'oss_qr_code_link_pdf', ',oss_qr_code_link_pdf,', '对象存储pdf二维码前缀', 500, 1, 'http://1711yz7467.imwork.net/pdfjs-2.4.456-es5-dist/web/viewer.html?file=', NULL);
INSERT INTO `sys_dict` VALUES (1362236153320103937, 4, b'0', '2021-02-18 11:03:03', '2021-02-19 13:56:40', 1207590691320098817, 1207590691320098817, 'wechat_type', ',wechat_type,', '微信号类型', 21, 1, 'wechat_type', NULL);
INSERT INTO `sys_dict` VALUES (1362236274380300290, 1, b'0', '2021-02-18 11:03:32', '2021-03-20 15:22:29', 1207590691320098817, 1207590691320098817, 'wechat_ma', ',wechat_type,wechat_ma,', '小程序', 1, 1, 'wechat_ma', 1362236153320103937);
INSERT INTO `sys_dict` VALUES (1362236410191863810, 1, b'0', '2021-02-18 11:04:04', '2021-03-20 15:22:35', 1207590691320098817, 1207590691320098817, 'wechat_mp', ',wechat_type,wechat_mp,', '公众号', 2, 1, 'wechat_mp', 1362236153320103937);
INSERT INTO `sys_dict` VALUES (1364490257811931138, 1, b'0', '2021-02-24 16:20:03', '2021-03-20 15:22:45', 1207590691320098817, 1207590691320098817, 'wechat_open', ',wechat_type,wechat_open,', '开放平台', 3, 1, '开放平台', 1362236153320103937);
INSERT INTO `sys_dict` VALUES (1364769496733851650, 3, b'0', '2021-02-25 10:49:39', '2021-03-20 15:23:14', 1207590691320098817, 1207590691320098817, 'upload_server_code', ',sys_config,upload_server_code,', 'oss上传存储类型', 1, 1, 'LocalServer', 1239842498828599297);

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` bigint(19) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '更新人',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `operation` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户操作',
  `method` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求方法',
  `params` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '请求参数',
  `time` bigint(20) NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'IP地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES (1373165451199971330, 0, b'0', '2021-03-20 14:52:10', '2021-03-20 14:52:10', 1207590691320098817, 1207590691320098817, 'admin', 'SysLog:分页查询', 'com.simba.scaffold.security.business.controller.SysLogController.list()', '[{\"pageSize\":10,\"orders\":[{\"column\":\"create_date\",\"sort\":\"desc\"}],\"page\":{\"current\":1,\"total\":0,\"pages\":0,\"size\":10,\"records\":[],\"searchCount\":true,\"orders\":[]},\"currentPage\":1}]', 167, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373169493561090050, 0, b'0', '2021-03-20 15:08:14', '2021-03-20 15:08:14', NULL, NULL, 'admin', '登陆', 'login', NULL, 0, '127.0.0.1');
INSERT INTO `sys_log` VALUES (1373171639585148930, 0, b'0', '2021-03-20 15:16:46', '2021-03-20 15:16:46', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691383013378]', 224, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373171641971707905, 0, b'0', '2021-03-20 15:16:46', '2021-03-20 15:16:46', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691445927939]', 165, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373171645692055554, 0, b'0', '2021-03-20 15:16:47', '2021-03-20 15:16:47', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691383013378]', 218, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373171658413379586, 0, b'0', '2021-03-20 15:16:50', '2021-03-20 15:16:50', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691383013378]', 198, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373171698217324546, 0, b'0', '2021-03-20 15:17:00', '2021-03-20 15:17:00', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1371699136230948865]', 196, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373171707138609154, 0, b'0', '2021-03-20 15:17:02', '2021-03-20 15:17:02', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:删除', 'com.simba.scaffold.security.business.controller.DictController.removeByIds()', '[\"1371699136230948865\"]', 270, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373171712599592962, 0, b'0', '2021-03-20 15:17:03', '2021-03-20 15:17:03', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1371699198524751873]', 220, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373171723194404866, 0, b'0', '2021-03-20 15:17:06', '2021-03-20 15:17:06', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1371699080572534785]', 171, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373171731629150210, 0, b'0', '2021-03-20 15:17:08', '2021-03-20 15:17:08', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1371699198524751873]', 220, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373171738902073345, 0, b'0', '2021-03-20 15:17:10', '2021-03-20 15:17:10', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:删除', 'com.simba.scaffold.security.business.controller.DictController.removeByIds()', '[\"1371699198524751873\"]', 263, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373171744681824257, 0, b'0', '2021-03-20 15:17:11', '2021-03-20 15:17:11', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1371699080572534785]', 171, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373171758401392641, 0, b'0', '2021-03-20 15:17:14', '2021-03-20 15:17:14', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1371699080572534785]', 176, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373171766416707585, 0, b'0', '2021-03-20 15:17:16', '2021-03-20 15:17:16', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:删除', 'com.simba.scaffold.security.business.controller.DictController.removeByIds()', '[\"1371699080572534785\"]', 254, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373172039189073921, 0, b'0', '2021-03-20 15:18:21', '2021-03-20 15:18:21', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691445927939]', 168, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373172052019449858, 0, b'0', '2021-03-20 15:18:24', '2021-03-20 15:18:24', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1370291691979214849]', 171, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373172166700109825, 0, b'0', '2021-03-20 15:18:52', '2021-03-20 15:18:52', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1288300148605235202]', 166, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373172183481520129, 0, b'0', '2021-03-20 15:18:56', '2021-03-20 15:18:56', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691500453890]', 207, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373172186128125953, 0, b'0', '2021-03-20 15:18:56', '2021-03-20 15:18:56', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691374624771]', 206, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373172192046288897, 0, b'0', '2021-03-20 15:18:58', '2021-03-20 15:18:58', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691403984898]', 206, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373172198845255682, 0, b'0', '2021-03-20 15:18:59', '2021-03-20 15:18:59', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691391401986]', 169, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373172268575559682, 0, b'0', '2021-03-20 15:19:16', '2021-03-20 15:19:16', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1364769496733851650]', 204, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373172293426810882, 0, b'0', '2021-03-20 15:19:22', '2021-03-20 15:19:22', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691320098817]', 209, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373172303845462018, 0, b'0', '2021-03-20 15:19:24', '2021-03-20 15:19:24', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1364769496733851650]', 207, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373172313181982721, 0, b'0', '2021-03-20 15:19:26', '2021-03-20 15:19:26', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1364769496733851650]', 197, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373172980697075714, 0, b'0', '2021-03-20 15:22:06', '2021-03-20 15:22:06', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691475288066]', 205, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373172985646354433, 0, b'0', '2021-03-20 15:22:07', '2021-03-20 15:22:07', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691475288066]', 193, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173041476734977, 0, b'0', '2021-03-20 15:22:20', '2021-03-20 15:22:20', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691475288066]', 204, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173044400164865, 0, b'0', '2021-03-20 15:22:21', '2021-03-20 15:22:21', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691412373508]', 205, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173047516532738, 0, b'0', '2021-03-20 15:22:22', '2021-03-20 15:22:22', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691412373508]', 209, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173068962009089, 0, b'0', '2021-03-20 15:22:27', '2021-03-20 15:22:27', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1362236274380300290]', 207, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173073596715010, 0, b'0', '2021-03-20 15:22:28', '2021-03-20 15:22:28', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1362236274380300290]', 204, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173097051262977, 0, b'0', '2021-03-20 15:22:33', '2021-03-20 15:22:33', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1362236410191863810]', 212, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173100830330881, 0, b'0', '2021-03-20 15:22:34', '2021-03-20 15:22:34', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1362236410191863810]', 208, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173136121204737, 0, b'0', '2021-03-20 15:22:43', '2021-03-20 15:22:43', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1364490257811931138]', 207, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173139665391617, 0, b'0', '2021-03-20 15:22:43', '2021-03-20 15:22:43', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1364490257811931138]', 199, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173181881061377, 0, b'0', '2021-03-20 15:22:54', '2021-03-20 15:22:54', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691383013378]', 213, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173186306052097, 0, b'0', '2021-03-20 15:22:55', '2021-03-20 15:22:55', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691383013378]', 195, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173207084634113, 0, b'0', '2021-03-20 15:23:00', '2021-03-20 15:23:00', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691454316546]', 204, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173210993725441, 0, b'0', '2021-03-20 15:23:00', '2021-03-20 15:23:00', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691454316546]', 198, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173232414035969, 0, b'0', '2021-03-20 15:23:06', '2021-03-20 15:23:06', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691462705153]', 210, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173237958905858, 0, b'0', '2021-03-20 15:23:07', '2021-03-20 15:23:07', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691462705153]', 208, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173259815424001, 0, b'0', '2021-03-20 15:23:12', '2021-03-20 15:23:12', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1364769496733851650]', 202, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173263061815297, 0, b'0', '2021-03-20 15:23:13', '2021-03-20 15:23:13', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1364769496733851650]', 196, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173283488075777, 0, b'0', '2021-03-20 15:23:18', '2021-03-20 15:23:18', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691374624769]', 208, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173287195840514, 0, b'0', '2021-03-20 15:23:19', '2021-03-20 15:23:19', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691374624769]', 205, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173304908386305, 0, b'0', '2021-03-20 15:23:23', '2021-03-20 15:23:23', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691483676674]', 167, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173313464766465, 0, b'0', '2021-03-20 15:23:25', '2021-03-20 15:23:25', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691437539330]', 206, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173317172531202, 0, b'0', '2021-03-20 15:23:26', '2021-03-20 15:23:26', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691437539330]', 206, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173339268124673, 0, b'0', '2021-03-20 15:23:31', '2021-03-20 15:23:31', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691454316548]', 203, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173342892003330, 0, b'0', '2021-03-20 15:23:32', '2021-03-20 15:23:32', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691454316548]', 205, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173391566901250, 0, b'0', '2021-03-20 15:23:44', '2021-03-20 15:23:44', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691492065281]', 208, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173396818169857, 0, b'0', '2021-03-20 15:23:45', '2021-03-20 15:23:45', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691492065281]', 194, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173422009159682, 0, b'0', '2021-03-20 15:23:51', '2021-03-20 15:23:51', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691332681730]', 203, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173427033935874, 0, b'0', '2021-03-20 15:23:52', '2021-03-20 15:23:52', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691332681730]', 205, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173448106119169, 0, b'0', '2021-03-20 15:23:57', '2021-03-20 15:23:57', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691320098817]', 208, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173452451418113, 0, b'0', '2021-03-20 15:23:58', '2021-03-20 15:23:58', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691320098817]', 199, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173473322274817, 0, b'0', '2021-03-20 15:24:03', '2021-03-20 15:24:03', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691362041859]', 204, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173478435131393, 0, b'0', '2021-03-20 15:24:04', '2021-03-20 15:24:04', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691362041859]', 206, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173498945277954, 0, b'0', '2021-03-20 15:24:09', '2021-03-20 15:24:09', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691475288067]', 205, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173503269605378, 0, b'0', '2021-03-20 15:24:10', '2021-03-20 15:24:10', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691475288067]', 205, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173523620368385, 0, b'0', '2021-03-20 15:24:15', '2021-03-20 15:24:15', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691466899457]', 205, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173528510926849, 0, b'0', '2021-03-20 15:24:16', '2021-03-20 15:24:16', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691466899457]', 204, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173549109153793, 0, b'0', '2021-03-20 15:24:21', '2021-03-20 15:24:21', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691429150722]', 205, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173554192650242, 0, b'0', '2021-03-20 15:24:22', '2021-03-20 15:24:22', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691429150722]', 206, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173574002348033, 0, b'0', '2021-03-20 15:24:27', '2021-03-20 15:24:27', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691466899458]', 204, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173579429777409, 0, b'0', '2021-03-20 15:24:28', '2021-03-20 15:24:28', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691466899458]', 199, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173601395347458, 0, b'0', '2021-03-20 15:24:34', '2021-03-20 15:24:34', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691466899459]', 205, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173606143299585, 0, b'0', '2021-03-20 15:24:35', '2021-03-20 15:24:35', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691466899459]', 194, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173626389204994, 0, b'0', '2021-03-20 15:24:40', '2021-03-20 15:24:40', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691403984898]', 196, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173631233626114, 0, b'0', '2021-03-20 15:24:41', '2021-03-20 15:24:41', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691403984898]', 195, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173652863651841, 0, b'0', '2021-03-20 15:24:46', '2021-03-20 15:24:46', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691341070338]', 205, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173656995041282, 0, b'0', '2021-03-20 15:24:47', '2021-03-20 15:24:47', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691341070338]', 206, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173677152866306, 0, b'0', '2021-03-20 15:24:52', '2021-03-20 15:24:52', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691500453890]', 206, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173681498165249, 0, b'0', '2021-03-20 15:24:53', '2021-03-20 15:24:53', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691500453890]', 206, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173702440325122, 0, b'0', '2021-03-20 15:24:58', '2021-03-20 15:24:58', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691374624771]', 196, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_log` VALUES (1373173706596880386, 0, b'0', '2021-03-20 15:24:59', '2021-03-20 15:24:59', 1207590691320098817, 1207590691320098817, 'admin', 'Dict:查看详情', 'com.simba.scaffold.security.business.controller.DictController.get()', '[1207590691374624771]', 203, '0:0:0:0:0:0:0:1');

-- ----------------------------
-- Table structure for sys_org
-- ----------------------------
DROP TABLE IF EXISTS `sys_org`;
CREATE TABLE `sys_org`  (
  `id` bigint(19) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '更新人',
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编码',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `parent_id` bigint(19) NULL DEFAULT NULL COMMENT '上级部门ID',
  `level` int(11) NULL DEFAULT NULL COMMENT '级别',
  `status` bit(1) NULL DEFAULT b'1' COMMENT '状态',
  `sequence` int(11) NULL DEFAULT NULL COMMENT '排序',
  `is_leaf` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否是末级节点',
  `code_linked` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '编码链',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '组织机构' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_org
-- ----------------------------
INSERT INTO `sys_org` VALUES (1351464551133786113, 6, b'0', '2021-03-15 17:16:28', '2021-03-16 14:51:11', 1207590691320098817, 1207590691320098817, 'investment_service_center', '投资服务中心', 1371271226493587458, 2, b'1', 18, b'1', ',u01u,investment_service_center,');
INSERT INTO `sys_org` VALUES (1351464812887715842, 7, b'0', '2021-03-15 09:54:35', '2021-03-16 14:52:42', 1207590691320098817, 1207590691320098817, 'bidding_office', '招标办', 1371271226493587458, 2, b'1', 9, b'1', ',u01u,bidding_office,');
INSERT INTO `sys_org` VALUES (1351495830084624386, 6, b'0', '2021-03-15 17:11:38', '2021-03-16 14:52:21', 1207590691320098817, 1207590691320098817, 'comprehensive_law_enforcement_bureau', '综合执法局', 1371271226493587458, 2, b'1', 5, b'1', ',u01u,comprehensive_law_enforcement_bureau,');
INSERT INTO `sys_org` VALUES (1351503210616823809, 7, b'0', '2021-03-15 17:15:27', '2021-03-18 13:19:37', 1207590691320098817, 1207590691320098817, 'economic_development_bureau', '经发局', 1371271226493587458, 2, b'1', 14, b'1', ',u01u,economic_development_bureau,');
INSERT INTO `sys_org` VALUES (1351503312634880002, 7, b'0', '2021-03-15 17:16:45', '2021-03-18 13:23:09', 1207590691320098817, 1207590691320098817, 'planning_bureau', '规划信息编研中心', 1371271226493587458, 2, b'1', 19, b'1', ',u01u,planning_bureau,');
INSERT INTO `sys_org` VALUES (1351503386588848130, 6, b'0', '2021-03-15 17:14:41', '2021-03-16 14:51:56', 1207590691320098817, 1207590691320098817, 'office', '办公室', 1371271226493587458, 2, b'1', 15, b'1', ',u01u,office,');
INSERT INTO `sys_org` VALUES (1351503484429377537, 7, b'0', '2021-03-15 17:10:49', '2021-03-16 14:52:29', 1207590691320098817, 1207590691320098817, 'bureau_of_land_and_resources', '国土局', 1371271226493587458, 2, b'1', 4, b'1', ',u01u,bureau_of_land_and_resources,');
INSERT INTO `sys_org` VALUES (1351503722670039041, 6, b'0', '2021-03-15 17:11:52', '2021-03-16 14:52:17', 1207590691320098817, 1207590691320098817, 'port_industrial_park', '港口产业园', 1371271226493587458, 2, b'1', 6, b'1', ',u01u,port_industrial_park,');
INSERT INTO `sys_org` VALUES (1351503827032711170, 9, b'0', '2021-03-15 09:54:35', '2021-03-18 13:21:30', 1207590691320098817, 1207590691320098817, 'social_services_bureau', '社事局', 1371271226493587458, 2, b'1', 12, b'1', ',u01u,social_services_bureau,');
INSERT INTO `sys_org` VALUES (1351503962542284802, 8, b'0', '2021-03-15 09:54:35', '2021-03-16 14:52:34', 1207590691320098817, 1207590691320098817, 'construction_bureau', '建设局', 1371271226493587458, 2, b'1', 10, b'1', ',u01u,construction_bureau,');
INSERT INTO `sys_org` VALUES (1351504144956760066, 8, b'0', '2021-03-15 09:54:35', '2021-03-16 14:52:50', 1207590691320098817, 1207590691320098817, 'party_work_depart', '党群工作部', 1371271226493587458, 2, b'1', 1, b'1', ',u01u,party_work_depart,');
INSERT INTO `sys_org` VALUES (1351504228993835010, 6, b'0', '2021-03-15 17:15:49', '2021-03-16 14:51:47', 1207590691320098817, 1207590691320098817, 'tax_collection_and_management_office', '征管办', 1371271226493587458, 2, b'1', 16, b'1', ',u01u,tax_collection_and_management_office,');
INSERT INTO `sys_org` VALUES (1351504453963718657, 6, b'0', '2021-03-15 17:12:13', '2021-03-16 14:52:13', 1207590691320098817, 1207590691320098817, 'legal_affairs_office', '法制办', 1371271226493587458, 2, b'1', 7, b'1', ',u01u,legal_affairs_office,');
INSERT INTO `sys_org` VALUES (1351504731681169410, 6, b'0', '2021-03-15 17:14:03', '2021-03-16 14:52:00', 1207590691320098817, 1207590691320098817, 'safety_supervision_bureau', '安监局', 1371271226493587458, 2, b'1', 13, b'1', ',u01u,safety_supervision_bureau,');
INSERT INTO `sys_org` VALUES (1351504807287693314, 7, b'0', '2021-03-15 17:12:39', '2021-03-18 13:20:26', 1207590691320098817, 1207590691320098817, 'sino_german_town_office', '中德制造小镇', 1371271226493587458, 2, b'1', 8, b'1', ',u01u,sino_german_town_office,');
INSERT INTO `sys_org` VALUES (1351504885595348993, 6, b'0', '2021-03-15 17:16:08', '2021-03-16 14:51:15', 1207590691320098817, 1207590691320098817, 'investment_promotion_center', '招商中心', 1371271226493587458, 2, b'1', 17, b'1', ',u01u,investment_promotion_center,');
INSERT INTO `sys_org` VALUES (1351504973868670978, 7, b'0', '2021-03-15 17:10:29', '2021-03-16 14:52:38', 1207590691320098817, 1207590691320098817, 'ministry_of_armed_forces', '武装部', 1371271226493587458, 2, b'1', 3, b'1', ',u01u,ministry_of_armed_forces,');
INSERT INTO `sys_org` VALUES (1351505061298937858, 7, b'0', '2021-03-15 09:54:35', '2021-03-16 14:52:46', 1207590691320098817, 1207590691320098817, 'labour_union', '工会', 1371271226493587458, 2, b'1', 2, b'1', ',u01u,labour_union,');
INSERT INTO `sys_org` VALUES (1351505148318162946, 7, b'0', '2021-03-15 17:13:47', '2021-03-18 16:07:41', 1207590691320098817, 1207590691320098817, 'discipline_office', '纪监监察室', 1371271226493587458, 2, b'1', 11, b'1', ',u01u,discipline_office,');
INSERT INTO `sys_org` VALUES (1351505230467801090, 7, b'0', '2021-03-15 17:17:32', '2021-03-18 13:23:38', 1207590691320098817, 1207590691320098817, 'finance_bureau', '财政局', 1371271226493587458, 2, b'1', 21, b'1', ',u01u,finance_bureau,');
INSERT INTO `sys_org` VALUES (1371271226493587458, 5, b'0', '2021-03-15 09:25:12', '2021-03-16 14:36:17', 1207590691320098817, 1207590691320098817, 'u01u', '开发区管委会', NULL, 1, b'1', 0, b'0', ',u01u,');
INSERT INTO `sys_org` VALUES (1372418152360607746, 8, b'0', '2021-03-18 13:22:40', '2021-03-18 13:22:40', 1207590691320098817, 1207590691320098817, 'natural_resources_bureau', '自然资源局', 1371271226493587458, 2, b'1', 20, b'1', ',u01u,natural_resources_bureau,');

-- ----------------------------
-- Table structure for sys_oss
-- ----------------------------
DROP TABLE IF EXISTS `sys_oss`;
CREATE TABLE `sys_oss`  (
  `id` bigint(19) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '更新人',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '自定义文件名',
  `file_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件名',
  `file_ext` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件扩展名',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件访问路径',
  `file_size` bigint(20) NULL DEFAULT NULL COMMENT '文件大小',
  `type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件类型',
  `server_code` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '存储服务器编码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'oss上传记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oss
-- ----------------------------

-- ----------------------------
-- Table structure for sys_oss_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_oss_config`;
CREATE TABLE `sys_oss_config`  (
  `id` bigint(19) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NOT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '更新人',
  `url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '本地存储访问url',
  `storage_location` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '本地存储路径',
  `qn_domain` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '七牛云域名',
  `qn_access_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '七牛云访问key',
  `qn_secret_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '七牛云key',
  `qn_bucket_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '七牛云空间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'oss配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oss_config
-- ----------------------------
INSERT INTO `sys_oss_config` VALUES (1207590691605311491, 7, b'0', '2019-10-21 14:03:06', '2019-10-22 16:22:10', 1207590691320098817, 1207590691320098817, '/vpath/data', '/file', 'http://qp2ujbmk4.hd-bkt.clouddn.com', '8X6lDqoNf0DbxNvxO66LW3Mq96NMVbMfmCsE0xli', 'b562QXKx9VLSsUNX2E6p-XaocIhYhtvXbudLK7DW', 'szpcc-songdu');

-- ----------------------------
-- Table structure for sys_resource
-- ----------------------------
DROP TABLE IF EXISTS `sys_resource`;
CREATE TABLE `sys_resource`  (
  `id` bigint(19) NOT NULL,
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(36) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(36) NULL DEFAULT NULL COMMENT '更新人',
  `code` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '授权代码',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '资源名称',
  `sequence` int(10) NULL DEFAULT NULL COMMENT '排序',
  `parent_id` bigint(19) NULL DEFAULT NULL,
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '前端路由',
  `type` int(1) NULL DEFAULT NULL COMMENT '类型 0：目录 1：菜单 2：按钮',
  `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '后台访问接口路径',
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '资源' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_resource
-- ----------------------------
INSERT INTO `sys_resource` VALUES (1207590691605311492, 0, b'0', '2019-03-01 10:20:14', '2019-03-01 10:20:17', 1207590691320098817, 1207590691320098817, 'sys:user:save', '新增', 103002, 1207590691924078594, NULL, 2, '/admin/user/save', NULL);
INSERT INTO `sys_resource` VALUES (1207590691613700099, 0, b'0', '2019-03-01 10:21:58', '2019-03-01 10:22:01', 1207590691320098817, 1207590691320098817, 'sys:user:update', '修改', 103003, 1207590691924078594, NULL, 2, '/admin/user/update', NULL);
INSERT INTO `sys_resource` VALUES (1207590691622088705, 0, b'0', '2019-03-01 10:23:03', '2019-03-01 10:23:06', 1207590691320098817, 1207590691320098817, 'sys:user:delete', '删除', 103004, 1207590691924078594, NULL, 2, '/admin/user/delete', NULL);
INSERT INTO `sys_resource` VALUES (1207590691655643138, 2, b'0', '2019-10-26 17:02:12', '2019-10-26 17:09:21', 1207590691320098817, 1207590691320098817, 'oss-upload', '上传文件', 109003, 1207590691877941251, NULL, 2, '/admin/oss/upload', NULL);
INSERT INTO `sys_resource` VALUES (1207590691664031745, 3, b'0', '2019-10-26 17:00:53', NULL, 1207590691320098817, NULL, 'sys:dict:delete', '删除', 104003, 1207590691668226050, NULL, 2, '/admin/dict/removeByIds', NULL);
INSERT INTO `sys_resource` VALUES (1207590691668226050, 2, b'0', '2019-02-27 15:30:18', '2019-10-18 13:54:54', 1207590691320098817, 1207590691320098817, 'sys:dict:list', '数据字典', 1040, 1207590691705974787, '/dict/dict', 1, '/admin/dict/all', 'dict1');
INSERT INTO `sys_resource` VALUES (1207590691668226051, 4, b'0', '2019-10-16 16:33:58', '2019-10-18 13:56:24', 1207590691320098817, 1207590691320098817, 'role:delete', '删除', 101003, 1207590691731140611, NULL, 2, '/admin/role/delete', NULL);
INSERT INTO `sys_resource` VALUES (1207590691705974787, 2, b'0', '2019-02-27 15:25:45', '2019-10-25 09:40:02', 1207590691320098817, 1207590691320098817, NULL, '系统管理', 10, NULL, NULL, 1, '#', 'system');
INSERT INTO `sys_resource` VALUES (1207590691722752002, 2, b'0', '2019-10-26 17:09:03', '2019-10-26 17:10:51', 1207590691320098817, 1207590691320098817, 'oss-delete', '删除', 109007, 1207590691877941251, NULL, 2, '/admin/oss/removeByIds', NULL);
INSERT INTO `sys_resource` VALUES (1207590691722752003, 2, b'0', '2019-10-31 14:37:51', '2019-10-31 14:39:11', 1207590691320098817, 1207590691320098817, 'wechat:menu', '微信管理', 40, NULL, '/wechat', 1, '/wechat', 'tixing');
INSERT INTO `sys_resource` VALUES (1207590691731140610, 2, b'0', '2019-10-26 17:03:21', '2019-10-26 17:09:40', 1207590691320098817, 1207590691320098817, 'oss-batch-remove', '批量删除', 109004, 1207590691877941251, NULL, 2, '/admin/oss/removeByIds', NULL);
INSERT INTO `sys_resource` VALUES (1207590691731140611, 0, b'0', '2019-02-27 15:27:40', '2019-02-27 15:33:05', 1207590691320098817, 1207590691320098817, 'sys:role:list', '角色管理', 1010, 1207590691705974787, 'sys/role', 1, '/admin/role/list', 'role');
INSERT INTO `sys_resource` VALUES (1207590691768889345, 2, b'0', '2019-10-19 13:56:13', '2019-10-19 13:58:28', 1207590691320098817, 1207590691320098817, 'sys:role:batchDelete', '批量删除', 101004, 1207590691731140611, NULL, 2, '/admin/role/removeByIds', NULL);
INSERT INTO `sys_resource` VALUES (1207590691798249476, 1, b'0', '2019-10-22 10:08:31', '2019-10-26 16:46:18', 1207590691320098817, 1207590691320098817, 'menu-save', '新增', 1, 1207590691924078593, NULL, 2, '/admin/resource/save', NULL);
INSERT INTO `sys_resource` VALUES (1207590691815026690, 1, b'0', '2019-10-22 09:02:18', '2019-10-22 09:10:55', 1207590691320098817, 1207590691320098817, 'sys:oss:config', 'OSS配置', 109001, 1207590691877941251, NULL, 2, '/admin/ossConfig/getConfig', NULL);
INSERT INTO `sys_resource` VALUES (1207590691819220994, 1, b'0', '2019-10-19 15:38:23', '2019-10-19 15:38:44', 1207590691320098817, 1207590691320098817, 'sys:schedule:list', '定时任务', 1050, 1207590691705974787, 'job/schedule', 1, '/admin/schedule/pages', 'job');
INSERT INTO `sys_resource` VALUES (1207590691835998210, 1, b'0', '2019-10-22 10:28:22', '2019-10-26 16:46:37', 1207590691320098817, 1207590691320098817, 'menu-delete', '删除 ', 3, 1207590691924078593, NULL, 2, '/admin/resource/delete', NULL);
INSERT INTO `sys_resource` VALUES (1207590691848581123, 1, b'0', '2019-05-14 13:25:50', '2019-10-18 17:43:16', 1207590691320098817, 1207590691320098817, 'sys:role:update', '修改', 101002, 1207590691731140611, NULL, 2, '/admin/reserve', NULL);
INSERT INTO `sys_resource` VALUES (1207590691856969731, 1, b'0', '2019-10-26 16:48:33', '2019-10-26 16:49:40', 1207590691320098817, 1207590691320098817, 'sysLog-list', '查询', 0, 1207590691877941250, NULL, 2, '/admin/sysLog/pages', NULL);
INSERT INTO `sys_resource` VALUES (1207590691865358338, 2, b'0', '2019-10-22 09:39:48', '2019-10-22 09:40:01', 1207590691320098817, 1207590691320098817, 'sys:oss:save', '保存OSS配置', 109002, 1207590691877941251, NULL, 2, '/admin/ossConfig/save', NULL);
INSERT INTO `sys_resource` VALUES (1207590691865358339, 2, b'0', '2019-10-26 17:06:42', '2019-10-26 17:10:15', 1207590691320098817, 1207590691320098817, 'oss-view', '查看', 109005, 1207590691877941251, NULL, 2, '/admin/oss', NULL);
INSERT INTO `sys_resource` VALUES (1207590691877941250, 7, b'0', '2019-05-14 13:28:34', '2019-10-26 16:49:27', 1207590691320098817, 1207590691320098817, '', '系统日志', 1080, 1207590691705974787, 'sys/log', 1, '/admin/sysLog/pages', 'log');
INSERT INTO `sys_resource` VALUES (1207590691877941251, 3, b'0', '2019-10-21 10:19:27', '2019-10-26 16:50:52', 1207590691320098817, 1207590691320098817, '', '对象存储', 1090, 1207590691705974787, '/sys/oss', 1, '/admin/oss/pages', 'zonghe');
INSERT INTO `sys_resource` VALUES (1207590691882135554, 0, b'0', '2019-10-26 17:17:03', '2019-10-26 17:17:03', 1207590691320098817, 1207590691320098817, 'oss-list', '查询', 109008, 1207590691877941251, NULL, 2, '/admin/oss/pages', NULL);
INSERT INTO `sys_resource` VALUES (1207590691890524161, 1, b'0', '2019-10-22 10:09:39', '2019-10-26 16:47:14', 1207590691320098817, 1207590691320098817, 'menu-update', '编辑', 2, 1207590691924078593, NULL, 2, '/admin/resource/update', NULL);
INSERT INTO `sys_resource` VALUES (1207590691924078593, 2, b'0', '2019-02-27 15:29:02', '2019-10-26 16:46:55', 1207590691320098817, 1207590691320098817, '', '菜单管理', 1020, 1207590691705974787, 'sys/menu', 1, '/admin/resource/all', 'menu');
INSERT INTO `sys_resource` VALUES (1207590691924078594, 0, b'0', '2019-02-27 15:29:40', '2019-02-27 15:29:40', 1207590691320098817, 1207590691320098817, 'sys:user:list', '用户管理', 1030, 1207590691705974787, 'sys/user', 1, '/admin/user/pages', 'admin');
INSERT INTO `sys_resource` VALUES (1207590691953438721, 2, b'0', '2019-10-26 17:08:24', '2019-10-26 17:10:34', 1207590691320098817, 1207590691320098817, 'oss-download', '下载', 109006, 1207590691877941251, NULL, 2, '#', NULL);
INSERT INTO `sys_resource` VALUES (1207590691957633027, 1, b'0', '2019-10-21 16:53:55', '2019-10-26 17:01:56', 1207590691320098817, 1207590691320098817, 'sys:dict:save', '新增', 104001, 1207590691668226050, NULL, 2, '/admin/dict/save', NULL);
INSERT INTO `sys_resource` VALUES (1207590691961827330, 0, b'0', '2019-07-16 09:35:29', '2019-07-16 09:35:26', 1207590691320098817, 1207590691320098817, 'sys:user:list', '查看', 103001, 1207590691924078594, NULL, 2, '/admin/reserve/his', NULL);
INSERT INTO `sys_resource` VALUES (1207590691961827332, 7, b'0', '2019-10-31 14:41:18', '2019-10-31 16:27:29', 1207590691320098817, 1207590691320098817, 'wechat:account', '微信号管理', 41, 1207590691722752003, 'wechat/account', 1, '/wechat/account', 'zonghe');
INSERT INTO `sys_resource` VALUES (1207590691974410242, 1, b'0', '2019-05-14 13:23:32', '2019-10-18 13:51:27', 1207590691320098817, 1207590691320098817, 'sys:role:save', '新增', 101001, 1207590691731140611, NULL, 2, '/admin/approve', NULL);
INSERT INTO `sys_resource` VALUES (1207590691982798850, 1, b'0', '2019-10-21 16:55:57', '2019-10-26 17:02:10', 1207590691320098817, 1207590691320098817, 'sys:dict:update', '修改', 104002, 1207590691668226050, NULL, 2, '/admin/dict/update', NULL);
INSERT INTO `sys_resource` VALUES (1240899560634548226, 0, b'0', '2020-03-20 15:14:45', '2020-03-20 15:14:45', 1207590691320098817, 1207590691320098817, NULL, '代码生成', 0, 1207590691705974787, '/code/code-generation', 1, '/admin/generator/page', 'admin');
INSERT INTO `sys_resource` VALUES (1351463768615071746, 0, b'1', '2021-01-19 17:37:26', '2021-01-19 17:37:26', 1207590691320098817, 1207590691320098817, NULL, '企业管理', 20, NULL, NULL, 1, '#', 'venue');
INSERT INTO `sys_resource` VALUES (1351465098503041026, 0, b'1', '2021-01-19 17:42:43', '2021-01-19 17:42:43', 1207590691320098817, 1207590691320098817, NULL, '企业库', 1, 1351463768615071746, '/company/list', 1, '/admin/ep/company/page', 'zonghe');
INSERT INTO `sys_resource` VALUES (1362283194947637250, 1, b'1', '2021-02-18 14:09:58', '2021-02-18 14:10:10', 1207590691320098817, 1207590691320098817, NULL, '专题管理', 50, NULL, '#', 1, '#', 'bianji');
INSERT INTO `sys_resource` VALUES (1362291043849392129, 3, b'1', '2021-02-18 14:41:10', '2021-02-22 09:35:45', 1207590691320098817, 1207590691320098817, NULL, '优秀作品库', 2, 1362283194947637250, '#', 1, '#', 'daohang');
INSERT INTO `sys_resource` VALUES (1362597989433360386, 1, b'1', '2021-02-19 11:00:51', '2021-02-19 11:14:28', 1207590691320098817, 1207590691320098817, NULL, '活动专题管理', 60, NULL, '#', 1, '#', 'menu');
INSERT INTO `sys_resource` VALUES (1362967562280144897, 1, b'1', '2021-02-20 11:29:24', '2021-02-20 11:29:37', 1207590691320098817, 1207590691320098817, NULL, '赛区管理', 70, NULL, '#', 1, '#', 'dangdifill');
INSERT INTO `sys_resource` VALUES (1364096651035123713, 2, b'1', '2021-02-23 14:16:00', '2021-02-23 14:19:19', 1207590691320098817, 1207590691320098817, NULL, '作品管理', 20, NULL, '#', 1, '#', 'editor');
INSERT INTO `sys_resource` VALUES (1364098989611261954, 0, b'1', '2021-02-23 14:25:18', '2021-02-23 14:25:18', 1207590691320098817, 1207590691320098817, NULL, '作品管理', 0, NULL, '#', 1, '#', 'editor');
INSERT INTO `sys_resource` VALUES (1364819051074785281, 3, b'1', '2021-02-25 14:06:34', '2021-03-04 16:05:03', 1207590691320098817, 1207590691320098817, 'excellenceopuslibrary:list', '优秀作品库', 1, 1362283194947637250, 'excellenceopuslibrary/list', 1, '/excellenceopuslibrary/page', 'shoucangfill');
INSERT INTO `sys_resource` VALUES (1366292972064243714, 0, b'1', '2021-03-01 15:43:24', '2021-03-01 15:43:24', 1207590691320098817, 1207590691320098817, 'oss-upload', '上传', 1, 1362601372542189569, NULL, 2, '/admin/oss/upload', NULL);
INSERT INTO `sys_resource` VALUES (1370251296193196034, 0, b'1', '2021-03-12 13:52:22', '2021-03-12 13:52:22', 1207590691320098817, 1207590691320098817, NULL, '用户管理', 30, NULL, NULL, 1, '#', 'admin');
INSERT INTO `sys_resource` VALUES (1370253549096480769, 2, b'1', '2021-03-12 14:01:19', '2021-03-12 16:37:56', 1207590691320098817, 1207590691320098817, NULL, '组织用户', 1, 1370251296193196034, '/user/office-user', 1, '/admin/user/officePages', 'role');
INSERT INTO `sys_resource` VALUES (1370253646756655106, 3, b'1', '2021-03-12 14:01:42', '2021-03-12 16:43:28', 1207590691320098817, 1207590691320098817, NULL, '企业用户', 2, 1370251296193196034, '/user/company-user', 1, '/admin/user/officePages', 'adminSignIn');
INSERT INTO `sys_resource` VALUES (1370257283084005378, 3, b'0', '2021-03-12 14:16:09', '2021-03-20 14:46:13', 1207590691320098817, 1207590691320098817, NULL, '组织架构', 15, 1207590691705974787, '/org/list', 1, '/admin/org', 'tubiao');
INSERT INTO `sys_resource` VALUES (1370266460644782081, 4, b'1', '2021-03-12 14:52:37', '2021-03-16 14:45:55', 1207590691320098817, 1207590691320098817, NULL, '信息登记', 25, NULL, '/inforegister/list', 1, '/admin/company/info/job/getByCompanyId', 'signIn');
INSERT INTO `sys_resource` VALUES (1370266634544820226, 3, b'1', '2021-03-12 14:53:19', '2021-03-15 13:59:17', 1207590691320098817, 1207590691320098817, NULL, '信息登记管理', 26, NULL, '/recruit/companyjoblist', 1, '#', 'zhedie');
INSERT INTO `sys_resource` VALUES (1370307010655744001, 0, b'0', '2021-03-12 17:33:45', '2021-03-12 17:33:45', 1207590691320098817, 1207590691320098817, 'org-add', '新增', 1, 1370257283084005378, NULL, 2, '/admin/org/save', NULL);
INSERT INTO `sys_resource` VALUES (1370307109247053825, 0, b'0', '2021-03-12 17:34:09', '2021-03-12 17:34:09', 1207590691320098817, 1207590691320098817, 'org-update', '修改', 2, 1370257283084005378, NULL, 2, '/admin/org/update', NULL);
INSERT INTO `sys_resource` VALUES (1370307267414257666, 0, b'0', '2021-03-12 17:34:46', '2021-03-12 17:34:46', 1207590691320098817, 1207590691320098817, 'org-delete', '删除', 3, 1370257283084005378, NULL, 2, '/admin/org/deleteById', NULL);
INSERT INTO `sys_resource` VALUES (1371992893431762946, 1, b'1', '2021-03-17 09:12:51', '2021-03-17 09:13:01', 1207590691320098817, 1207590691320098817, NULL, '内容管理', 27, NULL, '/content/list', 1, '/admin/cms/contentpages', 'log');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint(19) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(19) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(19) NULL DEFAULT NULL COMMENT '更新人',
  `code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色代码',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `sequence` int(10) NULL DEFAULT NULL COMMENT '排序',
  `status` int(1) NULL DEFAULT NULL COMMENT '激活状态（启用，未启用）',
  `role_linked` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建角色编码链表',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1207590691953438721, 0, b'0', '2019-02-26 11:11:50', '2021-03-20 14:51:25', 1207590691320098817, 1207590691320098817, 'SYS_ADMIN', '系统管理员', 1, 1, 'SYS_ADMIN');

-- ----------------------------
-- Table structure for sys_role_resource
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_resource`;
CREATE TABLE `sys_role_resource`  (
  `id` bigint(20) NOT NULL,
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `updater` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `role_id` bigint(19) NULL DEFAULT NULL COMMENT '角色主键id',
  `resource_id` bigint(20) NULL DEFAULT NULL COMMENT '菜单主键id',
  `role_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色名称（冗余字段）',
  `resource_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单名称（冗余字段）',
  `resource_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单请求（冗余字段）',
  `is_half_checked` bit(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色资源' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_resource
-- ----------------------------
INSERT INTO `sys_role_resource` VALUES (1373165261030227969, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691705974787, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261030227970, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1240899560634548226, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261030227971, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1370257283084005378, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261030227972, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1370307010655744001, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261030227973, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1370307109247053825, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261030227974, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1370307267414257666, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261097336834, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691731140611, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261097336835, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691974410242, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261097336836, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691848581123, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261097336837, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691668226051, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261097336838, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691768889345, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261097336839, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691924078593, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261097336840, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691798249476, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261097336841, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691890524161, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261097336842, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691835998210, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261097336843, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691924078594, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261097336844, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691961827330, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261097336845, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691605311492, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261164445698, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691613700099, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261164445699, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691622088705, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261164445700, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691668226050, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261164445701, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691957633027, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261164445702, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691982798850, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261164445703, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691664031745, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261164445704, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691819220994, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261164445705, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691877941250, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261164445706, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691856969731, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261164445707, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691877941251, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261227360258, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691815026690, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261227360259, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691865358338, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261227360260, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691655643138, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261227360261, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691731140610, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261227360262, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691865358339, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261227360263, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691953438721, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261227360264, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691722752002, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261227360265, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691882135554, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261227360266, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691722752003, '系统管理员', NULL, NULL, b'0');
INSERT INTO `sys_role_resource` VALUES (1373165261227360267, 0, b'0', NULL, '2021-03-20 14:51:25', '1207590691320098817', '1207590691320098817', 1207590691953438721, 1207590691961827332, '系统管理员', NULL, NULL, b'0');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint(19) NOT NULL,
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `updater` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `username` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登陆账号',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `tel` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '座机',
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色',
  `status` bit(1) NULL DEFAULT b'1' COMMENT '状态',
  `mobile` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '显示名称（冗余，保存时取员工姓名）',
  `email` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `union_id` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'union_id',
  `role_linked` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色编码链表',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1207590691320098817, 6, b'0', '2019-06-03 11:31:33', '2021-03-17 11:45:39', '1207590691320098817', '1207590691320098817', 'admin', '{bcrypt}$2a$10$F7bX8MzzLcLUEeedS.APzeqwv4tf84n92r8wXDpi4Oy9Xf4oQNrE6', '座机号', 1207590691953438721, b'1', '15851620817', '管理员', '邮箱号', NULL, NULL);

-- ----------------------------
-- Table structure for wechat_account
-- ----------------------------
DROP TABLE IF EXISTS `wechat_account`;
CREATE TABLE `wechat_account`  (
  `id` bigint(19) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `updater` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `type` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类型(小程序、公众号)',
  `code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '服务号代码',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '服务号名称',
  `app_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '一个公众号的appid',
  `secret` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公众号的appsecret',
  `token` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '接口配置里的Token值',
  `aes_key` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '接口配置里的EncodingAESKey值',
  `msg_data_format` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '消息格式，XML或者JSON',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '微信账号' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wechat_account
-- ----------------------------
INSERT INTO `wechat_account` VALUES (1, 0, b'0', NULL, NULL, NULL, NULL, 'wechat_mp', 'wechat_mp', '公众号', 'test', 'test', 'test', 'test', NULL);
INSERT INTO `wechat_account` VALUES (2, 0, b'0', NULL, NULL, NULL, NULL, 'wechat_ma', 'wechat_ma', '小程序', 'test', 'test', 'test', 'test', NULL);
INSERT INTO `wechat_account` VALUES (3, 0, b'0', NULL, NULL, NULL, NULL, 'wechat_open', 'wechat_open', '开放平台', 'test', 'test', 'test', 'test', NULL);

-- ----------------------------
-- Table structure for wechat_mp_menu
-- ----------------------------
DROP TABLE IF EXISTS `wechat_mp_menu`;
CREATE TABLE `wechat_mp_menu`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `updater` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `mp_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '服务号code',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类型',
  `url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'url',
  `wx_key` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'key',
  `app_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'appId',
  `page_path` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '小程序页面路径',
  `media_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '素材id',
  `parent_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '父菜单id',
  `sequence` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '微信菜单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wechat_mp_menu
-- ----------------------------

-- ----------------------------
-- Table structure for wechat_user
-- ----------------------------
DROP TABLE IF EXISTS `wechat_user`;
CREATE TABLE `wechat_user`  (
  `id` bigint(19) NOT NULL COMMENT '自定义主键',
  `version` int(10) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `creator` bigint(36) NULL DEFAULT NULL COMMENT '创建人',
  `updater` bigint(36) NULL DEFAULT NULL COMMENT '更新人',
  `openid` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '小程序openid',
  `nickname` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `head_img_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像地址',
  `gender` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '性别',
  `country` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '国家',
  `province` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '省份',
  `city` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '市',
  `mobile` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `union_id` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'unionid',
  `mp_openid` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '公众号openid',
  `web_openid` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '开放平台openid',
  `subscribe_status` bit(1) NULL DEFAULT b'1' COMMENT '公众号关注状态',
  `subscribe_time` datetime(0) NULL DEFAULT NULL COMMENT '公众号关注时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '微信用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wechat_user
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
